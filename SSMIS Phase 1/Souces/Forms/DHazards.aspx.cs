﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DIMS.Models;
using Microsoft.Reporting.WebForms;
using System.IO;

namespace DIMS.Forms
{
    public partial class DHazards : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DMISDBEntities db = new DMISDBEntities();


                ReportParameter p1 = new ReportParameter("division");
                ReportParameter p2 = new ReportParameter("district");
                ReportParameter p3 = new ReportParameter("upazila");
                ReportParameter p4 = new ReportParameter("union");
                ReportParameter p5 = new ReportParameter("areaType");


                string division = Request.QueryString["division"];
                string district = Request.QueryString["district"];
                string upazila = Request.QueryString["upazila"];
                string union = Request.QueryString["union"];
                string areaType = Request.QueryString["areaType"];

                if (division != null)
                {
                    DropDownListDiv.SelectedValue = division.ToString();
                }

                if (district != null)
                {
                    DropDownListDis.SelectedValue = district.ToString();
                }


                if (upazila != null)
                {
                    DropDownListUpa.SelectedValue = upazila.ToString();
                }


                if (union != null)
                {
                    DropDownListUnion.SelectedValue = union.ToString();
                }






                ReportViewer1.DocumentMapCollapsed = true;


                GraphModels grpModel = new GraphModels();

                ReportDataSource rptdataSource1 = new ReportDataSource();
                string path = Path.Combine(Server.MapPath("~/Rpts"), "Report1.rdlc");
                ReportViewer1.LocalReport.DataSources.Add(rptdataSource1);
                ReportViewer1.LocalReport.ReportPath = path;
                rptdataSource1.Name = "DataSet1";
              //  rptdataSource1.Value = grpModel.GetGeneralInformationsByStrcType(0, 0, 0, 0, "All");


                // this.ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
                this.ReportViewer1.LocalReport.Refresh();



                LoadControls();

            }
        }

        private void LoadControls()
        {
            DMISDBEntities db = new DMISDBEntities();
            var query = from s in db.Divisions select s;
            DropDownListDiv.DataSource = query.OrderBy(Items => Items.DivisionName).ToList();
            DropDownListDiv.DataTextField = "DivisionName";
            DropDownListDiv.DataValueField = "DivisionId";
            DropDownListDiv.DataBind();
            DropDownListDiv.Items.Insert(0, new ListItem("Division", "0"));




            var queryDis = from s in db.Districts select s;
            DropDownListDis.DataSource = queryDis.OrderBy(Items => Items.DistrictName).ToList();
            DropDownListDis.DataTextField = "DistrictName";
            DropDownListDis.DataValueField = "DistrictId";
            DropDownListDis.DataBind();
            DropDownListDis.Items.Insert(0, new ListItem("District", "0"));


            var queryUpa = from s in db.Upazillas select s;
            DropDownListUpa.DataSource = queryUpa.OrderBy(Items => Items.UpazillaName).ToList();
            DropDownListUpa.DataTextField = "UpazillaName";
            DropDownListUpa.DataValueField = "UpazillaId";
            DropDownListUpa.DataBind();
            DropDownListUpa.Items.Insert(0, new ListItem("Upazila", "0"));


            var queryUn = from s in db.Unions select s;
            DropDownListUnion.DataSource = queryUn.OrderBy(Items => Items.UnionName).ToList();
            DropDownListUnion.DataTextField = "UnionName";
            DropDownListUnion.DataValueField = "UnionId";
            DropDownListUnion.DataBind();
            DropDownListUnion.Items.Insert(0, new ListItem("Union", "0"));
        }


        public void Loaddata()
        {
            ReportViewer1.DocumentMapCollapsed = true;


            GraphModels grpModel = new GraphModels();

            ReportDataSource rptdataSource1 = new ReportDataSource();
            ReportViewer1.LocalReport.DataSources.Add(rptdataSource1);
            rptdataSource1.Name = "DataSet1";
           // rptdataSource1.Value = grpModel.GetGeneralInformationsByStrcType(0, 0, 0, 0, "All");


            // this.ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
            this.ReportViewer1.LocalReport.Refresh();
            if (DropDownListDiv.SelectedValue!= null)
            {

                ReportViewer1.Visible = true;

                ReportParameter p1 = new ReportParameter("division");
                ReportParameter p2 = new ReportParameter("district");
                ReportParameter p3 = new ReportParameter("upazila");
                ReportParameter p4 = new ReportParameter("union");
                ReportParameter p5 = new ReportParameter("areaType");


              
            }
           
        }

    }
}