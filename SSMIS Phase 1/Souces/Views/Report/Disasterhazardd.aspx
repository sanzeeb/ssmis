﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Disasterhazardd.aspx.cs" Inherits="DIMS.Views.Report.Disasterhazard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

       <link href="~/Content/Form/css/school.css" rel="stylesheet" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="~/Scripts/Form/js/jquery.selectbox-0.5.js"></script>
   

<script type="text/javascript">
    $(document).ready(function () {
        $('#Items').selectbox();
        $('#Items2').selectbox();
        $('#Items3').selectbox();
        $('#Items4').selectbox();
    });
</script>
</head>
<body>

    <div class="wrapper">
<div class="head">School & Community  Risk Assessment by Disaster/Hazard:</div>

<div class="ser-box" style="height:50px;">
<span class="sr-title" style="width:100%;">Search by Area Type :</span>
<span class="d-part" style="width:149px;"><input name="" type="radio" value="" /> All</span>
<span class="d-part" style="width:149px;"><input name="" type="radio" value="" /> Rural</span>
<span class="d-part" style="border-right:none;width:149px;"><input name="" type="radio" value="" /> Urban</span>
</div>
<div class="ser-box">
<span class="btn-box"><a class="sear-btn" href="#">OK</a></span>
<span class="sr-title">Search by Jurisdiction (Name) :</span>
<span class="d-part">
<select name="Items" id="Items">
    <option value="option1">Division</option>
    <option value="option2">Division1</option>
    <option value="option3">Division2</option>
    <option value="option4">Division3</option>	
</select>
</span>
<span class="d-part">
<select name="Items" id="Items2">
    <option value="option1">District</option>
    <option value="option2">District1</option>
    <option value="option3">District2</option>
    <option value="option4">District3</option>	
</select>
</span>
<span class="d-part">
<select name="Items" id="Items3">
    <option value="option1">Upazila</option>
    <option value="option2">Upazila1</option>
    <option value="option3">Upazila2</option>
    <option value="option4">Upazila3</option>	
</select>
</span>
<span class="d-part" style="border-right:none;">
<select name="Items" id="Items4">
    <option value="option1">Union/Ward</option>
    <option value="option2">Union1</option>
    <option value="option3">Union2</option>
    <option value="option4">Union3</option>	
</select>
</span>

</div>


    <div class="ser-box" style="width:920px; height:292px">

    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
        </div>
        </div>
</body>
</html>
