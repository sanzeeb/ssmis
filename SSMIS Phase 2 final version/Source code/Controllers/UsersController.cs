﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DIMS.Models;
using PagedList;
using System.Web.Security;
using WebMatrix.WebData;
using DIMS.Filters;

namespace DIMS.Controllers
{
    public class UsersController : Controller
    {
        Repository _repository = new Repository();
       // private UsersContext db = new UsersContext();
        private DMISDBEntities db = new DMISDBEntities();

        //
        // GET: /Users/

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
                    var users = from s in db.UserProfiles
                           select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        users = users.Where(s => s.UserName.ToUpper().Contains(searchString.ToUpper())
                                               || s.UserName.ToUpper().Contains(searchString.ToUpper()));
                    }
            switch (sortOrder)
            {
                case "Name_desc":
                    users = users.OrderByDescending(s => s.UserName);
                    break;
                
                default:
                    users = users.OrderBy(s => s.UserName);
                    break;
            }

            int pageSize = 30;
            int pageNumber = (page ?? 1);
            
            return View(users.ToPagedList(pageNumber, pageSize));

          //  return View(users.ToList());

          //  return View(db.UserProfiles.ToList());
        }

        //
        // GET: /Users/Details/5

        public ActionResult Details(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        //
        // GET: /Users/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Users/Create

        [HttpPost]
        public ActionResult Create(UserProfile userprofile)
        {
            if (ModelState.IsValid)
            {
                db.UserProfiles.Add(userprofile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(userprofile);
        }

        //
        // GET: /Users/Edit/5

        public ActionResult Edit(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        public ActionResult Reset(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
          //  MembershipUser user;
            //user = Membership.GetUser(userprofile, false);
            //user.ChangePassword(user.ResetPassword(), "123456");
            return View("ResetPassword",userprofile);
        }

        


                [Authorize(Roles = "Administrator")]
        public ActionResult Assign(int id = 0)
        {

            ViewModels vm = new ViewModels();
            vm.allRole = _repository.GetRole();
            vm.userInfo = _repository.GetInfo(id);
            return View(vm);

            //UserProfile userprofile = db.UserProfiles.Find(id);
            //if (userprofile == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(userprofile);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult Assign()
        {
            //WebSecurity.InitializeDatabaseConnection(
            //   "DefaultConnection",
            //   "UserProfile",
            //   "UserId",
            //   "UserName", autoCreateTables: true);

           string userId = Request["UserName"];
            string roleId = Request["RoleName"];
            if (!Roles.RoleExists(roleId))
                Roles.CreateRole(roleId);
            if (Request["RoleName"] != "dumy")
            {
                var roles = Roles.GetAllRoles();
                // var roles = (DmisRoleProvider)Roles.Provider;
                //if (!Roles.GetRolesForUser(Request["UserId"]).Contains(Request["RoleId"]))
                //    Roles.AddUsersToRoles(new[] { Request["UserId"] }, new[] { Request["RoleName"] });

                //var roles = Roles.GetAllRoles();
                ////Code to add a user to a role
                //var userToAddTo = Request["userToAddTo"];
                //var roleToAdd = Request["roleToAdd"];
                if (userId != null && roleId != null)
                {
                  var rol=  Roles.GetRolesForUser(userId);
                   // Roles.RemoveUserFromRole(userId, );

                //  Roles.RemoveUserFromRole(userId,"");

                  if (rol.Count()> 0)
                  {
                      Roles.RemoveUserFromRoles(userId, rol);
                  }

                System.Web.Security.Roles.AddUserToRole(userId, roleId);


                    return RedirectToAction("index", "users");
                    //TempData["AssignErrorMessage"] = "Role added successfully ";

                }
            }
            else
            {
                TempData["AssignErrorMessage"] = "Please select role ";
            }

            return RedirectToAction("Assign", "users", Request["UserId"]);
            //return View();
        }



        //
        // POST: /Users/Edit/5

        [HttpPost]
        public ActionResult Edit(UserProfile userprofile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userprofile).State = EntityState.Modified;
                db.SaveChanges();
                if (User.IsInRole("Administrator"))
                {
                    return RedirectToAction("index", "users");
                }
                else
                {
                    return RedirectToAction("index", "domain");
                }
            }
            return View(userprofile);
        }
        [Authorize(Roles = "Administrator")]
        public ActionResult Disable(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        [HttpPost, ActionName("Disable")]
        [Authorize(Roles = "Administrator")]
        public ActionResult DisableConfirm(int id)
        {
            UsersModel umo = new UsersModel();
            UserProfile dto = new UserProfile();
            dto.UserId = id;
            dto.Status = "0";
            umo.EditStatus(dto);
            return RedirectToAction("index", "users");
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Enable(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }
        [HttpPost, ActionName("Enable")]
        [Authorize(Roles = "Administrator")]
        public ActionResult EnableConfirm(int id)
        {
            UsersModel umo = new UsersModel();
            UserProfile dto = new UserProfile();
            dto.UserId = id;
            dto.Status = "1";
            umo.EditStatus(dto);
            return RedirectToAction("index", "users");
        }
        //
        // GET: /Users/Delete/5
         [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        //
        // POST: /Users/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteConfirmed(int id)
        {
          

            UserProfile userprofile = db.UserProfiles.Find(id);

            var roles = userprofile.webpages_Roles;

         //string[] roleName=Roles.GetRolesForUser(userprofile.webpages_Roles.First().RoleName);
         foreach (var role in roles)
         {
             Response.Write(role.RoleName);
         }

         //Response.Write(roleName);


     //       string roleName = userprofile.webpages_Roles.First().RoleName;
       //     if(roleName!=null)
         //  Roles.RemoveUserFromRoles(userprofile.UserName, roleName);
            db.UserProfiles.Remove(userprofile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}