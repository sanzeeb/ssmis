﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DIMS.Models;
using PagedList;
using Microsoft.Reporting.WebForms;
using System.IO;


namespace DIMS.Controllers
{
    public class PostDisasterController : Controller
    {
        private DMISDBEntities db = new DMISDBEntities();
        PostdisasterModels postMo = new PostdisasterModels();
        UsersModel userMo = new UsersModel();
        Repository _repository = new Repository();
        ViewModels vm = new ViewModels();
        //
        // GET: /PostDisaster/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit(int postId = 0)
        {

            if (User.Identity.Name == "")
            {
                return RedirectToAction("index", "Home");
            }
            else
            {
                ViewModels vm = new ViewModels();

                vm.PostDisasterInfo = _repository.GetDomainPostById(postId);

                if (vm.PostDisasterInfo.Count > 0)
                {
                    UsersModel userMo = new UsersModel();

                    var uinfo = userMo.GetInfoByName(User.Identity.Name);
                    int? userId = uinfo.First().UserId;
                    int? divisionid= vm.PostDisasterInfo.First().DivisionId;
                    int? districtid = vm.PostDisasterInfo.First().DistrictId;
                    int? unionid = vm.PostDisasterInfo.First().UnionId;
                    int? upazillaid = vm.PostDisasterInfo.First().UpazillaId;
                    

                    var divisions= _repository.DivisioninfoById(divisionid);
                    string divisionname = divisions.First().DivisionName;

                    var unions = _repository.UnioninfoById(unionid);
                    string unionname = unions.First().UnionName;

                    var districts = _repository.DistrictinfoById(districtid);
                    string districtname = districts.First().DistrictName;

                    var upazillas = _repository.UpazillainfoById(upazillaid);
                    string upazillaname = upazillas.First().UpazillaName;


                    vm.DivisionInfo = _repository.Divisioninfo();
                    vm.DistrictInfo = _repository.Districtinfo();
                    vm.UpazillaInfo = _repository.Upazillainfo();
                    vm.UnionInfo = _repository.Unioninfo();

                    ViewBag.division = divisionname;
                    ViewBag.district = districtname;
                    ViewBag.union = unionname;
                    ViewBag.upazilla = upazillaname;

                    ViewBag.Title = "Operational Panel : Post-Disaster Information Management : Form Edit";
                    return View("Postedit", vm);
                }
                else
                {

                    return RedirectToAction("PostDatacollection");

                }

            }
        }


       

        [HttpPost]
        public ActionResult Postdisaster()
        {
            UsersModel userMo = new UsersModel();
            PostdisasterModels postMo = new PostdisasterModels();
            string eduClsId = Request["Division1"] + Request["Division2"] + "-" + Request["District1"] + Request["District2"] 
                + "-" + Request["Upazila1"] + Request["Upazila2"] + "-" +
                Request["Union1"] + Request["Union2"] + "-" + Request["educlsl1"] + Request["educlsl2"] + Request["educlsl3"] + Request["educlsl4"] + Request["educlsl5"] + Request["educlsl6"];


            string GobEiin = Request["GobEIIN1"] + Request["GobEIIN2"] + Request["GobEIIN3"] + Request["GobEIIN4"] + Request["GobEIIN5"] + Request["GobEIIN6"]
                + Request["GobEIIN7"] + Request["GobEIIN8"] + Request["GobEIIN9"] + Request["GobEIIN10"] + Request["GobEIIN11"];
            var uinfo = userMo.GetInfoByName(User.Identity.Name);
            PostDisasterInfo dginfo = new PostDisasterInfo();

            dginfo.Altitude = Request["Altitude"];
            dginfo.GpsLatitude = Request["Latitude"]; ;
            dginfo.GpsLongtitude = Request["Longitude"];
            dginfo.GraphicalLocation = Request["GraphicalLocation"];
            
            dginfo.AuthorityType = Request["authorityType"];
            dginfo.SerialNo = Request["SerialNo"];


            dginfo.InstituteId = eduClsId;

            dginfo.GobEIIN = GobEiin;
            dginfo.HazardName = Request["DisasterName"];
            dginfo.NameOfAgency = Request["Agencies"];
            dginfo.HazardType = Request["DisasterName1"];
            dginfo.DamageType = Request["DamageType"];
            dginfo.DamageAmount =int.Parse(Request["DamageAmount"]);
            dginfo.EstabilishmentDate = Request["EstabilishmentDates"];
            dginfo.DamageDescription = Request["DamageDescription"];
            dginfo.MinBdtReq = int.Parse(Request["MinBdtReq"]);
            

            dginfo.LocalSupport =int.Parse( Request["LocalSupport"]);

            dginfo.DataCollectionMethod = Request["dataMethod"];

            dginfo.GpsLongtitude = Request["GpsLongtitude"];
            dginfo.GpsLatitude = Request["GpsLatitude"];
            dginfo.Altitude = Request["Altitude"];

            dginfo.InstitudeHouse = Request["InstitudeHouse"];
            dginfo.InstitudeRoad = Request["InstitudeRoad"];
            dginfo.InstitudeSection = Request["InstitudeSection"];
            dginfo.InstitudeVillage = Request["InstitudeVillage"];

            dginfo.ProviderName = Request["DataProName"];
            dginfo.ProviderDesignation = Request["DataProDesignation"];
            dginfo.ProviderSignature = Request["DataProSig"];

            dginfo.InstituteBng = Request["InstituteBng"];
            dginfo.InstituteEng = Request["InstituteEng"];
            dginfo.Embankment = Request["embankment"];

            dginfo.VerifierName = Request["DataVrfyName"];
            dginfo.VerifierSignature = Request["DataVrfyDesignation"];
            dginfo.VerifierDesignation = Request["sinature"];
       

            dginfo.InstituteType = Request["InstituteType"];
                dginfo.ManagementType = Request["ManagementType"];
                dginfo.DataCollectionDate = Request["DataCollectionDate"];
                dginfo.DataEntryDate = Request["DataEntryDate"];
            dginfo.SchoolArea = Request["SchoolArea"];

            dginfo.LocalSuptDtl = Request["LocalSupportDtl"];
            dginfo.MinimumReqDtl = Request["MinBdtReqDtl"];

           

            dginfo.StudentDisableFeMale = Request["StudentDisableFeMale"];

            dginfo.StudentDisableMale = Request["StudentDisableMale"];
            dginfo.StudentEthnicFeMale = Request["StudentEthnicFeMale"];
            dginfo.StudentEthnicMale = Request["StudentEthnicMale"];
            dginfo.StudentFemale = Request["StudentFemale"];
            dginfo.StudentMale = Request["StudentMale"];
            dginfo.TeacherFemale = Request["teachersFemale"];
            dginfo.TeacherMale = Request["teachersMale"];

            dginfo.DomainStatus = 0;
            dginfo.userId = uinfo.First().UserId;



            dginfo.InstitudeSection = Request["InstitudeSection"];
            dginfo.InstitudeVillage = Request["InstitudeVillage"];
            dginfo.InstitudeRoad = Request["InstitudeRoad"];
            dginfo.InstitudeHouse = Request["InstitudeHouse"];

            dginfo.UnionId = int.Parse(Request["selectunion"]);
            dginfo.UpazillaId = int.Parse(Request["selectupazilla"]);
            dginfo.DivisionId = int.Parse(Request["selectdivision"]);
            dginfo.DistrictId = int.Parse(Request["selectdistrict"]);


            HttpPostedFileBase myFile = Request.Files["MyFile"];
        
            string message = "File upload failed";

            if (myFile != null && myFile.ContentLength != 0)
            {
                string pathForSaving = Server.MapPath("~/Uploads/Post");
                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        string fExtension = Path.GetExtension(myFile.FileName);

                        if (fExtension == ".jpg")
                        {
                            string fileName = eduClsId + fExtension;

                            myFile.SaveAs(Path.Combine(pathForSaving, fileName));
                           
                            message = "File uploaded successfully!";
                        }
                        else
                        {
                            message = "You Can upload Only jpg file ";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = string.Format("File upload failed: {0}", ex.Message);
                    }
                }
            }

            if (Request["postId"] != "" && Request["postId"] != null)
            {
                dginfo.postId =int.Parse(Request["postId"]);
                postMo.Edit(dginfo);
            }
            else
            {
                postMo.Add(dginfo);
            }
            return RedirectToAction("PostDatacollection");
        }

        private bool CreateFolderIfNeeded(string pathForSaving)
        {
            bool result = true;
            if (!Directory.Exists(pathForSaving))
            {
                try
                {
                    Directory.CreateDirectory(pathForSaving);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        public ActionResult Postform()
        {
            ViewBag.ActiveForm = "active";
            if (Request.IsAuthenticated)
            {
                UsersModel userMo = new UsersModel();

                var uinfo = userMo.GetInfoByName(User.Identity.Name);
              


                vm.DivisionInfo = _repository.Divisioninfo();
                vm.DistrictInfo = _repository.Districtinfo();
                vm.UpazillaInfo = _repository.Upazillainfo();
                vm.UnionInfo = _repository.Unioninfo();



                ViewBag.Title = "Operational Panel :Form";
                return View("Postdisaster", vm);

            }
            else
            {
                return RedirectToAction("index", "home");
            }

        }



      


        public ActionResult PostDisasterReport(string id)
        {
            LocalReport lr = new LocalReport();
            PostdisasterModels pmo = new PostdisasterModels();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "PostdisasterRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            var query = pmo.GetPostDisasterRpt();

            //using (PopulationEntities dc = new PopulationEntities())
            //{
            //    cm = dc.StateAreas.ToList();
            //}
            ReportDataSource rd = new ReportDataSource("DataSet1", query);
            lr.DataSources.Add(rd);
            string reportType = "Excel";
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>Excel</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.2in</MarginTop>" +
            "  <MarginLeft>0.2in</MarginLeft>" +
            "  <MarginRight>0.2in</MarginRight>" +
            "  <MarginBottom>0.2in</MarginBottom>" +
            "</DeviceInfo>";
           
          

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            return File(renderedBytes, mimeType);
        }







        public ActionResult PostDatacollection(string sortOrder, string currentFilter, string searchString, int? page)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("index", "Home");

            }
            else
            {

                var uinfo = userMo.GetInfoByName(User.Identity.Name);
                int uid = uinfo.First().UserId;
                vm.PostDisasterInfo = postMo.GetDomainPost();



                ViewBag.DatacollectionTab = "active";


                ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                ViewBag.CurrentFilter = searchString;

                var comdatas = from s in db.PostDisasterInfoes
                               select s;

                if (User.IsInRole("Entry Operator"))
                {
                    comdatas = comdatas.Where(s => s.UserProfile.UserId == uid);
                }
                else if (User.IsInRole("DPE"))
                {

                    comdatas = comdatas.Where(s => s.AuthorityType.Contains("DPE"));

                }
                else if (User.IsInRole("DSHE"))
                {


                    comdatas = comdatas.Where(s => s.AuthorityType.Contains("DSHE"));

                }
                else if (User.IsInRole("BMEB"))
                {

                    comdatas = comdatas.Where(s => s.AuthorityType.Contains("BMEB"));

                }
                else if (User.IsInRole("BNFE"))
                {

                    comdatas = comdatas.Where(s => s.AuthorityType.Contains("BNFE"));

                }
                else if (User.IsInRole("BTEB"))
                {

                    comdatas = comdatas.Where(s => s.AuthorityType.Contains("BTEB"));

                }
                else
                {
                    comdatas = from s in db.PostDisasterInfoes
                               select s;
                }


                if (comdatas.Count() > 0)
                {

                    //int count = comdatas.Count();
                    if (!String.IsNullOrEmpty(searchString))
                    {

                        comdatas = comdatas.Where(s => s.InstituteEng.ToUpper().Contains(searchString.ToUpper())
                                               || s.InstituteEng.ToUpper().Contains(searchString.ToUpper()));
                    }
                    switch (sortOrder)
                    {
                        case "Name_desc":
                            comdatas = comdatas.OrderByDescending(s => s.InstituteEng);
                            break;

                        default:
                            comdatas = comdatas.OrderBy(s => s.InstituteEng);
                            break;
                    }



                    int pageSize = 20;
                    int pageNumber = (page ?? 1);
                    //if (comdatas != null)
                    //    vm.DomainInfo = comdatas.ToPagedList(pageNumber, pageSize).ToList();
                    ViewBag.Title = "Operational Panel :Post Disaster Data Collection Tools ";
                    return View("PostDataCollection", comdatas.ToPagedList(pageNumber, pageSize));
                }
                else
                {
                    int pageSize = 20;
                    int pageNumber = (page ?? 1);
                    //if (comdatas != null)
                    //    vm.DomainInfo = comdatas.ToPagedList(pageNumber, pageSize).ToList();
                    ViewBag.Title = "Operational Panel : Post Disaster Data Collection Tools ";
                    return View("PostDataCollection", comdatas.ToPagedList(pageNumber, pageSize));
                }
                //return View("DataCollection", vm);

            }


        }
        #region Actions
        [HttpPost]
        public JsonResult Delete()
        {

            int domainId = int.Parse(Request["postId"]);
            var res = new DIMS.Controllers.DomainController.Result { Text = "" };
            if (User.Identity.Name == "")
            {
                res = new DIMS.Controllers.DomainController.Result { Text = "Please login first than then try again" };

                return Json(res);
            }
            else
            {
                ViewModels vm = new ViewModels();

                var DomainInfo = _repository.DomaininfoById(domainId);
                PostdisasterModels domModel = new PostdisasterModels();

                UsersModel userMo = new UsersModel();

                var uinfo = userMo.GetInfoByName(User.Identity.Name);
                int? userId = uinfo.First().UserId;

                if (DomainInfo.Count > 0)
                {
                    int? genId = DomainInfo.First().GeneralInformationId;

                   
                    domModel.removePostDomain(userId, domainId);

                }

                res = new DIMS.Controllers.DomainController.Result { Text = "success" };

                return Json(res);
            }
        }

        #endregion

    }
}
