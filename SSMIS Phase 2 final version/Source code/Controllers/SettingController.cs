﻿using DIMS.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DIMS.Controllers
{

    public class SettingController : Controller
    {
        //
        // GET: /Setting/


        DMISDBEntities db = new DMISDBEntities();
        Repository _repo = new Repository();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UnAuth()
        {
            return View("Unauthorize");
        }
          [Authorize(Roles = "Administrator")]
        public ActionResult backuprestore()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["MasterConnection"].ConnectionString;

            try
            {
                if (Request["restoredbname"] != "")
                {

                   // string _DatabaseName = Request["restoredbname"];
                    string _BackupName = Request["restoredbname"];

                    SqlConnection sqlConnection = new SqlConnection();
                    sqlConnection.ConnectionString = connectionString;
                    sqlConnection.Open();
                   // string sqlQuery = "RESTORE DATABASE " + _DatabaseName + " FROM DISK ='" + _BackupName + "'";
                    string sqlQuery = "RESTORE DATABASE DMISDB FROM DISK ='" + _BackupName + "'";
                    SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection);
                    sqlCommand.CommandType = CommandType.Text;
                    int iRows = sqlCommand.ExecuteNonQuery();
                    sqlConnection.Close();
                    TempData["Message"] = "The DMISDB database restored with the name " + _BackupName + " successfully...";
                }
                else
                {
                    TempData["Message"] = "Select Database backup first than try again";
                }

            }
            catch (SqlException sqlException)
            {
                TempData["Message"] = sqlException.Message.ToString();

            }
            catch (Exception exception)
            {
                TempData["Message"] = exception.Message.ToString();

            }
            return RedirectToAction("databackup");
        }


          [Authorize(Roles = "Administrator")]
        public ActionResult backupsave()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                string path = Path.Combine(Server.MapPath("~/Backup/"));
                if (!Directory.Exists(@path))
                {
                    Directory.CreateDirectory(@path);
                }

                if (Request["backdbname"] != "")
                {
                    string _DatabaseName = "dbssmis";
                    string _BackupName = _DatabaseName + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString() + ".bak";

                    SqlConnection sqlConnection = new SqlConnection();
                    sqlConnection.ConnectionString = connectionString;
                    sqlConnection.Open();
                //    string sqlQuery = "BACKUP DATABASE " + _DatabaseName + " TO DISK = 'C:\\SQLServerBackups\\" + _BackupName + "' WITH FORMAT, MEDIANAME = 'Z_SQLServerBackups', NAME = '" + _BackupName + "';";
                  
                        string sqlQuery = "BACKUP DATABASE " + _DatabaseName + " TO DISK = '"+path+""+ _BackupName + "' WITH FORMAT, MEDIANAME = 'Z_SQLServerBackups', NAME = '" + _BackupName + "';";


                    SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection);
                    sqlCommand.CommandType = CommandType.Text;
                    int iRows = sqlCommand.ExecuteNonQuery();
                    sqlConnection.Close();

                    TempData["Message"] = "The " + _DatabaseName + " database Backup with the name " + _BackupName + " successfully...";
                    // ReadBackupFiles();
                }
                else
                {
                    TempData["Message"] = "Select Database first than try again";
                }
            }
            catch (SqlException sqlException)
            {
                TempData["Message"] = sqlException.Message.ToString();
 
            }
            catch (Exception exception)
            {
                TempData["Message"] = exception.Message.ToString();
               
            }

            return RedirectToAction("databackup");
        }

        


        string[] GetDataBase()
        {
            string sqlQuery = "SELECT * FROM sys.databases";
            DataTable data = GetDataFromQuery(sqlQuery);


            string[] array = data.AsEnumerable()
                             .Select(row => row.Field<string>("name"))
                             .ToArray();


          //  string[] array2 = data.AsEnumerable().OrderByDescending(row => row.Field<string>("name")).SelectMany(row => row.Field<string>("name"));
                 //.ToArray();

            string[,] stringArray = new string[data.Rows.Count, data.Columns.Count];

            for (int row = 0; row < data.Rows.Count; ++row)
            {
                for (int col = 0; col < data.Columns.Count; col++)
                {
                    stringArray[row, col] = data.Rows[row][col].ToString();
                }
            }

            //string[] array2 = null;
            //for (int i = 0; i < data.Rows.Count;i++ )
            //{
            //    array2=data[]
            //}


            return array;
        }

        DataTable GetDataFromQuery(string query)
        {
          var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlDataAdapter adap = new SqlDataAdapter(query,connectionString);

            DataTable data = new DataTable();
            adap.Fill(data);
            return data;
        }
          [Authorize(Roles = "Administrator")]
        public FileResult Download(string fileName)
        {
            //var path = Path.Combine(Server.MapPath("c:/SQLServerBackups/"), fileName);
            var path = Path.Combine(fileName);
            
            var fileStream = new FileStream(path, FileMode.Open);
            //var fileStreamResult = File(fileStream, "application/octet-stream");
           // fileStreamResult.FileDownloadName = fileName;
            return File(fileStream, "application/octet-stream","DMISDB.bak");
        }


   [Authorize(Roles = "Administrator")]
        public ActionResult databackup()
        {

            string[] arr = GetDataBase();
            string[] files = GetFiles();
           ViewData["file"] = files;
            ViewData["name"] = arr;
            return View("Backup");
        }


        string[] GetFiles()
        {
            //if (!Directory.Exists(@"c:\SQLServerBackups\"))
            //{
            //    Directory.CreateDirectory(@"c:\SQLServerBackups\");
            //}
            string path = Path.Combine(Server.MapPath("~/Backup/"));
            if (!Directory.Exists(@path))
            {
                Directory.CreateDirectory(@path);
            }


            //string[] files = Directory.GetFiles(@"c:\SQLServerBackups\", "*.bak");

            string[] files = Directory.GetFiles(@path, "*.bak");

            return files;
        }


        public ActionResult GetDiv()
        {

            int divId =int.Parse(Request["divisionId"]);

            ViewModels vm = new ViewModels();
            vm.DivisionInfo = _repo.DivisioninfoById(divId);



            return View("DivisionEdit", vm);
        }

        public ActionResult GetDis()
        {
            int disId = int.Parse(Request["disId"]);

            ViewModels vm = new ViewModels();
            vm.DivisionInfo = _repo.Divisioninfo();
            vm.DistrictInfo = _repo.DistrictinfoById(disId);



            return View("DistrictEdit", vm);
        }

        public ActionResult GetUpa()
        {
            ViewModels vm = new ViewModels();
            vm.DivisionInfo = _repo.Divisioninfo();
            vm.DistrictInfo = _repo.Districtinfo();
            int upaId = int.Parse(Request["upaId"]);
   
            vm.UpazillaInfo = _repo.UpazillainfoById(upaId);


            return View("UpazillaEdit", vm);
        }

        public ActionResult GetUn()
        {
            ViewModels vm = new ViewModels();

            vm.DivisionInfo = _repo.Divisioninfo();
            vm.DistrictInfo = _repo.Districtinfo();
            vm.UpazillaInfo = _repo.Upazillainfo();
            int unId = int.Parse(Request["unId"]);
            vm.UnionInfo = _repo.UnioninfoById(unId);
            return View("UnionEdit", vm);
        }


        public ActionResult GeneralSetting()
        {
            ViewModels vm = new ViewModels();
            vm.DivisionInfo = _repo.Divisioninfo();
            vm.DistrictInfo = _repo.Districtinfo();
            vm.UpazillaInfo = _repo.Upazillainfo();
            vm.UnionInfo = _repo.Unioninfo();
            ViewBag.ActiveForm = "active";

            ViewBag.Title = "Oparation Panel : General Setting : Setup Form";
            ViewBag.Href = "/setting/generalsetting";
            return View("DomainSetting",vm);
        }


        [HttpPost]
        public virtual JsonResult UnionRemove()
        {
            SettingModel settingModel=new SettingModel();
            int? divId = int.Parse(Request["divisionId"]);
            var res = new Code { };
            try
            {
                settingModel.removeUnion(divId);
                res = new Code { TxtCode = "Successfully remove", Viewdta = null, TxtName = null };
            }
            catch (SqlException sqlException)
            {
                 res = new Code { TxtCode = sqlException.Message.ToString(), Viewdta = null, TxtName = null };
                //TempData["Message"] = sqlException.Message.ToString();

            }
            catch (Exception exception)
            {
                 res = new Code { TxtCode = exception.Message.ToString(), Viewdta = null, TxtName = null };
               // TempData["Message"] = exception.Message.ToString();

            }
      

           // ViewModels vm = new ViewModels();
            // vm.DistrictInfo = _repo.DistrictinfoByDiv(divId);

          // 



            return Json(res);
        }




        [HttpPost]
        public virtual JsonResult UpazillaRemove()
        {
            SettingModel settingModel = new SettingModel();
            int? divId = int.Parse(Request["divisionId"]);
            var res = new Code { };
            try
            {
                settingModel.removeUpazilla(divId);
                res = new Code { TxtCode = "Successfully remove", Viewdta = null, TxtName = null };
            }
            catch (SqlException sqlException)
            {
                res = new Code { TxtCode = sqlException.Message.ToString(), Viewdta = null, TxtName = null };
                //TempData["Message"] = sqlException.Message.ToString();

            }
            catch (Exception exception)
            {
                res = new Code { TxtCode = exception.Message.ToString(), Viewdta = null, TxtName = null };
                // TempData["Message"] = exception.Message.ToString();

            }


            // ViewModels vm = new ViewModels();
            // vm.DistrictInfo = _repo.DistrictinfoByDiv(divId);

            // 



            return Json(res);
        }






        [HttpPost]
        public virtual JsonResult DivisionRemove()
        {
            SettingModel settingModel = new SettingModel();
            int? divId = int.Parse(Request["divisionId"]);
            var res = new Code { };
            try
            {
                settingModel.removeDivision(divId);
                res = new Code { TxtCode = "Successfully remove", Viewdta = null, TxtName = null };
            }
            catch (SqlException sqlException)
            {
                res = new Code { TxtCode = sqlException.Message.ToString(), Viewdta = null, TxtName = null };
                //TempData["Message"] = sqlException.Message.ToString();

            }
            catch (Exception exception)
            {
                res = new Code { TxtCode = exception.Message.ToString(), Viewdta = null, TxtName = null };
                // TempData["Message"] = exception.Message.ToString();

            }


            // ViewModels vm = new ViewModels();
            // vm.DistrictInfo = _repo.DistrictinfoByDiv(divId);

            // 



            return Json(res);
        }




        [HttpPost]
        public virtual JsonResult DistrictRemove()
        {
            SettingModel settingModel = new SettingModel();
            int? divId = int.Parse(Request["divisionId"]);
            var res = new Code { };
            try
            {
                settingModel.removeDistrict(divId);

                res = new Code { TxtCode = "Successfully remove", Viewdta = null, TxtName = null };
            }
            catch (SqlException sqlException)
            {
                res = new Code { TxtCode = sqlException.Message.ToString(), Viewdta = null, TxtName = null };
                //TempData["Message"] = sqlException.Message.ToString();

            }
            catch (Exception exception)
            {
                res = new Code { TxtCode = exception.Message.ToString(), Viewdta = null, TxtName = null };
                // TempData["Message"] = exception.Message.ToString();

            }


            // ViewModels vm = new ViewModels();
            // vm.DistrictInfo = _repo.DistrictinfoByDiv(divId);

            // 



            return Json(res);
        }


        [HttpPost]
        public virtual JsonResult DivisionCode()
        {
           
          int divId=int.Parse(Request["divisionId"]);
          var divCodeQ = _repo.DivisioninfoById(divId);
          ViewModels vm = new ViewModels();
          vm.DistrictInfo= _repo.DistrictinfoByDiv(divId);
          var res = new Code { TxtCode = divCodeQ.First().DivisionCode, Viewdta = RenderPartialViewToString("DistrictList", vm), TxtName = divCodeQ.First().DivisionName };
            

            return Json(res);
            
        }


        [HttpPost]
        public virtual JsonResult DistrictCode()
        {
            int divId = int.Parse(Request["divisionId"]);
            var divCodeQ = _repo.DistrictinfoById(divId);
            ViewModels vm = new ViewModels();
            vm.UpazillaInfo = _repo.UpazillainfoByDis(divId);
            var res = new Code { TxtCode = divCodeQ.First().DistrictCode, Viewdta = RenderPartialViewToString("UpazillaList", vm), TxtName = divCodeQ.First().DistrictName };



            return Json(res);
        }





        [HttpPost]
        public virtual JsonResult UpazillaCode()
        {
            int divId = int.Parse(Request["divisionId"]);
            var divCodeQ = _repo.UpazillainfoById(divId);
            ViewModels vm = new ViewModels();
            vm.UnionInfo = _repo.UnioninfoByUpa(divId);
            var res = new Code { TxtCode = divCodeQ.First().UpazillaCode, Viewdta = RenderPartialViewToString("UnionList", vm), TxtName = divCodeQ.First().UpazillaName };



            return Json(res);
        }
        [HttpPost]
        public virtual JsonResult UnionCode()
        {
            int divId = int.Parse(Request["divisionId"]);
            var divCodeQ = _repo.UnioninfoById(divId);
            ViewModels vm = new ViewModels();
           // vm.DistrictInfo = _repo.DistrictinfoByDiv(divId);
            var res = new Code { TxtCode = divCodeQ.First().UnionCode, Viewdta = null ,TxtName=divCodeQ.First().UnionName};



            return Json(res);
        }
        
        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        public class Code
        {
            public string TxtCode { get; set; }
            public string TxtName { get; set; }
            public string Viewdta { get; set; }
        }

        public ActionResult divisionsave()
        {

            Division div = new Division();
          div.DivisionName= Request["divisionname"];
          div.DivisionCode = Request["divisioncode"];
          div.Latitude = Request["divisionLatitude"];
          div.Longitude = Request["divisionLongitude"];
          db.Divisions.Add(div);
          db.SaveChanges();

            return RedirectToAction("GeneralSetting");
        }


        public ActionResult divisionupdate()
        {

            Division div = new Division();
           // var Comp = new DomainSafeLearning();
            int divId = int.Parse(Request["divisionid"]);
          div = db.Divisions.FirstOrDefault(o => o.DivisionId == divId);
         // div.DivisionId = divId;
            div.DivisionName = Request["divisionname"];
            div.DivisionCode = Request["divisioncode"];
            div.Latitude = Request["divisionLatitude"];
            div.Longitude = Request["divisionLongitude"];
          //  db.Divisions.Add(div);
            db.SaveChanges();

            return RedirectToAction("GeneralSetting");
        }


        public ActionResult unionsave()
        {

            Union unv = new Union();
            unv.UnionName = Request["unionname"];
            unv.UpazillaId=int.Parse(Request["upazillaselect"]);
            unv.UnionCode = Request["unioncode"];
            unv.Latitude = Request["unionLatitude"];
            unv.Longitiude = Request["unionLongitude"];
            db.Unions.Add(unv);
            db.SaveChanges();

            return RedirectToAction("GeneralSetting");
        }

        public ActionResult unionupdate()
        {

            Union unv = new Union();
            int disId = int.Parse(Request["id"]);
            unv = db.Unions.FirstOrDefault(o => o.UnionId == disId);

            unv.UnionName = Request["unionname"];
            unv.UpazillaId = int.Parse(Request["upazillaselect"]);
            unv.UnionCode = Request["unioncode"];
            unv.Latitude = Request["unionLatitude"];
            unv.Longitiude = Request["unionLongitude"];
         //   db.Unions.Add(unv);
            db.SaveChanges();

            return RedirectToAction("GeneralSetting");
        }


        public ActionResult districtsave()
        {


            District dis = new District();
            dis.DistrictName = Request["districtname"];
            dis.DistrictCode = Request["districtcode"];
            dis.DivisionId = int.Parse(Request["divisionselect"]);
            dis.Latitude = Request["districtLatitude"];
            dis.Longitiude = Request["districtLongitude"];
            db.Districts.Add(dis);
            db.SaveChanges();

            return RedirectToAction("GeneralSetting");
        }


        public ActionResult districtupdate()
        {


            District dis = new District();
            int disId = int.Parse(Request["id"]);
            dis = db.Districts.FirstOrDefault(o => o.DistrictId == disId);

            dis.DistrictName = Request["districtname"];
            dis.DistrictCode = Request["districtcode"];
            dis.DivisionId = int.Parse(Request["divisionselect"]);
            dis.Latitude = Request["districtLatitude"];
            dis.Longitiude = Request["districtLongitude"];
            
            db.SaveChanges();

            return RedirectToAction("GeneralSetting");
        }



        public ActionResult upazillaupdate()
        {

            Upazilla upz = new Upazilla();
            int disId = int.Parse(Request["id"]);
            upz = db.Upazillas.FirstOrDefault(o => o.UpazillaId == disId);

            upz.UpazillaName = Request["upazillaname"];
            upz.UpazillaCode = Request["upazillacode"];
            upz.DistrictId = int.Parse(Request["districtselect"]);
            upz.Latitude = Request["upazillaLatitude"];
            upz.Longitiude = Request["upazillaLongitude"];
            
            db.SaveChanges();

            return RedirectToAction("GeneralSetting");
        }


        public ActionResult upazillasave()
        {

            Upazilla upz = new Upazilla();
            upz.UpazillaName = Request["upazillaname"];
            upz.UpazillaCode = Request["upazillacode"];
            upz.DistrictId = int.Parse(Request["districtselect"]);
            upz.Latitude = Request["upazillaLatitude"];
            upz.Longitiude = Request["upazillaLongitude"];
            db.Upazillas.Add(upz);
            db.SaveChanges();

            return RedirectToAction("GeneralSetting");
        }

    }
    public class DatabaseList
    {
        string name { get;set;}
        int database_id{get;set;}
    }


}
