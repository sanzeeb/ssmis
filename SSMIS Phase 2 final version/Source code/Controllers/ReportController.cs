﻿using DIMS.Models;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DIMS.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/
        private DMISDBEntities dmisdb = new DMISDBEntities();
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult DisasterhazardImpact()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();

            return View(rm);
        }

        public ActionResult Disasterhazard()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();


          
            return View(rm);
        }



        public ActionResult Riskcategory()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }



        public ActionResult Geographicalpositionwise()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }
        public ActionResult GeographicalpositionwiseRpt()
        {

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "GeographicalpositionwiseRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Geographicalpositionwise");
            }

            GraphModels gpm = new GraphModels();
            List<DomainGeneralInformation> chklist = new List<DomainGeneralInformation>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            

            chklist = gpm.GetRptByGeoPostion(divisionId,districtId,upazilaId,unionId);

            ReportDataSource rd = new ReportDataSource("DataSet1", chklist);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>5in</PageHeight>" +
            "  <MarginTop>0.25in</MarginTop>" +
            "  <MarginLeft>0.25in</MarginLeft>" +
            "  <MarginRight>0.25in</MarginRight>" +
            "  <MarginBottom>0.25in</MarginBottom>" +
            "</DeviceInfo>";
            
            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }


        public ActionResult BuildingStructureType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }
        public ActionResult BuildingStructureTypeRpt()
        {

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "BuildingStructureTypeRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("BuildingStructureType");
            }

            GraphModels gpm = new GraphModels();
            List<DomainSafeLearning> result = new List<DomainSafeLearning>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string area = Request["area"];

            //var query = from s in dmisdb.DomainSafeLearnings  select s;
            result = gpm.GetByStrcType(divisionId, districtId, upazilaId, unionId, area);

            ReportDataSource rd = new ReportDataSource("DataSet1", result);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>5in</PageHeight>" +
            "  <MarginTop>0.25in</MarginTop>" +
            "  <MarginLeft>0.25in</MarginLeft>" +
            "  <MarginRight>0.25in</MarginRight>" +
            "  <MarginBottom>0.25in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }


        public ActionResult ClassRoomCondition()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }
        public ActionResult ClassRoomConditionRpt()
        {

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "ClassroomRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("ClassRoomCondition");
            }

            GraphModels gpm = new GraphModels();
            List<DomainSafeLearning> result = new List<DomainSafeLearning>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string area = Request["area"];

            //var query = from s in dmisdb.DomainSafeLearnings  select s;
            result = gpm.GetByStrcType(divisionId, districtId, upazilaId, unionId, area);

            ReportDataSource rd = new ReportDataSource("DataSet1", result);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>5in</PageHeight>" +
            "  <MarginTop>0.25in</MarginTop>" +
            "  <MarginLeft>0.25in</MarginLeft>" +
            "  <MarginRight>0.25in</MarginRight>" +
            "  <MarginBottom>0.25in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }


        public ActionResult CommonFacility()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }
        public ActionResult CommonFacilityRpt()
        {

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "CommonFacilityRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return RedirectToAction("CommonFacility");
            }

            GraphModels gpm = new GraphModels();
            List<DomainGeneralInformation> fire = new List<DomainGeneralInformation>();
            List<DomainGeneralInformation> aid = new List<DomainGeneralInformation>();
            List<DomainGeneralInformation> comp = new List<DomainGeneralInformation>();
            List<DomainGeneralInformation> inter = new List<DomainGeneralInformation>();
            List<DomainGeneralInformation> play = new List<DomainGeneralInformation>();
            List<DomainGeneralInformation> elect = new List<DomainGeneralInformation>();
            List<DomainGeneralInformation> tifi = new List<DomainGeneralInformation>();
            List<CheckBox> vol = new List<CheckBox>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string faci = Request["area"];

            fire = gpm.GetByDrinkingFaciltyFire(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd = new ReportDataSource("DataSet7", fire);


            aid = gpm.GetByDrinkingFaciltyAid(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd1 = new ReportDataSource("DataSet6", aid);

            comp = gpm.GetByDrinkingFaciltyComp(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd2 = new ReportDataSource("DataSet5", comp);

            inter = gpm.GetByDrinkingFaciltyInter(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd3 = new ReportDataSource("DataSet1", inter);

            play = gpm.GetByDrinkingFaciltyPlay(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd4 = new ReportDataSource("DataSet3", play);

            elect = gpm.GetByDrinkingFaciltyTubwellArsinicElect(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd5 = new ReportDataSource("DataSet4", elect);

            tifi = gpm.GetByDrinkingFaciltyTubwellTifi(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd6 = new ReportDataSource("DataSet2", tifi);

            vol = gpm.GetByDrinkingFaciltyTubwellVol(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd7 = new ReportDataSource("DataSet8", vol);

            lr.DataSources.Add(rd);
            lr.DataSources.Add(rd1);
            lr.DataSources.Add(rd2);
            lr.DataSources.Add(rd3);
            lr.DataSources.Add(rd4);
            lr.DataSources.Add(rd5);
            lr.DataSources.Add(rd6);
            lr.DataSources.Add(rd7);


            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>4.5in</PageHeight>" +
            "  <MarginTop>0.0in</MarginTop>" +
            "  <MarginLeft>0.0in</MarginLeft>" +
            "  <MarginRight>0.0in</MarginRight>" +
            "  <MarginBottom>0.0in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }

        public ActionResult StudentRatio()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }
        public ActionResult StudentRatioRpt()
        {

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "StudentRatioRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return RedirectToAction("StudentRatio");
            }

            GraphModels gpm = new GraphModels();
            List<DomainGeneralInformation> result = new List<DomainGeneralInformation>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string faci = Request["area"];

            //var query = from s in dmisdb.DomainSafeLearnings  select s;
            result = gpm.GetByAreaType(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd = new ReportDataSource("DataSet1", result);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>5in</PageHeight>" +
            "  <MarginTop>0.1in</MarginTop>" +
            "  <MarginLeft>0.1in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.1in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }

        public ActionResult VacantPositionRatio()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }
        public ActionResult VacantPositionRatioRpt()
        {

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "VacantPositionRatioRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return RedirectToAction("VacantPositionRatio");
            }

            GraphModels gpm = new GraphModels();
            List<DomainGeneralInformation> result = new List<DomainGeneralInformation>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string faci = Request["area"];

            //var query = from s in dmisdb.DomainSafeLearnings  select s;
            result = gpm.GetByAreaType(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd = new ReportDataSource("DataSet1", result);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>4.5in</PageHeight>" +
            "  <MarginTop>0.0in</MarginTop>" +
            "  <MarginLeft>0.0in</MarginLeft>" +
            "  <MarginRight>0.0in</MarginRight>" +
            "  <MarginBottom>0.0in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }

        public ActionResult StudentLearningSource()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }
        public ActionResult StudentLearningSourceRpt()
        {

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "StudentLearningSourceRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return RedirectToAction("StudentLearningSource");
            }

            GraphModels gpm = new GraphModels();
            List<CheckBoxDto> result = new List<CheckBoxDto>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string faci = Request["area"];

            //var query = from s in dmisdb.DomainSafeLearnings  select s;
            result = gpm.GetByRiskAreaCheckType(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd = new ReportDataSource("DataSet1", result);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>4.5in</PageHeight>" +
            "  <MarginTop>0.0in</MarginTop>" +
            "  <MarginLeft>0.0in</MarginLeft>" +
            "  <MarginRight>0.0in</MarginRight>" +
            "  <MarginBottom>0.0in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }


        public ActionResult DrinkingwaterFacility()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }

        public ActionResult DrinkingwaterFacilityRpt()
        {

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "DrinkingwaterFacilityRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return RedirectToAction("DrinkingwaterFacility");
            }

            GraphModels gpm = new GraphModels();
            List<DomainSafeLearning> funct = new List<DomainSafeLearning>();
            List<DomainSafeLearning> ares = new List<DomainSafeLearning>();
            List<DomainSafeLearning> iftwb = new List<DomainSafeLearning>();
            List<DomainSafeLearning> safe = new List<DomainSafeLearning>();
            List<DomainSafeLearning> own = new List<DomainSafeLearning>();
            List<DomainSafeLearning> child = new List<DomainSafeLearning>();
            List<DomainSafeLearning> dis = new List<DomainSafeLearning>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string faci = Request["area"];

            //var query = from s in dmisdb.DomainSafeLearnings  select s;
            funct = gpm.GetByDrinkingFaciltyFunc(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd = new ReportDataSource("DataSet1", funct);


            dis = gpm.GetByDrinkingFaciltyDisable(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd1 = new ReportDataSource("DataSet2", dis);

            child = gpm.GetByDrinkingFaciltyChild(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd2 = new ReportDataSource("DataSet7", child);

            own = gpm.GetByDrinkingFaciltyOwn(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd3 = new ReportDataSource("DataSet3", own);

            safe = gpm.GetByDrinkingFaciltySafe(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd4 = new ReportDataSource("DataSet4", safe);

            ares = gpm.GetByDrinkingFaciltyTubwellArsinicFree(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd5 = new ReportDataSource("DataSet5", ares);

            iftwb = gpm.GetByDrinkingFaciltyTubwellFunc(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd6 = new ReportDataSource("DataSet6", iftwb);

            lr.DataSources.Add(rd);
            lr.DataSources.Add(rd1);
            lr.DataSources.Add(rd2);
            lr.DataSources.Add(rd3);
            lr.DataSources.Add(rd4);
            lr.DataSources.Add(rd5);
            lr.DataSources.Add(rd6);

            lr.DataSources.Add(rd);


            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>4.5in</PageHeight>" +
            "  <MarginTop>0.0in</MarginTop>" +
            "  <MarginLeft>0.0in</MarginLeft>" +
            "  <MarginRight>0.0in</MarginRight>" +
            "  <MarginBottom>0.0in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }


        
        public ActionResult Resilienceeducation()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }
        public ActionResult ResilienceeducationRpt()
        {

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "ResilienceeducationRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return RedirectToAction("Resilienceeducation");
            }

            GraphModels gpm = new GraphModels();
            List<DomainDisasterManagement> continu = new List<DomainDisasterManagement>();
            List<DomainDisasterManagement> temporary = new List<DomainDisasterManagement>();

            List<DomainDisasterManagement> catchment = new List<DomainDisasterManagement>();
            List<DomainDisasterManagement> emergency = new List<DomainDisasterManagement>();
            List<RiskReductionResilienceEducation> drill = new List<RiskReductionResilienceEducation>();
            List<RiskReductionResilienceEducation> drr = new List<RiskReductionResilienceEducation>();


            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"]!= "" && Request["divisionId"]!= null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"]!= "" && Request["districtId"]!= null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"]!= "" && Request["upazilaId"]!= null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"]!= "" && Request["unionId"]!= null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string faci = Request["area"];

            //var query = from s in dmisdb.DomainSafeLearnings  select s;
            continu = gpm.GetDisasterContinuity(divisionId, districtId, upazilaId, unionId, faci);
            temporary = gpm.GetDisasterTemporary(divisionId, districtId, upazilaId, unionId, faci);
            catchment = gpm.GetDisasterCatchment(divisionId, districtId, upazilaId, unionId, faci);
            emergency = gpm.GetDisasterEmergency(divisionId, districtId, upazilaId, unionId, faci);
            drill = gpm.GetRiskDrill(divisionId, districtId, upazilaId, unionId, faci);
            drr = gpm.GetRiskDRR(divisionId, districtId, upazilaId, unionId, faci);


            ReportDataSource rd = new ReportDataSource("DataSet1", continu);
            ReportDataSource rd2 = new ReportDataSource("DataSet2", temporary);
            ReportDataSource rd3 = new ReportDataSource("DataSet3", catchment);
            ReportDataSource rd4 = new ReportDataSource("DataSet4", emergency);
            ReportDataSource rd5 = new ReportDataSource("DataSet5", drill);
            ReportDataSource rd6 = new ReportDataSource("DataSet6", drr);


            lr.DataSources.Add(rd);
            lr.DataSources.Add(rd2);
            lr.DataSources.Add(rd3);
            lr.DataSources.Add(rd4);
            lr.DataSources.Add(rd5);
            lr.DataSources.Add(rd6);
           
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;

            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>4.5in</PageHeight>" +
            "  <MarginTop>0.0in</MarginTop>" +
            "  <MarginLeft>0.0in</MarginLeft>" +
            "  <MarginRight>0.0in</MarginRight>" +
            "  <MarginBottom>0.0in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }



        public ActionResult Toiletfacility1()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }
        public ActionResult Toiletfacility1Rpt()
        {

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "Toiletfacility1Rpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return RedirectToAction("Toiletfacility1");
            }

            GraphModels gpm = new GraphModels();
            List<DomainSafeLearning> reglar = new List<DomainSafeLearning>();
            List<DomainSafeLearning> access = new List<DomainSafeLearning>();
            List<DomainSafeLearning> child = new List<DomainSafeLearning>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string faci = Request["area"];

            //var query = from s in dmisdb.DomainSafeLearnings  select s;
            reglar = gpm.GetByToiletType(divisionId, districtId, upazilaId, unionId, faci);
            access = gpm.GetByToiletType1(divisionId, districtId, upazilaId, unionId, faci);
            child = gpm.GetByToiletType2(divisionId, districtId, upazilaId, unionId, faci);

            ReportDataSource rd = new ReportDataSource("DataSet1", reglar);
            lr.DataSources.Add(rd);

            ReportDataSource rd1 = new ReportDataSource("DataSet2", access);
            lr.DataSources.Add(rd1);

            ReportDataSource rd2 = new ReportDataSource("DataSet3", child);
            lr.DataSources.Add(rd2);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>4.5in</PageHeight>" +
            "  <MarginTop>0.0in</MarginTop>" +
            "  <MarginLeft>0.0in</MarginLeft>" +
            "  <MarginRight>0.0in</MarginRight>" +
            "  <MarginBottom>0.0in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }
        public ActionResult Toiletfacility2()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }
        public ActionResult Toiletfacility2Rpt()
        {

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "Toiletfacility2Rpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return RedirectToAction("Toiletfacility2");
            }

            GraphModels gpm = new GraphModels();
            List<DomainSafeLearning> result = new List<DomainSafeLearning>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

          

            //var query = from s in dmisdb.DomainSafeLearnings  select s;
            result = gpm.GetByCommonToiletType(divisionId, districtId, upazilaId, unionId);

            ReportDataSource rd = new ReportDataSource("DataSet1", result);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>4.5in</PageHeight>" +
            "  <MarginTop>0.0in</MarginTop>" +
            "  <MarginLeft>0.0in</MarginLeft>" +
            "  <MarginRight>0.0in</MarginRight>" +
            "  <MarginBottom>0.0in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }
        public ActionResult Toiletfacility3()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }
        public ActionResult Toiletfacility3Rpt()
        {

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "Toiletfacility3Rpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return RedirectToAction("Toiletfacility3");
            }

            GraphModels gpm = new GraphModels();
            List<DomainSafeLearning> boy = new List<DomainSafeLearning>();
            List<DomainSafeLearning> girl = new List<DomainSafeLearning>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

         

            //var query = from s in dmisdb.DomainSafeLearnings  select s;
            boy = gpm.GetByAccetableBoy(divisionId, districtId, upazilaId, unionId);
          //  girl = gpm.GetByAccetableGirl(divisionId, districtId, upazilaId, unionId);

            ReportDataSource rd = new ReportDataSource("DataSet1", boy);
        //    ReportDataSource rd1 = new ReportDataSource("DataSet2", girl);
            lr.DataSources.Add(rd);
          //  lr.DataSources.Add(rd1);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;

            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>8.5in</PageHeight>" +
            "  <MarginTop>0.0in</MarginTop>" +
            "  <MarginLeft>0.0in</MarginLeft>" +
            "  <MarginRight>0.0in</MarginRight>" +
            "  <MarginBottom>0.0in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }
        /*
         
         * 
         * Post Disaster Report
         */

        public ActionResult DamageType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }
        public ActionResult DamageTypeRpt()
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "DamageTypeRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("DamageType");
            }
            GraphModels gpm = new GraphModels();
            List<PostDisasterInfo> chklist = new List<PostDisasterInfo>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string area = "";


            if (Request["hazardName"] != null)
            {
                area = Request["hazardName"];
            }
            // var query = gpm.GetHazardsByStrcType(0,0,0,0,"all");


            chklist = gpm.GetPostUrbanRuralAffected(divisionId, districtId, upazilaId, unionId, area);

            //var query = from s in dmisdb.CheckBoxs where s.QuestionId.Equals("19.0") select s;







            ReportDataSource rd = new ReportDataSource("DataSet1", chklist);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>5in</PageHeight>" +
            "  <MarginTop>0.25in</MarginTop>" +
            "  <MarginLeft>0.25in</MarginLeft>" +
            "  <MarginRight>0.25in</MarginRight>" +
            "  <MarginBottom>0.25in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }

        public ActionResult Areawise()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }


        public ActionResult AreawiseRpt()
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "AreawiseRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Areawise");
            }
            GraphModels gpm = new GraphModels();
            List<PostDisasterInfo> chklist = new List<PostDisasterInfo>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string area = "";


            if (Request["hazardName"] != null)
            {
                area = Request["hazardName"];
            }
            // var query = gpm.GetHazardsByStrcType(0,0,0,0,"all");


            chklist = gpm.GetPostUrbanRuralAffected(divisionId, districtId, upazilaId, unionId, area);

            //var query = from s in dmisdb.CheckBoxs where s.QuestionId.Equals("19.0") select s;


            ReportDataSource rd = new ReportDataSource("DataSet1", chklist);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>5in</PageHeight>" +
            "  <MarginTop>0.25in</MarginTop>" +
            "  <MarginLeft>0.25in</MarginLeft>" +
            "  <MarginRight>0.25in</MarginRight>" +
            "  <MarginBottom>0.25in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }
        public ActionResult FinancialRequremenst()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }

        public ActionResult FinancialRequremenstRpt()
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "FinancialRequremenstRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("FinancialRequremenst");
            }
            GraphModels gpm = new GraphModels();
            List<PostDisasterInfo> chklist = new List<PostDisasterInfo>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string area = "";


            if (Request["hazardName"] != null)
            {
                area = Request["hazardName"];
            }
            // var query = gpm.GetHazardsByStrcType(0,0,0,0,"all");


            chklist = gpm.GetPostUrbanRuralAffected(divisionId, districtId, upazilaId, unionId, area);

            //var query = from s in dmisdb.CheckBoxs where s.QuestionId.Equals("19.0") select s;







            ReportDataSource rd = new ReportDataSource("DataSet1", chklist);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>5in</PageHeight>" +
            "  <MarginTop>0.25in</MarginTop>" +
            "  <MarginLeft>0.25in</MarginLeft>" +
            "  <MarginRight>0.25in</MarginRight>" +
            "  <MarginBottom>0.25in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }


        public ActionResult GeographicalPosition()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }

        public ActionResult GeographicalPositionRpt()
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "GeographicalPositionRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("GeographicalPosition");
            }
            GraphModels gpm = new GraphModels();
            List<PostDisasterInfo> chklist = new List<PostDisasterInfo>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string area = "";


            if (Request["hazardName"] != null)
            {
                area = Request["hazardName"];
            }
            // var query = gpm.GetHazardsByStrcType(0,0,0,0,"all");


            chklist = gpm.GetPostUrbanRuralAffected(divisionId, districtId, upazilaId, unionId, area);

            //var query = from s in dmisdb.CheckBoxs where s.QuestionId.Equals("19.0") select s;







            ReportDataSource rd = new ReportDataSource("DataSet1", chklist);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>5in</PageHeight>" +
            "  <MarginTop>0.25in</MarginTop>" +
            "  <MarginLeft>0.25in</MarginLeft>" +
            "  <MarginRight>0.25in</MarginRight>" +
            "  <MarginBottom>0.25in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }





        public ActionResult ManagementType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }


        public ActionResult ManagementTypeRpt()
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "ManagementTypeRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("ManagementType");
            }
            GraphModels gpm = new GraphModels();
            List<PostDisasterInfo> chklist = new List<PostDisasterInfo>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string area = "";


            if (Request["hazardName"] != null)
            {
                area = Request["hazardName"];
            }
            // var query = gpm.GetHazardsByStrcType(0,0,0,0,"all");


            chklist = gpm.GetPostUrbanRuralAffected(divisionId, districtId, upazilaId, unionId, area);

            //var query = from s in dmisdb.CheckBoxs where s.QuestionId.Equals("19.0") select s;







            ReportDataSource rd = new ReportDataSource("DataSet1", chklist);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>5in</PageHeight>" +
            "  <MarginTop>0.25in</MarginTop>" +
            "  <MarginLeft>0.25in</MarginLeft>" +
            "  <MarginRight>0.25in</MarginRight>" +
            "  <MarginBottom>0.25in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }



        public ActionResult Ruralurban()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();



            return View(rm);
        }


        public ActionResult RuralUrbanRpt()
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "RuralUrbanRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Disasterhazard");
            }
            GraphModels gpm = new GraphModels();
            List<PostDisasterInfo> chklist = new List<PostDisasterInfo>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string area = "";


            if (Request["hazardName"] != null)
            {
                area = Request["hazardName"];
            }
            // var query = gpm.GetHazardsByStrcType(0,0,0,0,"all");


            chklist = gpm.GetPostUrbanRuralAffected(divisionId, districtId, upazilaId, unionId, area);

            //var query = from s in dmisdb.CheckBoxs where s.QuestionId.Equals("19.0") select s;







            ReportDataSource rd = new ReportDataSource("DataSet1", chklist);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>5in</PageHeight>" +
            "  <MarginTop>0.25in</MarginTop>" +
            "  <MarginLeft>0.25in</MarginLeft>" +
            "  <MarginRight>0.25in</MarginRight>" +
            "  <MarginBottom>0.25in</MarginBottom>" +
            "</DeviceInfo>";
          
            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }

     

        /*
         End Post Disaster
         
         */

        public ActionResult RiskcategoryRpt()
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "RiskcategoryRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Riskcategory");
            }
            GraphModels gpm = new GraphModels();
            List<DisasterHistory> chklist = new List<DisasterHistory>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string area = "All";


            if (Request["area"] != null)
            {
                area = Request["area"];
            }
            // var query = gpm.GetHazardsByStrcType(0,0,0,0,"all");


            chklist = gpm.GetRiskHistoryByStrcType(divisionId, districtId, upazilaId, unionId, area);

            //var query = from s in dmisdb.CheckBoxs where s.QuestionId.Equals("19.0") select s;







            ReportDataSource rd = new ReportDataSource("DataSet1", chklist);
            lr.DataSources.Add(rd);


            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>5in</PageHeight>" +
            "  <MarginTop>0.25in</MarginTop>" +
            "  <MarginLeft>0.25in</MarginLeft>" +
            "  <MarginRight>0.25in</MarginRight>" +
            "  <MarginBottom>0.25in</MarginBottom>" +
            "</DeviceInfo>";
            //string deviceInfo =

            // "<DeviceInfo>" +
            // "  <OutputFormat>" + id + "</OutputFormat>" +
            // "  <PageWidth>8.5in</PageWidth>" +
            // "  <PageHeight>11in</PageHeight>" +
            // "  <MarginTop>0.5in</MarginTop>" +
            // "  <MarginLeft>1in</MarginLeft>" +
            // "  <MarginRight>1in</MarginRight>" +
            // "  <MarginBottom>0.5in</MarginBottom>" +
            // "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
        }
        public ActionResult DisasterhazardRpt()
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "DhazardsReport.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Disasterhazard");
            }
            GraphModels gpm = new GraphModels();
            List<CheckBoxDto> chklist = new List<CheckBoxDto>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != "" && Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string area="All";


            if (Request["area"] != null)
            {
                area = Request["area"];
            }
          // var query = gpm.GetHazardsByStrcType(0,0,0,0,"all");


            chklist = gpm.GetHazardsBySafeType(divisionId, districtId, upazilaId, unionId, area);

            //var query = from s in dmisdb.CheckBoxs where s.QuestionId.Equals("19.0") select s;

            




            
            ReportDataSource rd = new ReportDataSource("DataSet1", chklist);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf";
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType = "Image";
            string imgmimeType = "image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>5in</PageHeight>" +
            "  <MarginTop>0.1in</MarginTop>" +
            "  <MarginLeft>0.1in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.1in</MarginBottom>" +
            "</DeviceInfo>";
            //string deviceInfo =

            // "<DeviceInfo>" +
            // "  <OutputFormat>" + id + "</OutputFormat>" +
            // "  <PageWidth>8.5in</PageWidth>" +
            // "  <PageHeight>11in</PageHeight>" +
            // "  <MarginTop>0.5in</MarginTop>" +
            // "  <MarginLeft>1in</MarginLeft>" +
            // "  <MarginRight>1in</MarginRight>" +
            // "  <MarginBottom>0.5in</MarginBottom>" +
            // "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

          //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle =imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);

            
            title.Save(Server.MapPath(@"~/Uploads/" +User.Identity.Name+".jpg"), ImageFormat.Jpeg);

                
            
           return File(pdfrenderedBytes, pdfmimeType);
            //return File(pdfrenderedBytes, "image/jpeg");
        }

        public ActionResult DisasterhazardImpactRpt()
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rpts"), "DhazardsImpactReport.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("DisasterhazardImpact");
            }
            GraphModels gpm = new GraphModels();
            List<CheckBoxDto> chklist = new List<CheckBoxDto>();

            int divisionId = 0, districtId = 0, upazilaId = 0, unionId = 0;


            if (Request["divisionId"] != "" && Request["divisionId"] != null)
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            if (Request["districtId"] != ""&&Request["districtId"] != null)
            {
                districtId = int.Parse(Request["districtId"]);
            }


            if (Request["upazilaId"] != "" && Request["upazilaId"] != null)
            {
                upazilaId = int.Parse(Request["upazilaId"]);
            }


            if (Request["unionId"] != "" && Request["unionId"] != null)
            {
                unionId = int.Parse(Request["unionId"]);
            }

            string area = "All";


            if (Request["area"] != null)
            {
                area = Request["area"];
            }
            // var query = gpm.GetHazardsByStrcType(0,0,0,0,"all");


            chklist = gpm.GetHazardsBySafeType(divisionId, districtId, upazilaId, unionId, area);

            //var query = from s in dmisdb.CheckBoxs where s.QuestionId.Equals("19.0") select s;







            ReportDataSource rd = new ReportDataSource("DataSet1", chklist);
            lr.DataSources.Add(rd);
            //string reportType = "pdf";
            string pdfreportType = "pdf";
            string pdfmimeType = "application/pdf"; ;
            string pdfencoding;
            string pdffileNameExtension;


            string imgreportType ="Image";
            string imgmimeType ="image/jpeg";
            string imgencoding;
            string imgfileNameExtension;




            string pdfdeviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";


            string imgdeviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>JPEG</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>5in</PageHeight>" +
            "  <MarginTop>0.1in</MarginTop>" +
            "  <MarginLeft>0.1in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.1in</MarginBottom>" +
            "</DeviceInfo>";
            //string deviceInfo =

            // "<DeviceInfo>" +
            // "  <OutputFormat>" + id + "</OutputFormat>" +
            // "  <PageWidth>8.5in</PageWidth>" +
            // "  <PageHeight>11in</PageHeight>" +
            // "  <MarginTop>0.5in</MarginTop>" +
            // "  <MarginLeft>1in</MarginLeft>" +
            // "  <MarginRight>1in</MarginRight>" +
            // "  <MarginBottom>0.5in</MarginBottom>" +
            // "</DeviceInfo>";

            Warning[] pdfwarnings;
            string[] pdfstreams;
            byte[] pdfrenderedBytes;


            Warning[] imgwarnings;
            string[] imgstreams;
            byte[] imgrenderedBytes;


            imgrenderedBytes = lr.Render(
                imgreportType,
                imgdeviceInfo,
                out imgmimeType,
                out imgencoding,
                out imgfileNameExtension,
                out imgstreams,
                out imgwarnings);

            pdfrenderedBytes = lr.Render(
                pdfreportType,
                pdfdeviceInfo,
                out pdfmimeType,
                out pdfencoding,
                out pdffileNameExtension,
                out pdfstreams,
                out pdfwarnings);

            //  byte[] reportContent = report.ServerReport.Render(reportFormat);

            System.Byte[] imgTitle = imgrenderedBytes;
            MemoryStream memStream = new MemoryStream(imgTitle);
            Bitmap title = new Bitmap(memStream);


            title.Save(Server.MapPath(@"~/Uploads/" + User.Identity.Name + ".jpg"), ImageFormat.Jpeg);



            return File(pdfrenderedBytes, pdfmimeType);
            //return File(pdfrenderedBytes, "image/jpeg");
        }




        public ActionResult Report(string id)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "ReportStateArea.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            var query = from s in dmisdb.DomainGeneralInformations select s;

            //using (PopulationEntities dc = new PopulationEntities())
            //{
            //    cm = dc.StateAreas.ToList();
            //}
            ReportDataSource rd = new ReportDataSource("MyDataset", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>pdf</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";
            //string deviceInfo =

            // "<DeviceInfo>" +
            // "  <OutputFormat>" + id + "</OutputFormat>" +
            // "  <PageWidth>8.5in</PageWidth>" +
            // "  <PageHeight>11in</PageHeight>" +
            // "  <MarginTop>0.5in</MarginTop>" +
            // "  <MarginLeft>1in</MarginLeft>" +
            // "  <MarginRight>1in</MarginRight>" +
            // "  <MarginBottom>0.5in</MarginBottom>" +
            // "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            return File(renderedBytes, mimeType);
        }


        public ActionResult Magicsearch()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();


            return View(rm);
        }

        public ActionResult Strcsearch()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();


            return View(rm);
        }

        public ActionResult Geosearch()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();


            return View(rm);
        }

        public ActionResult Commonfacilitiesearch()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();


            return View(rm);
        }


        public ActionResult Disabilitiessearch()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();


            return View(rm);
        }
        public ActionResult Drinkingwatersearch()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();


            return View(rm);
        }
        public ActionResult Contingencysearch()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();


            return View(rm);
        }
        public ActionResult Resiliencesearch()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();


            return View(rm);
        }

        public ActionResult Risksearch()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();


            return View(rm);
        }
        public ActionResult Sanitationsearch()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();


            return View(rm);
        }
        public ActionResult Vacantsearch()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();


            return View(rm);
        }


        public ActionResult Mis()
        {

            return View();
        }

        public ActionResult Advanced()
        {

            return View();
        }

        public ActionResult Reports()
        {

            return View();

        }

        public ActionResult Map()
        {
            ViewBag.PTitle = "Operation Panel: MIS / GIS : Map View"; 
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("index", "Home");

            }
            Reportrepository rpt=new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();
            rm.DistrictInfo = rpt.Districtinfo();
            rm.UnionInfo = rpt.Unioninfo();
            rm.UpazillaInfo = rpt.Upazillainfo();

           

            UsersModel userMo = new UsersModel();

            var uinfo = userMo.GetInfoByName(User.Identity.Name);

            
            if (User.IsInRole("Entry Operator"))
            {
                rm.GeneralInformationInfo = rpt.GeneralInformationById(uinfo.First().UserId);

            }
            else if (User.IsInRole("DPE"))
            {


                rm.GeneralInformationInfo = rpt.GeneralInformationByRole("DPE");
            }
            else if (User.IsInRole("DSHE"))
            {

                rm.GeneralInformationInfo = rpt.GeneralInformationByRole("DSHE");
               

            }
            else if (User.IsInRole("MIS User"))
            {
                rm.GeneralInformationInfo = rpt.GeneralInformationByRole("MIS User");
            }
            else if (User.IsInRole("BNFE"))
            {

                rm.GeneralInformationInfo = rpt.GeneralInformationByRole("BNFE");

            }
            else if (User.IsInRole("BTEB"))
            {
                rm.GeneralInformationInfo = rpt.GeneralInformationByRole("BTEB");
                

            }
            else if (User.IsInRole("BMEB"))
            {
                rm.GeneralInformationInfo = rpt.GeneralInformationByRole("BMEB");


            }
            
            else
            {
                rm.GeneralInformationInfo = rpt.GeneralInformation();
            }
           
            


            return View(rm);
        }

        #region Actions

        [HttpPost]
        public virtual JsonResult Jdvisions()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DivisionInfo = rpt.Divisioninfo();

            //var customers =for c in dmisdb.Divisions  select new { text = c., value = c.CustomerId};


            List<JDIVISION> list = new List<JDIVISION>();
          
           
            foreach(var item in rm.DivisionInfo){
                JDIVISION jdv = new JDIVISION();
            jdv.lat = item.Latitude;
            jdv.lng =item.Longitude;
            jdv.name =item.DivisionName;
            jdv.divsion_id =Convert.ToString(item.DivisionId);
            list.Add(jdv);
                
            }

          //  var res = new JDIVISION {name=rm.DivisionInfo.First().DivisionName,lang=rm.DivisionInfo.First().Longitude,lat=rm.DivisionInfo.First().Latitude};

            return Json(list);
        }






        [HttpPost]
        public virtual JsonResult Jdistricts()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.DistrictInfo= rpt.Districtinfo();

            //var customers =for c in dmisdb.Divisions  select new { text = c., value = c.CustomerId};


            List<JDISTRICT> list = new List<JDISTRICT>();
           
         
            foreach (var item in rm.DistrictInfo)
            {
                JDISTRICT jdv = new JDISTRICT();

                jdv.lat = item.Latitude;
                jdv.lng = item.Longitiude;
                jdv.name = item.DistrictName;
                jdv.district_id = Convert.ToString(item.DistrictId);
                list.Add(jdv);

            }

            //  var res = new JDIVISION {name=rm.DivisionInfo.First().DivisionName,lang=rm.DivisionInfo.First().Longitude,lat=rm.DivisionInfo.First().Latitude};

            return Json(list);
        }

        


        [HttpPost]
        public virtual JsonResult Jnions()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.UnionInfo = rpt.Unioninfo();

            //var customers =for c in dmisdb.Divisions  select new { text = c., value = c.CustomerId};


            List<JUNION> list = new List<JUNION>();
            
           
            foreach (var item in rm.UnionInfo)
            {
                JUNION jdv = new JUNION();
                jdv.lat = item.Latitude;
                jdv.lng = item.Longitiude;
                jdv.name = item.UnionName;
                jdv.union_id = Convert.ToString(item.UnionId);
                list.Add(jdv);

            }

            //  var res = new JDIVISION {name=rm.DivisionInfo.First().DivisionName,lang=rm.DivisionInfo.First().Longitude,lat=rm.DivisionInfo.First().Latitude};

            return Json(list);
        }



        [HttpPost]
        public virtual JsonResult Jupazillas()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            rm.UpazillaInfo = rpt.Upazillainfo();

            //var customers =for c in dmisdb.Divisions  select new { text = c., value = c.CustomerId};


            List<JUPAZILLA> list = new List<JUPAZILLA>();
            
           
            foreach (var item in rm.UpazillaInfo)
            {
                JUPAZILLA jdv = new JUPAZILLA();
                jdv.lat = item.Latitude;
                jdv.lng = item.Longitiude;
                jdv.name = item.UpazillaName;
                jdv.upazilla_id = Convert.ToString(item.UpazillaId);
                list.Add(jdv);

            }

            //  var res = new JDIVISION {name=rm.DivisionInfo.First().DivisionName,lang=rm.DivisionInfo.First().Longitude,lat=rm.DivisionInfo.First().Latitude};

            return Json(list);
        }
         [HttpPost]
        public virtual JsonResult Schoolsafeinfo(string id)
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

            rm.SafeInformationInfo=rpt.SafeLearninginfoByIns(id);

            List<SchoolsafeInfo> list = new List<SchoolsafeInfo>();


            foreach (var item in rm.SafeInformationInfo)
            {
                SchoolsafeInfo jdv = new SchoolsafeInfo();

                jdv.drink_source = item.OwnDrinkingSource;
                jdv.func_toilet = Convert.ToString(item.LatrineFacilityFunctional);
                jdv.kacca_bulid =Convert.ToString(item.TotalKaccaBuildingStructer);
                jdv.pacca_bulid =Convert.ToString( item.TotalPaccaBuildingStructer);
                jdv.kacca_class = item.TotalKaccaClassRoom;
                jdv.pacca_class = item.TotalPaccaClassRoom;
                list.Add(jdv);

            }

            //  var res = new JDIVISION {name=rm.DivisionInfo.First().DivisionName,lang=rm.DivisionInfo.First().Longitude,lat=rm.DivisionInfo.First().Latitude};

            return Json(list);

        }



         [HttpPost]
         public virtual JsonResult SchoolDominfo(string id)
         {
             Reportrepository rpt = new Reportrepository();

             ReportModel rm = new ReportModel();

             rm.SafeInformationInfo = rpt.SafeLearninginfoByIns(id);

             List<SchoolsafeInfo> list = new List<SchoolsafeInfo>();


             foreach (var item in rm.SafeInformationInfo)
             {
                 SchoolsafeInfo jdv = new SchoolsafeInfo();

                 jdv.drink_source = item.OwnDrinkingSource;
                 jdv.func_toilet = Convert.ToString(item.LatrineFacilityFunctional);
                 jdv.kacca_bulid = Convert.ToString(item.TotalKaccaBuildingStructer);
                 jdv.pacca_bulid = Convert.ToString(item.TotalPaccaBuildingStructer);
                 jdv.kacca_class = item.TotalKaccaClassRoom;
                 jdv.pacca_class = item.TotalPaccaClassRoom;
                 list.Add(jdv);

             }

             //  var res = new JDIVISION {name=rm.DivisionInfo.First().DivisionName,lang=rm.DivisionInfo.First().Longitude,lat=rm.DivisionInfo.First().Latitude};

             return Json(list);

         }


         [HttpPost]
         public virtual JsonResult SchoolRiskinfo(string id)
         {
             Reportrepository rpt = new Reportrepository();

             ReportModel rm = new ReportModel();

             rm.RiskReductionInformationInfo = rpt.RiskReductionResilienceEducationInfoByIns(id);

             List<SchoolRiskInfo> list = new List<SchoolRiskInfo>();


             foreach (var item in rm.RiskReductionInformationInfo)
             {
                 SchoolRiskInfo jdv = new SchoolRiskInfo();

                 jdv.drrTran = item.DisasterRelatedTraining;
                 
                // jdv.func_toilet = Convert.ToString(item.LatrineFacilityFunctional);
                
                 list.Add(jdv);

             }

             //  var res = new JDIVISION {name=rm.DivisionInfo.First().DivisionName,lang=rm.DivisionInfo.First().Longitude,lat=rm.DivisionInfo.First().Latitude};

             return Json(list);

         }



         [HttpPost]
         public virtual JsonResult SchoolDomInfo(string id)
         {
             Reportrepository rpt = new Reportrepository();

             ReportModel rm = new ReportModel();

             rm.DisasterManagementInformationInfo= rpt.DomainDisasterManagementinfoByIns(id);

             List<SchoolDomInfo> list = new List<SchoolDomInfo>();




             foreach (var item in rm.DisasterManagementInformationInfo)
             {
                 SchoolDomInfo jdv = new SchoolDomInfo();

                 //jdv.totalaffected=item.DisasterRelatedTraining
                 // jdv.func_toilet = Convert.ToString(item.LatrineFacilityFunctional);

                 list.Add(jdv);

             }

             //  var res = new JDIVISION {name=rm.DivisionInfo.First().DivisionName,lang=rm.DivisionInfo.First().Longitude,lat=rm.DivisionInfo.First().Latitude};

             return Json(list);

         }

        [HttpPost]
        public virtual JsonResult Jnionschool(string id)
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();
            UsersModel userMo = new UsersModel();

            var uinfo = userMo.GetInfoByName(User.Identity.Name);

            if (User.IsInRole("Entry Operator"))
            {
                rm.GeneralInformationInfo = rpt.GetGeneralInformationsByUserUnion(int.Parse(id),uinfo.First().UserId);
                
            }
            else if (User.IsInRole("DPE"))
            {

                rm.GeneralInformationInfo = rpt.GetGeneralInformationsByAuthoUnion(int.Parse(id),"DPE");

            }
            else if (User.IsInRole("DSHE"))
            {


                rm.GeneralInformationInfo = rpt.GetGeneralInformationsByAuthoUnion(int.Parse(id), "DSHE");

            }
            else if (User.IsInRole("Mis User"))
            {
                rm.GeneralInformationInfo = rpt.GetGeneralInformationsByAuthoUnion(int.Parse(id), "Mis User");
            }
            else if (User.IsInRole("Vocational"))
            {

                rm.GeneralInformationInfo = rpt.GetGeneralInformationsByAuthoUnion(int.Parse(id), "Vocational");

            }
            else if (User.IsInRole("Madrasa"))
            {

                rm.GeneralInformationInfo = rpt.GetGeneralInformationsByAuthoUnion(int.Parse(id), "Madrasa");

            }
            else
            {
                rm.GeneralInformationInfo = rpt.GetGeneralInformationsByUnion(int.Parse(id));
            }
           
            //var customers =for c in dmisdb.Divisions  select new { text = c., value = c.CustomerId};


            List<JGenINFO> list = new List<JGenINFO>();


            foreach (var item in rm.GeneralInformationInfo)
            {
                JGenINFO jdv = new JGenINFO();
                jdv.lat = item.GpsLatitude;
                jdv.lng = item.GpsLongtitude;
                jdv.name = item.InstituteEng;
               
                list.Add(jdv);

            }

            //  var res = new JDIVISION {name=rm.DivisionInfo.First().DivisionName,lang=rm.DivisionInfo.First().Longitude,lat=rm.DivisionInfo.First().Latitude};

            return Json(list);
        }


        [HttpPost]
        public virtual JsonResult MapLoadByCommonfacilitiesType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

            int divisionId = 0;
            int unionId = 0;
            int upazillaId = 0;
            int districtId = 0;
            if (Request["unionId"] != "")
            {
                unionId = int.Parse(Request["unionId"]);
            }
            if (Request["districtId"] != "")
            {
                districtId = int.Parse(Request["districtId"]);
            }
            if (Request["upazillaId"] != "")
            {
                upazillaId = int.Parse(Request["upazillaId"]);
            }
            if (Request["divisionId"] != "")
            {
                divisionId = int.Parse(Request["divisionId"]);
            }
             string tiffin =Request["tiffin"];
            string playing = Request["playing"];
            string electricity = Request["electricity"];
            string internet = Request["internet"];
            string computer = Request["computer"];
            string firstaid = Request["firstaid"];
            string fireext = Request["fireext"];
            string volunteering = Request["Volunteering"];
            string areaType = Request["areaType"];


            rm.GeneralInformationInfo = rpt.GetGeneralInformationsByCommonFaciType(divisionId, districtId, upazillaId, unionId, tiffin, playing, areaType, electricity, internet, computer, firstaid, fireext, volunteering);

            int cou = rm.GeneralInformationInfo.Count;
            List<JGenINFO> list = new List<JGenINFO>();


            foreach (var item in rm.GeneralInformationInfo)
            {
                JGenINFO jdv = new JGenINFO();
                jdv.lat = item.GpsLatitude;
                jdv.lng = item.GpsLongtitude;
                jdv.InstituteId = item.InstituteId;
                jdv.InstituteType = item.InstituteType;
                jdv.InstituteMobile = item.InstituteMobile;
                jdv.InstitutePhone = item.InstitutePhone;
                jdv.StudentTotal = Convert.ToString(item.StudentMale + item.StudentFemale);
                jdv.TeacherTotal = Convert.ToString(item.TeacherMale+ item.TeacherFemale);
                jdv.GobEIIN = item.GobEIIN;
                jdv.GraphicalLocation = item.GraphicalLocation;
                jdv.EstabilishmentDate = item.EstabilishmentDate;
                jdv.DomainStatus = item.DomainStatus;
                jdv.KaccaBuilding =Convert.ToString( Convert.ToString(item.Domains.First().DomainSafeLearning.TotalKaccaBuildingStructer));
                jdv.PaccaBuilding = Convert.ToString(Convert.ToString(item.Domains.First().DomainSafeLearning.TotalPaccaBuildingStructer));
                jdv.PaccaClass = item.Domains.First().DomainSafeLearning.TotalPaccaClassRoom;
                jdv.KaccaClass = item.Domains.First().DomainSafeLearning.TotalKaccaClassRoom;
                jdv.FunctioningToilet = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
                jdv.OwnDrinking = item.Domains.First().DomainSafeLearning.OwnDrinkingSource;
                jdv.InstituteEng = item.InstituteEng;
                jdv.ManagementType = item.ManagementType;
                jdv.AuthorityType = item.AuthorityType;
                jdv.LatrineFacilityFunctional = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
           
                jdv.DomainId = Convert.ToString(item.Domains.First().DomainId);

                list.Add(jdv);

            }

            return Json(list);
        }

        [HttpPost]
        public virtual JsonResult MapLoadByDisabilitiessearchType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

            int divisionId = 0;
            int unionId = 0;
            int upazillaId = 0;
            int districtId = 0;
            if (Request["unionId"] != "")
            {
                unionId = int.Parse(Request["unionId"]);
            }
            if (Request["districtId"] != "")
            {
                districtId = int.Parse(Request["districtId"]);
            }
            if (Request["upazillaId"] != "")
            {
                upazillaId = int.Parse(Request["upazillaId"]);
            }
            if (Request["divisionId"] != "")
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            string Disability = Request["Disability"];


            rm.GeneralInformationInfo = rpt.GetGeneralInformationsByDisabilityType(divisionId, districtId, upazillaId, unionId, Disability);

            int cou = rm.GeneralInformationInfo.Count;
            List<JGenINFO> list = new List<JGenINFO>();


            foreach (var item in rm.GeneralInformationInfo)
            {
                JGenINFO jdv = new JGenINFO();
                jdv.lat = item.GpsLatitude;
                jdv.lng = item.GpsLongtitude;
                jdv.InstituteId = item.InstituteId;
                jdv.InstituteType = item.InstituteType;
                jdv.InstituteMobile = item.InstituteMobile;
                jdv.InstitutePhone = item.InstitutePhone;
                jdv.StudentTotal = Convert.ToString(item.StudentMale + item.StudentFemale);
                jdv.TeacherTotal = Convert.ToString(item.TeacherMale + item.TeacherFemale);
                jdv.GobEIIN = item.GobEIIN;
                jdv.GraphicalLocation = item.GraphicalLocation;
                jdv.EstabilishmentDate = item.EstabilishmentDate;
                jdv.DomainStatus = item.DomainStatus;
                jdv.KaccaBuilding = Convert.ToString(Convert.ToString(item.Domains.First().DomainSafeLearning.TotalKaccaBuildingStructer));
                jdv.PaccaBuilding = Convert.ToString(Convert.ToString(item.Domains.First().DomainSafeLearning.TotalPaccaBuildingStructer));
                jdv.PaccaClass = item.Domains.First().DomainSafeLearning.TotalPaccaClassRoom;
                jdv.KaccaClass = item.Domains.First().DomainSafeLearning.TotalKaccaClassRoom;
                jdv.FunctioningToilet = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
                jdv.OwnDrinking = item.Domains.First().DomainSafeLearning.OwnDrinkingSource;
                jdv.InstituteEng = item.InstituteEng;
                jdv.ManagementType = item.ManagementType;
                jdv.AuthorityType = item.AuthorityType;
                jdv.LatrineFacilityFunctional = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
           
                jdv.DomainId = Convert.ToString(item.Domains.First().DomainId);

                list.Add(jdv);

            }

            return Json(list);
        }

        [HttpPost]
        public virtual JsonResult MapLoadByDrinkingwatersearchType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

            int divisionId = 0;
            int unionId = 0;
            int upazillaId = 0;
            int districtId = 0;
            if (Request["unionId"] != "")
            {
                unionId = int.Parse(Request["unionId"]);
            }
            if (Request["districtId"] != "")
            {
                districtId = int.Parse(Request["districtId"]);
            }
            if (Request["upazillaId"] != "")
            {
                upazillaId = int.Parse(Request["upazillaId"]);
            }
            if (Request["divisionId"] != "")
            {
                divisionId = int.Parse(Request["divisionId"]);
            }
           string areaType = Request["areaType"];


              string geographicalType =Request["geographicalType"];
            string owndrink =Request["owndrink"];
            string watersafe =Request["watersafe"];
            string functional =Request["functional"];
            string arsenic =Request["arsenic"];
            string childfrnd =Request["childfrnd"];
            string accessdisabilities =Request["accessdisabilities"];
            string functioning = Request["functioning"];


            rm.GeneralInformationInfo = rpt.GetGeneralInformationsByDrinkingwatersearchType(divisionId, districtId, upazillaId, unionId, geographicalType, owndrink, areaType, watersafe, functional, arsenic, childfrnd, accessdisabilities, functioning);

            int cou = rm.GeneralInformationInfo.Count;
            List<JGenINFO> list = new List<JGenINFO>();


            foreach (var item in rm.GeneralInformationInfo)
            {
                JGenINFO jdv = new JGenINFO();
                jdv.lat = item.GpsLatitude;
                jdv.lng = item.GpsLongtitude;
                jdv.InstituteId = item.InstituteId;
                jdv.InstituteType = item.InstituteType;
                jdv.InstituteMobile = item.InstituteMobile;
                jdv.InstitutePhone = item.InstitutePhone;
                jdv.StudentTotal = Convert.ToString(item.StudentMale + item.StudentFemale);
                jdv.TeacherTotal = Convert.ToString(item.TeacherMale + item.TeacherFemale);
                jdv.GobEIIN = item.GobEIIN;
                jdv.GraphicalLocation = item.GraphicalLocation;
                jdv.EstabilishmentDate = item.EstabilishmentDate;
                jdv.DomainStatus = item.DomainStatus;
                jdv.KaccaBuilding = Convert.ToString(Convert.ToString(item.Domains.First().DomainSafeLearning.TotalKaccaBuildingStructer));
                jdv.PaccaBuilding = Convert.ToString(Convert.ToString(item.Domains.First().DomainSafeLearning.TotalPaccaBuildingStructer));
                jdv.PaccaClass = item.Domains.First().DomainSafeLearning.TotalPaccaClassRoom;
                jdv.KaccaClass = item.Domains.First().DomainSafeLearning.TotalKaccaClassRoom;
                jdv.FunctioningToilet = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
                jdv.OwnDrinking = item.Domains.First().DomainSafeLearning.OwnDrinkingSource;
                jdv.InstituteEng = item.InstituteEng;
                jdv.ManagementType = item.ManagementType;
                jdv.AuthorityType = item.AuthorityType;
                jdv.LatrineFacilityFunctional = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
           
                jdv.DomainId = Convert.ToString(item.Domains.First().DomainId);

                list.Add(jdv);

            }

            return Json(list);
        }

        [HttpPost]
        public virtual JsonResult MapLoadByContingencysearchType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

            int divisionId = 0;
            int unionId = 0;
            int upazillaId = 0;
            int districtId = 0;
            if (Request["unionId"] != "")
            {
                unionId = int.Parse(Request["unionId"]);
            }
            if (Request["districtId"] != "")
            {
                districtId = int.Parse(Request["districtId"]);
            }
            if (Request["upazillaId"] != "")
            {
                upazillaId = int.Parse(Request["upazillaId"]);
            }
            if (Request["divisionId"] != "")
            {
                divisionId = int.Parse(Request["divisionId"]);
            }
           string areaType = Request["areaType"];


           string Backup = Request["Backup"];
              string Contingency = Request["Contingency"];
              string Temporary = Request["Temporary"];
              string emergency = Request["emergency"];


              rm.GeneralInformationInfo = rpt.GetGeneralInformationsByContingencysearchType(divisionId, districtId, upazillaId, unionId, Backup, Contingency, areaType, Temporary, emergency);

            int cou = rm.GeneralInformationInfo.Count;
            List<JGenINFO> list = new List<JGenINFO>();


            foreach (var item in rm.GeneralInformationInfo)
            {
                JGenINFO jdv = new JGenINFO();
                jdv.lat = item.GpsLatitude;
                jdv.lng = item.GpsLongtitude;
                jdv.InstituteId = item.InstituteId;
                jdv.InstituteType = item.InstituteType;
                jdv.InstituteMobile = item.InstituteMobile;
                jdv.InstitutePhone = item.InstitutePhone;
                jdv.StudentTotal = Convert.ToString(item.StudentMale + item.StudentFemale);
                jdv.TeacherTotal = Convert.ToString(item.TeacherMale + item.TeacherFemale);
                jdv.GobEIIN = item.GobEIIN;
                jdv.GraphicalLocation = item.GraphicalLocation;
                jdv.EstabilishmentDate = item.EstabilishmentDate;
                jdv.DomainStatus = item.DomainStatus;
                jdv.KaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalKaccaBuildingStructer);
                jdv.PaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalPaccaBuildingStructer);
                jdv.PaccaClass = item.Domains.First().DomainSafeLearning.TotalPaccaClassRoom;
                jdv.KaccaClass = item.Domains.First().DomainSafeLearning.TotalKaccaClassRoom;
                jdv.FunctioningToilet = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
                jdv.OwnDrinking = item.Domains.First().DomainSafeLearning.OwnDrinkingSource;
                jdv.InstituteEng = item.InstituteEng;
                jdv.ManagementType = item.ManagementType;
                jdv.AuthorityType = item.AuthorityType;
                jdv.LatrineFacilityFunctional = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
           
                jdv.DomainId = Convert.ToString(item.Domains.First().DomainId);

                list.Add(jdv);

            }

            return Json(list);
        }


        [HttpPost]
        public virtual JsonResult MapLoadByResilienceEducationType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

            int divisionId = 0;
            int unionId = 0;
            int upazillaId = 0;
            int districtId = 0;
            if (Request["unionId"] != "")
            {
                unionId = int.Parse(Request["unionId"]);
            }
            if (Request["districtId"] != "")
            {
                districtId = int.Parse(Request["districtId"]);
            }
            if (Request["upazillaId"] != "")
            {
                upazillaId = int.Parse(Request["upazillaId"]);
            }
            if (Request["divisionId"] != "")
            {
                divisionId = int.Parse(Request["divisionId"]);
            }
            string areaType = Request["areaType"];


          
            string DRR = Request["DRR"];
            string drills = Request["drills"];
            string understanding = Request["understanding"];


            rm.GeneralInformationInfo = rpt.GetGeneralInformationsByResilienceEducationType(divisionId, districtId, upazillaId, unionId, DRR, drills, areaType, understanding);

            int cou = rm.GeneralInformationInfo.Count;
            List<JGenINFO> list = new List<JGenINFO>();


            foreach (var item in rm.GeneralInformationInfo)
            {
                JGenINFO jdv = new JGenINFO();
                jdv.lat = item.GpsLatitude;
                jdv.lng = item.GpsLongtitude;
                jdv.InstituteId = item.InstituteId;
                jdv.InstituteType = item.InstituteType;
                jdv.InstituteMobile = item.InstituteMobile;
                jdv.InstitutePhone = item.InstitutePhone;
                jdv.StudentTotal = Convert.ToString(item.StudentMale + item.StudentFemale);
                jdv.TeacherTotal = Convert.ToString(item.TeacherMale + item.TeacherFemale);
                jdv.GobEIIN = item.GobEIIN;
                jdv.GraphicalLocation = item.GraphicalLocation;
                jdv.EstabilishmentDate = item.EstabilishmentDate;
                jdv.DomainStatus = item.DomainStatus;
                jdv.KaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalKaccaBuildingStructer);
                jdv.PaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalPaccaBuildingStructer);
                jdv.PaccaClass = item.Domains.First().DomainSafeLearning.TotalPaccaClassRoom;
                jdv.KaccaClass = item.Domains.First().DomainSafeLearning.TotalKaccaClassRoom;
                jdv.FunctioningToilet = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
                jdv.OwnDrinking = item.Domains.First().DomainSafeLearning.OwnDrinkingSource;
                jdv.InstituteEng = item.InstituteEng;
                jdv.ManagementType = item.ManagementType;
                jdv.AuthorityType = item.AuthorityType;
                jdv.LatrineFacilityFunctional = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
           
                jdv.DomainId = Convert.ToString(item.Domains.First().DomainId);

                list.Add(jdv);

            }

            return Json(list);
        }



        [HttpPost]
        public virtual JsonResult MapLoadByStrcType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

           

            int divisionId = 0;
            int unionId = 0;
            int upazillaId = 0;
            int districtId = 0;
            if (Request["unionId"] != "")
            {
                unionId = int.Parse(Request["unionId"]);
            }
            if (Request["districtId"] != "")
            {
                districtId = int.Parse(Request["districtId"]);
            }
            if (Request["upazillaId"] != "")
            {
                upazillaId = int.Parse(Request["upazillaId"]);
            }
            if (Request["divisionId"] != "")
            {
                divisionId = int.Parse(Request["divisionId"]);
            }

            string areaType = Request["areaType"];
            string geographicalType = Request["geographicalType"];
            string StructuralType = Request["StructuralType"];
            string ClassRoomType = Request["ClassRoomType"];


            rm.GeneralInformationInfo = rpt.GetGeneralInformationsByStrcType(divisionId, districtId, upazillaId, unionId, geographicalType, StructuralType, areaType,ClassRoomType);

            int cou = rm.GeneralInformationInfo.Count;
            List<JGenINFO> list = new List<JGenINFO>();


            foreach (var item in rm.GeneralInformationInfo)
            {
                JGenINFO jdv = new JGenINFO();
                jdv.lat = item.GpsLatitude;
                jdv.lng = item.GpsLongtitude;
                jdv.InstituteId = item.InstituteId;
                jdv.InstituteType = item.InstituteType;
                jdv.InstituteMobile = item.InstituteMobile;
                jdv.InstitutePhone = item.InstitutePhone;
                jdv.StudentTotal = Convert.ToString(item.StudentMale + item.StudentFemale);
                jdv.TeacherTotal = Convert.ToString(item.TeacherMale + item.TeacherFemale);
                jdv.GobEIIN = item.GobEIIN;
                jdv.GraphicalLocation = item.GraphicalLocation;
                jdv.EstabilishmentDate = item.EstabilishmentDate;
                jdv.DomainStatus = item.DomainStatus;
                jdv.KaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalKaccaBuildingStructer);
                jdv.PaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalPaccaBuildingStructer);
                jdv.PaccaClass = item.Domains.First().DomainSafeLearning.TotalPaccaClassRoom;
                jdv.KaccaClass = item.Domains.First().DomainSafeLearning.TotalKaccaClassRoom;
                jdv.FunctioningToilet = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
                jdv.OwnDrinking = item.Domains.First().DomainSafeLearning.OwnDrinkingSource;
                jdv.InstituteEng = item.InstituteEng;
                jdv.ManagementType = item.ManagementType;
                jdv.AuthorityType = item.AuthorityType;
                jdv.LatrineFacilityFunctional = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
           
                jdv.DomainId = Convert.ToString(item.Domains.First().DomainId);

                list.Add(jdv);

            }

            return Json(list);
        }



        [HttpPost]
        public virtual JsonResult MapLoadByGeoType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

            string areaType = Request["areaType"];

            int divisionId = 0;
            int unionId = 0;
            int upazillaId = 0;
            int districtId = 0;
            if (Request["unionId"] != "")
            {
                unionId = int.Parse(Request["unionId"]);
            }
            if (Request["districtId"] != "")
            {
                districtId = int.Parse(Request["districtId"]);
            }
            if (Request["upazillaId"] != "")
            {
                upazillaId = int.Parse(Request["upazillaId"]);
            }
            if (Request["divisionId"] != "")
            {
                divisionId = int.Parse(Request["divisionId"]);
            }


            string geographicalType = Request["geographicalType"];
            string EmbankmentType = Request["EmbankmentType"];

            rm.GeneralInformationInfo = rpt.GetGeneralInformationsByGeoType(divisionId, districtId, upazillaId, unionId, geographicalType, EmbankmentType, areaType);

            int cou = rm.GeneralInformationInfo.Count;
            List<JGenINFO> list = new List<JGenINFO>();


            foreach (var item in rm.GeneralInformationInfo)
            {
                JGenINFO jdv = new JGenINFO();
                jdv.lat = item.GpsLatitude;
                jdv.lng = item.GpsLongtitude;
                jdv.InstituteId = item.InstituteId;
                jdv.InstituteType = item.InstituteType;
                jdv.InstituteMobile = item.InstituteMobile;
                jdv.InstitutePhone = item.InstitutePhone;
                jdv.StudentTotal = Convert.ToString(item.StudentMale + item.StudentFemale);
                jdv.TeacherTotal = Convert.ToString(item.TeacherMale + item.TeacherFemale);
                jdv.GobEIIN = item.GobEIIN;
                jdv.GraphicalLocation = item.GraphicalLocation;
                jdv.EstabilishmentDate = item.EstabilishmentDate;
                jdv.DomainStatus = item.DomainStatus;
                jdv.KaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalKaccaBuildingStructer);
                jdv.PaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalPaccaBuildingStructer);
                jdv.PaccaClass = item.Domains.First().DomainSafeLearning.TotalPaccaClassRoom;
                jdv.KaccaClass = item.Domains.First().DomainSafeLearning.TotalKaccaClassRoom;
                jdv.FunctioningToilet = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
                jdv.OwnDrinking = item.Domains.First().DomainSafeLearning.OwnDrinkingSource;
                jdv.InstituteEng = item.InstituteEng;
                jdv.ManagementType = item.ManagementType;
                jdv.AuthorityType = item.AuthorityType;
                jdv.LatrineFacilityFunctional = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
           
                jdv.DomainId = Convert.ToString(item.Domains.First().DomainId);

                list.Add(jdv);

            }

            return Json(list);
        }

         [HttpPost]
        public virtual JsonResult MapLoadByType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

             string    InstituteType= Request["InstituteType"];

           int  divisionId=0;
           int unionId = 0;
           int upazillaId = 0;
           int districtId = 0;
           if (Request["unionId"] != "")
           {
               unionId = int.Parse(Request["unionId"]);
           }
           if (Request["districtId"] != "")
           {
               districtId = int.Parse(Request["districtId"]);
           }
           if (Request["upazillaId"] != "")
           {
               upazillaId = int.Parse(Request["upazillaId"]);
           }
           if (Request["divisionId"] != "")
           {
               divisionId = int.Parse(Request["divisionId"]);
           }
         
       
            string ManagementType=Request["ManagementType"];
            string authorityType = Request["authorityType"];

            rm.GeneralInformationInfo = rpt.GetGeneralInformationsByType(divisionId, districtId, upazillaId, unionId, ManagementType, InstituteType, authorityType);

            int cou = rm.GeneralInformationInfo.Count;
            List<JGenINFO> list = new List<JGenINFO>();


            foreach (var item in rm.GeneralInformationInfo)
            {
                JGenINFO jdv = new JGenINFO();
                jdv.lat = item.GpsLatitude;
                jdv.lng = item.GpsLongtitude;
                jdv.InstituteId = item.InstituteId;
                jdv.InstituteType = item.InstituteType;
                jdv.InstituteMobile = item.InstituteMobile;
                jdv.InstitutePhone = item.InstitutePhone;
                jdv.StudentTotal = Convert.ToString(item.StudentMale + item.StudentFemale);
                jdv.TeacherTotal = Convert.ToString(item.TeacherMale + item.TeacherFemale);
                jdv.GobEIIN = item.GobEIIN;
                jdv.GraphicalLocation = item.GraphicalLocation;
                jdv.EstabilishmentDate = item.EstabilishmentDate;
                jdv.DomainStatus = item.DomainStatus;
                jdv.KaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalKaccaBuildingStructer);
                jdv.PaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalPaccaBuildingStructer);
                jdv.PaccaClass = item.Domains.First().DomainSafeLearning.TotalPaccaClassRoom;
                jdv.KaccaClass = item.Domains.First().DomainSafeLearning.TotalKaccaClassRoom;
                jdv.FunctioningToilet = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
                jdv.OwnDrinking = item.Domains.First().DomainSafeLearning.OwnDrinkingSource;
                jdv.InstituteEng = item.InstituteEng;
                jdv.ManagementType = item.ManagementType;
                jdv.AuthorityType = item.AuthorityType;
                jdv.LatrineFacilityFunctional = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
           
                jdv.DomainId = Convert.ToString(item.Domains.First().DomainId);

                list.Add(jdv);

            }

            return Json(list);

        }
         [HttpPost]
        public virtual JsonResult MapLoadByName()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

            string schoolname = Request["schoolname"];
            rm.GeneralInformationInfo = rpt.GetGeneralInformationsByName(schoolname);
  
            List<JGenINFO> list = new List<JGenINFO>();


            foreach (var item in rm.GeneralInformationInfo)
            {
                JGenINFO jdv = new JGenINFO();
                jdv.lat = item.GpsLatitude;
                jdv.lng = item.GpsLongtitude;
                jdv.InstituteId = item.InstituteId;
                jdv.InstituteType = item.InstituteType;
                jdv.InstituteMobile = item.InstituteMobile;
                jdv.InstitutePhone = item.InstitutePhone;
                jdv.StudentTotal = Convert.ToString(item.StudentMale + item.StudentFemale);
                jdv.TeacherTotal = Convert.ToString(item.TeacherMale + item.TeacherFemale);
                jdv.GobEIIN = item.GobEIIN;
                jdv.GraphicalLocation = item.GraphicalLocation;
                jdv.EstabilishmentDate = item.EstabilishmentDate;
                jdv.DomainStatus = item.DomainStatus;
                jdv.KaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalKaccaBuildingStructer);
                jdv.PaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalPaccaBuildingStructer);
                jdv.PaccaClass = item.Domains.First().DomainSafeLearning.TotalPaccaClassRoom;
                jdv.KaccaClass = item.Domains.First().DomainSafeLearning.TotalKaccaClassRoom;
                jdv.FunctioningToilet = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
                jdv.OwnDrinking = item.Domains.First().DomainSafeLearning.OwnDrinkingSource;
                jdv.InstituteEng = item.InstituteEng;
                jdv.DomainId = Convert.ToString(item.Domains.First().DomainId);
                jdv.ManagementType = item.ManagementType;
                jdv.AuthorityType = item.AuthorityType;
                jdv.LatrineFacilityFunctional = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
           

                list.Add(jdv);

            }

            return Json(list);
        }

        [HttpPost]
        public virtual JsonResult MapLoadByIdentity(){
             Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

            string Id = Request["id"];
            string type = Request["type"];

            if (type == "cluster")
            {
                rm.GeneralInformationInfo = rpt.GetGeneralInformationsByClusterId(Id);
            }
            if (type == "eiin")
            {
                rm.GeneralInformationInfo = rpt.GetGeneralInformationsByEIIN(Id);
            }
            //var customers =for c in dmisdb.Divisions  select new { text = c., value = c.CustomerId};


            List<JGenINFO> list = new List<JGenINFO>();

           
            foreach (var item in rm.GeneralInformationInfo)
            {
                JGenINFO jdv = new JGenINFO();
            jdv.lat = item.GpsLatitude;
            jdv.lng =item.GpsLongtitude;
            jdv.InstituteId = item.InstituteId;
            jdv.InstituteType = item.InstituteType;
            jdv.InstituteMobile = item.InstituteMobile;
            jdv.InstitutePhone = item.InstitutePhone;
            jdv.StudentTotal = Convert.ToString(item.StudentMale + item.StudentFemale);
            jdv.TeacherTotal =Convert.ToString(item.TeacherMale + item.TeacherFemale);
            jdv.GobEIIN = item.GobEIIN;
            jdv.GraphicalLocation = item.GraphicalLocation;
            jdv.EstabilishmentDate = item.EstabilishmentDate;
            jdv.DomainStatus = item.DomainStatus;
                jdv.KaccaBuilding=Convert.ToString(item.Domains.First().DomainSafeLearning.TotalKaccaBuildingStructer);
                jdv.PaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalPaccaBuildingStructer);
                jdv.PaccaClass = item.Domains.First().DomainSafeLearning.TotalPaccaClassRoom;
                jdv.KaccaClass = item.Domains.First().DomainSafeLearning.TotalKaccaClassRoom;
                jdv.FunctioningToilet = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
                jdv.OwnDrinking = item.Domains.First().DomainSafeLearning.OwnDrinkingSource;
                jdv.InstituteEng = item.InstituteEng;
                jdv.ManagementType = item.ManagementType;
                jdv.AuthorityType = item.AuthorityType;
                jdv.LatrineFacilityFunctional =Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
                jdv.DomainId = Convert.ToString(item.Domains.First().DomainId);



            //jdv.name =item.DivisionName;
            //jdv.divsion_id =Convert.ToString(item.DivisionId);
            list.Add(jdv);
                
            }

          //  var res = new JDIVISION {name=rm.DivisionInfo.First().DivisionName,lang=rm.DivisionInfo.First().Longitude,lat=rm.DivisionInfo.First().Latitude};

            return Json(list);
        }

        [HttpPost]
        public virtual JsonResult MapLoadByRiskType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

            string areaType = Request["areaType"];

            int divisionId = 0;
            int unionId = 0;
            int upazillaId = 0;
            int districtId = 0;
            if (Request["unionId"] != "")
            {
                unionId = int.Parse(Request["unionId"]);
            }
            if (Request["districtId"] != "")
            {
                districtId = int.Parse(Request["districtId"]);
            }
            if (Request["upazillaId"] != "")
            {
                upazillaId = int.Parse(Request["upazillaId"]);
            }
            if (Request["divisionId"] != "")
            {
                divisionId = int.Parse(Request["divisionId"]);
            }


            
            string Flood = Request["Flood"];
            string Cyclone = Request["Cyclone"];
            string EarthQuack = Request["EarthQuack"];
            string RiverBankErosion = Request["RiverBankErosion"];
            string TidalSurge = Request["TidalSurge"];
            string WaterLogging = Request["WaterLogging"];
            string Strom = Request["Strom"];
            string SeasonalStrom = Request["SeasonalStrom"];
            string Tsunami = Request["Tsunami"];
            string BuildingCollapse = Request["BuildingCollapse"];
            string FlashFlood = Request["FlashFlood"];
            string Salinity = Request["Salinity"];
            string Fire = Request["Fire"];
            string LandSlide = Request["LandSlide"];
            string All = Request["All"];

            rm.CheckDtoInfo = rpt.GetGeneralInformationsByRiskType(divisionId, districtId, upazillaId, unionId, All, 
                Flood,Cyclone,EarthQuack,RiverBankErosion,TidalSurge,WaterLogging,Strom,SeasonalStrom,Tsunami,BuildingCollapse,FlashFlood,Salinity,Fire,LandSlide);

            List<JGenINFO> list = new List<JGenINFO>();


            foreach (var item in rm.CheckDtoInfo)
            {
                JGenINFO jdv = new JGenINFO();

                jdv.InstituteId = item.InstituteId;
                jdv.InstituteType = item.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.InstituteType;
                jdv.InstituteMobile = item.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.InstituteMobile;
                jdv.InstitutePhone = item.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.InstitutePhone;
                jdv.GobEIIN = item.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.GobEIIN;
                jdv.GraphicalLocation = item.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.GraphicalLocation;
                jdv.EstabilishmentDate = item.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.EstabilishmentDate;
                jdv.DomainStatus = item.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.DomainStatus;
                jdv.InstituteEng = item.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.InstituteEng;
                jdv.ManagementType = item.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.ManagementType;
                jdv.AuthorityType = item.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.AuthorityType;
                jdv.DomainId =Convert.ToString(item.DomainDisasterManagement.Domains.FirstOrDefault().DomainId);

                list.Add(jdv);

            }

            return Json(list);
        }


        [HttpPost]
        public virtual JsonResult MapLoadBySanitType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

            string areaType = Request["areaType"];

            int divisionId = 0;
            int unionId = 0;
            int upazillaId = 0;
            int districtId = 0;
            if (Request["unionId"] != "")
            {
                unionId = int.Parse(Request["unionId"]);
            }
            if (Request["districtId"] != "")
            {
                districtId = int.Parse(Request["districtId"]);
            }
            if (Request["upazillaId"] != "")
            {
                upazillaId = int.Parse(Request["upazillaId"]);
            }
            if (Request["divisionId"] != "")
            {
                divisionId = int.Parse(Request["divisionId"]);
            }



            string geographicalType = Request["geographicalType"];

            string regular = Request["regular"];
            string functions = Request["function"];
            string common = Request["common"];
            string overall = Request["overall"];
            string childfrnd = Request["childfrnd"];

            string accessdisabilities = Request["accessdisabilities"];
            

            rm.GeneralInformationInfo = rpt.GetGeneralInformationsBySanitType(divisionId, districtId, upazillaId, unionId, regular,functions, common, childfrnd,overall,accessdisabilities,geographicalType,areaType);

            List<JGenINFO> list = new List<JGenINFO>();


            foreach (var item in rm.GeneralInformationInfo)
            {
                JGenINFO jdv = new JGenINFO();
                jdv.InstituteId = item.InstituteId;
                jdv.InstituteType = item.InstituteType;
                jdv.InstituteMobile = item.InstituteMobile;
                jdv.InstitutePhone = item.InstitutePhone;
                jdv.StudentTotal = Convert.ToString(item.StudentMale + item.StudentFemale);
                jdv.TeacherTotal = Convert.ToString(item.TeacherMale + item.TeacherFemale);
                jdv.GobEIIN = item.GobEIIN;
                jdv.GraphicalLocation = item.GraphicalLocation;
                jdv.EstabilishmentDate = item.EstabilishmentDate;
                jdv.DomainStatus = item.DomainStatus;
                jdv.KaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalKaccaBuildingStructer);
                jdv.PaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalPaccaBuildingStructer);
                jdv.PaccaClass = item.Domains.First().DomainSafeLearning.TotalPaccaClassRoom;
                jdv.KaccaClass = item.Domains.First().DomainSafeLearning.TotalKaccaClassRoom;
                jdv.FunctioningToilet = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
                jdv.OwnDrinking = item.Domains.First().DomainSafeLearning.OwnDrinkingSource;
                jdv.InstituteEng = item.InstituteEng;
                jdv.ManagementType = item.ManagementType;
                jdv.AuthorityType = item.AuthorityType;
                jdv.LatrineFacilityFunctional = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
           
                jdv.DomainId = Convert.ToString(item.Domains.First().DomainId);
                

                list.Add(jdv);

            }

            return Json(list);
        }


        [HttpPost]
        public virtual JsonResult MapLoadByVacentType()
        {
            Reportrepository rpt = new Reportrepository();

            ReportModel rm = new ReportModel();

            string areaType = Request["areaType"];

            int divisionId = 0;
            int unionId = 0;
            int upazillaId = 0;
            int districtId = 0;
            if (Request["unionId"] != "")
            {
                unionId = int.Parse(Request["unionId"]);
            }
            if (Request["districtId"] != "")
            {
                districtId = int.Parse(Request["districtId"]);
            }
            if (Request["upazillaId"] != "")
            {
                upazillaId = int.Parse(Request["upazillaId"]);
            }
            if (Request["divisionId"] != "")
            {
                divisionId = int.Parse(Request["divisionId"]);
            }



            string geographicalType = Request["geographicalType"];

            

            rm.GeneralInformationInfo = rpt.GetGeneralInformationsByVacentType(divisionId, districtId, upazillaId, unionId, geographicalType, areaType);

            List<JGenINFO> list = new List<JGenINFO>();


            foreach (var item in rm.GeneralInformationInfo)
            {
                JGenINFO jdv = new JGenINFO();
                jdv.InstituteId = item.InstituteId;
                jdv.InstituteType = item.InstituteType;
                jdv.InstituteMobile = item.InstituteMobile;
                jdv.InstitutePhone = item.InstitutePhone;
                jdv.StudentTotal = Convert.ToString(item.StudentMale + item.StudentFemale);
                jdv.TeacherTotal = Convert.ToString(item.TeacherMale + item.TeacherFemale);
                jdv.GobEIIN = item.GobEIIN;
                jdv.GraphicalLocation = item.GraphicalLocation;
                jdv.EstabilishmentDate = item.EstabilishmentDate;
                jdv.DomainStatus = item.DomainStatus;
                jdv.KaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalKaccaBuildingStructer);
                jdv.PaccaBuilding = Convert.ToString(item.Domains.First().DomainSafeLearning.TotalPaccaBuildingStructer);
                jdv.PaccaClass = item.Domains.First().DomainSafeLearning.TotalPaccaClassRoom;
                jdv.KaccaClass = item.Domains.First().DomainSafeLearning.TotalKaccaClassRoom;
                jdv.FunctioningToilet = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
                jdv.OwnDrinking = item.Domains.First().DomainSafeLearning.OwnDrinkingSource;
                jdv.InstituteEng = item.InstituteEng;
                jdv.ManagementType = item.ManagementType;
                jdv.AuthorityType = item.AuthorityType;
                jdv.LatrineFacilityFunctional = Convert.ToString(item.Domains.First().DomainSafeLearning.LatrineFacilityFunctional);
           
                jdv.DomainId = Convert.ToString(item.Domains.First().DomainId);


                list.Add(jdv);

            }

            return Json(list);
        }


    }
     
   
   

    public class JGenINFO
    {
        public string name { get; set; }

        public string lat { get; set; }
        public string lng { get; set; }

        public string KaccaBuilding { get; set; }
        public string PaccaBuilding { get; set; }

        public string KaccaClass { get; set; }
        public string PaccaClass { get; set; }

        public string FunctioningToilet { get; set; }
        public string OwnDrinking { get; set; }

        public int GeneralInformationId { get; set; }
        public string DomainId { get; set; }
        public string InstituteId { get; set; }
        public string GobEIIN { get; set; }
        public string InstituteType { get; set; }
        public string EstabilishmentDate { get; set; }
        public string ManagementType { get; set; }
        public string InstituteEng { get; set; }
        public string InstituteBng { get; set; }
        public string InstituteAddress { get; set; }
        public string HowManyShift { get; set; }
        public string StudentMale { get; set; }
        public string StudentFemale { get; set; }
        public string StudentEthnicMale { get; set; }
        public string StudentDisableMale { get; set; }
        public string StudentDisableFeMale { get; set; }
        public string StudentEthnicFeMale { get; set; }
        public string InstitutePhone { get; set; }
        public string InstituteFax { get; set; }
        public string InstituteMobile { get; set; }
        public string InstituteWebUrl { get; set; }
        public string BankACNo { get; set; }
        public string BankName { get; set; }
        public string BankContact { get; set; }
        public string BankAddress { get; set; }
        public string GpsLongtitude { get; set; }
        public string GpsLatitude { get; set; }
        public string SchoolArea { get; set; }
        public string GraphicalLocation { get; set; }
        public string TransportionMode { get; set; }
        public string SmcExpiredate { get; set; }
        public string SmcPrisedentName { get; set; }
        public string SmcPrisedentMobile { get; set; }
        public string TeacherMale { get; set; }
        public string TeacherTotal { get; set; }
        public string StudentTotal { get; set; }
        public string LatrineFacilityFunctional { get; set; }
        
        public string TeacherFemale { get; set; }
        public string VacantTeacherPosition { get; set; }
        public string NonEducationalStuff { get; set; }
        public string TiffinFacility { get; set; }
        public string PlayingGround { get; set; }
        public string ElectricityFacillity { get; set; }
        public string InternetFacility { get; set; }
        public string ComputerLab { get; set; }
        public string FirstAid { get; set; }
        public string HasFireExtinguiser { get; set; }
        public string FunctionalFireExtinguise { get; set; }
        public string VolunteerServices { get; set; }
        public string AuthorityType { get; set; }
        public string BankBranch { get; set; }
        public string Altitude { get; set; }
        public string InstitudeHouse { get; set; }
        public string InstitudeRoad { get; set; }
        public string InstitudeSection { get; set; }
        public string InstitudeVillage { get; set; }
        public string InstitudeDistric { get; set; }
        public string InstitudeDivision { get; set; }
        public string InstitudeUpazilla { get; set; }
        public Nullable<int> DomainStatus { get; set; }
        public Nullable<int> userId { get; set; }
        public string InstitudeUnion { get; set; }
        public Nullable<int> UnionId { get; set; }
        public Nullable<int> DivisionId { get; set; }
        public Nullable<int> UpazillaId { get; set; }
        public Nullable<int> DistrictId { get; set; }
        public string Embankment { get; set; }
    }


    public class SchoolsafeInfo
    {
        public string kacca_bulid{ get; set; }
        public string pacca_bulid { get; set; }
        public string kacca_class { get; set; }
        public string pacca_class { get; set; }
        public string drink_source { get; set; }
        public string func_toilet { get; set; }
    }
    public class SchoolDomInfo
    {
        public string usedasshelter { get; set; }
        public string totalaffected { get; set; }
    }
    public class SchoolRiskInfo
    {
        public string drrTran { get; set; }
        public string inCont { get; set; }
       
    }

    public class JDIVISION
    {
        public string name { get; set; }
        public string divsion_id { get; set; }

        public string lat { get; set; }
        public string lng { get; set; }
    }


    public class JUNION
    {
        public string name { get; set; }
        public string union_id { get; set; }

        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class JDISTRICT
    {
        public string name { get; set; }
        public string district_id { get; set; }

        public string lat { get; set; }
        public string lng { get; set; }
    }
    public class JUPAZILLA
    {
        public string name { get; set; }
        public string upazilla_id { get; set; }

        public string lat { get; set; }
        public string lng { get; set; }
    }

    #endregion


}
