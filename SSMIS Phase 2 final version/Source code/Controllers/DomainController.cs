﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DIMS.Models;
using PagedList;
using WebMatrix.WebData;
using System.Web.Security;


namespace DIMS.Controllers
{
    public class DomainController : Controller
    {
        //
        // GET: /Domain/
        Repository _repository = new Repository();
        DMISDBEntities db = new DMISDBEntities();
        ViewModels vm = new ViewModels();
        //var memberId = WebSecurity.GetUserId(User.Identity.Name);


        public ActionResult Index()
        {
            ViewModels vm = new ViewModels();

            UsersModel userMo = new UsersModel();

            vm.userInfo = userMo.GetInfoByName(User.Identity.Name);
            
           

            var role = Roles.GetRolesForUser(User.Identity.Name);

            int roleCount = role.Count();
            if (!User.Identity.IsAuthenticated||roleCount==0)
            {
                return RedirectToAction("index", "Home");

            }

            string roleName = role.First();
           // var memberId = WebSecurity.GetUserId(User.Identity.Name);
            //Response.Write();
            ViewBag.CRole = roleName;
            ViewBag.userId = vm.userInfo.First().UserId;
            ViewBag.Homfoot = "hom-foot";
            ViewBag.DashTab = "active";
            ViewBag.Title = "Operational Panel";

            return View("Dash",vm);
        }

        public ActionResult Datacollection(string sortOrder, string currentFilter, string searchString, int? page)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("index", "Home");

            }
            else
            {
                ViewModels vm = new ViewModels();
                //var memberId = WebSecurity.GetUserId(User.Identity.Name);


                UsersModel userMo = new UsersModel();
                
                var uinfo = userMo.GetInfoByName(User.Identity.Name);
                int uid=uinfo.First().UserId;
                vm.GeneralInformationInfo = _repository.GetGeneralInformationsUnsaved(uid);

                ViewBag.UnsavedGenCount = vm.GeneralInformationInfo.Count;

                
               // vm.DomainInfo = _repository.DomaininfoByUserId(uinfo.First().UserId);
              
                ViewBag.DatacollectionTab = "active";


                ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                ViewBag.CurrentFilter = searchString;

                var comdatas = from s in db.Domains
                               select s;

                if (User.IsInRole("Entry Operator"))
                {
                    comdatas = comdatas.Where(s => s.UserProfile.UserId==uid );
                }
                else if (User.IsInRole("DPE"))
                {

                    comdatas = comdatas.Where(s => s.DomainGeneralInformation.AuthorityType.Contains("DPE"));
              
                }
                else if (User.IsInRole("DSHE"))
                {


                    comdatas = comdatas.Where(s => s.DomainGeneralInformation.AuthorityType.Contains("DSHE"));
              
                }
                else if (User.IsInRole("BMEB"))
                {

                    comdatas = comdatas.Where(s => s.DomainGeneralInformation.AuthorityType.Contains("BMEB"));
              
                }
                else if (User.IsInRole("BNFE"))
                {

                    comdatas = comdatas.Where(s => s.DomainGeneralInformation.AuthorityType.Contains("BNFE"));
              
                }
                else if (User.IsInRole("BTEB"))
                {

                    comdatas = comdatas.Where(s => s.DomainGeneralInformation.AuthorityType.Contains("BTEB"));

                }
                else
                {
                    comdatas = from s in db.Domains
                                   select s;
                }


                if (comdatas.Count()>0)
                {

                    //int count = comdatas.Count();
                    if (!String.IsNullOrEmpty(searchString))
                    {

                        comdatas = comdatas.Where(s => s.DomainGeneralInformation.InstituteEng.ToUpper().Contains(searchString.ToUpper())
                                               || s.DomainGeneralInformation.InstituteEng.ToUpper().Contains(searchString.ToUpper()));
                    }
                    switch (sortOrder)
                    {
                        case "Name_desc":
                            comdatas = comdatas.OrderByDescending(s => s.DomainGeneralInformation.InstituteEng);
                            break;

                        default:
                            comdatas = comdatas.OrderBy(s => s.DomainGeneralInformation.InstituteEng);
                            break;
                    }



                    int pageSize = 20;
                    int pageNumber = (page ?? 1);
                    //if (comdatas != null)
                    //    vm.DomainInfo = comdatas.ToPagedList(pageNumber, pageSize).ToList();
                    ViewBag.Title = "Operational Panel : Pre-Disaster Information Management ";
                    ViewBag.Total = comdatas.Count();
                    return View("DataCollection", comdatas.ToPagedList(pageNumber, pageSize));
                }
                else
                {
                    int pageSize = 20;
                    int pageNumber = (page ?? 1);
                    //if (comdatas != null)
                    //    vm.DomainInfo = comdatas.ToPagedList(pageNumber, pageSize).ToList();
                    ViewBag.Title = "Operational Panel : Pre-Disaster Information Management ";
                    ViewBag.Total = comdatas.Count();
                    return View("DataCollection", comdatas.ToPagedList(pageNumber, pageSize));
                }
                //return View("DataCollection", vm);
               
            }
        }



       
        public ActionResult Details(int DomainId = 0)
        {


            if (User.Identity.Name == "")
            {
                return RedirectToAction("index", "Home");
            }
            else
            {
                
                ViewModels vm = new ViewModels();

                var DomainInfo = _repository.DomaininfoById(DomainId);

                if (DomainInfo.Count > 0)
                {
                    int? genId = DomainInfo.First().GeneralInformationId;

                    int? disId = DomainInfo.First().DisasterManagementId;
                    int? riksId = DomainInfo.First().RiskResilienceId;
                    int? safeId = DomainInfo.First().SafeLearningId;

                    vm.GeneralInformationInfo = _repository.GetGeneralInformationsById(genId);
                    vm.DisasterManagementInfo = _repository.GetDisasterInfoById(disId);
                    vm.RiskReductionResilienceEducationInfo = _repository.GetRiskReductionResilienceEducationInfoByid(riksId);
                    vm.SafeLearningInfo = _repository.GetSafeLearningInfoById(safeId);


                    int digmgm = vm.DisasterManagementInfo.First().DisasterManagementId;
                    vm.DisasterManagementInfoHistory1 = _repository.DomainHistoryinfoById(digmgm, "1");
                    vm.DisasterManagementInfoHistory2 = _repository.DomainHistoryinfoById(digmgm, "2");
                    vm.DisasterManagementInfoHistory3 = _repository.DomainHistoryinfoById(digmgm, "3");



                    UsersModel userMo = new UsersModel();

                    var uinfo = userMo.GetInfoByName(User.Identity.Name);
                    int? userId = uinfo.First().UserId;


                    if (vm.GeneralInformationInfo.Count > 0)
                        vm.CheckBoxInfo15_8 = _repository.GetCheckBoxGenralInfo("15.8", userId, vm.GeneralInformationInfo.First().GeneralInformationId);
                    if (vm.SafeLearningInfo.Count > 0)
                    {
                        vm.CheckBoxInfo17_1 = _repository.GetCheckBoxSafeLearningInfo("17.1", userId, vm.SafeLearningInfo.First().SafeLearningId);
                        vm.CheckBoxInfo19_0 = _repository.GetCheckBoxSafeLearningInfo("19.0", userId, vm.SafeLearningInfo.First().SafeLearningId);
                    }
                    if (vm.DisasterManagementInfo.Count > 0)
                    {
                        vm.CheckBoxInfo21_0 = _repository.GetCheckBoxDisasterManagementInfo("21.0.0", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                        vm.CheckBoxInfo21_1 = _repository.GetCheckBoxDisasterManagementInfo("21.1.1", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                        vm.CheckBoxInfo21_2 = _repository.GetCheckBoxDisasterManagementInfo("21.1.2", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                        vm.CheckBoxInfo21_3 = _repository.GetCheckBoxDisasterManagementInfo("21.1.3", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                    }

                    if (vm.RiskReductionResilienceEducationInfo.Count > 0)
                        vm.CheckBoxInfo23_3 = _repository.GetCheckBoxRiskResilienceInfo("23.3", userId, vm.RiskReductionResilienceEducationInfo.First().RiskResilienceId);

                    vm.DivisionInfo = _repository.Divisioninfo();
                    vm.DistrictInfo = _repository.Districtinfo();
                    vm.UpazillaInfo = _repository.Upazillainfo();
                    vm.UnionInfo = _repository.Unioninfo();
                    //   ViewBag.Title = "Pre-Disaster Information Management :Form View";
                    ViewBag.Title = "Operational Panel : Pre-Disaster Information Management : Details View";
                    return View("Details", vm);
                }
                else
                {
                    return RedirectToAction("datacollection", "domain");

                }

            }
        }


        public ActionResult View(int domainId = 0)
        {

            if (User.Identity.Name == "")
            {
                return RedirectToAction("index", "Home");
            }
            else
            {
                ViewModels vm = new ViewModels();

                var DomainInfo = _repository.DomaininfoById(domainId);

                if (DomainInfo.Count > 0)
                {
                    int? genId = DomainInfo.First().GeneralInformationId;

                    int? disId = DomainInfo.First().DisasterManagementId;
                    int? riksId = DomainInfo.First().RiskResilienceId;
                    int? safeId = DomainInfo.First().SafeLearningId;

                    vm.GeneralInformationInfo = _repository.GetGeneralInformationsById(genId);
                    vm.DisasterManagementInfo = _repository.GetDisasterInfoById(disId);
                    vm.RiskReductionResilienceEducationInfo = _repository.GetRiskReductionResilienceEducationInfoByid(riksId);
                    vm.SafeLearningInfo = _repository.GetSafeLearningInfoById(safeId);


                    int digmgm = vm.DisasterManagementInfo.First().DisasterManagementId;
                    vm.DisasterManagementInfoHistory1 = _repository.DomainHistoryinfoById(digmgm, "1");
                    vm.DisasterManagementInfoHistory2 = _repository.DomainHistoryinfoById(digmgm, "2");
                    vm.DisasterManagementInfoHistory3 = _repository.DomainHistoryinfoById(digmgm, "3");



                    UsersModel userMo = new UsersModel();

                    var uinfo = userMo.GetInfoByName(User.Identity.Name);
                    int? userId = uinfo.First().UserId;


                    if (vm.GeneralInformationInfo.Count > 0)
                        vm.CheckBoxInfo15_8 = _repository.GetCheckBoxGenralInfo("15.8", userId, vm.GeneralInformationInfo.First().GeneralInformationId);
                    if (vm.SafeLearningInfo.Count > 0)
                    {
                        vm.CheckBoxInfo17_1 = _repository.GetCheckBoxSafeLearningInfo("17.1", userId, vm.SafeLearningInfo.First().SafeLearningId);
                        vm.CheckBoxInfo19_0 = _repository.GetCheckBoxSafeLearningInfo("19.0", userId, vm.SafeLearningInfo.First().SafeLearningId);
                    }
                    if (vm.DisasterManagementInfo.Count > 0)
                    {
                        vm.CheckBoxInfo21_0 = _repository.GetCheckBoxDisasterManagementInfo("21.0.0", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                        vm.CheckBoxInfo21_1 = _repository.GetCheckBoxDisasterManagementInfo("21.1.1", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                        vm.CheckBoxInfo21_2 = _repository.GetCheckBoxDisasterManagementInfo("21.1.2", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                        vm.CheckBoxInfo21_3 = _repository.GetCheckBoxDisasterManagementInfo("21.1.3", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                    }

                    if (vm.RiskReductionResilienceEducationInfo.Count > 0)
                        vm.CheckBoxInfo23_3 = _repository.GetCheckBoxRiskResilienceInfo("23.3", userId, vm.RiskReductionResilienceEducationInfo.First().RiskResilienceId);

                    vm.DivisionInfo = _repository.Divisioninfo();
                    vm.DistrictInfo = _repository.Districtinfo();
                    vm.UpazillaInfo = _repository.Upazillainfo();
                    vm.UnionInfo = _repository.Unioninfo();
                 //   ViewBag.Title = "Pre-Disaster Information Management :Form View";
                    ViewBag.Title = "Operational Panel : Pre-Disaster Information Management : Form View";
                    return View("FormcontentView", vm);
                }
                else
                {
                    return RedirectToAction("datacollection", "domain");

                }

            }
        }


        public ActionResult Edit(int domainId = 0)
        {

            if (User.Identity.Name == "")
            {
                return RedirectToAction("index", "Home");
            }
            else
            {
                ViewModels vm = new ViewModels();

                var DomainInfo = _repository.DomaininfoById(domainId);

                if (DomainInfo.Count > 0)
                {
                    UsersModel userMo = new UsersModel();

                    var uinfo = userMo.GetInfoByName(User.Identity.Name);
                    int? userId = uinfo.First().UserId;

                    int? genId = DomainInfo.First().GeneralInformationId;

                    int? disId = DomainInfo.First().DisasterManagementId;
                    int? riksId = DomainInfo.First().RiskResilienceId;
                    int? safeId = DomainInfo.First().SafeLearningId;

                    vm.GeneralInformationInfo = _repository.GetGeneralInformationsById(genId);
                    vm.DisasterManagementInfo = _repository.GetDisasterInfoById(disId);
                    vm.RiskReductionResilienceEducationInfo = _repository.GetRiskReductionResilienceEducationInfoByid(riksId);
                    vm.SafeLearningInfo = _repository.GetSafeLearningInfoById(safeId);

                    vm.GeneralInformationInfoUnsv = _repository.GetGeneralInformationsUnsaved(uinfo.First().UserId);
                    vm.DisasterManagementInfoUnsv = _repository.GetDisasterInfoUnsaved(uinfo.First().UserId);
                    vm.RiskReductionResilienceEducationInfoUnsv = _repository.GetRiskReductionResilienceEducationInfoUnsaved(uinfo.First().UserId);
                    vm.SafeLearningInfoUnsv = _repository.GetSafeLearningInfoUnsaved(uinfo.First().UserId);


                    int digmgm = vm.DisasterManagementInfo.First().DisasterManagementId;
                    vm.DisasterManagementInfoHistory1 = _repository.DomainHistoryinfoById(digmgm, "1");
                    vm.DisasterManagementInfoHistory2 = _repository.DomainHistoryinfoById(digmgm, "2");
                    vm.DisasterManagementInfoHistory3 = _repository.DomainHistoryinfoById(digmgm, "3");



                  

                    if (vm.GeneralInformationInfo.Count > 0)
                        vm.CheckBoxInfo15_8 = _repository.GetCheckBoxGenralInfo("15.8", userId, vm.GeneralInformationInfo.First().GeneralInformationId);
                    if (vm.SafeLearningInfo.Count > 0)
                    {
                        vm.CheckBoxInfo17_1 = _repository.GetCheckBoxSafeLearningInfo("17.1", userId, vm.SafeLearningInfo.First().SafeLearningId);
                        vm.CheckBoxInfo19_0 = _repository.GetCheckBoxSafeLearningInfo("19.0", userId, vm.SafeLearningInfo.First().SafeLearningId);
                    }
                    if (vm.DisasterManagementInfo.Count > 0)
                    {
                        vm.CheckBoxInfo21_0 = _repository.GetCheckBoxDisasterManagementInfo("21.0.0", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                        vm.CheckBoxInfo21_1 = _repository.GetCheckBoxDisasterManagementInfo("21.1.1", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                        vm.CheckBoxInfo21_2 = _repository.GetCheckBoxDisasterManagementInfo("21.1.2", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                        vm.CheckBoxInfo21_3 = _repository.GetCheckBoxDisasterManagementInfo("21.1.3", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                    }

                    if (vm.RiskReductionResilienceEducationInfo.Count > 0)
                    {
                        vm.CheckBoxInfo23_3 = _repository.GetCheckBoxRiskResilienceInfo("23.3", userId, vm.RiskReductionResilienceEducationInfo.First().RiskResilienceId);
                        vm.CheckBoxInfo24_7 = _repository.GetCheckBoxRiskResilienceInfo("24.7", userId, vm.RiskReductionResilienceEducationInfo.First().RiskResilienceId);
                    }
                    vm.DivisionInfo = _repository.Divisioninfo();
                    vm.DistrictInfo = _repository.Districtinfo();
                    vm.UpazillaInfo = _repository.Upazillainfo();
                    vm.UnionInfo = _repository.Unioninfo();
                    ViewBag.Title = "Operational Panel : Pre-Disaster Information Management : Form Edit";
                    return View("FormcontentExists", vm);
                }
                else
                {
                    return RedirectToAction("datacollection", "domain");

                }

            }
        }



        public ActionResult Form()
        {

            ViewBag.ActiveForm = "active";
            if (Request.IsAuthenticated)
            {
                UsersModel userMo = new UsersModel();

                var uinfo = userMo.GetInfoByName(User.Identity.Name);
                ViewModels vm = new ViewModels();
                vm.GeneralInformationInfo = _repository.GetGeneralInformationsUnsaved(uinfo.First().UserId);
                vm.DisasterManagementInfo = _repository.GetDisasterInfoUnsaved(uinfo.First().UserId);
                vm.RiskReductionResilienceEducationInfo = _repository.GetRiskReductionResilienceEducationInfoUnsaved(uinfo.First().UserId);
                vm.SafeLearningInfo = _repository.GetSafeLearningInfoUnsaved(uinfo.First().UserId);

                vm.GeneralInformationInfoUnsv = _repository.GetGeneralInformationsUnsaved(uinfo.First().UserId);
                vm.DisasterManagementInfoUnsv = _repository.GetDisasterInfoUnsaved(uinfo.First().UserId);
                vm.RiskReductionResilienceEducationInfoUnsv = _repository.GetRiskReductionResilienceEducationInfoUnsaved(uinfo.First().UserId);
                vm.SafeLearningInfoUnsv = _repository.GetSafeLearningInfoUnsaved(uinfo.First().UserId);


                if (vm.DisasterManagementInfo.Count > 0)
                {

                    int digmgm = vm.DisasterManagementInfo.First().DisasterManagementId;
                    if (digmgm > 0)
                    {
                        vm.DisasterManagementInfoHistory1 = _repository.DomainHistoryinfoById(digmgm, "1");
                        vm.DisasterManagementInfoHistory2 = _repository.DomainHistoryinfoById(digmgm, "2");
                        vm.DisasterManagementInfoHistory3 = _repository.DomainHistoryinfoById(digmgm, "3");


                    }
                }

                int? userId = uinfo.First().UserId;

                if (vm.GeneralInformationInfo.Count > 0)
                    vm.CheckBoxInfo15_8 = _repository.GetCheckBoxGenralInfo("15.8", userId, vm.GeneralInformationInfo.First().GeneralInformationId);
                if (vm.SafeLearningInfo.Count > 0)
                {
                    vm.CheckBoxInfo17_1 = _repository.GetCheckBoxSafeLearningInfo("17.1", userId, vm.SafeLearningInfo.First().SafeLearningId);
                    vm.CheckBoxInfo19_0 = _repository.GetCheckBoxSafeLearningInfo("19.0", userId, vm.SafeLearningInfo.First().SafeLearningId);
                }
                if (vm.DisasterManagementInfo.Count > 0)
                {
                    vm.CheckBoxInfo21_0 = _repository.GetCheckBoxDisasterManagementInfo("21.0.0", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                    vm.CheckBoxInfo21_1 = _repository.GetCheckBoxDisasterManagementInfo("21.1.1", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                    vm.CheckBoxInfo21_2 = _repository.GetCheckBoxDisasterManagementInfo("21.1.2", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                    vm.CheckBoxInfo21_3 = _repository.GetCheckBoxDisasterManagementInfo("21.1.3", userId, vm.DisasterManagementInfo.First().DisasterManagementId);
                }


                if (vm.RiskReductionResilienceEducationInfo.Count > 0)
                {
                    vm.CheckBoxInfo23_3 = _repository.GetCheckBoxRiskResilienceInfo("23.3", userId, vm.RiskReductionResilienceEducationInfo.First().RiskResilienceId);
                    vm.CheckBoxInfo24_7 = _repository.GetCheckBoxRiskResilienceInfo("24.7", userId, vm.RiskReductionResilienceEducationInfo.First().RiskResilienceId);
                }

                vm.DivisionInfo = _repository.Divisioninfo();
                vm.DistrictInfo = _repository.Districtinfo();
                vm.UpazillaInfo = _repository.Upazillainfo();
                vm.UnionInfo = _repository.Unioninfo();


                //if (vm.GeneralInformationInfo.Count > 0 || vm.SafeLearningInfo.Count > 0)
                //{

                //    return View("FormcontentExists", vm);
                //}
                //else
                //{
                ViewBag.Title = "Operational Panel : Pre-Disaster Information Management : Form Create";
                    return View("FormcontentNew", vm);
               // }
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }

        #region Actions

        /// <summary>
        /// Uploads the file.
        /// </summary>
        /// <returns></returns>
        /// 

        [HttpPost]


        public JsonResult Delete()
        {

            int domainId =int.Parse(Request["domainId"]);
            var res = new Result { Text = "" };
            if (User.Identity.Name == "")
            {
                res = new Result { Text = "Please login first than then try again" };

                return Json(res);
            }
            else
            {
                ViewModels vm = new ViewModels();

                var DomainInfo = _repository.DomaininfoById(domainId);


                UsersModel userMo = new UsersModel();

                var uinfo = userMo.GetInfoByName(User.Identity.Name);
                int? userId = uinfo.First().UserId;

                if (DomainInfo.Count > 0)
                {
                    int? genId = DomainInfo.First().GeneralInformationId;

                    int? disId = DomainInfo.First().DisasterManagementId;
                    int? riksId = DomainInfo.First().RiskResilienceId;
                    int? safeId = DomainInfo.First().SafeLearningId;

                    DomainGeneralInformationModels domaingenModel = new DomainGeneralInformationModels();
                    DomainDisasterManagementModels domaindismgmModel = new DomainDisasterManagementModels();
                    DomainSafeLearningModels domainsafeModel = new DomainSafeLearningModels();
                    RiskReductionResilienceEducationModels riskModel = new RiskReductionResilienceEducationModels();
                    DisasterHistoryModels dishisModel = new DisasterHistoryModels();
                    DomainsModels domModel = new DomainsModels();

                    CheckBoxsModels checkModel = new CheckBoxsModels();

                    domModel.removeDomain(userId, domainId);


                    checkModel.removeGeneralInfoCheck(userId, genId, "15.8");


                    checkModel.removeRiksCheck(userId, riksId, "23.3");
                    checkModel.removeRiksCheck(userId, riksId, "24.7");


                    checkModel.removeSafeLearningCheck(userId, safeId, "17.1");
                    checkModel.removeSafeLearningCheck(userId, safeId, "19.0");

                    checkModel.removeDisMgmCheck(userId, disId, "21.0.0");
                    checkModel.removeDisMgmCheck(userId, disId, "21.1.1");
                    checkModel.removeDisMgmCheck(userId, disId, "21.1.2");
                    checkModel.removeDisMgmCheck(userId, disId, "21.1.3");
                    dishisModel.removeDisHistory("1", disId);
                    dishisModel.removeDisHistory("2", disId);
                    dishisModel.removeDisHistory("3", disId);


                    domaingenModel.removeGenDomain(userId, genId);
                    domaindismgmModel.removeDisDomain(userId, disId);
                    riskModel.removeRiskDomain(userId, riksId);
                    domainsafeModel.removeSafeDomain(userId, safeId);










                }

                res = new Result { Text = "success" };

                return Json(res);
            }
        }




        public virtual JsonResult DomainSafeLearningAdd()
        {
            var res = new Result { Text = "" };

            if (User.Identity.Name == "")
            {
                res = new Result { Text = "Please login first than then try again" };

                return Json(res);
            }
            else{

               
            UsersModel userMo = new UsersModel();
            var uinfo = userMo.GetInfoByName(User.Identity.Name);
            DomainSafeLearning dsf = new DomainSafeLearning();
            DomainSafeLearningModels dsfmodel = new DomainSafeLearningModels();

            ViewModels vm = new ViewModels();
            //var memberId = WebSecurity.GetUserId(User.Identity.Name);

            int? userId = uinfo.First().UserId;
            CheckBoxsModels checkModel = new CheckBoxsModels();



                dsf.TotalBuildingStructer =int.Parse(Request["TotalBuildingStructer"]);
                dsf.TotalKaccaBuildingStructer =int.Parse(Request["TotalKaccaBuildingStructer"]);
                dsf.TotalPaccaBuildingStructer = int.Parse(Request["TotalPaccaBuildingStructer"]);
                dsf.TotalSemiPaccaBuildingStructer = int.Parse(Request["TotalSemiPaccaBuildingStructer"]);
                dsf.TotalUnsedBuildingStructer = int.Parse(Request["TotalUnsedBuildingStructer"]);
                dsf.TotalClassRoom = Request["TotalClassRoom"];
                dsf.TotalKaccaClassRoom = Request["TotalKaccaClassRoom"];
                dsf.TotalSemiPaccaClassRoom = Request["TotalSemiPaccaClassRoom"];
                dsf.TotalPaccaClassRoom = Request["TotalPaccaClassRoom"];
                dsf.TotalUnsedClassRoom = Request["TotalUnsedClassRoom"];
                dsf.SourceOfDrinkingWater = Request["SourceOfDrinkingWaterOther"];
                dsf.OwnDrinkingSource = Request["OwnDrinkingSource"];
                dsf.SafeDrinkingSource = Request["SafeDrinkingSource"];
                dsf.SourceOfWaterIfTubewellArsenicFree = Request["SourceOfWaterIfTubewellArsenicFree"];
                dsf.SourceOfWaterIfTubewellFunctional = Request["SourceOfWaterIfTubewellFunctional"];
                dsf.SourceOfWaterChildFriendly = Request["SourceOfWaterChildFriendly"];
                dsf.SourceOfWaterDisableFriendly = Request["SourceOfWaterDisableFriendly"];
                dsf.SourceOfWaterFunctionDuringDisaster = Request["SourceOfWaterFunctionDuringDisaster"];

                dsf.SourceOfWaterAwayFromSchool = Request["SourceOfWaterAwayFromSchool"];
                if (Request["LatrineFacilityForStudent"] == null || Request["LatrineFacilityForStudent"] == "0")
                {
                    dsf.LatrineFacilityForStudent = 0;
                }
                else
                {
                    dsf.LatrineFacilityForStudent =int.Parse(Request["LatrineFacilityForStudent"]);
                }
                
                dsf.LatrineForMaleStudent = Request["LatrineForMaleStudent"];
                dsf.LatrineForFemaleStudent = Request["LatrineForFemaleStudent"];
                if (Request["LatrineFacilityForTeacher"] == null || Request["LatrineFacilityForTeacher"] == "0")
                {
                    dsf.LatrineFacilityForTeacher = 0;
                }
                else
                {
                    dsf.LatrineFacilityForTeacher =int.Parse(Request["LatrineFacilityForTeacher"]);
                }
               
                dsf.LatrineFacilityForMaleTeacher = Request["LatrineFacilityForMaleTeacher"];
                dsf.LatrineFacilityForFeMaleTeacher = Request["LatrineFacilityForFeMaleTeacher"];
                dsf.LatrineRegularMaintain = Request["LatrineRegularMaintain"];
                dsf.LatrineChildAccessiblePhysicalDisabilties = Request["LatrineChildAccessiblePhysicalDisabilties"];
                dsf.LatrineFemaleCondtion = Request["LatrineFemaleCondtion"];
                dsf.LatrineMaleCondition = Request["LatrineMaleCondition"];
                dsf.LatrinePhysicalDisabilties = Request["LatrinePhysicalDisabilties"];
                if (Request["LatrineFacilityFunctional"] == null || Request["LatrineFacilityFunctional"] == "0")
                {
                    dsf.LatrineFacilityFunctional = 0;
                }
                else
                {
                    dsf.LatrineFacilityFunctional = int.Parse(Request["LatrineFacilityFunctional"]);
                }
               
                dsf.StudentToiletBoth=Request["StuBoth"];
                dsf.TeacherToiletBoth = Request["TcBoth"];
                dsf.DomainStatus = 0;
                dsf.userId = uinfo.First().UserId;
                dsf.InstituteId = Request["GobEIINs"];



                if (Request["slId"] == "000")
                {
                    vm.SafeLearningInfo = _repository.GetSafeLearningInfoUnsaved(uinfo.First().UserId);


                    if (vm.SafeLearningInfo.Count > 0)
                    {
                        res = new Result { Text = "Already one data processing " };

                    }
                    else
                    {
                        vm.GeneralInformationInfo = _repository.GetGeneralInformationsUnsaved(uinfo.First().UserId);

                        if (vm.GeneralInformationInfo.Count > 0)
                        {
                            res = new Result { Text = "Domain SafeLearning Saved  successfully." };

                            dsfmodel.Add(dsf);
                        }
                        else
                        {
                            res = new Result { Text = "Please fillup domain first than try again." };
                        }

                    }
                }
                else
                {
                    int slId = int.Parse(Request["slId"]);
                    checkModel.removeSafeLearningCheck(userId, slId, "17.1");
                    checkModel.removeSafeLearningCheck(userId, slId, "19.0");

                    res = new Result { Text = "Domain SafeLearning Updated  successfully." };
                    
                    dsf.SafeLearningId = slId;
                    dsfmodel.Edit(dsf);


                }

                string SourceOfDrinkingWaters = Request["SourceOfDrinkingWaters"];

                var lastSafe = _repository.GetDomainSafeLearningLastId(uinfo.First().UserId);
                if (SourceOfDrinkingWaters != "")
                {
                    string[] arraySourceOfDrinkingWaters = SourceOfDrinkingWaters.Split(',');
                    int cnt = arraySourceOfDrinkingWaters.Length;
                    CheckBoxsModels chkModel = new CheckBoxsModels();
                    for (int ij = 0; ij < cnt - 1; ij++)
                    {
                        CheckBox chkDto = new CheckBox();
                        chkDto.ChkName = arraySourceOfDrinkingWaters[ij].Replace(" ", "");
                        //mystring = mystring.Replace(" ", "");
                        chkDto.ChkValue = arraySourceOfDrinkingWaters[ij];
                        chkDto.InstituteId = Request["GobEIINs"];
                        chkDto.QuestionId = Request["checkQuestion"];

                        chkDto.SafeLearningId = lastSafe.First().SafeLearningId;

                        //chkDto.DisasterManagementId=1;
                        //chkDto.RiskResilienceId = 1;
                        //chkDto.SafeLearningId = 2;
                        chkDto.UserId = uinfo.First().UserId;

                        chkModel.Add(chkDto);


                    }
                }



                string hazards = Request["hazards"];
                if (hazards != "")
                {
                    string[] arrayhazards = hazards.Split(',');
                    int cnt = arrayhazards.Length;
                    CheckBoxsModels chkModel = new CheckBoxsModels();



                    for (int ij = 0; ij < cnt - 1; ij++)
                    {
                        CheckBox chkDto = new CheckBox();
                        chkDto.ChkName = arrayhazards[ij].Replace(" ", "");
                        //mystring = mystring.Replace(" ", "");
                        chkDto.ChkValue = arrayhazards[ij];
                        chkDto.InstituteId = Request["GobEIINs"];
                        chkDto.QuestionId = "19.0";
                       // chkDto.HazardsName=

                        if (arrayhazards[ij].Contains("Flood"))
                        {
                            chkDto.HazardsName = "Flood";
                        }

                        if (arrayhazards[ij].Contains("Cyclone"))
                        {
                            chkDto.HazardsName = "Cyclone";
                        }
                        if (arrayhazards[ij].Contains("Earth Quack"))
                        {
                            chkDto.HazardsName = "Earth Quack";
                        }
                        if (arrayhazards[ij].Contains("River Bank Erosion"))
                        {
                            chkDto.HazardsName = "River Bank Erosion";
                        }
                        if (arrayhazards[ij].Contains("Tidal Surge"))
                        {
                            chkDto.HazardsName = "Tidal Surge";
                        }
                        if (arrayhazards[ij].Contains("Water Logging"))
                        {
                            chkDto.HazardsName = "Water Logging";
                        }
                        if (arrayhazards[ij].Contains("Strom"))
                        {
                            chkDto.HazardsName = "Strom";
                        }
                        if (arrayhazards[ij].Contains("Land Slide"))
                        {
                            chkDto.HazardsName = "Land Slide";
                        }
                        if (arrayhazards[ij].Contains("Fire"))
                        {
                            chkDto.HazardsName = "Fire";
                        }

                        if (arrayhazards[ij].Contains("Salinity"))
                        {
                            chkDto.HazardsName = "Salinity";
                        }
                        if (arrayhazards[ij].Contains("Flash"))
                        {
                            chkDto.HazardsName = "Flash Flood";
                        }
                        if (arrayhazards[ij].Contains("Seasonal"))
                        {
                            chkDto.HazardsName = "Seasonal Strom";
                        }
                        if (arrayhazards[ij].Contains("Tsunami"))
                        {
                            chkDto.HazardsName = "Tsunami";
                        }

                        if (arrayhazards[ij].Contains("Buliding Collapse"))
                        {
                            chkDto.HazardsName = "Buliding Collapse";
                        }




                        if (arrayhazards[ij].Contains("Serious injuries or deaths"))
                        {
                            chkDto.ImpactName = "Serious injuries or deaths";
                        }

                        if (arrayhazards[ij].Contains("Damage to buildings"))
                        {
                            chkDto.ImpactName = "Damage to buildings";
                        }
                        if (arrayhazards[ij].Contains("Used as Shelter"))
                        {
                            chkDto.ImpactName = "Used as Shelter";
                        }
                        if (arrayhazards[ij].Contains("Communication interruption"))
                        {
                            chkDto.ImpactName = "Communication interruption";
                        }
                        if (arrayhazards[ij].Contains("Damage Roads and transport"))
                        {
                            chkDto.ImpactName = "Damage Roads and transport";
                        }
                        if (arrayhazards[ij].Contains("Electricity"))
                        {
                            chkDto.ImpactName = "Electricity";
                        }
                        if (arrayhazards[ij].Contains("Water"))
                        {
                            chkDto.ImpactName = "Water";
                        }
                        if (arrayhazards[ij].Contains("Toilet"))
                        {
                            chkDto.ImpactName = "Toilet";
                        }
                        if (arrayhazards[ij].Contains("School closure"))
                        {
                            chkDto.ImpactName = "School closure";
                        }

                        if (arrayhazards[ij].Contains("Reduced School attendance"))
                        {
                            chkDto.ImpactName = "Reduced School attendance";
                        }

                        if (arrayhazards[ij].Contains("Dropout"))
                        {
                            chkDto.ImpactName = "Dropout";
                        }

                        



                        chkDto.SafeLearningId = lastSafe.First().SafeLearningId;

                        //chkDto.DisasterManagementId=1;
                        //chkDto.RiskResilienceId = 1;
                        //chkDto.SafeLearningId = 2;
                        chkDto.UserId = uinfo.First().UserId;

                        chkModel.Add(chkDto);


                    }
                }



                //  dginfo.VolunteerServices = Request[""];

               // return Json(res);
            
           

            return Json(res);
        }

        }

        [HttpPost]
        public virtual JsonResult DomainGeneralInfoAdd()
        {
            var res = new Result { Text = "" };

            if (User.Identity.Name == "")
            {
                res = new Result { Text = "Please login first than then try again" };

                return Json(res);
            }
            else
            {
                UsersModel userMo = new UsersModel();
                var uinfo = userMo.GetInfoByName(User.Identity.Name);

                DomainGeneralInformation dginfo = new DomainGeneralInformation();
                dginfo.Altitude = Request["Altitude"];
                dginfo.GpsLatitude = Request["Latitude"]; ;
                dginfo.GpsLongtitude = Request["Longitude"];
                dginfo.GraphicalLocation = Request["GraphicalLocation"];

                dginfo.AuthorityType = Request["authorityType"];
                dginfo.BankACNo = Request["BankACNo"];
                dginfo.BankAddress = Request["BankAddress"];
                dginfo.BankBranch = Request["BankBranch"];
                dginfo.BankContact = Request["BankContact"];
                dginfo.BankName = Request["BankName"];
                dginfo.ComputerLab = Request["ComputerLab"];
                dginfo.ElectricityFacillity = Request["ElectricityFacillity"];
                dginfo.EstabilishmentDate =Request["EstabilishmentDates"];
                dginfo.FirstAid = Request["FirstAid"];

                dginfo.FunctionalFireExtinguise = Request["FunctionalFireExtinguise"];

                dginfo.GobEIIN = Request["GobEIINs"];
                dginfo.HasFireExtinguiser = Request["HasFireExtinguiser"];

                dginfo.HowManyShift = Request["HowManyShift"];

                dginfo.InstitudeDistric = Request["InstitudeDistric"];
                dginfo.InstitudeDivision = Request["InstitudeDivision"];
                dginfo.InstitudeHouse = Request["InstitudeHouse"];
                dginfo.InstitudeRoad = Request["InstitudeRoad"];
                dginfo.InstitudeSection = Request["InstitudeSection"];
                dginfo.InstitudeUpazilla = Request["InstitudeUpazilla"];
                dginfo.InstitudeVillage = Request["InstitudeVillage"];
                dginfo.InstituteAddress = Request["InstituteAddress"];
                dginfo.InstituteBng = Request["InstituteBng"];
                dginfo.InstituteEng = Request["InstituteEng"];
                dginfo.Embankment = Request["embankment"];

                dginfo.InstituteId = Request["InstituteIds"];

                dginfo.InstituteFax = Request["InstituteFax"];
                dginfo.InstituteMobile = Request["InstituteMobile"];
                dginfo.InstitutePhone = Request["InstitutePhone"];
                dginfo.InstituteType = Request["InstituteType"];
                dginfo.InstituteWebUrl = Request["InstituteWebUrl"];
                dginfo.InternetFacility = Request["InternetFacility"];
                dginfo.ManagementType = Request["ManagementType"];
                
                dginfo.PlayingGround = Request["PlayingGround"];
                dginfo.SchoolArea = Request["SchoolArea"];
                dginfo.SmcExpiredate =Request["smscexpirydate"];
                dginfo.SmcPrisedentMobile = Request["SmcPrisedentMobile"];
                dginfo.SmcPrisedentName = Request["SmcPrisedentName"];

                if (Request["StudentDisableFeMale"] == "0")
                {
                    dginfo.StudentDisableFeMale = 0;
                }
                else
                {
                    dginfo.StudentDisableFeMale =int.Parse(Request["StudentDisableFeMale"]);
                }

                if (Request["StudentDisableMale"] == "0")
                {
                    dginfo.StudentDisableMale = 0;
                }
                else
                {
                    dginfo.StudentDisableMale = int.Parse(Request["StudentDisableMale"]);
                }
                if (Request["StudentEthnicFeMale"] == "0")
                {
                    dginfo.StudentEthnicFeMale = 0;
                }
                else
                {
                    dginfo.StudentEthnicFeMale = int.Parse(Request["StudentEthnicFeMale"]);
                }

                if (Request["StudentEthnicMale"] == "0")
                {
                    dginfo.StudentEthnicMale = 0;
                }
                else
                {
                    dginfo.StudentEthnicMale = int.Parse(Request["StudentEthnicMale"]);
                }

                if (Request["StudentMale"] == "0")
                {
                    dginfo.StudentMale = 0;
                }
                else
                {
                    dginfo.StudentMale = int.Parse(Request["StudentMale"]);
                }
                if (Request["StudentFemale"] == "0")
                {
                    dginfo.StudentFemale = 0;
                }
                else
                {
                    dginfo.StudentFemale = int.Parse(Request["StudentFemale"]);
                }

                if (Request["TeacherFemale"] == "0")
                {
                    dginfo.TeacherFemale = 0;
               
                }
                else
                {

                    dginfo.TeacherFemale =int.Parse(Request["TeacherFemale"]);
                }
                if (Request["TeacherFemale"] == "0")
                {
                    dginfo.TeacherFemale = 0;

                }
                else
                {

                    dginfo.TeacherFemale = int.Parse(Request["TeacherFemale"]);
                }
                if (Request["TeacherMale"] == "0")
                {
                    dginfo.TeacherMale = 0;
               
                }
                else
                {

                    dginfo.TeacherMale =int.Parse(Request["TeacherMale"]);
                }
                if (Request["VacantTeacherPosition"] == "0")
                {
                    dginfo.VacantTeacherPosition =0;
               
                }
                else
                {

                    dginfo.VacantTeacherPosition =int.Parse(Request["VacantTeacherPosition"]);
                }


                dginfo.BankACNo = Request["InstituteBankAc"];
                dginfo.BankName = Request["Institutebankname"];
                dginfo.BankAddress = Request["Bankaddress"];
                dginfo.BankContact = Request["InstituteContactNo"];
                dginfo.BankBranch = Request["Institutebankbranch"];

                dginfo.TiffinFacility = Request["TiffinFacility"];
                dginfo.TransportionMode = Request["TransportionMode"];

                dginfo.DomainStatus = 0;
                dginfo.userId = uinfo.First().UserId;


                dginfo.InstitudeUnion = Request["InstitudeUnion"];
                dginfo.InstitudeUpazilla = Request["InstitudeUpazilla"];
                dginfo.InstitudeDivision = Request["InstitudeDivision"];
                dginfo.InstitudeDistric = Request["InstitudeDistric"];
                dginfo.InstitudeSection = Request["InstitudeSection"];
                dginfo.InstitudeVillage = Request["InstitudeVillage"];
                dginfo.InstitudeRoad = Request["InstitudeRoad"];
                dginfo.InstitudeHouse = Request["InstitudeHouse"];
                 
                dginfo.UnionId=int.Parse(Request["unId"]);
                dginfo.UpazillaId=int.Parse(Request["upaId"]);
                dginfo.DivisionId=int.Parse(Request["divId"]);
                dginfo.DistrictId=int.Parse(Request["disId"]);

                ViewModels vm = new ViewModels();
                DomainGeneralInformationModels dgmodel = new DomainGeneralInformationModels();
                vm.GeneralInformationInfo = _repository.GetGeneralInformationsUnsaved(uinfo.First().UserId);


              

                int genId = int.Parse(Request["genId"]);


                // Response.Write(institutionalvolunteeringservices);


                if (Request["genId"] == "000")
                {
                    if (vm.GeneralInformationInfo.Count > 0)
                    {
                        res = new Result { Text = "Already one data processing " };

                    }
                    else
                    {

                        var exists = dgmodel.CheckInstitutedIdExist(Request["InstituteIds"]);

                        if (exists.Count > 0)
                        {
                            res = new Result { Text = "InstituteIds already exists.", InsertedId = Request["InstituteIds"] };
                        }
                        else
                        {
                            dgmodel.Add(dginfo);


                            var lastGenAdd = _repository.GetDomainGeneralInfoLastId(uinfo.First().UserId);
                            // this.UploadFile(lastGenAdd.First().GeneralInformationId);

                            res = new Result { Text = "GeneralInformation saved successfully.", InsertedId = Convert.ToString(lastGenAdd.First().GeneralInformationId) };
                        }
                    }

                }
                else
                {

                  
                    dginfo.GeneralInformationId = genId;

                    dgmodel.Edit(dginfo);
                    res = new Result { Text = "GeneralInformation Updated successfully.", InsertedId = Convert.ToString(genId) };

                    //this.UploadFile(genId);

                }
                var lastGen = _repository.GetDomainGeneralInfoLastId(uinfo.First().UserId);
                string institutionalvolunteeringservices = Request["institutionalvolunteeringservices"];

                if (institutionalvolunteeringservices != "")
                {
                    string[] arrayvolunteeringservices = institutionalvolunteeringservices.Split(',');
                    int cnt = arrayvolunteeringservices.Length;
                    CheckBoxsModels chkModel = new CheckBoxsModels();

                    if (Request["genId"] == "000")
                    {

                        chkModel.removeGeneralInfoCheck(uinfo.First().UserId, lastGen.First().GeneralInformationId, "15.8");
                       // res = new Result { Text = "GeneralInformation saved successfully.", InsertedId = Convert.ToString(lastGen.First().GeneralInformationId) };
                    }
                    else
                    {
                        chkModel.removeGeneralInfoCheck(uinfo.First().UserId, int.Parse(Request["genId"]), "15.8");
                       // res = new Result { Text = "GeneralInformation Update successfully.", InsertedId = Convert.ToString(int.Parse(Request["genId"])) };
                    }
                    for (int ij = 0; ij < cnt - 1; ij++)
                    {
                        CheckBox chkDto = new CheckBox();
                        chkDto.ChkName = arrayvolunteeringservices[ij].Replace(" ", "");
                        //mystring = mystring.Replace(" ", "");
                        chkDto.ChkValue = arrayvolunteeringservices[ij];
                        chkDto.InstituteId = Request["InstituteIds"];

                        chkDto.QuestionId = Request["checkQuestion"];
                       if (Request["genId"] == "000")
                        {
                        chkDto.GeneralInformationId = lastGen.First().GeneralInformationId;
                        }
                        else
                        {
                           chkDto.GeneralInformationId = int.Parse(Request["genId"]);
                        }
                        //chkDto.DisasterManagementId=1;
                        //chkDto.RiskResilienceId = 1;
                        //chkDto.SafeLearningId = 2;
                        chkDto.UserId = uinfo.First().UserId;
                        chkModel.Add(chkDto);


                    }
                }




                //  dginfo.VolunteerServices = Request[""];


                return Json(res);
            }

        }
        [HttpPost]
        public virtual JsonResult RiskReductionResilienceEducationAdd()
        {
            var res = new Result { Text = "" };

            if (User.Identity.Name == "")
            {
                res = new Result { Text = "Please login first than then try again" };

                return Json(res);
            }
            else
            {
                UsersModel userMo = new UsersModel();
                var uinfo = userMo.GetInfoByName(User.Identity.Name);

                CheckBoxsModels chkModel = new CheckBoxsModels();
                RiskReductionResilienceEducation riskAdd = new RiskReductionResilienceEducation();
                  ViewModels vm = new ViewModels();
            //var memberId = WebSecurity.GetUserId(User.Identity.Name);


          
                riskAdd.DisasterRelatedTraining = Request["DisasterRelatedTraining"];
                riskAdd.TrainingOnName = Request["TrainingOnName"];
                riskAdd.HowManyTeacherAttended = Request["HowManyTeacherAttended"];
                riskAdd.SmcMemberAttented = Request["SmcMemberAttented"];
                riskAdd.StudentAttented = Request["StudentAttented"];
                riskAdd.RegularCurriculum = Request["RegularCurriculum"];
                riskAdd.FromExtraCurriculum = Request["FromExtraCurriculum"];
                riskAdd.SchoolAssemblies = Request["SchoolAssemblies"];
                riskAdd.AfterSchoolClubs = Request["AfterSchoolClubs"];
                riskAdd.HowStudentAwareAboutDisaster = Request["HowStudentAwareAboutDisaster"];
                riskAdd.DrilFacility = Request["DrilFacility"];
                riskAdd.OtherSource = Request["OtherSource"];
                riskAdd.userId = uinfo.First().UserId;
                riskAdd.InstituteId = Request["GobEIINs"]; 

                riskAdd.DomainStatus = 0;

                vm.RiskReductionResilienceEducationInfo = _repository.GetRiskReductionResilienceEducationInfoUnsaved(uinfo.First().UserId);
                RiskReductionResilienceEducationModels riskModel = new RiskReductionResilienceEducationModels();


                if (Request["riskId"] == "000")
                {
                    if (vm.RiskReductionResilienceEducationInfo.Count > 0)
                    {
                        res = new Result { Text = "Already one data processing " };

                    }
                    else
                    {

                          vm.GeneralInformationInfo = _repository.GetGeneralInformationsUnsaved(uinfo.First().UserId);

                          if (vm.GeneralInformationInfo.Count > 0)
                          {
                              res = new Result { Text = "Risk Reduction Resilience Education  successfully." };
                              riskModel.Add(riskAdd);

                          } else
                            {
                                res = new Result { Text = "Please fillup domain first than try again." };
                            }

                        
                    }


                }
                else
                {

                    res = new Result { Text = "Risk Reduction Resilience Education Updated successfully." };
                    int riskId = int.Parse(Request["riskId"]);
                    riskAdd.RiskResilienceId = riskId;

                    riskModel.Edit(riskAdd);


                }


                var lastRisk = _repository.GetDomainRiskReductionResilienceLastId(uinfo.First().UserId);


                string hearfor = Request["hearfor"];

                if (hearfor != "")
                {
                    string[] arrayhearfor = hearfor.Split(',');
                    int cnt = arrayhearfor.Length;
                    if (Request["riskId"] == "000")
                    {
                        chkModel.removeRiksCheck(uinfo.First().UserId, lastRisk.First().RiskResilienceId, "24.7");
                    }
                    else
                    {
                        int riskId = int.Parse(Request["riskId"]);
                        chkModel.removeRiksCheck(uinfo.First().UserId, riskId, "24.7");
                    }

                    for (int ij = 0; ij < cnt - 1; ij++)
                    {
                        CheckBox chkDto = new CheckBox();
                        chkDto.ChkName = arrayhearfor[ij].Replace(" ", "");
                        //mystring = mystring.Replace(" ", "");
                        chkDto.ChkValue = arrayhearfor[ij];

                        chkDto.InstituteId = Request["GobEIINs"];
                        chkDto.QuestionId = "24.7";


                        chkDto.RiskResilienceId = lastRisk.First().RiskResilienceId;

                        //chkDto.DisasterManagementId=1;
                        //chkDto.RiskResilienceId = 1;
                        //chkDto.SafeLearningId = 2;
                        chkDto.UserId = uinfo.First().UserId;
                        chkModel.Add(chkDto);


                    }
                }

                string trainingfor = Request["trainingfor"];

                if (trainingfor != "")
                {
                    string[] arraytrainingfor = trainingfor.Split(',');
                    int cnt = arraytrainingfor.Length;
                    if (Request["riskId"] == "000")
                    {
                        chkModel.removeRiksCheck(uinfo.First().UserId, lastRisk.First().RiskResilienceId, "23.3");
                    }
                    else
                    {
                        int riskId = int.Parse(Request["riskId"]);
                        chkModel.removeRiksCheck(uinfo.First().UserId, riskId, "23.3");
                    }

                    for (int ij = 0; ij < cnt - 1; ij++)
                    {
                        CheckBox chkDto = new CheckBox();
                        chkDto.ChkName = arraytrainingfor[ij].Replace(" ", "");
                        //mystring = mystring.Replace(" ", "");
                        chkDto.ChkValue = arraytrainingfor[ij];

                        chkDto.InstituteId = Request["GobEIINs"];
                        chkDto.QuestionId = "23.3";


                        chkDto.RiskResilienceId = lastRisk.First().RiskResilienceId;

                        //chkDto.DisasterManagementId=1;
                        //chkDto.RiskResilienceId = 1;
                        //chkDto.SafeLearningId = 2;
                        chkDto.UserId = uinfo.First().UserId;
                        chkModel.Add(chkDto);


                    }
                }
            
            //else
            //{
            //    res = new Result { Text = "Please fillup domain first than try again." };
            //}
                return Json(res);

            }
        }


        [HttpPost]
        public virtual JsonResult DomainManagmentAdd()
        {
            var res = new Result { Text = "" };

            if (User.Identity.Name == "")
            {
                res = new Result { Text = "Please login first than then try again" };

                return Json(res);
            }
            else
            {
                UsersModel userMo = new UsersModel();
                var uinfo = userMo.GetInfoByName(User.Identity.Name);


                ViewModels vm = new ViewModels();
                string conPlan = Request["ContingencyPlan"];
                string catchmentTempo = Request["CatchmentAreaForTemporaryLearning"];
                string CatchmentArea = Request["CatchmentAreaForTeacherBackup"];
                string accessiable = Request["AccessibleEmergencyFund"];
                //if (conPlan != null && CatchmentArea != null && catchmentTempo != null && accessiable != null)
                //{
                

                    

                
                     DomainDisasterManagement domaindistermgm = new DomainDisasterManagement();
                     domaindistermgm.ContingencyPlan = Request["ContingencyPlan"];
                     domaindistermgm.CatchmentAreaForTemporaryLearning = Request["CatchmentAreaForTemporaryLearning"];
                     domaindistermgm.CatchmentAreaForTeacherBackup = Request["CatchmentAreaForTeacherBackup"];
                     domaindistermgm.AccessibleEmergencyFund = Request["AccessibleEmergencyFund"];
                     domaindistermgm.userId = uinfo.First().UserId;
                     domaindistermgm.DomainStatus = 0;
                     domaindistermgm.InstituteId = Request["GobEIINs"];

                     if (Request["DisasterName"] != "" && Request["DisasterName"] != null)
                     {
                         string DisasterName = Request["DisasterName"];
                         string[] arrayDisasterName = DisasterName.Split(',');
                         int cnt = arrayDisasterName.Length;
                         domaindistermgm.DisasterCount = cnt;


                     }



                     vm.DisasterManagementInfo = _repository.GetDisasterInfoUnsaved(uinfo.First().UserId);


                     DomainDisasterManagementModels domainDisModel = new DomainDisasterManagementModels();
                     if (Request["dgmId"] == "000")
                     {
                         if (vm.DisasterManagementInfo.Count > 0)
                         {
                             res = new Result { Text = "Already one data processing " };

                         }
                         else
                         {
                              vm.GeneralInformationInfo = _repository.GetGeneralInformationsUnsaved(uinfo.First().UserId);

                 if (vm.GeneralInformationInfo.Count > 0)
                 {

                             domainDisModel.Add(domaindistermgm);



                             res = new Result { Text = "Domain Managment information saved successfully." };
                 }
                 else
                 {
                     res = new Result { Text = "Please fill up  form one first  then try again" };
                 }
                         }

                     }
                     else
                     {

                         int dgmId = int.Parse(Request["dgmId"]);
                         domaindistermgm.DisasterManagementId = dgmId;
                         domainDisModel.Edit(domaindistermgm);
                         res = new Result { Text = "Domain Managment information Updated successfully." };

                     }

                     var lastMgm = _repository.GetDomainDisterLastId(uinfo.First().UserId);

                     CheckBoxsModels chkModel = new CheckBoxsModels();

                     if (Request["DisasterName"] != "" && Request["DisasterName"] != null)
                     {
                         string DisasterName = Request["DisasterName"];
                         string[] arrayDisasterName = DisasterName.Split(',');
                         int cnt = arrayDisasterName.Length;



                         if (Request["dgmId"] == "000")
                         {

                             chkModel.removeGeneralInfoCheck(uinfo.First().UserId, lastMgm.First().DisasterManagementId, "21.0.0");

                         }
                         else
                         {
                             chkModel.removeGeneralInfoCheck(uinfo.First().UserId, int.Parse(Request["dgmId"]), "21.0.0");

                         }
                         for (int ij = 0; ij < cnt - 1; ij++)
                         {
                             CheckBox chkDto = new CheckBox();
                             chkDto.ChkName = arrayDisasterName[ij].Replace(" ", "");

                             chkDto.ChkValue = arrayDisasterName[ij];
                             chkDto.InstituteId = Request["GobEIINs"];
                             chkDto.QuestionId = "21.0.0";
                             chkDto.UserId = uinfo.First().UserId;
                             if (Request["dgmId"] == "000")
                             {

                                 chkDto.DisasterManagementId = lastMgm.First().DisasterManagementId;
                             }
                             else
                             {
                                 chkDto.DisasterManagementId = int.Parse(Request["dgmId"]);
                             }

                             chkModel.Add(chkDto);


                         }

                     }

                     DisasterHistoryModels dishisModel = new DisasterHistoryModels();
                     if (Request["dgmId"] == "000")
                     {
                         int disId = int.Parse(Request["dgmId"]);
                         dishisModel.removeDisHistory("1", lastMgm.First().DisasterManagementId);
                         dishisModel.removeDisHistory("2", lastMgm.First().DisasterManagementId);
                         dishisModel.removeDisHistory("3", lastMgm.First().DisasterManagementId);

                     }
                     else
                     {
                         int disId = int.Parse(Request["dgmId"]);
                         dishisModel.removeDisHistory("1", disId);
                         dishisModel.removeDisHistory("2", disId);
                         dishisModel.removeDisHistory("3", disId);

                     }


                     if (Request["DisasterName1"] != null)
                     {
                         DisasterHistory dishis1 = new DisasterHistory();

                         dishis1.DisasterImpact = Request["DisasterImpact1"];
                         dishis1.DisasterName = Request["DisasterName1"];
                         dishis1.DayRemainSchoolClose = Request["DayRemainSchoolClose1"];
                         dishis1.ImpactAttendanceForStudent = Request["ImpactAttendanceForStudent1"];
                         dishis1.ImpactAttandanceForTeacher = Request["ImpactAttendanceForTeacher1"];
                         dishis1.StudentDropout = Request["StudentDropout1"];
                         dishis1.StudentDead = Request["StudentDead1"];
                         dishis1.BulidingDamage = Request["BulidingDamage1"];
                         dishis1.MaterialDamage = Request["MaterialDamage1"];
                         dishis1.RecoveryInitiative = Request["RecoveryInitiative1"];
                         dishis1.AlternativeSchooling = Request["AlternativeSchooling1"];
                         dishis1.NoDayUsedAsShelter = Request["NoDayUsedAsShelter1"];
                         dishis1.UsedShelter = Request["UsedShelter1"];
                         dishis1.ToiletAffected = Request["ToiletAffected1"];
                         dishis1.AnyAid = Request["AnyAid1"];
                         dishis1.WhereFromAidTaken = Request["WhereFromAidTaken1"];
                         dishis1.AidType = Request["AidType1"];
                         dishis1.HaveWarningSystem = Request["HaveWarningSystem1"];
                         dishis1.DisasterDate = Request["DisasterDate1"];
                         dishis1.DidUnderstoodWarningMessage = Request["DidUnderstoodWarningMessage1"];

                         if (Request["dgmId"] == "000")
                         {

                             dishis1.DisasterManagementId = lastMgm.First().DisasterManagementId;
                         }
                         else
                         {
                             dishis1.DisasterManagementId = int.Parse(Request["dgmId"]);
                         }
                         dishis1.WasterSourceAffected = Request["WasterSourceAffected1"];
                         dishis1.RoadAffected = Request["RoadAffected1"];
                         dishis1.HistoryNo = "1";



                         dishisModel.Add(dishis1);




                         string DisasterName1 = Request["DisasterName1"];
                         string[] arrayDisasterName1 = DisasterName1.Split(',');
                         int cnt = arrayDisasterName1.Length;


                         if (Request["dgmId"] == "000")
                         {

                             chkModel.removeGeneralInfoCheck(uinfo.First().UserId, lastMgm.First().DisasterManagementId, "21.1.1");

                         }
                         else
                         {
                             chkModel.removeGeneralInfoCheck(uinfo.First().UserId, int.Parse(Request["dgmId"]), "21.1.1");

                         }

                         //for (int ij = 0; ij < cnt - 1; ij++)
                         //{
                         //    CheckBox chkDto = new CheckBox();
                         //    chkDto.ChkName = arrayDisasterName1[ij].Replace(" ", "") + "1";
                         //    //mystring = mystring.Replace(" ", "");
                         //    chkDto.ChkValue = arrayDisasterName1[ij];
                         //    chkDto.InstituteId = Request["GobEIINs"];
                         //    chkDto.QuestionId = "21.1.1";
                         //    if (Request["dgmId"] == "000")
                         //    {

                         //        chkDto.DisasterManagementId = lastMgm.First().DisasterManagementId;
                         //    }
                         //    else
                         //    {
                         //        chkDto.DisasterManagementId = int.Parse(Request["dgmId"]);
                         //    }
                         //    //chkDto.RiskResilienceId = 1;
                         //    //chkDto.SafeLearningId = 2;
                         //    chkDto.UserId = uinfo.First().UserId;
                         //    chkModel.Add(chkDto);


                         //}

                     }


                     if (Request["DisasterName2"] != null)
                     {
                         DisasterHistory dishis2 = new DisasterHistory();

                         dishis2.DisasterImpact = Request["DisasterImpact2"];
                         dishis2.DisasterName = Request["DisasterName2"];
                         dishis2.DayRemainSchoolClose = Request["DayRemainSchoolClose2"];
                         dishis2.ImpactAttendanceForStudent = Request["ImpactAttendanceForStudent2"];
                         dishis2.ImpactAttandanceForTeacher = Request["ImpactAttendanceForTeacher2"];
                         dishis2.StudentDropout = Request["StudentDropout2"];
                         dishis2.ToiletAffected = Request["ToiletAffected2"];
                         dishis2.StudentDead = Request["StudentDead2"];
                         dishis2.BulidingDamage = Request["BulidingDamage2"];
                         dishis2.MaterialDamage = Request["MaterialDamage2"];
                         dishis2.RecoveryInitiative = Request["RecoveryInitiative2"];
                         dishis2.AlternativeSchooling = Request["AlternativeSchooling2"];
                         dishis2.NoDayUsedAsShelter = Request["NoDayUsedAsShelter2"];
                         dishis2.UsedShelter = Request["UsedShelter2"];
                         dishis2.AnyAid = Request["AnyAid2"];
                         dishis2.WhereFromAidTaken = Request["WhereFromAidTaken2"];
                         dishis2.AidType = Request["AidType2"];
                         dishis2.HaveWarningSystem = Request["HaveWarningSystem2"];
                         dishis2.DisasterDate = Request["DisasterDate2"];
                         dishis2.DidUnderstoodWarningMessage = Request["DidUnderstoodWarningMessage2"];
                         if (Request["dgmId"] == "000")
                         {

                             dishis2.DisasterManagementId = lastMgm.First().DisasterManagementId;
                         }
                         else
                         {
                             dishis2.DisasterManagementId = int.Parse(Request["dgmId"]);
                         }

                         dishis2.WasterSourceAffected = Request["WasterSourceAffected2"];
                         dishis2.RoadAffected = Request["RoadAffected2"];
                         dishis2.HistoryNo = "2";

                         dishisModel.Add(dishis2);

                         string DisasterName2 = Request["DisasterName2"];
                         string[] arrayDisasterName2 = DisasterName2.Split(',');
                         int cnt = arrayDisasterName2.Length;

                         if (Request["dgmId"] == "000")
                         {

                             chkModel.removeGeneralInfoCheck(uinfo.First().UserId, lastMgm.First().DisasterManagementId, "21.1.2");

                         }
                         else
                         {
                             chkModel.removeGeneralInfoCheck(uinfo.First().UserId, int.Parse(Request["dgmId"]), "21.1.2");

                         }

                         //for (int ij = 0; ij < cnt - 1; ij++)
                         //{
                         //    CheckBox chkDto = new CheckBox();
                         //    chkDto.ChkName = arrayDisasterName2[ij].Replace(" ", "") + "2";
                         //    //mystring = mystring.Replace(" ", "");
                         //    chkDto.ChkValue = arrayDisasterName2[ij];
                         //    chkDto.InstituteId = Request["GobEIINs"];
                         //    chkDto.QuestionId = "21.1.2";

                         //    if (Request["dgmId"] == "000")
                         //    {

                         //        chkDto.DisasterManagementId = lastMgm.First().DisasterManagementId;
                         //    }
                         //    else
                         //    {
                         //        chkDto.DisasterManagementId = int.Parse(Request["dgmId"]);
                         //    }
                         //    //chkDto.RiskResilienceId = 1;
                         //    //chkDto.SafeLearningId = 2;
                         //    chkDto.UserId = uinfo.First().UserId;

                         //    chkModel.Add(chkDto);


                         //}


                     }

                     if (Request["DisasterName3"] != null)
                     {
                         DisasterHistory dishis3 = new DisasterHistory();

                         dishis3.DisasterImpact = Request["DisasterImpact3"];
                         dishis3.DisasterName = Request["DisasterName3"];
                         dishis3.DayRemainSchoolClose = Request["DayRemainSchoolClose3"];
                         dishis3.ImpactAttendanceForStudent = Request["ImpactAttendanceForStudent3"];
                         dishis3.ImpactAttandanceForTeacher = Request["ImpactAttendanceForTeacher3"];
                         dishis3.StudentDropout = Request["StudentDropout3"];
                         dishis3.StudentDead = Request["StudentDead3"];
                         dishis3.BulidingDamage = Request["BulidingDamage3"];
                         dishis3.MaterialDamage = Request["MaterialDamage3"];
                         dishis3.RecoveryInitiative = Request["RecoveryInitiative3"];
                         dishis3.AlternativeSchooling = Request["AlternativeSchooling3"];
                         dishis3.NoDayUsedAsShelter = Request["NoDayUsedAsShelter3"];
                         dishis3.ToiletAffected = Request["ToiletAffected3"];
                         dishis3.UsedShelter = Request["UsedShelter3"];
                         dishis3.AnyAid = Request["AnyAid3"];
                         dishis3.WhereFromAidTaken = Request["WhereFromAidTaken3"];
                         dishis3.AidType = Request["AidType3"];
                         dishis3.HaveWarningSystem = Request["HaveWarningSystem3"];
                         dishis3.DidUnderstoodWarningMessage = Request["DidUnderstoodWarningMessage3"];
                         dishis3.DisasterDate = Request["DisasterDate3"];
                         if (Request["dgmId"] == "000")
                         {

                             dishis3.DisasterManagementId = lastMgm.First().DisasterManagementId;
                         }
                         else
                         {
                             dishis3.DisasterManagementId = int.Parse(Request["dgmId"]);
                         }
                         //dishis3.DisasterManagementId = lastMgm.First().DisasterManagementId;
                         dishis3.WasterSourceAffected = Request["WasterSourceAffected3"];
                         dishis3.RoadAffected = Request["RoadAffected3"];
                         dishis3.HistoryNo = "3";


                         dishisModel.Add(dishis3);


                         string DisasterName3 = Request["DisasterName3"];
                         string[] arrayDisasterName3 = DisasterName3.Split(',');
                         int cnt = arrayDisasterName3.Length;

                         if (Request["dgmId"] == "000")
                         {

                             chkModel.removeGeneralInfoCheck(uinfo.First().UserId, lastMgm.First().DisasterManagementId, "21.1.3");

                         }
                         else
                         {
                             chkModel.removeGeneralInfoCheck(uinfo.First().UserId, int.Parse(Request["dgmId"]), "21.1.3");

                         }



                         //for (int ij = 0; ij < cnt - 1; ij++)
                         //{
                         //    CheckBox chkDto = new CheckBox();
                         //    chkDto.ChkName = arrayDisasterName3[ij].Replace(" ", "") + "3";
                         //    //mystring = mystring.Replace(" ", "");
                         //    chkDto.ChkValue = arrayDisasterName3[ij];
                         //    chkDto.InstituteId = Request["GobEIINs"];
                         //    chkDto.QuestionId = "21.1.3";

                         //    if (Request["dgmId"] == "000")
                         //    {

                         //        chkDto.DisasterManagementId = lastMgm.First().DisasterManagementId;
                         //    }
                         //    else
                         //    {
                         //        chkDto.DisasterManagementId = int.Parse(Request["dgmId"]);
                         //    }
                         //    //chkDto.RiskResilienceId = 1;
                         //    //chkDto.SafeLearningId = 2;
                         //    chkDto.UserId = uinfo.First().UserId;

                         //    chkModel.Add(chkDto);


                         //}

                     }




                     //else
                     //{
                     //    res = new Result { Text = "Please check Q20.1.  Q20.2 Q20.3 Q20.4 then try again" };

                     //}

                 
                 
                return Json(res);
            }

        }


        [HttpPost]
        public virtual JsonResult DomainSaveAdd()
        {

            var res = new Result { Text = "" };

            if (User.Identity.Name == "")
            {
                res = new Result { Text = "Please login first than then try again" };

                return Json(res);
            }
            else
            {
                UsersModel userMo = new UsersModel();


                var uinfo = userMo.GetInfoByName(User.Identity.Name);


                ViewModels vm = new ViewModels();
                vm.GeneralInformationInfo = _repository.GetGeneralInformationsUnsaved(uinfo.First().UserId);
                vm.DisasterManagementInfo = _repository.GetDisasterInfoUnsaved(uinfo.First().UserId);
                vm.RiskReductionResilienceEducationInfo = _repository.GetRiskReductionResilienceEducationInfoUnsaved(uinfo.First().UserId);
                vm.SafeLearningInfo = _repository.GetSafeLearningInfoUnsaved(uinfo.First().UserId);

                if (vm.GeneralInformationInfo.Count > 0 && vm.DisasterManagementInfo.Count > 0 && vm.RiskReductionResilienceEducationInfo.Count > 0 && vm.SafeLearningInfo.Count > 0)
                {

                    DomainDisasterManagementModels disasterModel = new DomainDisasterManagementModels();
                    DomainGeneralInformationModels genInfoModel = new DomainGeneralInformationModels();
                    DomainSafeLearningModels domaninsafe = new DomainSafeLearningModels();
                    RiskReductionResilienceEducationModels riskModel = new RiskReductionResilienceEducationModels();

                    DomainDisasterManagement ddmDto = new DomainDisasterManagement();
                    DomainGeneralInformation dgiDto = new DomainGeneralInformation();
                    DomainSafeLearning dslDto = new DomainSafeLearning();
                    RiskReductionResilienceEducation rrreDto = new RiskReductionResilienceEducation();

                    var DisasterManagementInfo = _repository.GetDisasterInfoUnsaved(uinfo.First().UserId);
                    ddmDto.DomainStatus = 1;
                    ddmDto.DisasterManagementId = DisasterManagementInfo.First().DisasterManagementId;
                    disasterModel.EditStatus(ddmDto);

                    var DomainGenInfo = _repository.GetGeneralInformationsUnsaved(uinfo.First().UserId);
                    dgiDto.DomainStatus = 1;
                    dgiDto.GeneralInformationId = DomainGenInfo.First().GeneralInformationId;
                    genInfoModel.EditStatus(dgiDto);

                    var DSafeLearningInfo = _repository.GetSafeLearningInfoUnsaved(uinfo.First().UserId);
                    dslDto.DomainStatus = 1;
                    dslDto.SafeLearningId = DSafeLearningInfo.First().SafeLearningId;
                    domaninsafe.EditStatus(dslDto);

                    var DRiskReductionResilienceEducationInfo = _repository.GetRiskReductionResilienceEducationInfoUnsaved(uinfo.First().UserId);
                    rrreDto.DomainStatus = 1;
                    rrreDto.RiskResilienceId = DRiskReductionResilienceEducationInfo.First().RiskResilienceId;
                    riskModel.EditStatus(rrreDto);


                    Domain dm = new Domain();
                    dm.DomainStatus = 1;
                    dm.GeneralInformationId = DomainGenInfo.First().GeneralInformationId;
                    dm.RiskResilienceId = DRiskReductionResilienceEducationInfo.First().RiskResilienceId;
                    dm.SafeLearningId = DSafeLearningInfo.First().SafeLearningId;
                    dm.DisasterManagementId = DisasterManagementInfo.First().DisasterManagementId;
                    dm.UserId = uinfo.First().UserId;


                    //  dm.UserId = User.Identity.Name;
                    dm.Domaindate = DateTime.Now;

                    DomainsModels dModel = new DomainsModels();
                    dModel.Add(dm);





                    res = new Result { Text = "Domain  information saved successfully." };
                }
                else
                {
                    res = new Result { Text = "Enter all information about  General Information,Disaster Management,Risk Reduction Resilience Education and Safe Learning first than try again " };
                }


                return Json(res);
            }

        }


        public class Result
        {
            public string Text { get; set; }
            public string InsertedId { get; set; }
        }


        [HttpPost]
        public virtual ActionResult UploadFile()
        {

          string ids=  Request["id"];
         string id1= Request.QueryString["id"];
         
            string Division1,Division2,District1,District2,Upazila1,Upazila2,Union1,Union2,edu1,edu2,edu3,edu4,edu5,edu6,name;

            Division1=Request["Division1"];
            Division2 = Request["Division2"];

            District1 = Request["District1"];
            District2 = Request["District2"];
            
            Upazila1 = Request["Upazila1"];
            Upazila2 = Request["Upazila2"];
            
            Union1 = Request["Union1"];
            Union2 = Request["Union2"];

            edu1 = Request["edu1"];
            edu2 = Request["edu2"];
            edu3 = Request["edu3"];
            edu4 = Request["edu4"];
            edu5 = Request["edu5"];
            edu6 = Request["edu6"];

            name = Division1 + Division2 + "-" + District1 + District2 + "-" + Upazila1 + Upazila2 + "-" + Union1 + Union2 + "-" + edu1 + edu2 + edu3 + edu4 + edu5 + edu6;

            HttpPostedFileBase myFile = Request.Files["MyFile"];
            bool isUploaded = false;
            string message = "File upload failed";

            if (myFile != null && myFile.ContentLength != 0)
            {
                string pathForSaving = Server.MapPath("~/Uploads");
                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        string fExtension = Path.GetExtension(myFile.FileName);

                        if (fExtension == ".jpg")
                        {
                            string fileName = name + fExtension;

                            myFile.SaveAs(Path.Combine(pathForSaving, fileName));
                            isUploaded = true;
                            message = "File uploaded successfully!";
                        }
                        else
                        {
                            message = "You Can upload Only jpg file ";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = string.Format("File upload failed: {0}", ex.Message);
                    }
                }
            }
            return Json(new { isUploaded = isUploaded, message = message}, "text/html");
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Creates the folder if needed.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        #endregion

    }
}
