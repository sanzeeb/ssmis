﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DHazards.aspx.cs" Inherits="DIMS.Forms.DHazards" %>


<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

       <link href="~/Content/Form/css/school.css" rel="stylesheet" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="~/Scripts/Form/js/jquery.selectbox-0.5.js"></script>
   

<script type="text/javascript">
    $(document).ready(function () {
        $('#Items').selectbox();
        $('#Items2').selectbox();
        $('#Items3').selectbox();
        $('#Items4').selectbox();
    });
</script>
</head>
<body>
      <form id="form1" runat="server">
    <div class="wrapper">
<div class="head">School & Community  Risk Assessment by Disaster/Hazard:</div>

<div class="ser-box" style="height:50px;">

<span class="sr-title" style="width:100%;">Search by Area Type :</span>
<span class="d-part" style="width:149px;">      <asp:RadioButton ID="RadioButton1" GroupName="area" runat="server" />All</span>
<span class="d-part" style="width:149px;"> <asp:RadioButton ID="RadioButton2" GroupName="area" runat="server" /> Rural</span>
<span class="d-part" style="border-right:none;width:149px;"> <asp:RadioButton ID="RadioButton3" GroupName="area" runat="server" /> Urban</span>
</div>
<div class="ser-box">
<span class="btn-box"><a class="sear-btn" href="#">OK</a></span>
<span class="sr-title">Search by Jurisdiction (Name) :</span>
<span class="d-part">&nbsp;<asp:DropDownList  ID="DropDownListDiv" runat="server">
    </asp:DropDownList>

</span>
<span class="d-part">&nbsp;<asp:DropDownList ID="DropDownListDis" runat="server">
    </asp:DropDownList>
</span>
<span class="d-part" style="border-right:none;">&nbsp;<asp:DropDownList ID="DropDownListUpa" runat="server">
    </asp:DropDownList>
</span>
    <span class="d-part" style="border-right:none;">&nbsp;<asp:DropDownList ID="DropDownListUnion" runat="server">
    </asp:DropDownList>
</span>

</div>


    <div class="ser-box" style="width:920px; height:450px">

  
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
   
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="908px">
            <%--<LocalReport ReportEmbeddedResource="DIMS.Rpts.Report1.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                </DataSources>
            </LocalReport>--%>
        </rsweb:ReportViewer>
        
   
       <%-- <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetGeneralInformationsByStrcType" TypeName="DIMS.Models.GraphModels, DIMS, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null">
            <SelectParameters>
                <asp:Parameter DefaultValue="" Name="division" Type="Int32"></asp:Parameter>
                <asp:Parameter DefaultValue="" Name="district" Type="Int32"></asp:Parameter>
                <asp:Parameter DefaultValue="" Name="upazila" Type="Int32"></asp:Parameter>
                <asp:Parameter DefaultValue="" Name="union" Type="Int32"></asp:Parameter>
                <asp:Parameter DefaultValue="" Name="areaType" Type="String"></asp:Parameter>
            </SelectParameters>
        </asp:ObjectDataSource>--%>
        
   
       <%-- <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetGeneralInformationsByStrcType" TypeName="DIMS.Models.GraphModels, DIMS, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownListDiv" DefaultValue="0" Name="division" PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="DropDownListDis" DefaultValue="0" Name="district" PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="DropDownListUpa" DefaultValue="0" Name="upazila" PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="DropDownListUnion" DefaultValue="0" Name="union" PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="RadioButton1" DefaultValue="All" Name="areaType" PropertyName="Checked" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>--%>
        
   
    </div>
 
        </div>
        </div>
             </form>
</body>
</html>
