USE [DMISDB]
GO
/****** Object:  Table [dbo].[webpages_Roles]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[RoleName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[webpages_Roles] ON
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (4, N'Administrator')
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (8, N'BMEB')
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (9, N'BNFE')
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (7, N'BTEB')
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (5, N'DPE')
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (6, N'DSHE')
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (1, N'Entry Operator')
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (3, N'MIS User')
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (2, N'Super User')
SET IDENTITY_INSERT [dbo].[webpages_Roles] OFF
/****** Object:  Table [dbo].[webpages_OAuthMembership]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_OAuthMembership](
	[Provider] [nvarchar](30) NOT NULL,
	[ProviderUserId] [nvarchar](100) NOT NULL,
	[UserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Provider] ASC,
	[ProviderUserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_Membership]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Membership](
	[UserId] [int] NOT NULL,
	[CreateDate] [datetime] NULL,
	[ConfirmationToken] [nvarchar](128) NULL,
	[IsConfirmed] [bit] NULL,
	[LastPasswordFailureDate] [datetime] NULL,
	[PasswordFailuresSinceLastSuccess] [int] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordChangedDate] [datetime] NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[PasswordVerificationToken] [nvarchar](128) NULL,
	[PasswordVerificationTokenExpirationDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (1, CAST(0x0000A288006E76EF AS DateTime), NULL, 1, CAST(0x0000A2BF00DBA116 AS DateTime), 0, N'AK0FAHq3pH1n+537DrGWuTjPqfeiO4mL3EpEWtR0/TgJC+v3UsMrG3wxcQD/RaajhQ==', CAST(0x0000A2BF00DBBA4D AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (2, CAST(0x0000A288006EBE06 AS DateTime), NULL, 1, NULL, 0, N'AB4JaZ2SS5bZDoDwgBm7B76ZPFqdHaOCO2910zVDQgvcYDeC+sIWXEgyOtt6FJf67w==', CAST(0x0000A288006EBE06 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (3, CAST(0x0000A2880074371B AS DateTime), NULL, 1, NULL, 0, N'AJREfnmet52Ga6MuEvZtXopbsN6YxZ/ySyPnQYgWzeC6/2PWN0UByCXI7/mPsCihVA==', CAST(0x0000A2880074371B AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (4, CAST(0x0000A288008436EA AS DateTime), NULL, 1, NULL, 0, N'AELUVO6q7UYbh0cBH9+v0SNkufYiVCqo+ixWBBzKcyTmrojmH/HOhy0/6AqqHN5y1w==', CAST(0x0000A288008436EA AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (5, CAST(0x0000A28E00B06E76 AS DateTime), NULL, 1, NULL, 0, N'AFZrJClIPWuCtLc8b6hpNaFuMbV3qFGBwHQ1ogl5urWtO98q/b8l9KhaQ6C9xDflaQ==', CAST(0x0000A28E00B06E76 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (6, CAST(0x0000A2A00070E9ED AS DateTime), NULL, 1, NULL, 0, N'AFXcL0zlua5KGV8fVtfAswBqsGYsTI/QY7NIA3sch7tFGd7qpyXnQrqev6gAIXP/Vw==', CAST(0x0000A2A00070E9ED AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (7, CAST(0x0000A2A00071D9BA AS DateTime), NULL, 1, NULL, 0, N'AKOouvbrhbteL2fazFkxhX+uDL+dFQlO9X0GQHbVUmrsM5uMEBf+SESK1Z2QntOj7w==', CAST(0x0000A2A00071D9BA AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (8, CAST(0x0000A2A000807623 AS DateTime), NULL, 1, NULL, 0, N'AO56HZfMg0vSqiMVwPIoa4RcroyBcWLew5Mfv9S+TYuGMlthQFuYsMkz+GXT7dWKew==', CAST(0x0000A2A000807623 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (9, CAST(0x0000A2A000825E8F AS DateTime), NULL, 1, NULL, 0, N'AIIPly8q3MTdABI/ZOB2y5NFCSbQyROloBiOWxHLbYpAYg2mFwXcXizqw2prk1RGyg==', CAST(0x0000A2A000825E8F AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (10, CAST(0x0000A2A0009BA6B5 AS DateTime), NULL, 1, NULL, 0, N'AJ6sFxDB9D/09l8ptSvHqX4k9AFYTFIWEHElKn+huNFeIUYpRLuJApl/6GXgXk3CKw==', CAST(0x0000A2A0009BA6B5 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (11, CAST(0x0000A2A000C14940 AS DateTime), NULL, 1, NULL, 0, N'APBoX+fnIrYEX0gtAQnZwVo246qE4y3YcMbara5UBsOeq9mXV7B5JD/RcFn4pzk+9A==', CAST(0x0000A2A000C14940 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (12, CAST(0x0000A2A700A99199 AS DateTime), NULL, 1, NULL, 0, N'ALWyoqzTfm9Ydi2huzk6Tz7DulYBueu5uJnYm1iUPYOFHf1QsgGNQBRTz409cs0MVA==', CAST(0x0000A2A700A99199 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (13, CAST(0x0000A2AC00CA01DF AS DateTime), NULL, 1, CAST(0x0000A2B400D3D43E AS DateTime), 1, N'AGQzjDZw7lHo/4EluVIZtr3G1gA8k0OzQSd5QXcK/AlR8/tQIMo3mWjCw6HyqkvthA==', CAST(0x0000A2B400D47FF0 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (14, CAST(0x0000A2AC00CEE723 AS DateTime), NULL, 1, NULL, 0, N'AOYawu4P7L8uvAsVS89YvM/O+I8KI2aGovX3azY+H0qPhD9NGHDWmDOxIUi5nL41dA==', CAST(0x0000A2AC00CEE723 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (15, CAST(0x0000A2AD006BB701 AS DateTime), NULL, 1, NULL, 0, N'ACtrLJRdlyu66MJv69LQ/mqEpueIn0r6GFgDlpn0Vm3zsPf9GE4z24Ip5p+egfWJUA==', CAST(0x0000A2AD006BB701 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (16, CAST(0x0000A2B200BB2534 AS DateTime), NULL, 1, NULL, 0, N'AGDcj7ZWo6LUorRRcSPcpSB9WD/fI+B0xfqIpYZOE+vEpxUQBs2lPOffv90MP4oxWQ==', CAST(0x0000A2B200BB2534 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (17, CAST(0x0000A2B200BB4A43 AS DateTime), NULL, 1, NULL, 0, N'AN8cyE6NwIUV+2EGDw+iKD0NlK2P5+A94DBzmVaZeoyxmx4K5NfAXBBfXWwnJCpE/g==', CAST(0x0000A2B800986A2C AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (18, CAST(0x0000A2BA0094EDB7 AS DateTime), NULL, 1, NULL, 0, N'AGfyMimR5upaxPnJclLdtDZ6qitvUMC79CBX9+8BUg8VOV6OlRF/znqsT5fxhyvczw==', CAST(0x0000A2BA0094EDB7 AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (19, CAST(0x0000A2C000697B62 AS DateTime), NULL, 1, NULL, 0, N'ABwehbspPgHu0GeuE+75HyXTwvRtPnH0DjUprSYZEIp6fZlQTNBkbl0hJ3MCftuVQw==', CAST(0x0000A2C000697B62 AS DateTime), N'', NULL, NULL)
/****** Object:  Table [dbo].[UserProfile]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserProfile](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[ContactNumber] [varchar](50) NULL,
	[Address] [varchar](max) NULL,
	[Reference] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[UserProfile] ON
INSERT [dbo].[UserProfile] ([UserId], [UserName], [ContactNumber], [Address], [Reference], [Email]) VALUES (1, N'maha', N'564654', N'Dhaka', N'sdfds', NULL)
INSERT [dbo].[UserProfile] ([UserId], [UserName], [ContactNumber], [Address], [Reference], [Email]) VALUES (5, N'mahabub', N'dfgf', N'fghf', N'asf', NULL)
INSERT [dbo].[UserProfile] ([UserId], [UserName], [ContactNumber], [Address], [Reference], [Email]) VALUES (7, N'mahabub11', N'0192929', N'Dhaka', N'tanvir', NULL)
INSERT [dbo].[UserProfile] ([UserId], [UserName], [ContactNumber], [Address], [Reference], [Email]) VALUES (8, N'mahdshfs', N'3487334543', N'Dhaka', N'teasdsdf', NULL)
INSERT [dbo].[UserProfile] ([UserId], [UserName], [ContactNumber], [Address], [Reference], [Email]) VALUES (10, N'mahabub14353', N'3487334543', N'Dhaka', N'Tanvir', NULL)
INSERT [dbo].[UserProfile] ([UserId], [UserName], [ContactNumber], [Address], [Reference], [Email]) VALUES (11, N'mahabub123456', N'23456789', N'Dhaka', N'Tanvir', NULL)
INSERT [dbo].[UserProfile] ([UserId], [UserName], [ContactNumber], [Address], [Reference], [Email]) VALUES (13, N'Admin2', N'4385893', N'Dhaka', N'dskjfd', NULL)
INSERT [dbo].[UserProfile] ([UserId], [UserName], [ContactNumber], [Address], [Reference], [Email]) VALUES (14, N'demo123', N'3458968', N'Dhaka', N'Admin', NULL)
INSERT [dbo].[UserProfile] ([UserId], [UserName], [ContactNumber], [Address], [Reference], [Email]) VALUES (15, N'Joshi', N'0192384348', N'Dhaka', N'tanvir', NULL)
INSERT [dbo].[UserProfile] ([UserId], [UserName], [ContactNumber], [Address], [Reference], [Email]) VALUES (16, N'Mis1', N'0192928', N'Dhaka', N'Dhaka', NULL)
INSERT [dbo].[UserProfile] ([UserId], [UserName], [ContactNumber], [Address], [Reference], [Email]) VALUES (17, N'DPE1', N'019293874', N'Dhaka', N'tanvir', NULL)
INSERT [dbo].[UserProfile] ([UserId], [UserName], [ContactNumber], [Address], [Reference], [Email]) VALUES (18, N'josi', N'345340953', N'Dhaka', N'Admin', N'josi@yahoo.com')
INSERT [dbo].[UserProfile] ([UserId], [UserName], [ContactNumber], [Address], [Reference], [Email]) VALUES (19, N'tumpa', N'0193247565', N'Dhaka', N'Tanvir', N'emial@yahoo.com')
SET IDENTITY_INSERT [dbo].[UserProfile] OFF
/****** Object:  Table [dbo].[Divisions]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Divisions](
	[DivisionId] [int] IDENTITY(1,1) NOT NULL,
	[DivisionName] [varchar](50) NULL,
	[DivisionCode] [varchar](50) NULL,
	[Longitude] [varchar](50) NULL,
	[Latitude] [varchar](50) NULL,
 CONSTRAINT [PK_Divisions] PRIMARY KEY CLUSTERED 
(
	[DivisionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Divisions] ON
INSERT [dbo].[Divisions] ([DivisionId], [DivisionName], [DivisionCode], [Longitude], [Latitude]) VALUES (2, N'Dhaka', N'02', N'90.412674', N'23.703553')
INSERT [dbo].[Divisions] ([DivisionId], [DivisionName], [DivisionCode], [Longitude], [Latitude]) VALUES (3, N'Sylhet', N'56', N'91.871796', N'24.868828')
INSERT [dbo].[Divisions] ([DivisionId], [DivisionName], [DivisionCode], [Longitude], [Latitude]) VALUES (4, N'Chittagong', N'03', N'91.851196', N'22.268086')
INSERT [dbo].[Divisions] ([DivisionId], [DivisionName], [DivisionCode], [Longitude], [Latitude]) VALUES (5, N'Rajshahi', N'09', N'88.599243', N'24.316398')
INSERT [dbo].[Divisions] ([DivisionId], [DivisionName], [DivisionCode], [Longitude], [Latitude]) VALUES (6, N'Khulna', N'06', N'89.549503', N'22.845489')
INSERT [dbo].[Divisions] ([DivisionId], [DivisionName], [DivisionCode], [Longitude], [Latitude]) VALUES (7, N'Barisal', N'07', N'90.346069', N'22.679916')
INSERT [dbo].[Divisions] ([DivisionId], [DivisionName], [DivisionCode], [Longitude], [Latitude]) VALUES (8, N'Rangpur', N'08', N'89.260712', N'25.762793')
SET IDENTITY_INSERT [dbo].[Divisions] OFF
/****** Object:  Table [dbo].[Hazards]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Hazards](
	[HazardId] [int] IDENTITY(1,1) NOT NULL,
	[HazardName] [varchar](50) NULL,
	[SeriousInjuriesOrDeath] [varchar](50) NULL,
	[DamageToBuilding] [varchar](50) NULL,
	[Shelter] [varchar](50) NULL,
	[ComunicationsIntrruption] [varchar](50) NULL,
	[RoadAndTransportDamage] [varchar](50) NULL,
	[WaterAid] [varchar](50) NULL,
	[Sanitation] [varchar](50) NULL,
	[SchoolClosures] [varchar](50) NULL,
	[SchoolAttandent] [varchar](50) NULL,
	[DropOut] [varchar](50) NULL,
	[SafeLearningId] [int] NULL,
 CONSTRAINT [PK_Hazards] PRIMARY KEY CLUSTERED 
(
	[HazardId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[webpages_UsersInRoles]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_UsersInRoles](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (1, 4)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (13, 3)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (14, 1)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (15, 4)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (16, 3)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (17, 5)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (18, 1)
INSERT [dbo].[webpages_UsersInRoles] ([UserId], [RoleId]) VALUES (19, 1)
/****** Object:  Table [dbo].[AuditTrail]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AuditTrail](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[LogMessage] [varchar](50) NULL,
	[EventRecordTime] [datetime] NULL,
	[Comments] [varchar](50) NULL,
	[Section] [varchar](50) NULL,
 CONSTRAINT [PK_AuditTrail] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AuditTrail] ON
INSERT [dbo].[AuditTrail] ([LogId], [UserId], [LogMessage], [EventRecordTime], [Comments], [Section]) VALUES (1, 1, N'xv', CAST(0x0000A0FC00000000 AS DateTime), N'dgf', N's')
SET IDENTITY_INSERT [dbo].[AuditTrail] OFF
/****** Object:  Table [dbo].[RiskReductionResilienceEducation]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RiskReductionResilienceEducation](
	[RiskResilienceId] [int] IDENTITY(1,1) NOT NULL,
	[DisasterRelatedTraining] [varchar](50) NULL,
	[TrainingOnName] [varchar](50) NULL,
	[TrainingForWhom] [varchar](50) NULL,
	[HowManyTeacherAttended] [varchar](50) NULL,
	[SmcMemberAttented] [varchar](50) NULL,
	[StudentAttented] [varchar](50) NULL,
	[RegularCurriculum] [varchar](50) NULL,
	[FromExtraCurriculum] [varchar](50) NULL,
	[SchoolAssemblies] [varchar](50) NULL,
	[AfterSchoolClubs] [varchar](50) NULL,
	[HowStudentAwareAboutDisaster] [varchar](50) NULL,
	[DrilFacility] [varchar](50) NULL,
	[OtherSource] [varchar](50) NULL,
	[DomainStatus] [int] NULL,
	[userId] [int] NULL,
	[InstituteId] [varchar](50) NULL,
 CONSTRAINT [PK_RiskReductionResilienceEducation] PRIMARY KEY CLUSTERED 
(
	[RiskResilienceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[RiskReductionResilienceEducation] ON
INSERT [dbo].[RiskReductionResilienceEducation] ([RiskResilienceId], [DisasterRelatedTraining], [TrainingOnName], [TrainingForWhom], [HowManyTeacherAttended], [SmcMemberAttented], [StudentAttented], [RegularCurriculum], [FromExtraCurriculum], [SchoolAssemblies], [AfterSchoolClubs], [HowStudentAwareAboutDisaster], [DrilFacility], [OtherSource], [DomainStatus], [userId], [InstituteId]) VALUES (8, N'yes', N'Fire', NULL, N'20', N'10', N'120', N'From regular curriculum', N'From extra curricular activity', N'From school assemblies', N'From after-school clubs', N'some', N'more', NULL, 1, 1, N'56-90-89-34-544612')
INSERT [dbo].[RiskReductionResilienceEducation] ([RiskResilienceId], [DisasterRelatedTraining], [TrainingOnName], [TrainingForWhom], [HowManyTeacherAttended], [SmcMemberAttented], [StudentAttented], [RegularCurriculum], [FromExtraCurriculum], [SchoolAssemblies], [AfterSchoolClubs], [HowStudentAwareAboutDisaster], [DrilFacility], [OtherSource], [DomainStatus], [userId], [InstituteId]) VALUES (9, N'yes', N'fire', NULL, N'23', N'1', N'2', N'From regular curriculum', N'From extra curricular activity', N'From school assemblies', N'From after-school clubs', N'some', N'twice', NULL, 1, 1, N'56-90-89-34-544615')
INSERT [dbo].[RiskReductionResilienceEducation] ([RiskResilienceId], [DisasterRelatedTraining], [TrainingOnName], [TrainingForWhom], [HowManyTeacherAttended], [SmcMemberAttented], [StudentAttented], [RegularCurriculum], [FromExtraCurriculum], [SchoolAssemblies], [AfterSchoolClubs], [HowStudentAwareAboutDisaster], [DrilFacility], [OtherSource], [DomainStatus], [userId], [InstituteId]) VALUES (10, N'yes', N'Test', NULL, N'1', N'2', N'2', N'From regular curriculum', N'From extra curricular activity', N'From school assemblies', N'From after-school clubs', N'some', N'twice', NULL, 1, 1, N'56-90-89-34-544618')
INSERT [dbo].[RiskReductionResilienceEducation] ([RiskResilienceId], [DisasterRelatedTraining], [TrainingOnName], [TrainingForWhom], [HowManyTeacherAttended], [SmcMemberAttented], [StudentAttented], [RegularCurriculum], [FromExtraCurriculum], [SchoolAssemblies], [AfterSchoolClubs], [HowStudentAwareAboutDisaster], [DrilFacility], [OtherSource], [DomainStatus], [userId], [InstituteId]) VALUES (11, N'yes', N'fire', NULL, N'20', N'12', N'200', N'From regular curriculum', N'From extra curricular activity', N'From school assemblies', N'From after-school clubs', N'minimal', N'once', NULL, 1, 1, N'02-44-96-54-315677')
INSERT [dbo].[RiskReductionResilienceEducation] ([RiskResilienceId], [DisasterRelatedTraining], [TrainingOnName], [TrainingForWhom], [HowManyTeacherAttended], [SmcMemberAttented], [StudentAttented], [RegularCurriculum], [FromExtraCurriculum], [SchoolAssemblies], [AfterSchoolClubs], [HowStudentAwareAboutDisaster], [DrilFacility], [OtherSource], [DomainStatus], [userId], [InstituteId]) VALUES (12, N'yes', N'', NULL, N'', N'', N'', N'From regular curriculum', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, N'02-44-96-54-344468')
INSERT [dbo].[RiskReductionResilienceEducation] ([RiskResilienceId], [DisasterRelatedTraining], [TrainingOnName], [TrainingForWhom], [HowManyTeacherAttended], [SmcMemberAttented], [StudentAttented], [RegularCurriculum], [FromExtraCurriculum], [SchoolAssemblies], [AfterSchoolClubs], [HowStudentAwareAboutDisaster], [DrilFacility], [OtherSource], [DomainStatus], [userId], [InstituteId]) VALUES (13, N'yes', N'Fire', NULL, N'20', N'55', N'500', N'From regular curriculum', N'From extra curricular activity', NULL, N'From after-school clubs', N'some', N'once', NULL, 1, 19, N'02-44-96-54-545567')
SET IDENTITY_INSERT [dbo].[RiskReductionResilienceEducation] OFF
/****** Object:  Table [dbo].[DomainDisasterManagement]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DomainDisasterManagement](
	[DisasterManagementId] [int] IDENTITY(1,1) NOT NULL,
	[CatchmentAreaForTemporaryLearning] [varchar](50) NULL,
	[CatchmentAreaForTeacherBackup] [varchar](50) NULL,
	[AccessibleEmergencyFund] [varchar](50) NULL,
	[ContingencyPlan] [varchar](50) NULL,
	[DomainStatus] [int] NULL,
	[userId] [int] NULL,
	[InstituteId] [varchar](50) NULL,
	[DisasterCount] [int] NULL,
 CONSTRAINT [PK_DomainDisasterManagement] PRIMARY KEY CLUSTERED 
(
	[DisasterManagementId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DomainDisasterManagement] ON
INSERT [dbo].[DomainDisasterManagement] ([DisasterManagementId], [CatchmentAreaForTemporaryLearning], [CatchmentAreaForTeacherBackup], [AccessibleEmergencyFund], [ContingencyPlan], [DomainStatus], [userId], [InstituteId], [DisasterCount]) VALUES (19, N'no', N'yes', N'no', N'yes', 1, 5, NULL, NULL)
INSERT [dbo].[DomainDisasterManagement] ([DisasterManagementId], [CatchmentAreaForTemporaryLearning], [CatchmentAreaForTeacherBackup], [AccessibleEmergencyFund], [ContingencyPlan], [DomainStatus], [userId], [InstituteId], [DisasterCount]) VALUES (20, N'no', N'yes', N'yes', N'yes', 1, 5, NULL, NULL)
INSERT [dbo].[DomainDisasterManagement] ([DisasterManagementId], [CatchmentAreaForTemporaryLearning], [CatchmentAreaForTeacherBackup], [AccessibleEmergencyFund], [ContingencyPlan], [DomainStatus], [userId], [InstituteId], [DisasterCount]) VALUES (22, N'no', N'yes', N'no', N'yes', 1, 1, N'56-90-89-34-544612', 3)
INSERT [dbo].[DomainDisasterManagement] ([DisasterManagementId], [CatchmentAreaForTemporaryLearning], [CatchmentAreaForTeacherBackup], [AccessibleEmergencyFund], [ContingencyPlan], [DomainStatus], [userId], [InstituteId], [DisasterCount]) VALUES (23, N'no', N'yes', N'no', N'no', 1, 1, N'56-90-89-34-544615', NULL)
INSERT [dbo].[DomainDisasterManagement] ([DisasterManagementId], [CatchmentAreaForTemporaryLearning], [CatchmentAreaForTeacherBackup], [AccessibleEmergencyFund], [ContingencyPlan], [DomainStatus], [userId], [InstituteId], [DisasterCount]) VALUES (24, N'yes', N'no', N'yes', N'yes', 1, 1, N'56-90-89-34-544618', NULL)
INSERT [dbo].[DomainDisasterManagement] ([DisasterManagementId], [CatchmentAreaForTemporaryLearning], [CatchmentAreaForTeacherBackup], [AccessibleEmergencyFund], [ContingencyPlan], [DomainStatus], [userId], [InstituteId], [DisasterCount]) VALUES (25, N'yes', N'no', N'yes', N'yes', 1, 1, N'02-44-96-54-315677', 3)
INSERT [dbo].[DomainDisasterManagement] ([DisasterManagementId], [CatchmentAreaForTemporaryLearning], [CatchmentAreaForTeacherBackup], [AccessibleEmergencyFund], [ContingencyPlan], [DomainStatus], [userId], [InstituteId], [DisasterCount]) VALUES (26, N'no', N'yes', NULL, NULL, 1, 14, N'02-44-96-54-344468', NULL)
INSERT [dbo].[DomainDisasterManagement] ([DisasterManagementId], [CatchmentAreaForTemporaryLearning], [CatchmentAreaForTeacherBackup], [AccessibleEmergencyFund], [ContingencyPlan], [DomainStatus], [userId], [InstituteId], [DisasterCount]) VALUES (27, N'yes', N'yes', N'no', N'no', 1, 19, N'02-44-96-54-545567', NULL)
SET IDENTITY_INSERT [dbo].[DomainDisasterManagement] OFF
/****** Object:  Table [dbo].[DomainSafeLearning]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DomainSafeLearning](
	[SafeLearningId] [int] IDENTITY(1,1) NOT NULL,
	[TotalBuildingStructer] [varchar](50) NULL,
	[TotalKaccaBuildingStructer] [varchar](50) NULL,
	[TotalPaccaBuildingStructer] [varchar](50) NULL,
	[TotalSemiPaccaBuildingStructer] [varchar](50) NULL,
	[TotalUnsedBuildingStructer] [varchar](50) NULL,
	[TotalClassRoom] [varchar](50) NULL,
	[TotalKaccaClassRoom] [varchar](50) NULL,
	[TotalSemiPaccaClassRoom] [varchar](50) NULL,
	[TotalPaccaClassRoom] [varchar](50) NULL,
	[TotalUnsedClassRoom] [varchar](50) NULL,
	[SourceOfDrinkingWater] [varchar](150) NULL,
	[OwnDrinkingSource] [varchar](50) NULL,
	[SafeDrinkingSource] [varchar](50) NULL,
	[SourceOfWaterIfTubewellArsenicFree] [varchar](50) NULL,
	[SourceOfWaterIfTubewellFunctional] [varchar](50) NULL,
	[SourceOfWaterChildFriendly] [varchar](50) NULL,
	[SourceOfWaterDisableFriendly] [varchar](50) NULL,
	[SourceOfWaterFunctionDuringDisaster] [varchar](50) NULL,
	[SourceOfWaterAwayFromSchool] [varchar](50) NULL,
	[LatrineFacilityForStudent] [varchar](50) NULL,
	[LatrineForMaleStudent] [varchar](50) NULL,
	[LatrineForFemaleStudent] [varchar](50) NULL,
	[LatrineFacilityForTeacher] [varchar](50) NULL,
	[LatrineFacilityForMaleTeacher] [varchar](50) NULL,
	[LatrineFacilityForFeMaleTeacher] [varchar](50) NULL,
	[LatrineRegularMaintain] [varchar](50) NULL,
	[LatrineChildAccessiblePhysicalDisabilties] [varchar](50) NULL,
	[LatrineFemaleCondtion] [varchar](50) NULL,
	[LatrineMaleCondition] [varchar](50) NULL,
	[LatrinePhysicalDisabilties] [varchar](50) NULL,
	[DomainStatus] [int] NULL,
	[LatrineFacilityFunctional] [varchar](50) NULL,
	[userId] [int] NULL,
	[InstituteId] [varchar](50) NULL,
 CONSTRAINT [PK_DomainSafeLearning] PRIMARY KEY CLUSTERED 
(
	[SafeLearningId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DomainSafeLearning] ON
INSERT [dbo].[DomainSafeLearning] ([SafeLearningId], [TotalBuildingStructer], [TotalKaccaBuildingStructer], [TotalPaccaBuildingStructer], [TotalSemiPaccaBuildingStructer], [TotalUnsedBuildingStructer], [TotalClassRoom], [TotalKaccaClassRoom], [TotalSemiPaccaClassRoom], [TotalPaccaClassRoom], [TotalUnsedClassRoom], [SourceOfDrinkingWater], [OwnDrinkingSource], [SafeDrinkingSource], [SourceOfWaterIfTubewellArsenicFree], [SourceOfWaterIfTubewellFunctional], [SourceOfWaterChildFriendly], [SourceOfWaterDisableFriendly], [SourceOfWaterFunctionDuringDisaster], [SourceOfWaterAwayFromSchool], [LatrineFacilityForStudent], [LatrineForMaleStudent], [LatrineForFemaleStudent], [LatrineFacilityForTeacher], [LatrineFacilityForMaleTeacher], [LatrineFacilityForFeMaleTeacher], [LatrineRegularMaintain], [LatrineChildAccessiblePhysicalDisabilties], [LatrineFemaleCondtion], [LatrineMaleCondition], [LatrinePhysicalDisabilties], [DomainStatus], [LatrineFacilityFunctional], [userId], [InstituteId]) VALUES (13, N'2', N'11', N'1', N'1', N'1', N'11', N'1', N'11', N'1', N'11', NULL, N'yes', N'yes', N'yes', N'no', N'yes', N'no', N'yes', NULL, NULL, N'1', N'1', N'1', N'11', N'1', N'yes', N'yes', N'acceptable', N'poor', N'yes', 1, NULL, 1, N'56-90-89-34-544612')
INSERT [dbo].[DomainSafeLearning] ([SafeLearningId], [TotalBuildingStructer], [TotalKaccaBuildingStructer], [TotalPaccaBuildingStructer], [TotalSemiPaccaBuildingStructer], [TotalUnsedBuildingStructer], [TotalClassRoom], [TotalKaccaClassRoom], [TotalSemiPaccaClassRoom], [TotalPaccaClassRoom], [TotalUnsedClassRoom], [SourceOfDrinkingWater], [OwnDrinkingSource], [SafeDrinkingSource], [SourceOfWaterIfTubewellArsenicFree], [SourceOfWaterIfTubewellFunctional], [SourceOfWaterChildFriendly], [SourceOfWaterDisableFriendly], [SourceOfWaterFunctionDuringDisaster], [SourceOfWaterAwayFromSchool], [LatrineFacilityForStudent], [LatrineForMaleStudent], [LatrineForFemaleStudent], [LatrineFacilityForTeacher], [LatrineFacilityForMaleTeacher], [LatrineFacilityForFeMaleTeacher], [LatrineRegularMaintain], [LatrineChildAccessiblePhysicalDisabilties], [LatrineFemaleCondtion], [LatrineMaleCondition], [LatrinePhysicalDisabilties], [DomainStatus], [LatrineFacilityFunctional], [userId], [InstituteId]) VALUES (14, N'3', N'33', N'33', N'3', N'3', N'33', N'3', N'33', N'33', N'33', N'', N'no', N'yes', N'yes', N'no', N'yes', N'yes', N'no', NULL, NULL, N'6', N'3', N'88', N'8', N'88', N'no', N'yes', N'poor', N'acceptable', N'yes', 1, NULL, 1, N'56-90-89-34-544615')
INSERT [dbo].[DomainSafeLearning] ([SafeLearningId], [TotalBuildingStructer], [TotalKaccaBuildingStructer], [TotalPaccaBuildingStructer], [TotalSemiPaccaBuildingStructer], [TotalUnsedBuildingStructer], [TotalClassRoom], [TotalKaccaClassRoom], [TotalSemiPaccaClassRoom], [TotalPaccaClassRoom], [TotalUnsedClassRoom], [SourceOfDrinkingWater], [OwnDrinkingSource], [SafeDrinkingSource], [SourceOfWaterIfTubewellArsenicFree], [SourceOfWaterIfTubewellFunctional], [SourceOfWaterChildFriendly], [SourceOfWaterDisableFriendly], [SourceOfWaterFunctionDuringDisaster], [SourceOfWaterAwayFromSchool], [LatrineFacilityForStudent], [LatrineForMaleStudent], [LatrineForFemaleStudent], [LatrineFacilityForTeacher], [LatrineFacilityForMaleTeacher], [LatrineFacilityForFeMaleTeacher], [LatrineRegularMaintain], [LatrineChildAccessiblePhysicalDisabilties], [LatrineFemaleCondtion], [LatrineMaleCondition], [LatrinePhysicalDisabilties], [DomainStatus], [LatrineFacilityFunctional], [userId], [InstituteId]) VALUES (15, N'1', N'3', N'3', N'33', N'33', N'33', N'3', N'33', N'33', N'3', N'10', N'yes', N'no', N'yes', N'no', N'yes', N'yes', N'no', NULL, NULL, N'11', N'1', N'14', N'1', N'13', N'yes', N'yes', N'poor', N'acceptable', N'yes', 1, NULL, 1, N'56-90-89-34-544618')
INSERT [dbo].[DomainSafeLearning] ([SafeLearningId], [TotalBuildingStructer], [TotalKaccaBuildingStructer], [TotalPaccaBuildingStructer], [TotalSemiPaccaBuildingStructer], [TotalUnsedBuildingStructer], [TotalClassRoom], [TotalKaccaClassRoom], [TotalSemiPaccaClassRoom], [TotalPaccaClassRoom], [TotalUnsedClassRoom], [SourceOfDrinkingWater], [OwnDrinkingSource], [SafeDrinkingSource], [SourceOfWaterIfTubewellArsenicFree], [SourceOfWaterIfTubewellFunctional], [SourceOfWaterChildFriendly], [SourceOfWaterDisableFriendly], [SourceOfWaterFunctionDuringDisaster], [SourceOfWaterAwayFromSchool], [LatrineFacilityForStudent], [LatrineForMaleStudent], [LatrineForFemaleStudent], [LatrineFacilityForTeacher], [LatrineFacilityForMaleTeacher], [LatrineFacilityForFeMaleTeacher], [LatrineRegularMaintain], [LatrineChildAccessiblePhysicalDisabilties], [LatrineFemaleCondtion], [LatrineMaleCondition], [LatrinePhysicalDisabilties], [DomainStatus], [LatrineFacilityFunctional], [userId], [InstituteId]) VALUES (16, N'2', N'22', N'23', N'3', N'22', N'2', N'22', N'2', N'22', N'22', N'23', N'yes', N'no', N'yes', N'no', N'yes', N'yes', N'no', NULL, NULL, N'2', N'2', N'2', N'2', N'2', N'no', N'no', N'acceptable', N'poor', N'yes', 1, NULL, 1, N'02-44-96-54-315677')
INSERT [dbo].[DomainSafeLearning] ([SafeLearningId], [TotalBuildingStructer], [TotalKaccaBuildingStructer], [TotalPaccaBuildingStructer], [TotalSemiPaccaBuildingStructer], [TotalUnsedBuildingStructer], [TotalClassRoom], [TotalKaccaClassRoom], [TotalSemiPaccaClassRoom], [TotalPaccaClassRoom], [TotalUnsedClassRoom], [SourceOfDrinkingWater], [OwnDrinkingSource], [SafeDrinkingSource], [SourceOfWaterIfTubewellArsenicFree], [SourceOfWaterIfTubewellFunctional], [SourceOfWaterChildFriendly], [SourceOfWaterDisableFriendly], [SourceOfWaterFunctionDuringDisaster], [SourceOfWaterAwayFromSchool], [LatrineFacilityForStudent], [LatrineForMaleStudent], [LatrineForFemaleStudent], [LatrineFacilityForTeacher], [LatrineFacilityForMaleTeacher], [LatrineFacilityForFeMaleTeacher], [LatrineRegularMaintain], [LatrineChildAccessiblePhysicalDisabilties], [LatrineFemaleCondtion], [LatrineMaleCondition], [LatrinePhysicalDisabilties], [DomainStatus], [LatrineFacilityFunctional], [userId], [InstituteId]) VALUES (17, N'9', N'3', N'5', N'1', N'', N'8', N'2', N'1', N'5', N'', N'56', N'yes', N'no', N'no', N'yes', N'yes', N'no', N'yes', NULL, NULL, N'', N'', N'', N'', N'', NULL, N'yes', N'acceptable', N'good', N'yes', 1, NULL, 1, N'02-44-96-54-344468')
INSERT [dbo].[DomainSafeLearning] ([SafeLearningId], [TotalBuildingStructer], [TotalKaccaBuildingStructer], [TotalPaccaBuildingStructer], [TotalSemiPaccaBuildingStructer], [TotalUnsedBuildingStructer], [TotalClassRoom], [TotalKaccaClassRoom], [TotalSemiPaccaClassRoom], [TotalPaccaClassRoom], [TotalUnsedClassRoom], [SourceOfDrinkingWater], [OwnDrinkingSource], [SafeDrinkingSource], [SourceOfWaterIfTubewellArsenicFree], [SourceOfWaterIfTubewellFunctional], [SourceOfWaterChildFriendly], [SourceOfWaterDisableFriendly], [SourceOfWaterFunctionDuringDisaster], [SourceOfWaterAwayFromSchool], [LatrineFacilityForStudent], [LatrineForMaleStudent], [LatrineForFemaleStudent], [LatrineFacilityForTeacher], [LatrineFacilityForMaleTeacher], [LatrineFacilityForFeMaleTeacher], [LatrineRegularMaintain], [LatrineChildAccessiblePhysicalDisabilties], [LatrineFemaleCondtion], [LatrineMaleCondition], [LatrinePhysicalDisabilties], [DomainStatus], [LatrineFacilityFunctional], [userId], [InstituteId]) VALUES (18, N'16', N'6', N'3', N'7', N'', N'15', N'5', N'5', N'5', N'', N'', N'no', N'no', N'yes', N'yes', N'yes', N'no', N'yes', N'20', N'12', N'6', N'6', N'3', N'6', N'66', N'yes', N'no', N'poor', N'acceptable', N'yes', 1, N'20', 19, N'02-44-96-54-545567')
SET IDENTITY_INSERT [dbo].[DomainSafeLearning] OFF
/****** Object:  Table [dbo].[Districts]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Districts](
	[DistrictId] [int] IDENTITY(1,1) NOT NULL,
	[DistrictName] [varchar](50) NULL,
	[DistrictCode] [varchar](50) NULL,
	[DivisionId] [int] NULL,
	[Longitiude] [varchar](50) NULL,
	[Latitude] [varchar](50) NULL,
 CONSTRAINT [PK_Districts] PRIMARY KEY CLUSTERED 
(
	[DistrictId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Districts] ON
INSERT [dbo].[Districts] ([DistrictId], [DistrictName], [DistrictCode], [DivisionId], [Longitiude], [Latitude]) VALUES (1, N'Mymunshing', N'44', 2, N'90.41371', N'24.74032')
INSERT [dbo].[Districts] ([DistrictId], [DistrictName], [DistrictCode], [DivisionId], [Longitiude], [Latitude]) VALUES (2, N'Hobigong', N'50', 3, NULL, NULL)
INSERT [dbo].[Districts] ([DistrictId], [DistrictName], [DistrictCode], [DivisionId], [Longitiude], [Latitude]) VALUES (3, N'Molobibazar', N'90', 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Districts] OFF
/****** Object:  Table [dbo].[DisasterHistory]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DisasterHistory](
	[DisasterHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[DisasterManagementId] [int] NULL,
	[DisasterName] [varchar](50) NULL,
	[DisasterDate] [varchar](50) NULL,
	[DisasterImpact] [varchar](50) NULL,
	[DayRemainSchoolClose] [varchar](50) NULL,
	[ImpactAttendanceForStudent] [varchar](50) NULL,
	[ImpactAttandanceForTeacher] [varchar](50) NULL,
	[StudentDropout] [varchar](50) NULL,
	[StudentDead] [varchar](50) NULL,
	[BulidingDamage] [varchar](50) NULL,
	[ToiletAffected] [varchar](50) NULL,
	[WasterSourceAffected] [varchar](50) NULL,
	[RoadAffected] [varchar](50) NULL,
	[MaterialDamage] [varchar](50) NULL,
	[RecoveryInitiative] [varchar](50) NULL,
	[UsedShelter] [varchar](50) NULL,
	[NoDayUsedAsShelter] [varchar](50) NULL,
	[AlternativeSchooling] [varchar](50) NULL,
	[AnyAid] [varchar](50) NULL,
	[WhereFromAidTaken] [varchar](50) NULL,
	[AidType] [varchar](50) NULL,
	[HaveWarningSystem] [varchar](50) NULL,
	[DidUnderstoodWarningMessage] [varchar](50) NULL,
	[DomainStatus] [int] NULL,
	[HistoryNo] [varchar](20) NULL,
 CONSTRAINT [PK_DisasterHistory] PRIMARY KEY CLUSTERED 
(
	[DisasterHistoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DisasterHistory] ON
INSERT [dbo].[DisasterHistory] ([DisasterHistoryId], [DisasterManagementId], [DisasterName], [DisasterDate], [DisasterImpact], [DayRemainSchoolClose], [ImpactAttendanceForStudent], [ImpactAttandanceForTeacher], [StudentDropout], [StudentDead], [BulidingDamage], [ToiletAffected], [WasterSourceAffected], [RoadAffected], [MaterialDamage], [RecoveryInitiative], [UsedShelter], [NoDayUsedAsShelter], [AlternativeSchooling], [AnyAid], [WhereFromAidTaken], [AidType], [HaveWarningSystem], [DidUnderstoodWarningMessage], [DomainStatus], [HistoryNo]) VALUES (55, 24, N'Tornado', N'2014', N'moderate', N'', N'', NULL, N'', N'67', NULL, NULL, N'no', N'yes', NULL, NULL, NULL, N'34', NULL, NULL, N'', N'', NULL, NULL, NULL, N'1')
INSERT [dbo].[DisasterHistory] ([DisasterHistoryId], [DisasterManagementId], [DisasterName], [DisasterDate], [DisasterImpact], [DayRemainSchoolClose], [ImpactAttendanceForStudent], [ImpactAttandanceForTeacher], [StudentDropout], [StudentDead], [BulidingDamage], [ToiletAffected], [WasterSourceAffected], [RoadAffected], [MaterialDamage], [RecoveryInitiative], [UsedShelter], [NoDayUsedAsShelter], [AlternativeSchooling], [AnyAid], [WhereFromAidTaken], [AidType], [HaveWarningSystem], [DidUnderstoodWarningMessage], [DomainStatus], [HistoryNo]) VALUES (56, 27, N'Tornado', N'2010', N'moderate', N'20', N'200', NULL, N'', N'', N'Slightly Damage', NULL, NULL, N'yes', NULL, NULL, N'no', N'', NULL, N'yes', N'', N'', N'no', NULL, NULL, N'1')
INSERT [dbo].[DisasterHistory] ([DisasterHistoryId], [DisasterManagementId], [DisasterName], [DisasterDate], [DisasterImpact], [DayRemainSchoolClose], [ImpactAttendanceForStudent], [ImpactAttandanceForTeacher], [StudentDropout], [StudentDead], [BulidingDamage], [ToiletAffected], [WasterSourceAffected], [RoadAffected], [MaterialDamage], [RecoveryInitiative], [UsedShelter], [NoDayUsedAsShelter], [AlternativeSchooling], [AnyAid], [WhereFromAidTaken], [AidType], [HaveWarningSystem], [DidUnderstoodWarningMessage], [DomainStatus], [HistoryNo]) VALUES (57, 25, N'FLood', N'', N'minor', N'20', N'20', NULL, N'12', N'13', N'Fully Damage', NULL, NULL, N'no', N'yes', NULL, N'no', N'23', NULL, N'no', N'from1', N'help1', N'no', N'yes', NULL, N'1')
INSERT [dbo].[DisasterHistory] ([DisasterHistoryId], [DisasterManagementId], [DisasterName], [DisasterDate], [DisasterImpact], [DayRemainSchoolClose], [ImpactAttendanceForStudent], [ImpactAttandanceForTeacher], [StudentDropout], [StudentDead], [BulidingDamage], [ToiletAffected], [WasterSourceAffected], [RoadAffected], [MaterialDamage], [RecoveryInitiative], [UsedShelter], [NoDayUsedAsShelter], [AlternativeSchooling], [AnyAid], [WhereFromAidTaken], [AidType], [HaveWarningSystem], [DidUnderstoodWarningMessage], [DomainStatus], [HistoryNo]) VALUES (65, 22, N'Tidal Surge', N'2005', N'Severe', N'20', N'50', NULL, N'4', N'4', N'No/Slightly Damage', N'no', N'yes', N'no', N'yes', N'no', N'yes', N'', N'yes', N'no', N'', N'', N'no', N'yes', NULL, N'1')
INSERT [dbo].[DisasterHistory] ([DisasterHistoryId], [DisasterManagementId], [DisasterName], [DisasterDate], [DisasterImpact], [DayRemainSchoolClose], [ImpactAttendanceForStudent], [ImpactAttandanceForTeacher], [StudentDropout], [StudentDead], [BulidingDamage], [ToiletAffected], [WasterSourceAffected], [RoadAffected], [MaterialDamage], [RecoveryInitiative], [UsedShelter], [NoDayUsedAsShelter], [AlternativeSchooling], [AnyAid], [WhereFromAidTaken], [AidType], [HaveWarningSystem], [DidUnderstoodWarningMessage], [DomainStatus], [HistoryNo]) VALUES (66, 22, N'FLood', N'', N'minor', N'', N'', NULL, N'', N'', N'Slightly Damage', N'yes', N'no', N'yes', N'no', N'yes', N'no', N'', N'yes', N'no', N'', N'', N'yes', N'yes', NULL, N'2')
INSERT [dbo].[DisasterHistory] ([DisasterHistoryId], [DisasterManagementId], [DisasterName], [DisasterDate], [DisasterImpact], [DayRemainSchoolClose], [ImpactAttendanceForStudent], [ImpactAttandanceForTeacher], [StudentDropout], [StudentDead], [BulidingDamage], [ToiletAffected], [WasterSourceAffected], [RoadAffected], [MaterialDamage], [RecoveryInitiative], [UsedShelter], [NoDayUsedAsShelter], [AlternativeSchooling], [AnyAid], [WhereFromAidTaken], [AidType], [HaveWarningSystem], [DidUnderstoodWarningMessage], [DomainStatus], [HistoryNo]) VALUES (67, 22, N'Earth Quick', N'', N'minor', N'', N'', NULL, N'', N'', N'Slightly Damage', N'no', N'yes', N'no', N'yes', N'no', N'yes', N'', N'no', N'yes', N'', N'', N'yes', N'yes', NULL, N'3')
SET IDENTITY_INSERT [dbo].[DisasterHistory] OFF
/****** Object:  Table [dbo].[Upazillas]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Upazillas](
	[UpazillaId] [int] IDENTITY(1,1) NOT NULL,
	[UpazillaName] [varchar](50) NULL,
	[UpazillaCode] [varchar](50) NULL,
	[DistrictId] [int] NULL,
	[Longitiude] [varchar](50) NULL,
	[Latitude] [varchar](50) NULL,
 CONSTRAINT [PK_Upazillas] PRIMARY KEY CLUSTERED 
(
	[UpazillaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Upazillas] ON
INSERT [dbo].[Upazillas] ([UpazillaId], [UpazillaName], [UpazillaCode], [DistrictId], [Longitiude], [Latitude]) VALUES (2, N'Kisorgong', N'96', 1, N'90.785608', N'24.432761')
INSERT [dbo].[Upazillas] ([UpazillaId], [UpazillaName], [UpazillaCode], [DistrictId], [Longitiude], [Latitude]) VALUES (3, N'Upazilla', N'89', 3, N'123.234', N'123.8966')
INSERT [dbo].[Upazillas] ([UpazillaId], [UpazillaName], [UpazillaCode], [DistrictId], [Longitiude], [Latitude]) VALUES (4, N'Upazilla 2', N'54', 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Upazillas] OFF
/****** Object:  Table [dbo].[Unions]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Unions](
	[UnionId] [int] IDENTITY(1,1) NOT NULL,
	[UnionName] [varchar](50) NULL,
	[UnionCode] [varchar](50) NULL,
	[UpazillaId] [int] NULL,
	[Longitiude] [varchar](50) NULL,
	[Latitude] [varchar](50) NULL,
 CONSTRAINT [PK_Unions] PRIMARY KEY CLUSTERED 
(
	[UnionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Unions] ON
INSERT [dbo].[Unions] ([UnionId], [UnionName], [UnionCode], [UpazillaId], [Longitiude], [Latitude]) VALUES (1, N'Josadul Union', N'54678', 2, N'90.79943', N'24.40420')
INSERT [dbo].[Unions] ([UnionId], [UnionName], [UnionCode], [UpazillaId], [Longitiude], [Latitude]) VALUES (2, N'Union 1', N'34', 3, NULL, NULL)
INSERT [dbo].[Unions] ([UnionId], [UnionName], [UnionCode], [UpazillaId], [Longitiude], [Latitude]) VALUES (4, N'Kisorgong Union', N'32', 2, N'123.14456', N'190.54684')
SET IDENTITY_INSERT [dbo].[Unions] OFF
/****** Object:  Table [dbo].[DomainGeneralInformation]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DomainGeneralInformation](
	[GeneralInformationId] [int] IDENTITY(1,1) NOT NULL,
	[InstituteId] [varchar](50) NULL,
	[GobEIIN] [varchar](50) NULL,
	[InstituteType] [varchar](50) NULL,
	[EstabilishmentDate] [varchar](20) NULL,
	[ManagementType] [varchar](50) NULL,
	[InstituteEng] [varchar](50) NULL,
	[InstituteBng] [nvarchar](50) NULL,
	[InstituteAddress] [varchar](50) NULL,
	[HowManyShift] [varchar](50) NULL,
	[StudentMale] [varchar](50) NULL,
	[StudentFemale] [varchar](50) NULL,
	[StudentEthnicMale] [varchar](50) NULL,
	[StudentDisableMale] [varchar](50) NULL,
	[StudentDisableFeMale] [varchar](50) NULL,
	[StudentEthnicFeMale] [varchar](50) NULL,
	[InstitutePhone] [varchar](50) NULL,
	[InstituteFax] [varchar](50) NULL,
	[InstituteMobile] [varchar](50) NULL,
	[InstituteWebUrl] [varchar](50) NULL,
	[BankACNo] [varchar](50) NULL,
	[BankName] [varchar](50) NULL,
	[BankContact] [varchar](50) NULL,
	[BankAddress] [varchar](50) NULL,
	[GpsLongtitude] [varchar](50) NULL,
	[GpsLatitude] [varchar](50) NULL,
	[SchoolArea] [varchar](50) NULL,
	[GraphicalLocation] [varchar](50) NULL,
	[TransportionMode] [varchar](50) NULL,
	[SmcExpiredate] [varchar](50) NULL,
	[SmcPrisedentName] [varchar](50) NULL,
	[SmcPrisedentMobile] [varchar](50) NULL,
	[TeacherMale] [varchar](50) NULL,
	[TeacherFemale] [varchar](50) NULL,
	[VacantTeacherPosition] [varchar](50) NULL,
	[NonEducationalStuff] [varchar](50) NULL,
	[TiffinFacility] [varchar](50) NULL,
	[PlayingGround] [varchar](50) NULL,
	[ElectricityFacillity] [varchar](50) NULL,
	[InternetFacility] [varchar](50) NULL,
	[ComputerLab] [varchar](50) NULL,
	[FirstAid] [varchar](50) NULL,
	[HasFireExtinguiser] [varchar](50) NULL,
	[FunctionalFireExtinguise] [varchar](50) NULL,
	[VolunteerServices] [varchar](50) NULL,
	[AuthorityType] [varchar](50) NULL,
	[BankBranch] [varchar](50) NULL,
	[Altitude] [varchar](50) NULL,
	[InstitudeHouse] [varchar](50) NULL,
	[InstitudeRoad] [varchar](50) NULL,
	[InstitudeSection] [varchar](50) NULL,
	[InstitudeVillage] [varchar](50) NULL,
	[InstitudeDistric] [varchar](50) NULL,
	[InstitudeDivision] [varchar](50) NULL,
	[InstitudeUpazilla] [varchar](50) NULL,
	[DomainStatus] [int] NULL,
	[userId] [int] NULL,
	[InstitudeUnion] [varchar](50) NULL,
	[UnionId] [int] NULL,
	[DivisionId] [int] NULL,
	[UpazillaId] [int] NULL,
	[DistrictId] [int] NULL,
	[Embankment] [varchar](50) NULL,
 CONSTRAINT [PK_doamin_general_information] PRIMARY KEY CLUSTERED 
(
	[GeneralInformationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DomainGeneralInformation] ON
INSERT [dbo].[DomainGeneralInformation] ([GeneralInformationId], [InstituteId], [GobEIIN], [InstituteType], [EstabilishmentDate], [ManagementType], [InstituteEng], [InstituteBng], [InstituteAddress], [HowManyShift], [StudentMale], [StudentFemale], [StudentEthnicMale], [StudentDisableMale], [StudentDisableFeMale], [StudentEthnicFeMale], [InstitutePhone], [InstituteFax], [InstituteMobile], [InstituteWebUrl], [BankACNo], [BankName], [BankContact], [BankAddress], [GpsLongtitude], [GpsLatitude], [SchoolArea], [GraphicalLocation], [TransportionMode], [SmcExpiredate], [SmcPrisedentName], [SmcPrisedentMobile], [TeacherMale], [TeacherFemale], [VacantTeacherPosition], [NonEducationalStuff], [TiffinFacility], [PlayingGround], [ElectricityFacillity], [InternetFacility], [ComputerLab], [FirstAid], [HasFireExtinguiser], [FunctionalFireExtinguise], [VolunteerServices], [AuthorityType], [BankBranch], [Altitude], [InstitudeHouse], [InstitudeRoad], [InstitudeSection], [InstitudeVillage], [InstitudeDistric], [InstitudeDivision], [InstitudeUpazilla], [DomainStatus], [userId], [InstitudeUnion], [UnionId], [DivisionId], [UpazillaId], [DistrictId], [Embankment]) VALUES (25, N'56-90-89-34-544612', N'33167718191', N'Primary', N'Jan  8 2014  2:13PM', N'Non-Government', N'Wali Nawaz Khan Collage', N'বাংলা', NULL, N'1', N'2', N'4', N'1', N'2', N'5', N'2', N'111111111', N'171924131', N'12181916191', N'www.com', N'12181916191', N'Dhaka', N'12181916191', N'Dhaka', N' 	90.78610', N'24.42824', N'Urban', N'Hill Tract', N'Water way', N'2014-01-08', N'Mr.President', N'14171818191', N'2', N'65', N'66', N'6', N'yes', N'yes', N'no', N'no', N'yes', N'yes', N'no', N'2', NULL, N'DPE', N'Branch', NULL, N'1', N'3', N'', N'', N'Mymunshing', N'Dhaka', N'Kisorgong', 1, 1, N'Josadul Union', 1, 2, 2, 1, NULL)
INSERT [dbo].[DomainGeneralInformation] ([GeneralInformationId], [InstituteId], [GobEIIN], [InstituteType], [EstabilishmentDate], [ManagementType], [InstituteEng], [InstituteBng], [InstituteAddress], [HowManyShift], [StudentMale], [StudentFemale], [StudentEthnicMale], [StudentDisableMale], [StudentDisableFeMale], [StudentEthnicFeMale], [InstitutePhone], [InstituteFax], [InstituteMobile], [InstituteWebUrl], [BankACNo], [BankName], [BankContact], [BankAddress], [GpsLongtitude], [GpsLatitude], [SchoolArea], [GraphicalLocation], [TransportionMode], [SmcExpiredate], [SmcPrisedentName], [SmcPrisedentMobile], [TeacherMale], [TeacherFemale], [VacantTeacherPosition], [NonEducationalStuff], [TiffinFacility], [PlayingGround], [ElectricityFacillity], [InternetFacility], [ComputerLab], [FirstAid], [HasFireExtinguiser], [FunctionalFireExtinguise], [VolunteerServices], [AuthorityType], [BankBranch], [Altitude], [InstitudeHouse], [InstitudeRoad], [InstitudeSection], [InstitudeVillage], [InstitudeDistric], [InstitudeDivision], [InstitudeUpazilla], [DomainStatus], [userId], [InstitudeUnion], [UnionId], [DivisionId], [UpazillaId], [DistrictId], [Embankment]) VALUES (28, N'56-90-89-34-544615', N'33167718191', N'NGO School', N'Jan  9 2014 12:00AM', N'Non-Government', N'Sylet1 govt. ideal School', N'বাংলা', NULL, N'2', N'3', N'3', N'3', N'3', N'3', N'2', N'111111111', N'171924131', N'12181916191', N'www.ngo', N'12181916191', N'2', N'12181916191', N'1', N'90.78023', N'24.42938', N'Urban', N'Hill Tract', N'Water way', N'2014-01-09', N'2', N'14171818191', N'2', N'2', N'3', N'4', N'yes', N'no', N'no', N'no', N'no', N'no', N'no', N'', NULL, N'DSHE', N'Bank Branch', NULL, N'3', N'3', N'4', N'', N'Mymunshing', N'Dhaka', N'Kisorgong', 1, 1, N'Josadul Union', 1, 3, 2, 1, NULL)
INSERT [dbo].[DomainGeneralInformation] ([GeneralInformationId], [InstituteId], [GobEIIN], [InstituteType], [EstabilishmentDate], [ManagementType], [InstituteEng], [InstituteBng], [InstituteAddress], [HowManyShift], [StudentMale], [StudentFemale], [StudentEthnicMale], [StudentDisableMale], [StudentDisableFeMale], [StudentEthnicFeMale], [InstitutePhone], [InstituteFax], [InstituteMobile], [InstituteWebUrl], [BankACNo], [BankName], [BankContact], [BankAddress], [GpsLongtitude], [GpsLatitude], [SchoolArea], [GraphicalLocation], [TransportionMode], [SmcExpiredate], [SmcPrisedentName], [SmcPrisedentMobile], [TeacherMale], [TeacherFemale], [VacantTeacherPosition], [NonEducationalStuff], [TiffinFacility], [PlayingGround], [ElectricityFacillity], [InternetFacility], [ComputerLab], [FirstAid], [HasFireExtinguiser], [FunctionalFireExtinguise], [VolunteerServices], [AuthorityType], [BankBranch], [Altitude], [InstitudeHouse], [InstitudeRoad], [InstitudeSection], [InstitudeVillage], [InstitudeDistric], [InstitudeDivision], [InstitudeUpazilla], [DomainStatus], [userId], [InstitudeUnion], [UnionId], [DivisionId], [UpazillaId], [DistrictId], [Embankment]) VALUES (29, N'56-90-89-34-544618', N'33167718191', N'NGO School', N'2014', N'Non-Government', N'Kisorgong Govt Collage', N'বাংলা', NULL, N'1', N'1', N'1', N'1', N'1', N'1', N'1', N'111111111', N'171924131', N'12181916191', N'www.com', N'12181916191', N'Dhaka Bank', N'12181916191', N'Motijil', N'90.77910', N'24.44590', N'Urban', N'Seashore', N'Both', N'2014-01-15', N'Mr.President', N'14171818191', N'1', N'11', N'1', N'11', N'yes', N'no', N'yes', N'no', N'yes', N'yes', N'no', N'', NULL, N'DSHE', N'Bank Branch', NULL, N'', N'', N'', N'', N'Molobibazar', N'Sylet', N'Upazilla', 1, 1, N'Union 1', 1, 2, 2, 1, N'In The Embankment')
INSERT [dbo].[DomainGeneralInformation] ([GeneralInformationId], [InstituteId], [GobEIIN], [InstituteType], [EstabilishmentDate], [ManagementType], [InstituteEng], [InstituteBng], [InstituteAddress], [HowManyShift], [StudentMale], [StudentFemale], [StudentEthnicMale], [StudentDisableMale], [StudentDisableFeMale], [StudentEthnicFeMale], [InstitutePhone], [InstituteFax], [InstituteMobile], [InstituteWebUrl], [BankACNo], [BankName], [BankContact], [BankAddress], [GpsLongtitude], [GpsLatitude], [SchoolArea], [GraphicalLocation], [TransportionMode], [SmcExpiredate], [SmcPrisedentName], [SmcPrisedentMobile], [TeacherMale], [TeacherFemale], [VacantTeacherPosition], [NonEducationalStuff], [TiffinFacility], [PlayingGround], [ElectricityFacillity], [InternetFacility], [ComputerLab], [FirstAid], [HasFireExtinguiser], [FunctionalFireExtinguise], [VolunteerServices], [AuthorityType], [BankBranch], [Altitude], [InstitudeHouse], [InstitudeRoad], [InstitudeSection], [InstitudeVillage], [InstitudeDistric], [InstitudeDivision], [InstitudeUpazilla], [DomainStatus], [userId], [InstitudeUnion], [UnionId], [DivisionId], [UpazillaId], [DistrictId], [Embankment]) VALUES (31, N'02-44-96-54-315677', N'33167718191', N'High School', N'2013', N'Private', N'Green Leaf School', N'Bng', NULL, N'2', N'1', N'1', N'11', N'11', N'11', N'11', N'121617181', N'171924131', N'12181916191', N'21', N'14566878888', N'112', N'12181916191', N'12', N'89.247932', N'24.8976543', N'Rural', N'Plain Land', N'Water way', N'2014', N'p', N'14171818191', N'11', N'1', N'11', N'1', N'no', N'yes', N'no', N'yes', N'no', N'no', N'yes', N'20', NULL, N'BNFE', N'12', NULL, N'', N'', N'', N'', N'Mymunshing', N'Dhaka', N'Kisorgong', 1, 1, N'Josadul Union', 1, 3, 2, 1, N'In The Embankment')
INSERT [dbo].[DomainGeneralInformation] ([GeneralInformationId], [InstituteId], [GobEIIN], [InstituteType], [EstabilishmentDate], [ManagementType], [InstituteEng], [InstituteBng], [InstituteAddress], [HowManyShift], [StudentMale], [StudentFemale], [StudentEthnicMale], [StudentDisableMale], [StudentDisableFeMale], [StudentEthnicFeMale], [InstitutePhone], [InstituteFax], [InstituteMobile], [InstituteWebUrl], [BankACNo], [BankName], [BankContact], [BankAddress], [GpsLongtitude], [GpsLatitude], [SchoolArea], [GraphicalLocation], [TransportionMode], [SmcExpiredate], [SmcPrisedentName], [SmcPrisedentMobile], [TeacherMale], [TeacherFemale], [VacantTeacherPosition], [NonEducationalStuff], [TiffinFacility], [PlayingGround], [ElectricityFacillity], [InternetFacility], [ComputerLab], [FirstAid], [HasFireExtinguiser], [FunctionalFireExtinguise], [VolunteerServices], [AuthorityType], [BankBranch], [Altitude], [InstitudeHouse], [InstitudeRoad], [InstitudeSection], [InstitudeVillage], [InstitudeDistric], [InstitudeDivision], [InstitudeUpazilla], [DomainStatus], [userId], [InstitudeUnion], [UnionId], [DivisionId], [UpazillaId], [DistrictId], [Embankment]) VALUES (32, N'02-44-96-54-344468', N'', N'Kindergarten', N'2010', N'Private', N'Ideal high school', N'', NULL, NULL, N'1', N'4', N'0', N'0', N'0', N'0', N'', N'', N'', N'', N'', N'', N'', N'', N'90.373363', N'23.860272', N'Urban', N'Marsh', N'Water way', N'', N'', N'', N'2', N'1', N'', N'4', NULL, N'yes', N'yes', NULL, NULL, N'no', N'yes', N'45', NULL, N'BMEB', N'', NULL, N'', N'', N'', N'', N'Mymunshing', N'Dhaka', N'Kisorgong', 1, 14, N'Josadul Union', 1, 2, 2, 1, N'In The Embankment')
INSERT [dbo].[DomainGeneralInformation] ([GeneralInformationId], [InstituteId], [GobEIIN], [InstituteType], [EstabilishmentDate], [ManagementType], [InstituteEng], [InstituteBng], [InstituteAddress], [HowManyShift], [StudentMale], [StudentFemale], [StudentEthnicMale], [StudentDisableMale], [StudentDisableFeMale], [StudentEthnicFeMale], [InstitutePhone], [InstituteFax], [InstituteMobile], [InstituteWebUrl], [BankACNo], [BankName], [BankContact], [BankAddress], [GpsLongtitude], [GpsLatitude], [SchoolArea], [GraphicalLocation], [TransportionMode], [SmcExpiredate], [SmcPrisedentName], [SmcPrisedentMobile], [TeacherMale], [TeacherFemale], [VacantTeacherPosition], [NonEducationalStuff], [TiffinFacility], [PlayingGround], [ElectricityFacillity], [InternetFacility], [ComputerLab], [FirstAid], [HasFireExtinguiser], [FunctionalFireExtinguise], [VolunteerServices], [AuthorityType], [BankBranch], [Altitude], [InstitudeHouse], [InstitudeRoad], [InstitudeSection], [InstitudeVillage], [InstitudeDistric], [InstitudeDivision], [InstitudeUpazilla], [DomainStatus], [userId], [InstitudeUnion], [UnionId], [DivisionId], [UpazillaId], [DistrictId], [Embankment]) VALUES (38, N'02-44-96-54-545567', N'01233444444', N'Primary', N'2008', N'Non-Government', N'Hazi Ahmed Ali High school', N'বাংলা', NULL, N'1', N'500', N'100', N'0', N'0', N'0', N'0', N'', N'', N'', N'', N'', N'', N'', N'', N'89.270063', N'24.650006', N'Rural', N'Marsh', N'Water way', N'2013', N'', N'', N'20', N'20', N'', N'12', N'no', N'yes', N'no', N'yes', N'no', N'yes', N'no', N'', NULL, N'DPE', N'', NULL, N'', N'', N'', N'Gosala', N'Mymunshing', N'Dhaka', N'Kisorgong', 1, 19, N'Josadul Union', 1, 2, 2, 1, N'On The Embankment')
SET IDENTITY_INSERT [dbo].[DomainGeneralInformation] OFF
/****** Object:  Table [dbo].[Domains]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Domains](
	[DomainId] [int] IDENTITY(1,1) NOT NULL,
	[GeneralInformationId] [int] NULL,
	[SafeLearningId] [int] NULL,
	[DisasterManagementId] [int] NULL,
	[RiskResilienceId] [int] NULL,
	[UserId] [int] NULL,
	[DomainStatus] [int] NULL,
	[Domaindate] [date] NULL,
 CONSTRAINT [PK_Doamins] PRIMARY KEY CLUSTERED 
(
	[DomainId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Domains] ON
INSERT [dbo].[Domains] ([DomainId], [GeneralInformationId], [SafeLearningId], [DisasterManagementId], [RiskResilienceId], [UserId], [DomainStatus], [Domaindate]) VALUES (6, 25, 13, 22, 8, 1, 1, CAST(0x08380B00 AS Date))
INSERT [dbo].[Domains] ([DomainId], [GeneralInformationId], [SafeLearningId], [DisasterManagementId], [RiskResilienceId], [UserId], [DomainStatus], [Domaindate]) VALUES (7, 28, 14, 23, 9, 1, 1, CAST(0x0C380B00 AS Date))
INSERT [dbo].[Domains] ([DomainId], [GeneralInformationId], [SafeLearningId], [DisasterManagementId], [RiskResilienceId], [UserId], [DomainStatus], [Domaindate]) VALUES (8, 29, 15, 24, 10, 1, 1, CAST(0x10380B00 AS Date))
INSERT [dbo].[Domains] ([DomainId], [GeneralInformationId], [SafeLearningId], [DisasterManagementId], [RiskResilienceId], [UserId], [DomainStatus], [Domaindate]) VALUES (9, 31, 16, 25, 11, 1, 1, CAST(0x15380B00 AS Date))
INSERT [dbo].[Domains] ([DomainId], [GeneralInformationId], [SafeLearningId], [DisasterManagementId], [RiskResilienceId], [UserId], [DomainStatus], [Domaindate]) VALUES (10, 32, 17, 26, 12, 14, 1, CAST(0x1A380B00 AS Date))
INSERT [dbo].[Domains] ([DomainId], [GeneralInformationId], [SafeLearningId], [DisasterManagementId], [RiskResilienceId], [UserId], [DomainStatus], [Domaindate]) VALUES (11, 38, 18, 27, 13, 19, 1, CAST(0x1B380B00 AS Date))
SET IDENTITY_INSERT [dbo].[Domains] OFF
/****** Object:  Table [dbo].[CheckBoxs]    Script Date: 02/06/2014 14:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CheckBoxs](
	[ChkId] [int] IDENTITY(1,1) NOT NULL,
	[InstituteId] [varchar](50) NULL,
	[UserId] [int] NULL,
	[DomainId] [int] NULL,
	[QuestionId] [varchar](50) NULL,
	[ChkValue] [varchar](50) NULL,
	[ChkName] [varchar](50) NULL,
	[DisasterManagementId] [int] NULL,
	[GeneralInformationId] [int] NULL,
	[SafeLearningId] [int] NULL,
	[RiskResilienceId] [int] NULL,
	[HazardsName] [varchar](50) NULL,
	[ImpactName] [varchar](50) NULL,
 CONSTRAINT [PK_CheckBoxs] PRIMARY KEY CLUSTERED 
(
	[ChkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[CheckBoxs] ON
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (395, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Primary', N'Primary', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (396, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'17.1', N'NGO School', N'NGOSchool', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (397, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Madrasa', N'Madrasa', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (398, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Flood Communication interruption', N'FloodCommunicationinterruption', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (399, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Cyclone Serious injuries or deaths', N'CycloneSeriousinjuriesordeaths', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (400, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Earth Quack Used as Shelter', N'EarthQuackUsedasShelter', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (401, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'19.0', N'River Bank Erosion Serious injuries or deaths', N'RiverBankErosionSeriousinjuriesordeaths', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (402, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'19.0', N'River Bank Erosion Damage to buildings', N'RiverBankErosionDamagetobuildings', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (403, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Tidal Surge Used as Shelter', N'TidalSurgeUsedasShelter', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (404, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Water Logging Damage to buildings', N'WaterLoggingDamagetobuildings', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (405, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Water Logging Water', N'WaterLoggingWater', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (406, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Fire Damage to buildings', N'FireDamagetobuildings', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (407, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Fire Used as Shelter', N'FireUsedasShelter', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (408, N'1,1,1,1,1,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Salinity Serious injuries or deaths', N'SalinitySeriousinjuriesordeaths', NULL, NULL, 13, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (409, NULL, 1, NULL, N'21.0.0', N'Flood', N'Flood', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (410, NULL, 1, NULL, N'21.0.0', N'Cyclone', N'Cyclone', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (414, NULL, 1, NULL, N'21.1.1', N'Flood', N'Flood1', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (456, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'17.1', N'Primary', N'Primary', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (457, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'17.1', N'Vocational', N'Vocational', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (458, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Flood Damage Roads and transport', N'FloodDamageRoadsandtransport', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (459, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Earth Quack Damage to buildings', N'EarthQuackDamagetobuildings', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (460, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Earth Quack Communication interruption', N'EarthQuackCommunicationinterruption', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (461, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Water Logging Serious injuries or deaths', N'WaterLoggingSeriousinjuriesordeaths', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (462, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Water Logging Damage Roads and transport', N'WaterLoggingDamageRoadsandtransport', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (463, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Salinity Used as Shelter', N'SalinityUsedasShelter', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (464, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Tsunami', N'Tsunami', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (465, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Building Collapse', N'BuildingCollapse', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (466, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'17.1', N'Primary', N'Primary', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (467, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'17.1', N'NGO School', N'NGOSchool', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (468, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'17.1', N'Vocational', N'Vocational', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (469, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (470, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Flood Damage Roads and transport', N'FloodDamageRoadsandtransport', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (471, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Earth Quack Damage to buildings', N'EarthQuackDamagetobuildings', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (472, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Earth Quack Communication interruption', N'EarthQuackCommunicationinterruption', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (473, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Water Logging Serious injuries or deaths', N'WaterLoggingSeriousinjuriesordeaths', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (474, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Water Logging Damage Roads and transport', N'WaterLoggingDamageRoadsandtransport', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (475, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Salinity Used as Shelter', N'SalinityUsedasShelter', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (476, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Tsunami', N'Tsunami', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (477, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Building Collapse', N'BuildingCollapse', NULL, NULL, 14, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (478, NULL, 1, NULL, N'21.0.0', N'FLood', N'FLood', 23, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (479, NULL, 1, NULL, N'21.0.0', N'Tidal Surge', N'TidalSurge', 23, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (480, NULL, 1, NULL, N'21.0.0', N'Earth Quick', N'EarthQuick', 23, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (481, NULL, 1, NULL, N'21.0.0', N'Cyclone', N'Cyclone', 23, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (482, NULL, 1, NULL, N'21.0.0', N'Tidal Surge', N'TidalSurge', 23, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (483, NULL, 1, NULL, N'21.0.0', N'Earth Quick', N'EarthQuick', 23, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (494, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'17.1', N'NGO School', N'NGOSchool', NULL, NULL, 15, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (495, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'17.1', N'Madrasa', N'Madrasa', NULL, NULL, 15, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (496, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 15, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (497, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Flood Damage to buildings', N'FloodDamagetobuildings', NULL, NULL, 15, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (498, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Fire Serious injuries or deaths', N'FireSeriousinjuriesordeaths', NULL, NULL, 15, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (499, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Salinity Damage to buildings', N'SalinityDamagetobuildings', NULL, NULL, 15, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (500, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Flash Flood Serious injuries or deaths', N'FlashFloodSeriousinjuriesordeaths', NULL, NULL, 15, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (501, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Tsunami', N'Tsunami', NULL, NULL, 15, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (502, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'19.0', N'Building Collapse', N'BuildingCollapse', NULL, NULL, 15, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (503, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'21.0.0', N'FLood', N'FLood', 24, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (504, N'1,2,2,2,2,2,2,2,2,2,2,', 1, NULL, N'21.0.0', N'Tidal Surge', N'TidalSurge', 24, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (535, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Rain Water Harvest', N'RainWaterHarvest', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (536, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'River', N'River', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (537, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Stream', N'Stream', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (538, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (539, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Flood Communication interruption', N'FloodCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (540, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Earth Quack Communication interruption', N'EarthQuackCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (541, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Water Logging Serious injuries or deaths', N'WaterLoggingSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (542, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Strom Serious injuries or deaths', N'StromSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (543, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Land Slide Damage to buildings', N'LandSlideDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (544, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Fire Damage Roads and transport', N'FireDamageRoadsandtransport', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (545, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Tsunami Used as Shelter', N'TsunamiUsedasShelter', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (546, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Buliding Collapse Damage to buildings', N'BulidingCollapseDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (547, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Pond', N'Pond', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (548, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'River', N'River', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (549, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Supply Water', N'SupplyWater', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (550, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (551, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Flood Communication interruption', N'FloodCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (552, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Earth Quack Communication interruption', N'EarthQuackCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (553, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Water Logging Serious injuries or deaths', N'WaterLoggingSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (554, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Strom Serious injuries or deaths', N'StromSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (555, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Land Slide Damage to buildings', N'LandSlideDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (556, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Fire Damage Roads and transport', N'FireDamageRoadsandtransport', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (557, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Tsunami Used as Shelter', N'TsunamiUsedasShelter', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (558, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Buliding Collapse Damage to buildings', N'BulidingCollapseDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (559, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Rain Water Harvest', N'RainWaterHarvest', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (560, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Pond', N'Pond', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (561, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'River', N'River', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (562, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Supply Water', N'SupplyWater', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (563, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Stream', N'Stream', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (564, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Others', N'Others', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (565, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (566, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Flood Communication interruption', N'FloodCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (567, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Earth Quack Communication interruption', N'EarthQuackCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (568, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Water Logging Serious injuries or deaths', N'WaterLoggingSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (569, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Strom Serious injuries or deaths', N'StromSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (570, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Land Slide Damage to buildings', N'LandSlideDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (571, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Fire Damage Roads and transport', N'FireDamageRoadsandtransport', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (572, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Tsunami Used as Shelter', N'TsunamiUsedasShelter', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (573, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Buliding Collapse Damage to buildings', N'BulidingCollapseDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (574, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Rain Water Harvest', N'RainWaterHarvest', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (575, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Pond', N'Pond', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (576, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'River', N'River', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (577, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Supply Water', N'SupplyWater', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (578, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Stream', N'Stream', NULL, NULL, 16, NULL, NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (579, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Others', N'Others', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (580, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (581, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Flood Communication interruption', N'FloodCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (582, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Earth Quack Communication interruption', N'EarthQuackCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (583, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Water Logging Serious injuries or deaths', N'WaterLoggingSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (584, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Strom Serious injuries or deaths', N'StromSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (585, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Land Slide Damage to buildings', N'LandSlideDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (586, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Fire Damage Roads and transport', N'FireDamageRoadsandtransport', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (587, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Tsunami Used as Shelter', N'TsunamiUsedasShelter', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (588, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Buliding Collapse Damage to buildings', N'BulidingCollapseDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (589, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Rain Water Harvest', N'RainWaterHarvest', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (590, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Pond', N'Pond', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (591, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'River', N'River', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (592, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Supply Water', N'SupplyWater', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (593, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Stream', N'Stream', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (594, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Others', N'Others', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (595, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (596, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Flood Communication interruption', N'FloodCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (597, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Earth Quack Communication interruption', N'EarthQuackCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (598, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Water Logging Serious injuries or deaths', N'WaterLoggingSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (599, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Strom Serious injuries or deaths', N'StromSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (600, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Land Slide Damage to buildings', N'LandSlideDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (601, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Fire Damage Roads and transport', N'FireDamageRoadsandtransport', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (602, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Tsunami Used as Shelter', N'TsunamiUsedasShelter', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (603, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Buliding Collapse Damage to buildings', N'BulidingCollapseDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (604, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Rain Water Harvest', N'RainWaterHarvest', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (605, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Pond', N'Pond', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (606, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'River', N'River', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (607, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Supply Water', N'SupplyWater', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (608, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Stream', N'Stream', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (609, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'17.1', N'Others', N'Others', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (610, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (611, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Flood Communication interruption', N'FloodCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (612, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Earth Quack Communication interruption', N'EarthQuackCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (613, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Water Logging Serious injuries or deaths', N'WaterLoggingSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (614, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Strom Serious injuries or deaths', N'StromSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (615, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Land Slide Damage to buildings', N'LandSlideDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (616, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Fire Damage Roads and transport', N'FireDamageRoadsandtransport', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (617, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Tsunami Used as Shelter', N'TsunamiUsedasShelter', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (618, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'19.0', N'Buliding Collapse Damage to buildings', N'BulidingCollapseDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (619, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'21.0.0', N'Tornado', N'Tornado', 25, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (620, N'0,2,4,4,8,9,5,4,1,1,1,1,1,1,', 1, NULL, N'21.0.0', N'Fire', N'Fire', 25, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (706, N'56-90-89-34-544618', 1, NULL, N'17.1', N'Rain Water Harvest', N'RainWaterHarvest', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (707, N'56-90-89-34-544618', 1, NULL, N'17.1', N'Well', N'Well', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (708, N'56-90-89-34-544618', 1, NULL, N'17.1', N'Fall', N'Fall', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (709, N'56-90-89-34-544618', 1, NULL, N'17.1', N'Others', N'Others', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (710, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (711, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Flood Damage to buildings', N'FloodDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (712, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Fire Serious injuries or deaths', N'FireSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (713, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Salinity Damage to buildings', N'SalinityDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (714, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Flash Flood Serious injuries or deaths', N'FlashFloodSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (715, N'56-90-89-34-544618', 1, NULL, N'21.0.0', N'Tidal Surge', N'TidalSurge', 24, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (716, N'56-90-89-34-544618', 1, NULL, N'21.0.0', N'Tidal Surge', N'TidalSurge', 24, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (717, N'56-90-89-34-544618', 1, NULL, N'21.0.0', N'Tidal Surge', N'TidalSurge', 24, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (722, N'02-44-96-54-344468', 14, NULL, N'17.1', N'Rain Water Harvest', N'RainWaterHarvest', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (723, N'02-44-96-54-344468', 14, NULL, N'17.1', N'Pond', N'Pond', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (724, N'02-44-96-54-344468', 14, NULL, N'17.1', N'Tube Well', N'TubeWell', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (725, N'02-44-96-54-344468', 14, NULL, N'17.1', N'Others', N'Others', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (726, N'02-44-96-54-344468', 14, NULL, N'19.0', N'Flood Electricity', N'FloodElectricity', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (727, N'02-44-96-54-344468', 14, NULL, N'19.0', N'Land Slide Electricity', N'LandSlideElectricity', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (728, N'02-44-96-54-344468', 14, NULL, N'19.0', N'Tsunami Electricity', N'TsunamiElectricity', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (729, N'02-44-96-54-344468', 14, NULL, N'19.0', N'Buliding Collapse Electricity', N'BulidingCollapseElectricity', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (740, N'02-44-96-54-344468', 14, NULL, N'17.1', N'Rain Water Harvest', N'RainWaterHarvest', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (741, N'02-44-96-54-344468', 14, NULL, N'17.1', N'Pond', N'Pond', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (742, N'02-44-96-54-344468', 14, NULL, N'17.1', N'Tube Well', N'TubeWell', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (743, N'02-44-96-54-344468', 14, NULL, N'17.1', N'Others', N'Others', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (744, N'02-44-96-54-344468', 14, NULL, N'19.0', N'Flood Electricity', N'FloodElectricity', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (745, N'02-44-96-54-344468', 14, NULL, N'19.0', N'Tsunami Electricity', N'TsunamiElectricity', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (746, N'02-44-96-54-344468', 14, NULL, N'19.0', N'Buliding Collapse Electricity', N'BulidingCollapseElectricity', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (747, N'02-44-96-54-344468', 14, NULL, N'17.1', N'Rain Water Harvest', N'RainWaterHarvest', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (748, N'02-44-96-54-344468', 14, NULL, N'17.1', N'Pond', N'Pond', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (749, N'02-44-96-54-344468', 14, NULL, N'17.1', N'Tube Well', N'TubeWell', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (750, N'02-44-96-54-344468', 14, NULL, N'17.1', N'Others', N'Others', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (751, N'02-44-96-54-344468', 14, NULL, N'19.0', N'Flood Electricity', N'FloodElectricity', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (752, N'02-44-96-54-344468', 14, NULL, N'19.0', N'Tsunami Electricity', N'TsunamiElectricity', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (753, N'02-44-96-54-344468', 14, NULL, N'19.0', N'Buliding Collapse Electricity', N'BulidingCollapseElectricity', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (755, N'56-90-89-34-544618', 1, NULL, N'15.8', N'BNCC', N'BNCC', NULL, 29, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (756, N'56-90-89-34-544618', 1, NULL, N'15.8', N'Cub', N'Cub', NULL, 29, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (757, N'02-44-96-54-344468', 14, NULL, N'23.3', N'Local Community', N'LocalCommunity', NULL, NULL, NULL, 12, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (758, N'02-44-96-54-344468', 14, NULL, N'15.8', N'Rover', N'Rover', NULL, 32, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (759, N'02-44-96-54-344468', 14, NULL, N'15.8', N'Cub', N'Cub', NULL, 32, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (760, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Rain Water Harvest', N'RainWaterHarvest', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (761, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Pond', N'Pond', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (762, N'02-44-96-54-315677', 1, NULL, N'17.1', N'River', N'River', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (763, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Well', N'Well', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (764, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Supply Water', N'SupplyWater', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (765, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Fall', N'Fall', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (766, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Stream', N'Stream', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (767, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Others', N'Others', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (768, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (769, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Flood Damage to buildings', N'FloodDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (770, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Flood Communication interruption', N'FloodCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (771, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Earth Quack Communication interruption', N'EarthQuackCommunicationinterruption', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (772, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Water Logging Serious injuries or deaths', N'WaterLoggingSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (773, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Strom Serious injuries or deaths', N'StromSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (774, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Land Slide Damage to buildings', N'LandSlideDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (775, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Fire Serious injuries or deaths', N'FireSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (776, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Fire Damage Roads and transport', N'FireDamageRoadsandtransport', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (777, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Salinity Damage to buildings', N'SalinityDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (778, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Flash Flood Serious injuries or deaths', N'FlashFloodSeriousinjuriesordeaths', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (779, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Tsunami Used as Shelter', N'TsunamiUsedasShelter', NULL, NULL, 16, NULL, NULL, NULL)
GO
print 'Processed 200 total records'
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (780, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Buliding Collapse Damage to buildings', N'BulidingCollapseDamagetobuildings', NULL, NULL, 16, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (781, N'02-44-96-54-315677', 1, NULL, N'21.0.0', N'Tornado', N'Tornado', 25, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (782, N'02-44-96-54-315677', 1, NULL, N'21.0.0', N'Fire', N'Fire', 25, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (783, N'02-44-96-54-315677', 1, NULL, N'24.7', N'Radio', N'Radio', NULL, NULL, NULL, 11, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (784, N'02-44-96-54-315677', 1, NULL, N'24.7', N'Television', N'Television', NULL, NULL, NULL, 11, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (785, N'02-44-96-54-315677', 1, NULL, N'24.7', N'Mobile', N'Mobile', NULL, NULL, NULL, 11, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (786, N'02-44-96-54-315677', 1, NULL, N'23.3', N'SMC Governing body member', N'SMCGoverningbodymember', NULL, NULL, NULL, 11, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (787, N'02-44-96-54-315677', 1, NULL, N'23.3', N'Teacher', N'Teacher', NULL, NULL, NULL, 11, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (788, N'56-90-89-34-544612', 1, NULL, N'15.8', N'Scout', N'Scout', NULL, 25, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (789, N'56-90-89-34-544612', 1, NULL, N'15.8', N'Rover', N'Rover', NULL, 25, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (790, N'56-90-89-34-544612', 1, NULL, N'15.8', N'BNCC', N'BNCC', NULL, 25, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (791, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Flood Communication interruption', N'FloodCommunicationinterruption', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (792, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Cyclone Serious injuries or deaths', N'CycloneSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (793, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Earth Quack Used as Shelter', N'EarthQuackUsedasShelter', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (794, N'56-90-89-34-544612', 1, NULL, N'19.0', N'River Bank Erosion Serious injuries or deaths', N'RiverBankErosionSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (795, N'56-90-89-34-544612', 1, NULL, N'19.0', N'River Bank Erosion Damage to buildings', N'RiverBankErosionDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (796, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Tidal Surge Used as Shelter', N'TidalSurgeUsedasShelter', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (797, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Water Logging Damage to buildings', N'WaterLoggingDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (798, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Water Logging Water', N'WaterLoggingWater', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (799, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Fire Damage to buildings', N'FireDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (800, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Fire Used as Shelter', N'FireUsedasShelter', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (801, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Salinity Serious injuries or deaths', N'SalinitySeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (802, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Flood', N'Flood', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (803, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Cyclone', N'Cyclone', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (806, N'02-44-96-54-344468', 1, NULL, N'15.8', N'Rover', N'Rover', NULL, 32, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (807, N'02-44-96-54-344468', 1, NULL, N'15.8', N'Cub', N'Cub', NULL, 32, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (808, N'02-44-96-54-344468', 1, NULL, N'15.8', N'Girls Guide', N'GirlsGuide', NULL, 32, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (809, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Flood Communication interruption', N'FloodCommunicationinterruption', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (810, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Cyclone Serious injuries or deaths', N'CycloneSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (811, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Earth Quack Used as Shelter', N'EarthQuackUsedasShelter', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (812, N'02-44-96-54-344468', 1, NULL, N'19.0', N'River Bank Erosion Serious injuries or deaths', N'RiverBankErosionSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (813, N'02-44-96-54-344468', 1, NULL, N'19.0', N'River Bank Erosion Damage to buildings', N'RiverBankErosionDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (814, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Tidal Surge Used as Shelter', N'TidalSurgeUsedasShelter', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (815, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Water Logging Damage to buildings', N'WaterLoggingDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (816, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Water Logging Water', N'WaterLoggingWater', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (817, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Fire Damage to buildings', N'FireDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (818, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Fire Used as Shelter', N'FireUsedasShelter', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (819, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Salinity Serious injuries or deaths', N'SalinitySeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (820, N'02-44-96-54-344468', 1, NULL, N'23.3', N'Teacher', N'Teacher', NULL, NULL, NULL, 12, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (821, N'02-44-96-54-344468', 1, NULL, N'23.3', N'Students', N'Students', NULL, NULL, NULL, 12, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (822, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (823, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Flood Damage to buildings', N'FloodDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (824, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Fire Serious injuries or deaths', N'FireSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (825, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Salinity Damage to buildings', N'SalinityDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (826, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Flash Flood Serious injuries or deaths', N'FlashFloodSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (827, N'56-90-89-34-544618', 1, NULL, N'21.0.0', N'Tidal Surge', N'TidalSurge', 24, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (828, N'56-90-89-34-544618', 1, NULL, N'21.0.0', N'Tidal Surge', N'TidalSurge', 24, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (829, N'56-90-89-34-544618', 1, NULL, N'23.3', N'SMC Governing body member', N'SMCGoverningbodymember', NULL, NULL, NULL, 12, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (830, N'02-44-96-54-344468', 1, NULL, N'17.1', N'River', N'River', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (831, N'02-44-96-54-344468', 1, NULL, N'17.1', N'Fall', N'Fall', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (832, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (833, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Flood Damage to buildings', N'FloodDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (834, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Flood Communication interruption', N'FloodCommunicationinterruption', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (835, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Cyclone Serious injuries or deaths', N'CycloneSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (836, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Earth Quack Used as Shelter', N'EarthQuackUsedasShelter', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (837, N'02-44-96-54-344468', 1, NULL, N'19.0', N'River Bank Erosion Serious injuries or deaths', N'RiverBankErosionSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (838, N'02-44-96-54-344468', 1, NULL, N'19.0', N'River Bank Erosion Damage to buildings', N'RiverBankErosionDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (839, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Tidal Surge Used as Shelter', N'TidalSurgeUsedasShelter', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (840, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Water Logging Damage to buildings', N'WaterLoggingDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (841, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Water Logging Water', N'WaterLoggingWater', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (842, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Fire Serious injuries or deaths', N'FireSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (843, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Fire Damage to buildings', N'FireDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (844, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Fire Used as Shelter', N'FireUsedasShelter', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (845, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Salinity Serious injuries or deaths', N'SalinitySeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (846, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Salinity Damage to buildings', N'SalinityDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (847, N'02-44-96-54-344468', 1, NULL, N'19.0', N'Flash Flood Serious injuries or deaths', N'FlashFloodSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (851, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (852, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Flood Damage Roads and transport', N'FloodDamageRoadsandtransport', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (853, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Earth Quack Damage to buildings', N'EarthQuackDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (854, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Earth Quack Communication interruption', N'EarthQuackCommunicationinterruption', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (855, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Water Logging Serious injuries or deaths', N'WaterLoggingSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (856, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Water Logging Damage Roads and transport', N'WaterLoggingDamageRoadsandtransport', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (857, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Salinity Used as Shelter', N'SalinityUsedasShelter', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (858, N'56-90-89-34-544615', 1, NULL, N'15.8', N'Cub', N'Cub', NULL, 28, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (859, N'56-90-89-34-544615', 1, NULL, N'15.8', N'Girls Guide', N'GirlsGuide', NULL, 28, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (860, N'56-90-89-34-544615', 1, NULL, N'15.8', N'Red Crescent', N'RedCrescent', NULL, 28, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (861, N'56-90-89-34-544615', 1, NULL, N'21.0.0', N'Cyclone', N'Cyclone', 23, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (862, N'56-90-89-34-544615', 1, NULL, N'21.0.0', N'Tidal Surge', N'TidalSurge', 23, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (863, N'56-90-89-34-544615', 1, NULL, N'23.3', N'Teacher', N'Teacher', NULL, NULL, NULL, 12, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (864, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (865, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Flood Damage Roads and transport', N'FloodDamageRoadsandtransport', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (866, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Earth Quack Damage to buildings', N'EarthQuackDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (867, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Earth Quack Communication interruption', N'EarthQuackCommunicationinterruption', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (868, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Water Logging Serious injuries or deaths', N'WaterLoggingSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (869, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Water Logging Damage Roads and transport', N'WaterLoggingDamageRoadsandtransport', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (870, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Salinity Used as Shelter', N'SalinityUsedasShelter', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (871, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (872, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Flood Damage Roads and transport', N'FloodDamageRoadsandtransport', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (873, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Earth Quack Damage to buildings', N'EarthQuackDamagetobuildings', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (874, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Earth Quack Communication interruption', N'EarthQuackCommunicationinterruption', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (875, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Water Logging Serious injuries or deaths', N'WaterLoggingSeriousinjuriesordeaths', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (876, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Water Logging Damage Roads and transport', N'WaterLoggingDamageRoadsandtransport', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (877, N'56-90-89-34-544615', 1, NULL, N'19.0', N'Salinity Used as Shelter', N'SalinityUsedasShelter', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (909, N'02-44-96-54-545567', 19, NULL, N'17.1', N'Rain Water Harvest', N'RainWaterHarvest', NULL, NULL, 18, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (910, N'02-44-96-54-545567', 19, NULL, N'17.1', N'River', N'River', NULL, NULL, 18, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (911, N'02-44-96-54-545567', 19, NULL, N'17.1', N'Well', N'Well', NULL, NULL, 18, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (912, N'02-44-96-54-545567', 19, NULL, N'17.1', N'Fall', N'Fall', NULL, NULL, 18, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (913, N'02-44-96-54-545567', 19, NULL, N'19.0', N'Flood Electricity', N'FloodElectricity', NULL, NULL, 18, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (914, N'02-44-96-54-545567', 19, NULL, N'19.0', N'River Bank Erosion Serious injuries or deaths', N'RiverBankErosionSeriousinjuriesordeaths', NULL, NULL, 18, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (915, N'02-44-96-54-545567', 19, NULL, N'19.0', N'Water Logging Communication interruption', N'WaterLoggingCommunicationinterruption', NULL, NULL, 18, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (916, N'02-44-96-54-545567', 19, NULL, N'19.0', N'Tsunami Serious injuries or deaths', N'TsunamiSeriousinjuriesordeaths', NULL, NULL, 18, NULL, NULL, NULL)
GO
print 'Processed 300 total records'
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (917, N'02-44-96-54-545567', 19, NULL, N'19.0', N'Buliding Collapse Electricity', N'BulidingCollapseElectricity', NULL, NULL, 18, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (918, N'02-44-96-54-545567', 19, NULL, N'15.8', N'BNCC', N'BNCC', NULL, 38, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (919, N'02-44-96-54-545567', 19, NULL, N'15.8', N'Cub', N'Cub', NULL, 38, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (920, N'02-44-96-54-545567', 19, NULL, N'21.0.0', N'Cyclone', N'Cyclone', 27, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (921, N'02-44-96-54-545567', 19, NULL, N'21.0.0', N'Tidal Surge', N'TidalSurge', 27, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (922, N'02-44-96-54-545567', 19, NULL, N'21.0.0', N'River Bank Erosion', N'RiverBankErosion', 27, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (923, N'02-44-96-54-545567', 19, NULL, N'24.7', N'Radio', N'Radio', NULL, NULL, NULL, 13, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (924, N'02-44-96-54-545567', 19, NULL, N'24.7', N'Television', N'Television', NULL, NULL, NULL, 13, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (925, N'02-44-96-54-545567', 19, NULL, N'24.7', N'Mobile', N'Mobile', NULL, NULL, NULL, 13, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (926, N'02-44-96-54-545567', 19, NULL, N'23.3', N'SMC Governing body member', N'SMCGoverningbodymember', NULL, NULL, NULL, 13, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (927, N'02-44-96-54-545567', 19, NULL, N'23.3', N'Teacher', N'Teacher', NULL, NULL, NULL, 13, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (928, N'02-44-96-54-545567', 19, NULL, N'23.3', N'Students', N'Students', NULL, NULL, NULL, 13, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (929, N'02-44-96-54-545567', 19, NULL, N'23.3', N'Local Community', N'LocalCommunity', NULL, NULL, NULL, 13, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (930, N'02-44-96-54-315677', 1, NULL, N'15.8', N'Scout', N'Scout', NULL, 31, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (931, N'02-44-96-54-315677', 1, NULL, N'15.8', N'Red Crescent', N'RedCrescent', NULL, 31, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (932, N'02-44-96-54-315677', 1, NULL, N'21.0.0', N'Tornado', N'Tornado', 25, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (933, N'02-44-96-54-315677', 1, NULL, N'21.0.0', N'Fire', N'Fire', 25, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (934, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Rain Water Harvest', N'RainWaterHarvest', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (935, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Pond', N'Pond', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (936, N'02-44-96-54-315677', 1, NULL, N'17.1', N'River', N'River', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (937, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Well', N'Well', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (938, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Supply Water', N'SupplyWater', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (939, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Fall', N'Fall', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (940, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Stream', N'Stream', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (941, N'02-44-96-54-315677', 1, NULL, N'17.1', N'Others', N'Others', NULL, NULL, 17, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (942, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 17, NULL, N'Flood', N'Serious injuries or deaths')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (943, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Flood Damage to buildings', N'FloodDamagetobuildings', NULL, NULL, 17, NULL, N'Flood', N'Damage to buildings')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (944, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Flood Communication interruption', N'FloodCommunicationinterruption', NULL, NULL, 17, NULL, N'Flood', N'Communication interruption')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (945, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Flood Electricity', N'FloodElectricity', NULL, NULL, 17, NULL, N'Flood', N'Electricity')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (946, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Cyclone Water', N'CycloneWater', NULL, NULL, 17, NULL, N'Cyclone', N'Water')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (947, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Cyclone School closure', N'CycloneSchoolclosure', NULL, NULL, 17, NULL, N'Cyclone', N'School closure')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (948, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Earth Quack Communication interruption', N'EarthQuackCommunicationinterruption', NULL, NULL, 17, NULL, N'Earth Quack', N'Communication interruption')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (949, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Earth Quack Toilet', N'EarthQuackToilet', NULL, NULL, 17, NULL, N'Earth Quack', N'Toilet')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (950, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Tidal Surge Used as Shelter', N'TidalSurgeUsedasShelter', NULL, NULL, 17, NULL, N'Tidal Surge', N'Used as Shelter')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (951, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Water Logging Serious injuries or deaths', N'WaterLoggingSeriousinjuriesordeaths', NULL, NULL, 17, NULL, N'Water Logging', N'Water')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (952, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Water Logging School closure', N'WaterLoggingSchoolclosure', NULL, NULL, 17, NULL, N'Water Logging', N'School closure')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (953, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Strom Serious injuries or deaths', N'StromSeriousinjuriesordeaths', NULL, NULL, 17, NULL, N'Strom', N'Serious injuries or deaths')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (954, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Strom Water', N'StromWater', NULL, NULL, 17, NULL, N'Strom', N'Water')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (955, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Land Slide Damage to buildings', N'LandSlideDamagetobuildings', NULL, NULL, 17, NULL, N'Land Slide', N'Damage to buildings')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (956, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Fire Serious injuries or deaths', N'FireSeriousinjuriesordeaths', NULL, NULL, 17, NULL, N'Fire', N'Serious injuries or deaths')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (957, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Fire Damage Roads and transport', N'FireDamageRoadsandtransport', NULL, NULL, 17, NULL, N'Fire', N'Damage Roads and transport')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (958, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Salinity Damage to buildings', N'SalinityDamagetobuildings', NULL, NULL, 17, NULL, N'Salinity', N'Damage to buildings')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (959, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Flash Flood Serious injuries or deaths', N'FlashFloodSeriousinjuriesordeaths', NULL, NULL, 17, NULL, N'Flash Flood', N'Serious injuries or deaths')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (960, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Tsunami Used as Shelter', N'TsunamiUsedasShelter', NULL, NULL, 17, NULL, N'Tsunami', N'Used as Shelter')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (961, N'02-44-96-54-315677', 1, NULL, N'19.0', N'Buliding Collapse Damage to buildings', N'BulidingCollapseDamagetobuildings', NULL, NULL, 17, NULL, N'Buliding Collapse', N'Damage to buildings')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (962, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Flood Serious injuries or deaths', N'FloodSeriousinjuriesordeaths', NULL, NULL, 17, NULL, N'Flood', N'Serious injuries or deaths')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (963, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Flood Damage to buildings', N'FloodDamagetobuildings', NULL, NULL, 17, NULL, N'Flood', N'Damage to buildings')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (964, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Flood Electricity', N'FloodElectricity', NULL, NULL, 17, NULL, N'Flood', N'Electricity')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (965, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Cyclone Dropout', N'CycloneDropout', NULL, NULL, 17, NULL, N'Cyclone', N'Dropout')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (966, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Earth Quack Damage Roads and transport', N'EarthQuackDamageRoadsandtransport', NULL, NULL, 17, NULL, N'Earth Quack', N'Damage Roads and transport')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (967, N'56-90-89-34-544618', 1, NULL, N'19.0', N'River Bank Erosion Damage to buildings', N'RiverBankErosionDamagetobuildings', NULL, NULL, 17, NULL, N'River Bank Erosion', N'Damage to buildings')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (968, N'56-90-89-34-544618', 1, NULL, N'19.0', N'River Bank Erosion Reduced School attendance', N'RiverBankErosionReducedSchoolattendance', NULL, NULL, 17, NULL, N'River Bank Erosion', N'Reduced School attendance')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (969, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Water Logging Communication interruption', N'WaterLoggingCommunicationinterruption', NULL, NULL, 17, NULL, N'Water Logging', N'Water')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (970, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Water Logging Toilet', N'WaterLoggingToilet', NULL, NULL, 17, NULL, N'Water Logging', N'Toilet')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (971, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Land Slide Toilet', N'LandSlideToilet', NULL, NULL, 17, NULL, N'Land Slide', N'Toilet')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (972, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Fire Serious injuries or deaths', N'FireSeriousinjuriesordeaths', NULL, NULL, 17, NULL, N'Fire', N'Serious injuries or deaths')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (973, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Fire Dropout', N'FireDropout', NULL, NULL, 17, NULL, N'Fire', N'Dropout')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (974, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Salinity Damage to buildings', N'SalinityDamagetobuildings', NULL, NULL, 17, NULL, N'Salinity', N'Damage to buildings')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (975, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Flash Flood Serious injuries or deaths', N'FlashFloodSeriousinjuriesordeaths', NULL, NULL, 17, NULL, N'Flash Flood', N'Serious injuries or deaths')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (976, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Flash Flood Reduced School attendance', N'FlashFloodReducedSchoolattendance', NULL, NULL, 17, NULL, N'Flash Flood', N'Reduced School attendance')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (977, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Buliding Collapse Used as Shelter', N'BulidingCollapseUsedasShelter', NULL, NULL, 17, NULL, N'Buliding Collapse', N'Used as Shelter')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (978, N'56-90-89-34-544618', 1, NULL, N'19.0', N'Buliding Collapse Electricity', N'BulidingCollapseElectricity', NULL, NULL, 17, NULL, N'Buliding Collapse', N'Electricity')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (979, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Flood Communication interruption', N'FloodCommunicationinterruption', NULL, NULL, 17, NULL, N'Flood', N'Communication interruption')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (980, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Cyclone Serious injuries or deaths', N'CycloneSeriousinjuriesordeaths', NULL, NULL, 17, NULL, N'Cyclone', N'Serious injuries or deaths')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (981, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Earth Quack Used as Shelter', N'EarthQuackUsedasShelter', NULL, NULL, 17, NULL, N'Earth Quack', N'Used as Shelter')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (982, N'56-90-89-34-544612', 1, NULL, N'19.0', N'River Bank Erosion Serious injuries or deaths', N'RiverBankErosionSeriousinjuriesordeaths', NULL, NULL, 17, NULL, N'River Bank Erosion', N'Serious injuries or deaths')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (983, N'56-90-89-34-544612', 1, NULL, N'19.0', N'River Bank Erosion Damage to buildings', N'RiverBankErosionDamagetobuildings', NULL, NULL, 17, NULL, N'River Bank Erosion', N'Damage to buildings')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (984, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Tidal Surge Used as Shelter', N'TidalSurgeUsedasShelter', NULL, NULL, 17, NULL, N'Tidal Surge', N'Used as Shelter')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (985, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Water Logging Damage to buildings', N'WaterLoggingDamagetobuildings', NULL, NULL, 17, NULL, N'Water Logging', N'Water')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (986, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Water Logging Water', N'WaterLoggingWater', NULL, NULL, 17, NULL, N'Water Logging', N'Water')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (987, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Fire Damage to buildings', N'FireDamagetobuildings', NULL, NULL, 17, NULL, N'Fire', N'Damage to buildings')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (988, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Fire Used as Shelter', N'FireUsedasShelter', NULL, NULL, 17, NULL, N'Fire', N'Used as Shelter')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (989, N'56-90-89-34-544612', 1, NULL, N'19.0', N'Salinity Serious injuries or deaths', N'SalinitySeriousinjuriesordeaths', NULL, NULL, 17, NULL, N'Salinity', N'Serious injuries or deaths')
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (990, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Flood', N'Flood', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (991, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Cyclone', N'Cyclone', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (992, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Flood', N'Flood', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (993, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Cyclone', N'Cyclone', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (994, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Flood', N'Flood', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (995, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Cyclone', N'Cyclone', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (996, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Flood', N'Flood', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (997, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Cyclone', N'Cyclone', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (998, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Flood', N'Flood', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (999, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Cyclone', N'Cyclone', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (1000, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Flood', N'Flood', 22, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[CheckBoxs] ([ChkId], [InstituteId], [UserId], [DomainId], [QuestionId], [ChkValue], [ChkName], [DisasterManagementId], [GeneralInformationId], [SafeLearningId], [RiskResilienceId], [HazardsName], [ImpactName]) VALUES (1001, N'56-90-89-34-544612', 1, NULL, N'21.0.0', N'Cyclone', N'Cyclone', 22, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[CheckBoxs] OFF
/****** Object:  Default [DF__webpages___IsCon__24927208]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[webpages_Membership] ADD  DEFAULT ((0)) FOR [IsConfirmed]
GO
/****** Object:  Default [DF__webpages___Passw__25869641]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[webpages_Membership] ADD  DEFAULT ((0)) FOR [PasswordFailuresSinceLastSuccess]
GO
/****** Object:  ForeignKey [fk_RoleId]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[webpages_Roles] ([RoleId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_RoleId]
GO
/****** Object:  ForeignKey [fk_UserId]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_UserId]
GO
/****** Object:  ForeignKey [FK_AuditTrail_UserProfile]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[AuditTrail]  WITH CHECK ADD  CONSTRAINT [FK_AuditTrail_UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[AuditTrail] CHECK CONSTRAINT [FK_AuditTrail_UserProfile]
GO
/****** Object:  ForeignKey [FK_RiskReductionResilienceEducation_UserProfile]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[RiskReductionResilienceEducation]  WITH CHECK ADD  CONSTRAINT [FK_RiskReductionResilienceEducation_UserProfile] FOREIGN KEY([userId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[RiskReductionResilienceEducation] CHECK CONSTRAINT [FK_RiskReductionResilienceEducation_UserProfile]
GO
/****** Object:  ForeignKey [FK_DomainDisasterManagement_UserProfile]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[DomainDisasterManagement]  WITH CHECK ADD  CONSTRAINT [FK_DomainDisasterManagement_UserProfile] FOREIGN KEY([userId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[DomainDisasterManagement] CHECK CONSTRAINT [FK_DomainDisasterManagement_UserProfile]
GO
/****** Object:  ForeignKey [FK_DomainSafeLearning_UserProfile]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[DomainSafeLearning]  WITH CHECK ADD  CONSTRAINT [FK_DomainSafeLearning_UserProfile] FOREIGN KEY([userId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[DomainSafeLearning] CHECK CONSTRAINT [FK_DomainSafeLearning_UserProfile]
GO
/****** Object:  ForeignKey [FK_Districts_Divisions]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[Districts]  WITH CHECK ADD  CONSTRAINT [FK_Districts_Divisions] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Divisions] ([DivisionId])
GO
ALTER TABLE [dbo].[Districts] CHECK CONSTRAINT [FK_Districts_Divisions]
GO
/****** Object:  ForeignKey [FK_DisasterHistory_DomainDisasterManagement]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[DisasterHistory]  WITH CHECK ADD  CONSTRAINT [FK_DisasterHistory_DomainDisasterManagement] FOREIGN KEY([DisasterManagementId])
REFERENCES [dbo].[DomainDisasterManagement] ([DisasterManagementId])
GO
ALTER TABLE [dbo].[DisasterHistory] CHECK CONSTRAINT [FK_DisasterHistory_DomainDisasterManagement]
GO
/****** Object:  ForeignKey [FK_Upazillas_Districts]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[Upazillas]  WITH CHECK ADD  CONSTRAINT [FK_Upazillas_Districts] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[Districts] ([DistrictId])
GO
ALTER TABLE [dbo].[Upazillas] CHECK CONSTRAINT [FK_Upazillas_Districts]
GO
/****** Object:  ForeignKey [FK_Unions_Upazillas]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[Unions]  WITH CHECK ADD  CONSTRAINT [FK_Unions_Upazillas] FOREIGN KEY([UpazillaId])
REFERENCES [dbo].[Upazillas] ([UpazillaId])
GO
ALTER TABLE [dbo].[Unions] CHECK CONSTRAINT [FK_Unions_Upazillas]
GO
/****** Object:  ForeignKey [FK_DomainGeneralInformation_Districts]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[DomainGeneralInformation]  WITH CHECK ADD  CONSTRAINT [FK_DomainGeneralInformation_Districts] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[Districts] ([DistrictId])
GO
ALTER TABLE [dbo].[DomainGeneralInformation] CHECK CONSTRAINT [FK_DomainGeneralInformation_Districts]
GO
/****** Object:  ForeignKey [FK_DomainGeneralInformation_Divisions]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[DomainGeneralInformation]  WITH CHECK ADD  CONSTRAINT [FK_DomainGeneralInformation_Divisions] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Divisions] ([DivisionId])
GO
ALTER TABLE [dbo].[DomainGeneralInformation] CHECK CONSTRAINT [FK_DomainGeneralInformation_Divisions]
GO
/****** Object:  ForeignKey [FK_DomainGeneralInformation_Unions]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[DomainGeneralInformation]  WITH CHECK ADD  CONSTRAINT [FK_DomainGeneralInformation_Unions] FOREIGN KEY([UnionId])
REFERENCES [dbo].[Unions] ([UnionId])
GO
ALTER TABLE [dbo].[DomainGeneralInformation] CHECK CONSTRAINT [FK_DomainGeneralInformation_Unions]
GO
/****** Object:  ForeignKey [FK_DomainGeneralInformation_Upazillas]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[DomainGeneralInformation]  WITH CHECK ADD  CONSTRAINT [FK_DomainGeneralInformation_Upazillas] FOREIGN KEY([UpazillaId])
REFERENCES [dbo].[Upazillas] ([UpazillaId])
GO
ALTER TABLE [dbo].[DomainGeneralInformation] CHECK CONSTRAINT [FK_DomainGeneralInformation_Upazillas]
GO
/****** Object:  ForeignKey [FK_DomainGeneralInformation_UserProfile]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[DomainGeneralInformation]  WITH CHECK ADD  CONSTRAINT [FK_DomainGeneralInformation_UserProfile] FOREIGN KEY([userId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[DomainGeneralInformation] CHECK CONSTRAINT [FK_DomainGeneralInformation_UserProfile]
GO
/****** Object:  ForeignKey [FK_Domains_DomainDisasterManagement]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[Domains]  WITH CHECK ADD  CONSTRAINT [FK_Domains_DomainDisasterManagement] FOREIGN KEY([DisasterManagementId])
REFERENCES [dbo].[DomainDisasterManagement] ([DisasterManagementId])
GO
ALTER TABLE [dbo].[Domains] CHECK CONSTRAINT [FK_Domains_DomainDisasterManagement]
GO
/****** Object:  ForeignKey [FK_Domains_DomainGeneralInformation]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[Domains]  WITH CHECK ADD  CONSTRAINT [FK_Domains_DomainGeneralInformation] FOREIGN KEY([GeneralInformationId])
REFERENCES [dbo].[DomainGeneralInformation] ([GeneralInformationId])
GO
ALTER TABLE [dbo].[Domains] CHECK CONSTRAINT [FK_Domains_DomainGeneralInformation]
GO
/****** Object:  ForeignKey [FK_Domains_DomainSafeLearning]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[Domains]  WITH CHECK ADD  CONSTRAINT [FK_Domains_DomainSafeLearning] FOREIGN KEY([SafeLearningId])
REFERENCES [dbo].[DomainSafeLearning] ([SafeLearningId])
GO
ALTER TABLE [dbo].[Domains] CHECK CONSTRAINT [FK_Domains_DomainSafeLearning]
GO
/****** Object:  ForeignKey [FK_Domains_RiskReductionResilienceEducation]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[Domains]  WITH CHECK ADD  CONSTRAINT [FK_Domains_RiskReductionResilienceEducation] FOREIGN KEY([RiskResilienceId])
REFERENCES [dbo].[RiskReductionResilienceEducation] ([RiskResilienceId])
GO
ALTER TABLE [dbo].[Domains] CHECK CONSTRAINT [FK_Domains_RiskReductionResilienceEducation]
GO
/****** Object:  ForeignKey [FK_Domains_UserProfile]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[Domains]  WITH CHECK ADD  CONSTRAINT [FK_Domains_UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[Domains] CHECK CONSTRAINT [FK_Domains_UserProfile]
GO
/****** Object:  ForeignKey [FK_CheckBoxs_DomainDisasterManagement]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[CheckBoxs]  WITH CHECK ADD  CONSTRAINT [FK_CheckBoxs_DomainDisasterManagement] FOREIGN KEY([DisasterManagementId])
REFERENCES [dbo].[DomainDisasterManagement] ([DisasterManagementId])
GO
ALTER TABLE [dbo].[CheckBoxs] CHECK CONSTRAINT [FK_CheckBoxs_DomainDisasterManagement]
GO
/****** Object:  ForeignKey [FK_CheckBoxs_DomainGeneralInformation]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[CheckBoxs]  WITH CHECK ADD  CONSTRAINT [FK_CheckBoxs_DomainGeneralInformation] FOREIGN KEY([GeneralInformationId])
REFERENCES [dbo].[DomainGeneralInformation] ([GeneralInformationId])
GO
ALTER TABLE [dbo].[CheckBoxs] CHECK CONSTRAINT [FK_CheckBoxs_DomainGeneralInformation]
GO
/****** Object:  ForeignKey [FK_CheckBoxs_Domains]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[CheckBoxs]  WITH CHECK ADD  CONSTRAINT [FK_CheckBoxs_Domains] FOREIGN KEY([DomainId])
REFERENCES [dbo].[Domains] ([DomainId])
GO
ALTER TABLE [dbo].[CheckBoxs] CHECK CONSTRAINT [FK_CheckBoxs_Domains]
GO
/****** Object:  ForeignKey [FK_CheckBoxs_DomainSafeLearning]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[CheckBoxs]  WITH CHECK ADD  CONSTRAINT [FK_CheckBoxs_DomainSafeLearning] FOREIGN KEY([SafeLearningId])
REFERENCES [dbo].[DomainSafeLearning] ([SafeLearningId])
GO
ALTER TABLE [dbo].[CheckBoxs] CHECK CONSTRAINT [FK_CheckBoxs_DomainSafeLearning]
GO
/****** Object:  ForeignKey [FK_CheckBoxs_RiskReductionResilienceEducation]    Script Date: 02/06/2014 14:13:59 ******/
ALTER TABLE [dbo].[CheckBoxs]  WITH CHECK ADD  CONSTRAINT [FK_CheckBoxs_RiskReductionResilienceEducation] FOREIGN KEY([RiskResilienceId])
REFERENCES [dbo].[RiskReductionResilienceEducation] ([RiskResilienceId])
GO
ALTER TABLE [dbo].[CheckBoxs] CHECK CONSTRAINT [FK_CheckBoxs_RiskReductionResilienceEducation]
GO
