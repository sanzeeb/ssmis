var GoogleMap = function(){
    this.map = '';
    this.lat = '';
    this.lng = '';
    this.latLng = '';
    this.locationTitle = '';
    this.locationLevel = '';
    this.locationDesc = '';
    this.locationImg = '';
    this.mapRenderer = '';
    
};


var goldStar = {
  //  path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
    path: google.maps.SymbolPath.CIRCLE,
    fillColor: 'yellow',
    fillOpacity: 0.8,
    scale: 10,
    strokeColor: 'gold',
    strokeWeight: 5
};

var goldStarBlue = {
    //  path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
    path: google.maps.SymbolPath.CIRCLE,
    fillColor: 'blue',
    fillOpacity: 0.8,
    scale: 10,
    strokeColor: 'gold',
    strokeWeight: 5
};

var goldStarRed = {
    //  path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
    path: google.maps.SymbolPath.CIRCLE,
    fillColor: 'red',
    fillOpacity: 0.8,
    scale: 10,
    strokeColor: 'gold',
    strokeWeight: 5
};
GoogleMap.prototype.initialize = function(markers,z){
    
   // console.log(markers);
    this.lat = (markers[0].lat != undefined) ? markers[0].lat : '23.709921000000000000';  //37.0625
    this.lng = (markers[0].lng != undefined) ? markers[0].lng : '90.407143000000020000'; //-95.677068
    this.latLng = new google.maps.LatLng(this.lat, this.lng); //center
    this.mapRenderer = document.getElementById('mapRenderer');
    var mapOptions = {
            zoom: (z != undefined)? z : 6,
            center: this.latLng,
            mapTypeControl: true,
            mapTypeControlOptions: {
              style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            },

            zoomControl:true,
            
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            streetViewControl:false,
            panControl: true,
            scaleControl:true
        };

    this.map = new google.maps.Map(this.mapRenderer,mapOptions);
    
    

};


GoogleMap.prototype.Bangladeshinitialize = function (markers, z) {

    this.lat = '23.709921';  //37.0625
    this.lng =  '90.407143'; //-95.677068
    this.latLng = new google.maps.LatLng(this.lat, this.lng); //center
    this.mapRenderer = document.getElementById('mapRenderer');
    var mapOptions = {
        zoom: (z != undefined) ? z : 6,
        center: this.latLng,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },

        zoomControl: true,

        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        panControl: true,
        scaleControl: true
    };

    this.map = new google.maps.Map(this.mapRenderer, mapOptions);



};

GoogleMap.prototype.initializeBangladesh = function (markers, z) {

    this.lat = (markers[0].lat != undefined) ? markers[0].lat : '23.709921';  //37.0625
    this.lng = (markers[0].lng != undefined) ? markers[0].lng : '90.407143'; //-95.677068
    this.latLng = new google.maps.LatLng(this.lat, this.lng); //center
    this.mapRenderer = document.getElementById('mapRenderer');
    var mapOptions = {
        zoom: (z != undefined) ? z : 6,
        center: this.latLng,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },

        zoomControl: true,

        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        panControl: true,
        scaleControl: true
    };

    this.map = new google.maps.Map(this.mapRenderer, mapOptions);



};

GoogleMap.prototype.initializeByClick = function (lat,lng, z) {


    this.lat = (lat != undefined) ? lat : '23.709921';  //37.0625
    this.lng = (lng != undefined) ? lng : '90.407143'; //-95.677068
    this.latLng = new google.maps.LatLng(this.lat, this.lng); //center
    this.mapRenderer = document.getElementById('mapRenderer');
    var mapOptions = {
        zoom: (z != undefined) ? z : 6,
        center: this.latLng,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },

        zoomControl: true,

        //mapTypeId: google.maps.MapTypeId.SATELLITE,
        mapTypeId: google.maps.MapTypeId.Map,

        streetViewControl: false,
        panControl: true,
        scaleControl: true
    };

    this.map = new google.maps.Map(this.mapRenderer, mapOptions);



};

GoogleMap.prototype.showMarker = function()
{

   
    var image = "";
  
    if (this.locationLevel == "l3") {
        image = baseURLImg + 'upazila.png';
    }
    if (this.locationLevel == "l1") {

        if (this.locationTitle == "Dhaka") {
            image = baseURLImg + 'division.png';
        }
        if (this.locationTitle == "Sylhet") {
            image = baseURLImg + 'division.png';
        }

        if (this.locationTitle == "Chittagong") {
            image = baseURLImg + 'division.png';
        }

        if (this.locationTitle == "Rajshahi") {
            image = baseURLImg + 'division.png';
        }
        if (this.locationTitle == "Khulna") {
            image = baseURLImg + 'division.png';
        }

        if (this.locationTitle == "Barisal") {
            image = baseURLImg + 'division.png';
        }
        if (this.locationTitle == "Rangpur") {
            image = baseURLImg + 'division.png';
        }
       
    }
    //if (this.locationLevel == "l2") {
    //    image = baseURLImg + 'g-dark-blue-pin.png';
    //}


    if (this.locationLevel == "l2") {
        image = baseURLImg + 'district.png';
    }
    //if (this.locationLevel == "l5") {
    //    image = baseURLImg + 'red.png';
    //}
    //if (this.locationLevel == "l4") {
    //    image = baseURLImg + 'red.png';
    //}

   
    var mapMarker = new google.maps.Marker({
        position: this.latLng,
        map: this.map,
        title: this.locationTitle,
        icon:image
        //animation: google.maps.Animation.DROP
    });
    var contentString = '<div id="info_id" class="info"><h4>' + this.locationTitle + '</h4><div class="imgContent"><img style="width:40%; float:left;" src="' + baseURL + this.locationImg + '"/></div><p>' + this.locationDesc + '</p></div>';
    //<img style="width:40%; float:left;" src="'+BASE+'uploads/'+this.locationImg+'"/>
    var infowindow = new google.maps.InfoWindow({
        content: contentString
        
       
    });

 
    google.maps.event.addListener(mapMarker, 'click', function() {

        if (this.locationTitle != "" || this.locationTitle != undefined) {
            infowindow.open(this.map, mapMarker);
           // marker.setIcon('https://www.google.com/mapfiles/marker_green.png');
        }

    });
};

GoogleMap.prototype.showMarkers = function(markers)
{
    var locations = markers;
    var mapMarker;
    var infowindow = new google.maps.InfoWindow();
    var l = 0;
    var image ="";
    for (l = 0; l < locations.length; l++) {

        
        if (locations[l].name == "Dhaka") {
            image = baseURLImg + 'division.png';
        }
        if (locations[l].name == "Sylhet") {
            image = baseURLImg + 'division.png';
        }

        if (locations[l].name == "Chittagong") {
            image = baseURLImg + 'division.png';
        }

        if (locations[l].name == "Rajshahi") {
            image = baseURLImg + 'division.png';
        }
        if (locations[l].name == "Khulna") {
            image = baseURLImg + 'division.png';
        }

        if (locations[l].name == "Barisal") {
            image = baseURLImg + 'division.png';
        }
        if (locations[l].name == "Rangpur") {
            image = baseURLImg + 'division.png';
        }
        mapMarker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[l].lat, locations[l].lng),
            map: this.map,
            title: locations[l].name,
            label: locations[l].divsion_id,
            labelAnchor: new google.maps.Point(22, 0),
            labelClass: "labels",
            id: locations[l].divsion_id,
            icon: image

        });
        var contentString = '<div class="info" style="min-width:200px;"><h4>' + locations[l].name + '</h4><p>' + locations[l].divsion_id + '</p></div>';
        //<img style="width:25%;" src="'+BASE+'uploads/'+locations[l].photo+'"/>
        
        if (locations[l].name.label == 5) {
            google.maps.event.addListener(mapMarker, 'click', (function (mapMarker, l, contentString) {
                return function () {

                    infowindow.setContent(contentString);

                    infowindow.open(this.map, mapMarker);

                    // marker.setIcon('https://www.google.com/mapfiles/marker_green.png');
                }
            })(mapMarker, l, contentString));
       }

        
    }
};









GoogleMap.prototype.showSchools = function (markers) {
    var locations = markers;
    var mapMarker;
    var infowindow = new google.maps.InfoWindow();
    var l = 0;
    var image = "";
    for (l = 0; l < locations.length; l++) {

        mapMarker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[l].lat, locations[l].lng),
            map: this.map,
            title: locations[l].InstituteEng,
            label: locations[l].InstituteEng,
            labelAnchor: new google.maps.Point(22, 0),
            labelClass: "labels",
            id: locations[l].InstituteEng,
            icon: image

        });
        var desc = "<div class='gmapContent' ><p>Cluster Id:<span>" + locations[l].InstituteId + "</span></p><p>Institute Type:<span>" + locations[l].InstituteType + "</span></p><p>Estabilishment Date :<span>" + locations[l].EstabilishmentDate + "</span></p><p>Graphical Location:<span>" + locations[l].GraphicalLocation + "</span></p><p>Gob Eiin Id: <span>" + locations[l].GobEIIN + "</span></p><p>" + " Mobile No:<span>" + locations[l].InstituteMobile + "</span></p><p> Contact No:<span>" + locations[l].InstitutePhone + "</span></p><p>" + "Teacher Total:<span>" + locations[l].TeacherTotal + "</span></p><p>" + " Student Total:<span>" + locations[l].StudentTotal + "</span></p><p>Own drinking water source:<span>" + locations[l].OwnDrinking + "</span></p> <p># of Functioning toilet:<span>" + locations[l].LatrineFacilityFunctional + "</span></p><p># of Kacha building /structure:<span>" + locations[l].KaccaBuilding + "</span></p><p># of Pacca building /structure:<span>" + locations[l].PaccaBuilding + "</span></p><p># of Kacha class:<span>" + locations[l].KaccaClass + "</span></p><p># of Pacca class:<span>" + locations[l].PaccaClass + "</span></p></div>";
        var contentString = '<div id="info_id" class="info"><h4>' + locations[l].InstituteEng + '</h4><div class="imgContent"><img style="width:40%; float:left;" src="' + baseURL + locations[l].InstituteId + ".jpg" + '"/></div><p>' + desc + '</p></div>';

        google.maps.event.addListener(mapMarker, 'click', (function (mapMarker, l, contentString) {
            return function () {

                infowindow.setContent(contentString);

                infowindow.open(this.map, mapMarker);

           
            }
        })(mapMarker, l, contentString));
       


    }
};










GoogleMap.prototype.setLocTitle = function(title)
{
    this.locationTitle = (title != undefined)? title : '';
}

GoogleMap.prototype.setLocLevel = function (level) {
    this.locationLevel = (level != undefined) ? level : '';
}
GoogleMap.prototype.setLocDesc = function(desc)
{
    this.locationDesc = (desc != undefined)? desc : '';
}
GoogleMap.prototype.setLocImg = function(img)
{
    this.locationImg = (img != undefined)? img : '';
}

GoogleMap.prototype.clear = function(){
    $(this.mapRenderer).html('');
}




