﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIMS.Models
{
    public class UsersModel
    {
        private DMISDBEntities dmisdb = new DMISDBEntities();

        public List<UserProfile> GetInfoByName(string name)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.UserProfiles
                         where s.UserName==name
                         select s;

            return result.ToList<UserProfile>(); ;
        }


        public void EditStatus(UserProfile DTO)
        {
            using (var container = new DMISDBEntities())
            {
                var Comp = new UserProfile();
                Comp = container.UserProfiles.FirstOrDefault(o => o.UserId==DTO.UserId);
                Comp.Status= DTO.Status;
                container.SaveChanges();
            }
        }

    }
}