﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIMS.Models
{
    class DomainDisasterManagementModels
    {
        public void Add(DomainDisasterManagement DTO)
        {
            using (var container = new DMISDBEntities())
            {
                //DomainDisasterManagement domaingen = new DomainDisasterManagement();
                container.DomainDisasterManagements.Add(DTO);
                container.SaveChanges();
            }
        }


        public void removeDisDomain(int? userId, int? mgmId)
        {

            using (var container = new DMISDBEntities())
            {
                var objcheck = (from e in container.DomainDisasterManagements where  e.DisasterManagementId == mgmId select e).FirstOrDefault();
                if (objcheck != null)
                {
                    container.DomainDisasterManagements.Remove(objcheck);
                    container.SaveChanges();
                }


            }

        }


        public void Edit(DomainDisasterManagement DTO)
        {
            using (var container = new DMISDBEntities())
            {
                var Comp = new DomainDisasterManagement();
                Comp = container.DomainDisasterManagements.FirstOrDefault(o => o.DisasterManagementId == DTO.DisasterManagementId);
                Comp.DisasterManagementId = DTO.DisasterManagementId;
                Comp.ContingencyPlan = DTO.ContingencyPlan;
                Comp.CatchmentAreaForTeacherBackup = DTO.CatchmentAreaForTeacherBackup;
                Comp.CatchmentAreaForTemporaryLearning = DTO.CatchmentAreaForTemporaryLearning;
                Comp.AccessibleEmergencyFund = DTO.AccessibleEmergencyFund;
                Comp.InstituteId = DTO.InstituteId;
                Comp.DisasterCount = DTO.DisasterCount;
                container.SaveChanges();

            }
        }

        public void EditStatus(DomainDisasterManagement DTO)
        {
            using (var container = new DMISDBEntities())
            {
                var Comp = new DomainDisasterManagement();
                Comp = container.DomainDisasterManagements.FirstOrDefault(o => o.DisasterManagementId.Equals(DTO.DisasterManagementId));
                Comp.DisasterManagementId = DTO.DisasterManagementId;
                Comp.DomainStatus = DTO.DomainStatus;

                container.SaveChanges();
            }
        }


        public List<DomainDisasterManagement> GetDomainGeneral(int proid, string productname, int catid, int unitid, int compid)
        {
            using (var Container = new DMISDBEntities())
            {
                var query = from s in Container.DomainDisasterManagements
                           
                            select new { s };


               

                var result = from o in query
                             orderby o.s.DisasterManagementId descending

                             select new DomainDisasterManagement
                             {
                                 DisasterManagementId = o.s.DisasterManagementId
                             };
                return result.ToList<DomainDisasterManagement>();


            }

        }



        public List<DomainDisasterManagementDto> GetDomainDisterLastId()
        {
            using (var Container = new DMISDBEntities())
            {
                var query = from s in Container.DomainDisasterManagements

                            select new { s };




                var result = from o in query
                             orderby o.s.DisasterManagementId descending

                             select new DomainDisasterManagementDto
                             {
                                 DisasterManagementId = o.s.DisasterManagementId
                             };
                return result.ToList<DomainDisasterManagementDto>();


            }

        }

    }
    public  class DomainDisasterManagementDto
    {
        
        public int DisasterManagementId { get; set; }
        public string CatchmentAreaForTemporaryLearning { get; set; }
        public string CatchmentAraeForTeacherBackup { get; set; }
        public string AccessibleEmergencyFund { get; set; }
        public string ContingencyPlan { get; set; }

        
    }

}
