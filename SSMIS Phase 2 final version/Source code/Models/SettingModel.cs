﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIMS.Models
{
    public class SettingModel
    {

        public void removeDivision(int? mgmId)
        {

            using (var container = new DMISDBEntities())
            {
                var objcheck = (from e in container.Divisions where  e.DivisionId == mgmId select e).FirstOrDefault();
                if (objcheck != null)
                {
                    container.Divisions.Remove(objcheck);
                    container.SaveChanges();
                }


            }

        }


        public void removeDistrict( int? mgmId)
        {

            using (var container = new DMISDBEntities())
            {
                var objcheck = (from e in container.Districts where  e.DistrictId == mgmId select e).FirstOrDefault();
                if (objcheck != null)
                {
                    container.Districts.Remove(objcheck);
                    container.SaveChanges();
                }


            }

        }


        public void removeUnion(int? mgmId)
        {

            using (var container = new DMISDBEntities())
            {
                var objcheck = (from e in container.Unions where  e.UnionId == mgmId select e).FirstOrDefault();
                if (objcheck != null)
                {
                    container.Unions.Remove(objcheck);
                    container.SaveChanges();
                }


            }

        }

        public void removeUpazilla(int? mgmId)
        {

            using (var container = new DMISDBEntities())
            {
                var objcheck = (from e in container.Upazillas where  e.UpazillaId == mgmId select e).FirstOrDefault();
                if (objcheck != null)
                {
                    container.Upazillas.Remove(objcheck);
                    container.SaveChanges();
                }


            }

        }

    }
}