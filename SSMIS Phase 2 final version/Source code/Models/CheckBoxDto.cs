﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIMS.Models
{
    public class CheckBoxDto
    {
        public int ChkId { get; set; }
        public string InstituteId { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<int> DomainId { get; set; }
        public string QuestionId { get; set; }
        public string ChkValue { get; set; }
        public string ChkName { get; set; }
        public Nullable<int> DisasterManagementId { get; set; }
        public Nullable<int> GeneralInformationId { get; set; }
        public Nullable<int> SafeLearningId { get; set; }
        public Nullable<int> RiskResilienceId { get; set; }
        public string HazardsName { get; set; }
        public string ImpactName { get; set; }
        public Nullable<int> DivisionId { get; set; }
        public Nullable<int> UnionId { get; set; }
        public Nullable<int> UpazillaId { get; set; }
        public Nullable<int> DistrictId { get; set; }
        public string SchoolArea { get; set; }
    }
}