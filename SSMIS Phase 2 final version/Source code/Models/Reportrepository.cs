﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIMS.Models
{
    public class Reportrepository
    {
        private DMISDBEntities dmisdb = new DMISDBEntities();

        public List<Division> DivisioninfoById(int divId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Divisions
                         where s.DivisionId == divId

                         select s;



            return result.ToList<Division>(); ;
        }


        public List<Division> Divisioninfo()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Divisions

                         select s;



            return result.ToList<Division>(); ;
        }


        public List<District> DistrictinfoByDiv(int divId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Districts
                         where s.DivisionId == divId

                         select s;



            return result.ToList<District>(); ;
        }



        public List<District> Districtinfo()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Districts

                         select s;



            return result.ToList<District>(); ;
        }

        public List<District> DistrictinfoById(int disId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Districts
                         where s.DistrictId == disId

                         select s;



            return result.ToList<District>(); ;
        }

        public List<Upazilla> Upazillainfo()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Upazillas

                         select s;



            return result.ToList<Upazilla>(); ;
        }



        public List<Upazilla> UpazillainfoByDis(int disId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Upazillas
                         where s.DistrictId == disId

                         select s;



            return result.ToList<Upazilla>(); ;
        }

        public List<Upazilla> UpazillainfoById(int upId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Upazillas
                         where s.UpazillaId == upId

                         select s;



            return result.ToList<Upazilla>(); ;
        }
        public List<Union> Unioninfo()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Unions

                         select s;



            return result.ToList<Union>(); ;
        }



        public List<Union> UnioninfoByUpa(int upaId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Unions
                         where s.UpazillaId == upaId

                         select s;



            return result.ToList<Union>(); ;
        }

        public List<Union> UnioninfoById(int unId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Unions
                         where s.UnionId == unId

                         select s;



            return result.ToList<Union>(); ;
        }

        public List<DomainGeneralInformation> GetGeneralInformationsByEIIN(string GEiiNId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainGeneralInformations
                         where s.GobEIIN.Equals(GEiiNId) && s.DomainStatus == 1
                         select s;

            return result.ToList<DomainGeneralInformation>();
        }


        public List<DomainGeneralInformation> GetGeneralInformationsByClusterId(string ClusId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainGeneralInformations
                         where s.InstituteId.Equals(ClusId) && s.DomainStatus == 1
                         select s;

            return result.ToList<DomainGeneralInformation>();
        }

        public List<DomainGeneralInformation> GetGeneralInformationsByName(string name)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainGeneralInformations
                         where s.InstituteEng.Contains(name) && s.DomainStatus==1
                         select s;

            return result.ToList<DomainGeneralInformation>();
        }


        public List<DomainGeneralInformation> GetGeneralInformationsByContingencysearchType(int divisionId, int districtId, int upazillaId, int unionId, string Backup, string Contingency, string areaType, string Temporary, string emergency)
        {
            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            if (divisionId != 0)
            {
                query = query.Where(c => c.DivisionId == divisionId);

            }
            if (districtId != 0)
            {
                query = query.Where(c => c.DistrictId == districtId);
            }
            if (upazillaId != 0)
            {
                query = query.Where(c => c.UpazillaId == upazillaId);

            }
            if (unionId != 0)
            {
                query = query.Where(c => c.UnionId == unionId);
            }
            if (areaType != "All")
            {
                query = query.Where(c => c.SchoolArea.Equals(areaType));
            }

            if (Backup != "All")
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainDisasterManagement.CatchmentAreaForTeacherBackup.Equals(Backup));
            }
            if (Contingency != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainDisasterManagement.ContingencyPlan.Equals(Contingency));


            }
            if (Temporary != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainDisasterManagement.CatchmentAreaForTemporaryLearning.Equals(Temporary));


            }
            if (emergency != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainDisasterManagement.AccessibleEmergencyFund.Equals(emergency));


            }



            return query.ToList<DomainGeneralInformation>();
        }

        public List<DomainGeneralInformation> GetGeneralInformationsByResilienceEducationType(int divisionId, int districtId, int upazillaId, int unionId, string DRR, string drills, string areaType, string understanding)
        {
            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            if (divisionId != 0)
            {
                query = query.Where(c => c.DivisionId == divisionId);

            }
            if (districtId != 0)
            {
                query = query.Where(c => c.DistrictId == districtId);
            }
            if (upazillaId != 0)
            {
                query = query.Where(c => c.UpazillaId == upazillaId);

            }
            if (unionId != 0)
            {
                query = query.Where(c => c.UnionId == unionId);
            }
            if (areaType != "All")
            {
                query = query.Where(c => c.SchoolArea.Equals(areaType));
            }

            if (DRR != "All")
            {
                query = query.Where(c => c.Domains.FirstOrDefault().RiskReductionResilienceEducation.DisasterRelatedTraining.Equals(DRR));
            }
            if (understanding != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().RiskReductionResilienceEducation.HowStudentAwareAboutDisaster.Equals(understanding));


            }
            if (drills != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().RiskReductionResilienceEducation.DrilFacility.Equals(drills));


            }




            return query.ToList<DomainGeneralInformation>();
        }


        public List<DomainGeneralInformation> GetGeneralInformationsByDisabilityType(int division, int district, int upazila, int union, string Disability)
        {


            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);

            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
            }
            if (Disability == "Disability")
            {
                
                query = query.Where(c => c.StudentDisableFeMale!=0 || c.StudentDisableMale!=0);
            }

            if (Disability == "Ethnic")
            {
                query = query.Where(c => c.StudentEthnicFeMale != 0 || c.StudentEthnicMale!=0);
            }


            return query.ToList<DomainGeneralInformation>();
        }



        public List<DomainGeneralInformation> GetGeneralInformationsByCommonFaciType(int division, int district, int upazila, int union,string tiffin,string playing, string areaType, string electricity,string internet,string computer, string firstaid,string fireext, string volunteering)
        {


            var query = from s in dmisdb.DomainGeneralInformations  where s.DomainStatus==1 select s;

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);

            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
            }
            if (areaType != "All")
            {
                query = query.Where(c => c.SchoolArea.Equals(areaType));
            }

            if (computer != "All")
            {
                query = query.Where(c => c.ComputerLab.Equals(computer));
            }
            if (tiffin != "All")
            {

                query = query.Where(c => c.TiffinFacility.Equals(tiffin));
               

            }
            if (playing != "All")
            {

                query = query.Where(c => c.PlayingGround.Equals(playing));


            }
            if (electricity != "All")
            {

                query = query.Where(c => c.ElectricityFacillity.Equals(electricity));


            }
            if (internet != "All")
            {

                query = query.Where(c => c.InternetFacility.Equals(internet));


            }

            if (firstaid != "All")
            {

                query = query.Where(c => c.FirstAid.Equals(firstaid));
               

            }

            if (fireext != "All")
            {

                query = query.Where(c => c.FunctionalFireExtinguise.Equals(fireext));


            }

            if (volunteering != "All")
            {

                query = query.Where(c => c.VolunteerServices.Equals(volunteering));


            }



            return query.ToList<DomainGeneralInformation>();
        }


        public List<DomainGeneralInformation> GetGeneralInformationsByDrinkingwatersearchType(int division, int district, int upazila, int union, string geographicalType, string owndrink, string areaType, string watersafe, string functional, string arsenic, string childfrnd, string accessdisabilities, string functioning)
        {


            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus ==1 select s;

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);

            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
            }
            if (areaType != "All")
            {
                query = query.Where(c => c.SchoolArea.Equals(areaType));
            }

            if (geographicalType != "All")
            {
                query = query.Where(c => c.GraphicalLocation.Equals(geographicalType));
            }
            if (owndrink != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.OwnDrinkingSource.Equals(owndrink));
               

            }
            if (watersafe != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.SafeDrinkingSource.Equals(watersafe));


            }
            if (functional != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.SourceOfWaterIfTubewellFunctional.Equals(functional));


            }
            if (functioning != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.SourceOfWaterFunctionDuringDisaster.Equals(functioning));


            }

            if (arsenic != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.SourceOfWaterIfTubewellArsenicFree.Equals(arsenic));
               

            }

            if (childfrnd != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.SourceOfWaterChildFriendly.Equals(childfrnd));


            }

            if (accessdisabilities != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.LatrineChildAccessiblePhysicalDisabilties.Equals(accessdisabilities));


            }



            return query.ToList<DomainGeneralInformation>();
        }


        

        public List<DomainGeneralInformation> GetGeneralInformationsByStrcType(int division, int district, int upazila, int union, string geographicalType,string StructuralType,string areaType,string ClassRoomType)
        {


            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus==1 select s;

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId==division);
                      
            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
            }
            if (areaType != "All")
            {
                query = query.Where(c => c.SchoolArea.Equals(areaType));
            }

            if (geographicalType != "All")
            {
                query = query.Where(c => c.GraphicalLocation.Equals(geographicalType));
            }
            if (StructuralType != "All")
            {
                if (StructuralType == "Pacca"){
                    query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.TotalPaccaBuildingStructer!=0);
                }
                if (StructuralType == "Semi Pacca")
                {
                    query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.TotalSemiPaccaBuildingStructer!=0);
                }
                if (StructuralType == "Kacha")
                {
                    query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.TotalKaccaBuildingStructer!=0);
                }
                if (StructuralType == "Abounded")
                {
                    query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.TotalUnsedBuildingStructer!=0 );
                }
               
            }
            
          
            if (ClassRoomType != "All")
            {
               

                if (ClassRoomType == "Pacca")
                {
                    query = query.Where(c => !c.Domains.FirstOrDefault().DomainSafeLearning.TotalPaccaClassRoom.Equals("0"));
                }
                if (ClassRoomType == "Semi Pacca")
                {
                    query = query.Where(c => !c.Domains.FirstOrDefault().DomainSafeLearning.TotalSemiPaccaClassRoom.Equals("0"));
                }
                if (ClassRoomType == "Kacha")
                {
                    query = query.Where(c => !c.Domains.FirstOrDefault().DomainSafeLearning.TotalKaccaClassRoom.Equals("0"));
                }
                if (ClassRoomType == "Abounded")
                {
                    query = query.Where(c => !c.Domains.FirstOrDefault().DomainSafeLearning.TotalUnsedClassRoom.Equals("0") && !c.Domains.FirstOrDefault().DomainSafeLearning.TotalUnsedClassRoom.Equals(""));
                }
                
            }

             



            return query.ToList<DomainGeneralInformation>();
        }



        public List<DomainGeneralInformation> GetGeneralInformationsByGeoType(int division, int district, int upazila, int union, string geographicalType, string EmbankmentType, string areaType)
        {


            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            int cc = query.Count();
            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);
                query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
            }
            if (areaType != "All")
            {
                query = query.Where(c => c.SchoolArea.Equals(areaType));
                cc = query.Count();
            }

            if (geographicalType != "All")
            {
                query = query.Where(c => c.GraphicalLocation.Equals(geographicalType));
                cc = query.Count();
            }
            if (EmbankmentType != "All")
            {
                query = query.Where(c => c.Embankment.Equals(EmbankmentType));
                cc = query.Count();

            }

            cc = query.Count();

            return query.ToList<DomainGeneralInformation>();




        }




        public List<DomainGeneralInformation> GetGeneralInformationsByType(int division,int district,int upazila,int union,string mgmtype,string instype,string authotype)
        {
            
            

            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            int cc = query.Count();
            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);
                query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
            }
            if (mgmtype != "All")
            {
                query = query.Where(c => c.ManagementType.Equals(mgmtype));
                cc = query.Count();
            }

            if (authotype != "All")
            {
                query = query.Where(c => c.AuthorityType.Equals(authotype));
                cc = query.Count();
            }
            if (instype != "All")
            {
                query = query.Where(c => c.InstituteType.Equals(instype));
                cc = query.Count();

            }

            cc=query.Count();

            return query.ToList<DomainGeneralInformation>();
        }



        public List<DomainGeneralInformation> GetGeneralInformationsByMgmType(int division, int district, int upazila, int union, string mgmtype)
        {


            var result = from s in dmisdb.DomainGeneralInformations
                         where s.DivisionId == division || s.DistrictId == district || s.UnionId == union || s.UpazillaId == upazila
                         || s.ManagementType.Equals(mgmtype) 
                         select s;

            return result.ToList<DomainGeneralInformation>();
        }

        public List<DomainGeneralInformation> GetGeneralInformationsByInstituteType(int division, int district, int upazila, int union, string InstituteType)
        {


            var result = from s in dmisdb.DomainGeneralInformations
                         where s.DivisionId == division || s.DistrictId == district || s.UnionId == union || s.UpazillaId == upazila
                         || s.InstituteType.Equals(InstituteType)
                         select s;

            return result.ToList<DomainGeneralInformation>();
        }


        public List<DomainGeneralInformation> GetGeneralInformationsByAuthorityType(int division, int district, int upazila, int union, string AuthorityType)
        {


            var result = from s in dmisdb.DomainGeneralInformations
                         where s.DivisionId == division || s.DistrictId == district || s.UnionId == union || s.UpazillaId == upazila
                         || s.AuthorityType.Equals(AuthorityType)
                         select s;

            return result.ToList<DomainGeneralInformation>();
        }


        public List<DomainGeneralInformation> GetGeneralInformationsByUserUnion(int id,int userId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainGeneralInformations
                         where s.UnionId == id &&  s.userId==userId
                         select s;

            return result.ToList<DomainGeneralInformation>();
        }

        public List<DomainGeneralInformation> GetGeneralInformationsByAuthoUnion(int id,string authoType)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainGeneralInformations
                         where s.UnionId == id && s.AuthorityType.Equals(authoType)
                         select s;

            return result.ToList<DomainGeneralInformation>();
        }

        public List<DomainGeneralInformation> GetGeneralInformationsByUnion(int id)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainGeneralInformations
                         where s.UnionId==id
                         select s;

            return result.ToList<DomainGeneralInformation>();
        }

        public List<DomainGeneralInformation> GetGeneralInformationsById(int? id)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainGeneralInformations
                         where s.GeneralInformationId == id
                         select s;

            return result.ToList<DomainGeneralInformation>();
        }

        public List<DomainGeneralInformation> GetGeneralInformations()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainGeneralInformations
                       
                         select s;

            return result.ToList<DomainGeneralInformation>();
        }


        public List<DomainGeneralInformation> GeneralInformationByRole(string role)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainGeneralInformations where s.AuthorityType.Equals(role) 

                         select s;



            return result.ToList<DomainGeneralInformation>(); ;
        }

        public List<DomainGeneralInformation> GeneralInformationById(int id)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainGeneralInformations
                         where s.userId==id

                         select s;



            return result.ToList<DomainGeneralInformation>(); ;
        }
        public List<DomainGeneralInformation> GeneralInformation()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainGeneralInformations
                        

                         select s;



            return result.ToList<DomainGeneralInformation>(); ;
        }
        public List<Union> UnioninfoByRole()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Unions

                         select s;



            return result.ToList<Union>(); ;
        }


        public List<Upazilla> UpazillainfoByRole()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Upazillas

                         select s;



            return result.ToList<Upazilla>(); ;
        }


        public List<District> DistrictinfoByRole()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Districts

                         select s;



            return result.ToList<District>(); ;
        }

        public List<DomainSafeLearning> SafeLearninginfoByIns(string insId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainSafeLearnings
                         where s.InstituteId.Equals(insId)

                         select s;



            return result.ToList<DomainSafeLearning>(); ;
        }


        public List<DomainDisasterManagement> DomainDisasterManagementinfoByIns(string insId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainDisasterManagements
                         where s.InstituteId.Equals(insId)

                         select s;



            return result.ToList<DomainDisasterManagement>(); ;
        }

        public List<RiskReductionResilienceEducation> RiskReductionResilienceEducationInfoByIns(string insId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.RiskReductionResilienceEducations
                         where s.InstituteId.Equals(insId)

                         select s;



            return result.ToList<RiskReductionResilienceEducation>(); ;
        }

        //public List<CheckBox> GetGeneralInformationsByRiskTypeHazard(int divisionId, int districtId, int upazillaId, int unionId, string All, string Flood, string Cyclone, string EarthQuack, string RiverBankErosion, string TidalSurge, string WaterLogging, string Strom, string SeasonalStrom, string Tsunami, string BuildingCollapse, string FlashFlood, string Salinity, string Fire, string LandSlide)
        //{

        //    var query = from s in dmisdb.CheckBoxs where s.DomainDisasterManagement.DomainStatus == 1 select s;
        //    int cc = query.Count();

        //    if (divisionId != 0)
        //    {
        //        query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == divisionId);

        //    }
        //    if (districtId != 0)
        //    {
        //        query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == districtId);
        //    }
        //    if (upazillaId != 0)
        //    {
        //        query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazillaId);

        //    }
        //    if (unionId != 0)
        //    {
        //        query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == unionId);
        //    }
        //    if (Flood != null)
        //    {

        //        query = query.Where(c => c.HazardsName.Equals(Flood));
        //    }

        //    if (Cyclone != null)
        //    {

        //        query = query.Where(c => c.HazardsName.Equals(Cyclone));
        //    }
        //    if (EarthQuack != null)
        //    {

        //        query = query.Where(c => c.HazardsName.Equals(EarthQuack));


        //    }
        //    if (RiverBankErosion != null)
        //    {


        //        query = query.Where(c => c.HazardsName.Equals(RiverBankErosion));

        //    }
        //    if (TidalSurge != null)
        //    {

        //        query = query.Where(c => c.HazardsName.Equals(TidalSurge));

        //        cc = query.Count();


        //    }

        //    if (WaterLogging != null)
        //    {

        //        query = query.Where(c => c.HazardsName.Equals(WaterLogging));

        //        cc = query.Count();

        //    }

        //    if (Strom != null)
        //    {
        //        query = query.Where(c => c.HazardsName.Equals(Strom));

        //    }

        //    if (SeasonalStrom != null)
        //    {
        //        query = query.Where(c => c.HazardsName.Equals(SeasonalStrom));

        //    }

        //    if (Tsunami != null)
        //    {
        //        query = query.Where(c => c.HazardsName.Equals(Tsunami));

        //    }
        //    if (BuildingCollapse != null)
        //    {
        //        query = query.Where(c => c.HazardsName.Equals(BuildingCollapse));
        //    }
        //    if (FlashFlood != null)
        //    {
        //        query = query.Where(c => c.HazardsName.Equals(FlashFlood));
        //    }
        //    if (Salinity != null)
        //    {
        //        query = query.Where(c => c.HazardsName.Equals(Salinity));
        //    }

        //    if (Fire != null)
        //    {
        //        query = query.Where(c => c.HazardsName.Equals(Fire));
        //        cc = query.Count();
        //    }

        //    if (LandSlide != null)
        //    {
        //        query = query.Where(c => c.HazardsName.Equals(LandSlide));

        //    }

        //    if (All != null)
        //    {
        //        query = query.Where(c => c.HazardsName.Equals(Cyclone));
        //        query = query.Where(c => c.HazardsName.Equals(Flood));
        //        query = query.Where(c => c.HazardsName.Equals(EarthQuack));
        //        query = query.Where(c => c.HazardsName.Equals(RiverBankErosion));
        //        query = query.Where(c => c.HazardsName.Equals(TidalSurge));
        //        query = query.Where(c => c.HazardsName.Equals(WaterLogging));
        //        query = query.Where(c => c.HazardsName.Equals(Strom));
        //        query = query.Where(c => c.HazardsName.Equals(SeasonalStrom));
        //        query = query.Where(c => c.HazardsName.Equals(Tsunami));
        //        query = query.Where(c => c.HazardsName.Equals(BuildingCollapse));
        //        query = query.Where(c => c.HazardsName.Equals(FlashFlood));
        //        query = query.Where(c => c.HazardsName.Equals(Salinity));
        //        query = query.Where(c => c.HazardsName.Equals(Fire));
        //        query = query.Where(c => c.HazardsName.Equals(LandSlide));

        //    }
        //    return query.ToList<CheckBox>();
        //}

    

        public List<CheckBox> GetGeneralInformationsByRiskType(int divisionId, int districtId, int upazillaId, int unionId, string All, string Flood, string Cyclone, string EarthQuack, string RiverBankErosion, string TidalSurge, string WaterLogging, string Strom, string SeasonalStrom, string Tsunami, string BuildingCollapse, string FlashFlood, string Salinity, string Fire, string LandSlide)
        {

            var query = from s in dmisdb.CheckBoxs where s.QuestionId.Equals("21.0.0") select s;
            int cc = query.Count();

            if (divisionId != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == divisionId);

            }
            if (districtId != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == districtId);
            }
            if (upazillaId != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazillaId);

            }
            if (unionId != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == unionId);
            }
            if (Flood != null)
            {

                query = query.Where(c => c.ChkValue.Equals(Flood));
                cc = query.Count();

            }

            if (Cyclone != null)
            {

                query = query.Where(c => c.ChkValue.Equals(Cyclone));
            }
            if (EarthQuack != null)
            {

                query = query.Where(c => c.ChkValue.Equals(EarthQuack));


            }
            if (RiverBankErosion != null)
            {

                
                query = query.Where(c => c.ChkValue.Equals(RiverBankErosion));

            }
            if (TidalSurge != null)
            {

                query = query.Where(c => c.ChkValue.Equals(TidalSurge));

                cc = query.Count();


            }

            if (WaterLogging != null)
            {

                query = query.Where(c => c.ChkValue.Equals(WaterLogging));

                cc = query.Count();

            }

            if (Strom != null)
            {
                query = query.Where(c => c.ChkValue.Equals(Strom));
                
            }

            if (SeasonalStrom != null)
            {
                query = query.Where(c => c.ChkValue.Equals(SeasonalStrom));
               
            }

            if (Tsunami != null)
            {
                query = query.Where(c => c.ChkValue.Equals(Tsunami));

            }
            if (BuildingCollapse != null)
            {
                query = query.Where(c => c.ChkValue.Equals(BuildingCollapse));
            }
            if (FlashFlood != null)
            {
                query = query.Where(c => c.ChkValue.Equals(FlashFlood));
            }
            if (Salinity != null)
            {
                query = query.Where(c => c.ChkValue.Equals(Salinity));
            }

            if (Fire != null)
            {
                query = query.Where(c => c.ChkValue.Equals(Fire));
                cc = query.Count();
            }

            if (LandSlide != null)
            {
                query = query.Where(c => c.ChkValue.Equals(LandSlide));

            }

            if (All != null)
            {
                query = query.Where(c => c.ChkValue.Equals(Cyclone));
                query = query.Where(c => c.ChkValue.Equals(Flood));
                query = query.Where(c => c.ChkValue.Equals(EarthQuack));
                query = query.Where(c => c.ChkValue.Equals(RiverBankErosion));
                query = query.Where(c => c.ChkValue.Equals(TidalSurge));
                query = query.Where(c => c.ChkValue.Equals(WaterLogging));
                query = query.Where(c => c.ChkValue.Equals(Strom));
                query = query.Where(c => c.ChkValue.Equals(SeasonalStrom));
                query = query.Where(c => c.ChkValue.Equals(Tsunami));
                query = query.Where(c => c.ChkValue.Equals(BuildingCollapse));
                query = query.Where(c => c.ChkValue.Equals(FlashFlood));
                query = query.Where(c => c.ChkValue.Equals(Salinity));
                query = query.Where(c => c.ChkValue.Equals(Fire));
                query = query.Where(c => c.ChkValue.Equals(LandSlide));

            }
            return query.ToList<CheckBox>();
        }

        internal List<DomainGeneralInformation> GetGeneralInformationsBySanitType(int division, int district, int upazila, int union, string regular, string functions, string common, string childfrnd, string overall, string accessdisabilities, string geographicalType, string areaType)
        {
            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);

            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
            }
            if (areaType != "All")
            {
                query = query.Where(c => c.SchoolArea.Equals(areaType));
            }

            if (geographicalType != "All")
            {
                query = query.Where(c => c.GraphicalLocation.Equals(geographicalType));
            }
            if (regular != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.LatrineRegularMaintain.Equals(regular));


            }
            if (functions != "All")
            {
                if (functions == "yes")
                {
                    query = query.Where(c => !c.Domains.FirstOrDefault().DomainSafeLearning.LatrineFacilityFunctional.Equals("0") && !c.Domains.FirstOrDefault().DomainSafeLearning.LatrineFacilityFunctional.Equals(""));
                }
                if (functions == "no")
                {
                    query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.LatrineFacilityFunctional.Equals("0") || c.Domains.FirstOrDefault().DomainSafeLearning.LatrineFacilityFunctional.Equals(""));
                }
            }
            if (common != "All")
            {
                if (common == "student")
                {
                    query = query.Where(c => !c.Domains.FirstOrDefault().DomainSafeLearning.LatrineFacilityForStudent.Equals("0") && !c.Domains.FirstOrDefault().DomainSafeLearning.LatrineFacilityForStudent.Equals(""));

                }

                if (common == "teacher")
                {
                    query = query.Where(c => !c.Domains.FirstOrDefault().DomainSafeLearning.LatrineFacilityForTeacher.Equals("") && !c.Domains.FirstOrDefault().DomainSafeLearning.LatrineFacilityForTeacher.Equals("0"));

                }

                

            }
            if (overall != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.LatrineMaleCondition.Equals(overall) || c.Domains.FirstOrDefault().DomainSafeLearning.LatrineFemaleCondtion.Equals(overall));


            }

            

            if (childfrnd != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.SourceOfWaterChildFriendly.Equals(childfrnd));


            }

            if (accessdisabilities != "All")
            {

                query = query.Where(c => c.Domains.FirstOrDefault().DomainSafeLearning.LatrineChildAccessiblePhysicalDisabilties.Equals(accessdisabilities));


            }



            return query.ToList<DomainGeneralInformation>();
        }

        internal List<DomainGeneralInformation> GetGeneralInformationsByVacentType(int division, int district, int upazila, int union, string geographicalType, string areaType)
        {
            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 && s.VacantTeacherPosition!=0 && s.VacantTeacherPosition!=null select s;

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);

            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
            }
            if (areaType != "All")
            {
                query = query.Where(c => c.SchoolArea.Equals(areaType));
            }

            if (geographicalType != "All")
            {
                query = query.Where(c => c.GraphicalLocation.Equals(geographicalType));
            }
            


            return query.ToList<DomainGeneralInformation>();
        }
        
    }
}