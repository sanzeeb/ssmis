﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace DIMS.Models
{
    public class Repository
    {
        private UsersContext db = new UsersContext();
        private DMISDBEntities dmisdb = new DMISDBEntities();

        public List<UserProfile> GetInfo(int id)
        {
          //  List<UserProfile> info = new List<UserProfile>();
          //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.UserProfiles
                         where s.UserId == id
                       select s;

            return result.ToList<UserProfile>(); ;
        }

        public List<CheckBox> GetCheckBoxInfo(string qId, int? userId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.CheckBoxs
                         where s.UserId == userId && s.QuestionId.Equals(qId)
                         select s;

            return result.ToList<CheckBox>(); ;
        }



        public List<CheckBox> GetCheckBoxGenralInfo(string qId, int? userId,int genId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.CheckBoxs
                         where  s.QuestionId.Equals(qId) && s.GeneralInformationId==genId
                         select s;

            return result.ToList<CheckBox>(); ;
        }


        public List<CheckBox> GetCheckBoxSafeLearningInfo(string qId, int? userId, int safeId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.CheckBoxs
                         where  s.QuestionId.Equals(qId) && s.SafeLearningId == safeId
                         select s;

            return result.ToList<CheckBox>(); ;
        }


        public List<CheckBox> GetCheckBoxRiskResilienceInfo(string qId, int? userId, int riskId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.CheckBoxs
                         where  s.QuestionId.Equals(qId) && s.RiskResilienceId == riskId
                         select s;

            return result.ToList<CheckBox>(); ;
        }

        public List<CheckBox> GetCheckBoxDisasterManagementInfo(string qId, int? userId, int dgmId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.CheckBoxs
                         where  s.QuestionId.Equals(qId) && s.DisasterManagementId == dgmId
                         select s;

            return result.ToList<CheckBox>(); ;
        }

        public List<Domain> Domaininfo()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Domains
                       
                         select s;



            return result.ToList<Domain>(); ;
        }

        public List<Domain> DomaininfoByUserId(int uid)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Domains where s.UserId==uid

                         select s;



            return result.ToList<Domain>(); ;
        }


        public List<Division> DivisioninfoById(int? divId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Divisions where s.DivisionId==divId

                         select s;



            return result.ToList<Division>(); ;
        }


        public List<Division> Divisioninfo()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Divisions

                         select s;



            return result.ToList<Division>(); ;
        }
        public List<PostDisasterInfo> GetDomainPostById(int id)
        {
            using (var Container = new DMISDBEntities())
            {
                var query = from s in Container.PostDisasterInfoes
                            where s.postId == id 

                            select s;

                return query.ToList<PostDisasterInfo>();


            }

        }

        public List<District> DistrictinfoByDiv(int divId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Districts where s.DivisionId==divId

                         select s;



            return result.ToList<District>(); ;
        }



        public List<District> Districtinfo()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Districts

                         select s;



            return result.ToList<District>(); ;
        }

        public List<District> DistrictinfoById(int? disId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Districts where s.DistrictId==disId

                         select s;



            return result.ToList<District>(); ;
        }

        public List<Upazilla> Upazillainfo()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Upazillas
                         
                         select s;



            return result.ToList<Upazilla>(); ;
        }



        public List<Upazilla> UpazillainfoByDis(int disId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Upazillas where s.DistrictId==disId

                         select s;



            return result.ToList<Upazilla>(); ;
        }

        public List<Upazilla> UpazillainfoById(int? upId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Upazillas where s.UpazillaId==upId

                         select s;



            return result.ToList<Upazilla>(); ;
        }
        public List<Union> Unioninfo()
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Unions

                         select s;



            return result.ToList<Union>(); ;
        }



        public List<Union> UnioninfoByUpa(int upaId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Unions where s.UpazillaId==upaId

                         select s;



            return result.ToList<Union>(); ;
        }

        public List<Union> UnioninfoById(int? unId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Unions where s.UnionId==unId

                         select s;



            return result.ToList<Union>(); ;
        }

        public List<Domain> DomaininfoById(int did)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.Domains

                         where s.DomainId == did
                         select s;



            return result.ToList<Domain>();
        }


        public List<DisasterHistory> DomainHistoryinfoById(int dismgm_id, string his_no)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DisasterHistories

                         where s.DisasterManagementId == dismgm_id && s.HistoryNo.Equals(his_no)
                         select s;



            return result.ToList<DisasterHistory>();
        }
        

        public List<DomainDisasterManagement> GetDisasterInfoUnsaved(int userId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);
            
            var result = from s in dmisdb.DomainDisasterManagements
                         where s.DomainStatus==0 && s.userId==userId
                         select s;



            return result.ToList<DomainDisasterManagement>(); ;
        }

        public List<DomainGeneralInformation> GetGeneralInformationsUnsaved(int userId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainGeneralInformations
                         where s.DomainStatus == 0 && s.userId == userId
                         select s;

            return result.ToList<DomainGeneralInformation>(); 
        }
        public List<DomainSafeLearning> GetSafeLearningInfoUnsaved(int userId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainSafeLearnings
                         where s.DomainStatus == 0 && s.userId == userId
                         select s;

            return result.ToList<DomainSafeLearning>(); ;
        }

        public List<RiskReductionResilienceEducation> GetRiskReductionResilienceEducationInfoUnsaved(int userId)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.RiskReductionResilienceEducations
                         where s.DomainStatus == 0 && s.userId == userId
                         select s;

            return result.ToList<RiskReductionResilienceEducation>(); ;
        }






        public List<DomainDisasterManagement> GetDisasterInfoById(int? id)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainDisasterManagements
                         where s.DisasterManagementId == id
                         select s;



            return result.ToList<DomainDisasterManagement>();
        }

        public List<DomainGeneralInformation> GetGeneralInformationsById(int? id)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainGeneralInformations
                         where s.GeneralInformationId==id
                         select s;

            return result.ToList<DomainGeneralInformation>();
        }
        public List<DomainSafeLearning> GetSafeLearningInfoById(int? id)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.DomainSafeLearnings
                         where s.SafeLearningId == id
                         select s;

            return result.ToList<DomainSafeLearning>(); ;
        }

        public List<RiskReductionResilienceEducation> GetRiskReductionResilienceEducationInfoByid(int? id)
        {
            //  List<UserProfile> info = new List<UserProfile>();
            //  info =  db.UserProfiles.Find(id);

            var result = from s in dmisdb.RiskReductionResilienceEducations
                         where s.RiskResilienceId == id
                         select s;

            return result.ToList<RiskReductionResilienceEducation>(); ;
        }












        public List<webpages_Roles> GetRole()
        {
            var result = from s in dmisdb.webpages_Roles
                        
                         select s;

            return result.ToList<webpages_Roles>(); 

        }

          public List<DomainDisasterManagement> GetDomainDisterLastId(int userId)
        {

            var result = from s in dmisdb.DomainDisasterManagements

                        orderby s.DisasterManagementId descending  where s.userId==userId  select s  ;

            return result.ToList<DomainDisasterManagement>(); 


        }



          public List<DomainGeneralInformation> GetDomainGeneralInfoLastId(int userId)
          {

              var result = from s in dmisdb.DomainGeneralInformations

                           orderby s.GeneralInformationId descending
                           where s.userId == userId
                           select s;

              return result.ToList<DomainGeneralInformation>();


          }


          public List<RiskReductionResilienceEducation> GetDomainRiskReductionResilienceLastId(int userId)
          {

              var result = from s in dmisdb.RiskReductionResilienceEducations

                           orderby s.RiskResilienceId descending
                           where s.userId == userId
                           select s;

              return result.ToList<RiskReductionResilienceEducation>();


          }

          public List<DomainSafeLearning> GetDomainSafeLearningLastId(int userId)
          {

              var result = from s in dmisdb.DomainSafeLearnings

                           orderby s.SafeLearningId descending
                           where s.userId == userId
                           select s;

              return result.ToList<DomainSafeLearning>();


          }

    



    }
}