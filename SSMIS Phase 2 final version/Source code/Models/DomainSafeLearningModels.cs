﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIMS.Models
{
    class DomainSafeLearningModels
    {
        public void Add(DomainSafeLearning DTO)
        {
            using (var container = new DMISDBEntities())
            {
                //DomainSafeLearning domaingen = new DomainSafeLearning();
                container.DomainSafeLearnings.Add(DTO);
                container.SaveChanges();
            }
        }



        public void removeSafeDomain(int? userId, int? mgmId)
        {

            using (var container = new DMISDBEntities())
            {
                var objcheck = (from e in container.DomainSafeLearnings where  e.SafeLearningId == mgmId select e).FirstOrDefault();
                if (objcheck != null)
                {
                    container.DomainSafeLearnings.Remove(objcheck);
                    container.SaveChanges();
                }


            }

        }

        public void Edit(DomainSafeLearning DTO)
        {
            using (var container = new DMISDBEntities())
            {
                var Comp = new DomainSafeLearning();
                Comp = container.DomainSafeLearnings.FirstOrDefault(o => o.SafeLearningId==DTO.SafeLearningId);
                
                Comp.TotalBuildingStructer = DTO.TotalBuildingStructer;
                Comp.TotalKaccaBuildingStructer = DTO.TotalKaccaBuildingStructer;
                Comp.TotalPaccaBuildingStructer = DTO.TotalPaccaBuildingStructer;
                Comp.TotalSemiPaccaBuildingStructer = DTO.TotalSemiPaccaBuildingStructer;
                Comp.TotalUnsedBuildingStructer = DTO.TotalUnsedBuildingStructer;
                Comp.TotalClassRoom = DTO.TotalClassRoom;
                Comp.TotalKaccaClassRoom = DTO.TotalKaccaClassRoom;
                Comp.TotalSemiPaccaClassRoom = DTO.TotalSemiPaccaClassRoom;
                Comp.TotalPaccaClassRoom = DTO.TotalPaccaClassRoom;
                Comp.TotalUnsedClassRoom = DTO.TotalUnsedClassRoom;
                Comp.SourceOfDrinkingWater = DTO.SourceOfDrinkingWater;
                Comp.OwnDrinkingSource = DTO.OwnDrinkingSource;
                Comp.SafeDrinkingSource = DTO.SafeDrinkingSource;
                Comp.SourceOfWaterIfTubewellArsenicFree = DTO.SourceOfWaterIfTubewellArsenicFree;
                Comp.SourceOfWaterIfTubewellFunctional = DTO.SourceOfWaterIfTubewellFunctional;
                Comp.SourceOfWaterChildFriendly = DTO.SourceOfWaterChildFriendly;
                Comp.SourceOfWaterDisableFriendly = DTO.SourceOfWaterDisableFriendly;
                Comp.SourceOfWaterFunctionDuringDisaster = DTO.SourceOfWaterFunctionDuringDisaster;

                Comp.SourceOfWaterAwayFromSchool = DTO.SourceOfWaterAwayFromSchool;
                Comp.LatrineFacilityForStudent = DTO.LatrineFacilityForStudent;
                Comp.LatrineForMaleStudent = DTO.LatrineForMaleStudent;
                Comp.LatrineForFemaleStudent = DTO.LatrineForFemaleStudent;
                Comp.LatrineFacilityForTeacher = DTO.LatrineFacilityForTeacher;
                Comp.LatrineFacilityForMaleTeacher = DTO.LatrineFacilityForMaleTeacher;
                Comp.LatrineFacilityForFeMaleTeacher = DTO.LatrineFacilityForFeMaleTeacher;
                Comp.LatrineRegularMaintain = DTO.LatrineRegularMaintain;
                Comp.LatrineChildAccessiblePhysicalDisabilties = DTO.LatrineChildAccessiblePhysicalDisabilties;
                Comp.LatrineFemaleCondtion = DTO.LatrineFemaleCondtion;
                Comp.LatrineMaleCondition = DTO.LatrineMaleCondition;
                Comp.LatrinePhysicalDisabilties = DTO.LatrinePhysicalDisabilties;
                Comp.LatrineFacilityFunctional = DTO.LatrineFacilityFunctional;
                Comp.InstituteId = DTO.InstituteId;
                Comp.StudentToiletBoth = DTO.StudentToiletBoth;
                Comp.TeacherToiletBoth = DTO.TeacherToiletBoth;
                Comp.userId = DTO.userId;
                container.SaveChanges();
            }
        }

        public void EditStatus(DomainSafeLearning DTO)
        {
            using (var container = new DMISDBEntities())
            {
                var Comp = new DomainSafeLearning();
                Comp = container.DomainSafeLearnings.FirstOrDefault(o => o.SafeLearningId.Equals(DTO.SafeLearningId));
                Comp.DomainStatus = DTO.DomainStatus;
                Comp.SafeLearningId = DTO.SafeLearningId;

                container.SaveChanges();
            }
        }


        public List<DomainSafeLearning> GetDomainGeneral(int proid, string productname, int catid, int unitid, int compid)
        {
            using (var Container = new DMISDBEntities())
            {
                var query = from s in Container.DomainSafeLearnings
                            //join unit in Container.Units on s.UnitId equals unit.UnitId
                            //join cat in Container.Categories on s.CategoryId equals cat.CatId
                            select new { s };


                //  return query.ToList<DomainSafeLearning>();

                var result = from o in query
                             orderby o.s.SafeLearningId descending

                             select new DomainSafeLearning
                             {
                                 SafeLearningId=o.s.SafeLearningId
                             };
                return result.ToList<DomainSafeLearning>();


            }

        }

    }
}
