﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIMS.Models
{
    public class GraphModels
    {
        private DMISDBEntities dmisdb = new DMISDBEntities();

        public List<DomainGeneralInformation> GetRptByGeoPostion(int division, int district, int upazila, int union)
        {


            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 && !s.GraphicalLocation.Equals("") select s;

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);

            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
            }
            


            return query.ToList<DomainGeneralInformation>();
        }

        public List<CheckBox> GetHazardsByStrcType(int division, int district, int upazila, int union,  string areaType)
        {


            var query = from s in dmisdb.CheckBoxs where s.QuestionId.Equals("19.0") && s.SafeLearningId!=null select s;

            List<CheckBox> list = new List<CheckBox>();

            list = query.ToList();

          
            int cc = query.Count();
            int lcc = list.Count();
           
            if (division != 0)
            {
                query = query.Where(c => c.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
               // cc = query.Count();
            }

            
            if (district != 0)
            {
               // query = query.Where(c => c.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                list.Find(c => c.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                list.Find(item=>item.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId==upazila);
               // list.Where(item => item.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                // query = query.Where(c =>c.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                lcc = list.Count();

            }
            if (union != 0)
            {
               
                list.Find(item => item.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
               // query = query.Where(c => c.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                lcc = list.Count();
            }
          
           
            if (areaType != "All")
            {
              //  query = query.Where(c => c.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.SchoolArea.Equals(areaType));

                list.Where(item => item.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.SchoolArea.Equals(areaType));
               
                lcc = list.Count();
            }

           // query = query.Where(c=>c.);



         





            return list.ToList<CheckBox>();
        }


        public List<CheckBoxDto> GetHazardsBySafeType(int division, int district, int upazila, int union, string areaType)
        {


             var query = from s in dmisdb.CheckBoxs
                           
                            join safe in dmisdb.DomainSafeLearnings on s.SafeLearningId equals safe.SafeLearningId
                            join dom in dmisdb.Domains on safe.SafeLearningId equals dom.SafeLearningId
                              join gen in dmisdb.DomainGeneralInformations on dom.GeneralInformationId equals gen.GeneralInformationId
                           
                            select new {s,dom,safe,gen};



            
                 query = query.Where(c => c.s.QuestionId.Equals("19.0"));
                 query = query.Where(c=>!c.s.HazardsName.Equals(""));
                 query = query.Where(c => !c.s.ImpactName.Equals(""));

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.gen.DivisionId==division);
                // cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.gen.DistrictId == district);
                // cc = query.Count();
            }
            if (union != 0)
            {
                query = query.Where(c => c.gen.UnionId == union);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.gen.UpazillaId == upazila);
                cc = query.Count();

            }

            if (areaType != "All")
            {
                query = query.Where(c => c.gen.SchoolArea.Equals(areaType));
            }

            var result = from o in query
                         orderby o.s.ChkId descending

                         select new CheckBoxDto
                         {
                             ChkId = o.s.ChkId,
                             ChkValue=o.s.ChkValue,
                             HazardsName=o.s.HazardsName,
                             ImpactName=o.s.ImpactName
                         };
            // query = query.Where(c=>c.);









            return result.ToList<CheckBoxDto>();
        }

        public List<DomainSafeLearning> GetByStrcType(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DomainSafeLearnings  select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All")
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.SchoolArea.Equals(areaType));
            }


            return query.ToList<DomainSafeLearning>();
        }


        public List<DisasterHistory> GetRiskHistoryByStrcType(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DisasterHistories  select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All")
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.SchoolArea.Equals(areaType));
            }

            // query = query.Where(c=>c.);









            return query.ToList<DisasterHistory>();
        }



        public List<DisasterHistory> GetRiskHistoryByStrcModerate(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DisasterHistories where s.DisasterImpact.Equals("moderate") select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All")
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.SchoolArea.Equals(areaType));
            }

            // query = query.Where(c=>c.);









            return query.ToList<DisasterHistory>();
        }



        public List<DisasterHistory> GetRiskHistoryByStrcSevere(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DisasterHistories where s.DisasterImpact.Equals("Severe") select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All")
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.SchoolArea.Equals(areaType));
            }

            // query = query.Where(c=>c.);









            return query.ToList<DisasterHistory>();
        }


        public List<DisasterHistory> GetRiskHistoryByStrcMinor(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DisasterHistories where s.DisasterImpact.Equals("minor") select s;


            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All")
            {
                query = query.Where(c => c.DomainDisasterManagement.Domains.FirstOrDefault().DomainGeneralInformation.SchoolArea.Equals(areaType));
            }

            // query = query.Where(c=>c.);









            return query.ToList<DisasterHistory>();
        }



        public List<PostDisasterInfo> GetPostUrbanRuralAffected(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.PostDisasterInfoes select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "")
            {
                query = query.Where(c => c.HazardName.Equals(areaType));
            }

            // query = query.Where(c=>c.);









            return query.ToList<PostDisasterInfo>();
        }








        public List<DomainGeneralInformation> GetByAreaType(int division, int district, int upazila, int union, string faci)
        {
            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
                cc = query.Count();
            }
            if (faci != "All"&&faci!=null)
            {
                query = query.Where(c => c.SchoolArea.Equals(faci));


            }


            return query.ToList<DomainGeneralInformation>();

        }

        public List<RiskReductionResilienceEducation> GetByRiskAreaType(int division, int district, int upazila, int union, string faci)
        {
            var query = from s in dmisdb.RiskReductionResilienceEducations where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (faci != "All" && faci != null)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.SchoolArea.Equals(faci));


            }


            return query.ToList<RiskReductionResilienceEducation>();

        }

        public List<CheckBoxDto> GetByRiskAreaCheckType(int division, int district, int upazila, int union, string faci)
        {
            
            var query = from s in dmisdb.CheckBoxs

                        join safe in dmisdb.RiskReductionResilienceEducations on s.RiskResilienceId equals safe.RiskResilienceId
                        join dom in dmisdb.Domains on safe.RiskResilienceId equals dom.RiskResilienceId
                        join gen in dmisdb.DomainGeneralInformations on dom.GeneralInformationId equals gen.GeneralInformationId

                        select new { s, dom, safe, gen };




            query = query.Where(c => c.s.QuestionId.Equals("24.7"));

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.gen.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.gen.DistrictId == district);
                 cc = query.Count();
            }
            if (union != 0)
            {
                query = query.Where(c => c.gen.UnionId == union);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.gen.UpazillaId == upazila);
                cc = query.Count();

            }

            if (faci != "All" && faci != null)
            {
                query = query.Where(c => c.gen.SchoolArea.Equals(faci));
                cc = query.Count();
            }

            var result = from o in query
                         orderby o.s.ChkId descending

                         select new CheckBoxDto
                         {
                             ChkId = o.s.ChkId,
                             ChkValue = o.s.ChkValue,
                             HazardsName = o.s.HazardsName,
                             ImpactName = o.s.ImpactName
                         };
            // query = query.Where(c=>c.);









            return result.ToList<CheckBoxDto>();

        }


        public List<DomainSafeLearning> GetByToiletType(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DomainSafeLearnings where s.DomainStatus==1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.LatrineRegularMaintain.Equals(areaType));
            }


            return query.ToList<DomainSafeLearning>();
        }

        public List<DomainSafeLearning> GetByToiletType1(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DomainSafeLearnings where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
            
                query = query.Where(c => c.LatrinePhysicalDisabilties.Equals(areaType));
            }


            return query.ToList<DomainSafeLearning>();
        }

        public List<DomainSafeLearning> GetByToiletType2(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DomainSafeLearnings where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.LatrineChildAccessiblePhysicalDisabilties.Equals(areaType));
            }


            return query.ToList<DomainSafeLearning>();
        }


        public List<DomainSafeLearning> GetByDrinkingFaciltyFunc(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DomainSafeLearnings where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.SourceOfWaterFunctionDuringDisaster.Equals(areaType));
            }


            return query.ToList<DomainSafeLearning>();
        }

        public List<DomainSafeLearning> GetByDrinkingFaciltyDisable(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DomainSafeLearnings where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.SourceOfWaterDisableFriendly.Equals(areaType));
            }


            return query.ToList<DomainSafeLearning>();
        }

        public List<DomainSafeLearning> GetByDrinkingFaciltyChild(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DomainSafeLearnings where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.SourceOfWaterChildFriendly.Equals(areaType));
            }


            return query.ToList<DomainSafeLearning>();
        }

        public List<DomainSafeLearning> GetByDrinkingFaciltyTubwellFunc(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DomainSafeLearnings where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.SourceOfWaterIfTubewellFunctional.Equals(areaType));
            }


            return query.ToList<DomainSafeLearning>();
        }

        public List<DomainSafeLearning> GetByDrinkingFaciltyTubwellArsinicFree(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DomainSafeLearnings where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.SourceOfWaterIfTubewellArsenicFree.Equals(areaType));
            }


            return query.ToList<DomainSafeLearning>();
        }

        public List<DomainSafeLearning> GetByDrinkingFaciltySafe(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DomainSafeLearnings where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.SafeDrinkingSource.Equals(areaType));
            }


            return query.ToList<DomainSafeLearning>();
        }
        public List<DomainSafeLearning> GetByDrinkingFaciltyOwn(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DomainSafeLearnings where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.OwnDrinkingSource.Equals(areaType));
            }


            return query.ToList<DomainSafeLearning>();
        }



        public List<DomainSafeLearning> GetByCommonToiletType(int division, int district, int upazila, int union)
        {
            var query = from s in dmisdb.DomainSafeLearnings where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
           


            return query.ToList<DomainSafeLearning>();
        }

        public List<DomainSafeLearning> GetByAccetableGirl(int division, int district, int upazila, int union)
        {
            var query = from s in dmisdb.DomainSafeLearnings where s.DomainStatus == 1 && !s.LatrineFemaleCondtion.Equals("")  select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }



            return query.ToList<DomainSafeLearning>();
        }

        public List<DomainSafeLearning> GetByAccetableBoy(int division, int district, int upazila, int union)
        {
            var query = from s in dmisdb.DomainSafeLearnings where s.DomainStatus == 1 && !s.LatrineMaleCondition.Equals("")  select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }



            return query.ToList<DomainSafeLearning>();
        }



        public List<DomainGeneralInformation> GetByDrinkingFaciltyFire(int division, int district, int upazila, int union, string faci)
        {
            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
                cc = query.Count();
            }
            if (faci != "All" && faci != null)
            {
                query = query.Where(c => c.HasFireExtinguiser.Equals(faci));


            }


            return query.ToList<DomainGeneralInformation>();

        }

        public List<DomainGeneralInformation> GetByDrinkingFaciltyAid(int division, int district, int upazila, int union, string faci)
        {
            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
                cc = query.Count();
            }
            if (faci != "All" && faci != null)
            {
                query = query.Where(c => c.FirstAid.Equals(faci));


            }


            return query.ToList<DomainGeneralInformation>();
        }

        public List<DomainGeneralInformation> GetByDrinkingFaciltyComp(int division, int district, int upazila, int union, string faci)
        {
            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
                cc = query.Count();
            }
            if (faci != "All" && faci != null)
            {
                query = query.Where(c => c.ComputerLab.Equals(faci));


            }


            return query.ToList<DomainGeneralInformation>();
        }

        public List<DomainGeneralInformation> GetByDrinkingFaciltyInter(int division, int district, int upazila, int union, string faci)
        {
            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
                cc = query.Count();
            }
            if (faci != "All" && faci != null)
            {
                query = query.Where(c => c.InternetFacility.Equals(faci));


            }


            return query.ToList<DomainGeneralInformation>();
        }

        public List<DomainGeneralInformation> GetByDrinkingFaciltyPlay(int division, int district, int upazila, int union, string faci)
        {
            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
                cc = query.Count();
            }
            if (faci != "All" && faci != null)
            {
                query = query.Where(c => c.PlayingGround.Equals(faci));


            }


            return query.ToList<DomainGeneralInformation>();
        }

        public List<DomainGeneralInformation> GetByDrinkingFaciltyTubwellArsinicElect(int division, int district, int upazila, int union, string faci)
        {
             var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
                cc = query.Count();
            }
            if (faci != "All" && faci != null)
            {
                query = query.Where(c => c.ElectricityFacillity.Equals(faci));


            }


            return query.ToList<DomainGeneralInformation>();
        }

        public List<DomainGeneralInformation> GetByDrinkingFaciltyTubwellTifi(int division, int district, int upazila, int union, string faci)
        {
            var query = from s in dmisdb.DomainGeneralInformations where s.DomainStatus == 1 select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.UnionId == union);
                cc = query.Count();
            }
            if (faci != "All" && faci != null)
            {
                query = query.Where(c => c.TiffinFacility.Equals(faci));


            }


            return query.ToList<DomainGeneralInformation>();
        }

        public List<CheckBox> GetByDrinkingFaciltyTubwellVol(int division, int district, int upazila, int union, string faci)
        {
            var query = from s in dmisdb.CheckBoxs where s.QuestionId.Equals("15.8") select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.DomainSafeLearning.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
           

            return query.ToList<CheckBox>();
        }


        public List<DomainDisasterManagement> GetDisasterContinuity(int division, int district, int upazila, int union, string areaType)
        {


            var query = from s in dmisdb.DomainDisasterManagements  select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.ContingencyPlan.Equals(areaType));
            }

            // query = query.Where(c=>c.);









            return query.ToList<DomainDisasterManagement>();
        }
        public List<DomainDisasterManagement> GetDisasterTemporary(int division, int district, int upazila, int union, string areaType)
        {

            var query = from s in dmisdb.DomainDisasterManagements select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.CatchmentAreaForTemporaryLearning.Equals(areaType));
            }

            // query = query.Where(c=>c.);









            return query.ToList<DomainDisasterManagement>();
        }
        public List<DomainDisasterManagement> GetDisasterCatchment(int division, int district, int upazila, int union, string areaType)
        {

            var query = from s in dmisdb.DomainDisasterManagements select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.CatchmentAreaForTeacherBackup.Equals(areaType));
            }

            // query = query.Where(c=>c.);









            return query.ToList<DomainDisasterManagement>();
        }
        public List<DomainDisasterManagement> GetDisasterEmergency(int division, int district, int upazila, int union, string areaType)
        {

            var query = from s in dmisdb.DomainDisasterManagements select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.AccessibleEmergencyFund.Equals(areaType));
            }

            // query = query.Where(c=>c.);









            return query.ToList<DomainDisasterManagement>();
        }
        public List<RiskReductionResilienceEducation> GetRiskDRR(int division, int district, int upazila, int union, string areaType)
        {

            var query = from s in dmisdb.RiskReductionResilienceEducations select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            if (areaType != "All" && areaType != null)
            {
                query = query.Where(c => c.DisasterRelatedTraining.Equals(areaType));
            }


            return query.ToList<RiskReductionResilienceEducation>();
        }
        public List<RiskReductionResilienceEducation> GetRiskDrill(int division, int district, int upazila, int union, string areaType)
        {

            var query = from s in dmisdb.RiskReductionResilienceEducations where !s.DrilFacility.Equals("") select s;

            int cc = query.Count();

            if (division != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DivisionId == division);
                cc = query.Count();
            }
            if (district != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.DistrictId == district);
                cc = query.Count();
            }
            if (upazila != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UpazillaId == upazila);
                cc = query.Count();

            }
            if (union != 0)
            {
                query = query.Where(c => c.Domains.FirstOrDefault().DomainGeneralInformation.UnionId == union);
                cc = query.Count();
            }
            

            return query.ToList<RiskReductionResilienceEducation>();
        }



    }
}