﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIMS.Models
{
    public class ReportModel
    {
        public List<DomainGeneralInformation> GeneralInformationInfo { get; set; }
        public List<CheckBox> CheckDtoInfo { get; set; }
        public List<DomainSafeLearning> SafeInformationInfo { get; set; }
        public List<DomainDisasterManagement> DisasterManagementInformationInfo { get; set; }
        public List<RiskReductionResilienceEducation> RiskReductionInformationInfo { get; set; }
        public List<Division> DivisionInfo { get; set; }
        public List<District> DistrictInfo { get; set; }
        public List<Upazilla> UpazillaInfo { get; set; }
        public List<Union> UnionInfo { get; set; } 
    }
}