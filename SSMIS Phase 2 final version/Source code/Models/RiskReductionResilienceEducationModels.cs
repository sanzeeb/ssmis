﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DIMS.Models
{
    class RiskReductionResilienceEducationModels
    {
        public void Add(RiskReductionResilienceEducation DTO)
        {
            using (var container = new DMISDBEntities())
            {
                //RiskReductionResilienceEducation domaingen = new RiskReductionResilienceEducation();
                container.RiskReductionResilienceEducations.Add(DTO);
                container.SaveChanges();
            }
        }


        public void removeRiskDomain(int? userId, int? mgmId)
        {

            using (var container = new DMISDBEntities())
            {
                var objcheck = (from e in container.RiskReductionResilienceEducations where  e.RiskResilienceId == mgmId select e).FirstOrDefault();
                if (objcheck != null)
                {
                    container.RiskReductionResilienceEducations.Remove(objcheck);
                    container.SaveChanges();
                }

            }

        }

        public void Edit(RiskReductionResilienceEducation DTO)
        {
            using (var container = new DMISDBEntities())
            {
                var Comp = new RiskReductionResilienceEducation();
                Comp = container.RiskReductionResilienceEducations.FirstOrDefault(o => o.RiskResilienceId==DTO.RiskResilienceId);
                Comp.SchoolAssemblies = DTO.SchoolAssemblies;
                Comp.DisasterRelatedTraining = DTO.DisasterRelatedTraining;
                Comp.TrainingOnName = DTO.TrainingOnName;
                Comp.HowManyTeacherAttended = DTO.HowManyTeacherAttended;
                Comp.SmcMemberAttented = DTO.SmcMemberAttented;
                Comp.StudentAttented = DTO.StudentAttented;
                Comp.RegularCurriculum = DTO.RegularCurriculum;
                Comp.FromExtraCurriculum = DTO.FromExtraCurriculum;
                Comp.SchoolAssemblies = DTO.SchoolAssemblies;
                Comp.AfterSchoolClubs = DTO.AfterSchoolClubs;
                Comp.HowStudentAwareAboutDisaster = DTO.HowStudentAwareAboutDisaster;
                Comp.DrilFacility = DTO.DrilFacility;
                Comp.OtherSource = DTO.OtherSource;
                Comp.userId = DTO.userId;
                Comp.InstituteId = DTO.InstituteId;
                container.SaveChanges();
            }
        }
        public void EditStatus(RiskReductionResilienceEducation DTO)
        {
            using (var container = new DMISDBEntities())
            {
                var Comp = new RiskReductionResilienceEducation();
                Comp = container.RiskReductionResilienceEducations.FirstOrDefault(o => o.RiskResilienceId.Equals(DTO.RiskResilienceId));
                Comp.RiskResilienceId = DTO.RiskResilienceId;
                Comp.DomainStatus = DTO.DomainStatus;

                container.SaveChanges();
            }
        }

        public List<RiskReductionResilienceEducation> GetDomainGeneral(int proid, string productname, int catid, int unitid, int compid)
        {
            using (var Container = new DMISDBEntities())
            {
                var query = from s in Container.RiskReductionResilienceEducations

                            select new { s };




                var result = from o in query
                             orderby o.s.RiskResilienceId descending

                             select new RiskReductionResilienceEducation
                             {
                                 RiskResilienceId = o.s.RiskResilienceId
                             };
                return result.ToList<RiskReductionResilienceEducation>();


            }

        }
    }
}
