﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIMS.Models
{
    class CheckBoxsModels
    {
        public void Add(CheckBox DTO)
        {
            using (var container = new DMISDBEntities())
            {
                
                container.CheckBoxs.Add(DTO);
                container.SaveChanges();
            }
        }

        public void removeRiksCheck(int? userId,int? rikId,string questionNo)
        {

            using (var container = new DMISDBEntities())
            {
                var objcheck = from e in container.CheckBoxs where  e.RiskResilienceId==rikId && e.QuestionId.Equals(questionNo) select e;


                //for (int i = 0; i<objcheck.Count(); i++)
                //{


                //}
                foreach (var subject in objcheck)
                {
                    container.CheckBoxs.Remove(subject);
                }
                container.SaveChanges();

                
            }
          
        }


        public void removeDisMgmCheck(int? userId, int? mgmId, string questionNo)
        {

            using (var container = new DMISDBEntities())
            {
                var objcheck = from e in container.CheckBoxs where  e.DisasterManagementId == mgmId && e.QuestionId.Equals(questionNo) select e;


                //for (int i = 0; i<objcheck.Count(); i++)
                //{


                //}
                foreach (var subject in objcheck)
                {
                    container.CheckBoxs.Remove(subject);
                }
                container.SaveChanges();


            }

        }


        public void removeGeneralInfoCheck(int? userId, int? genId, string questionNo)
        {

            using (var container = new DMISDBEntities())
            {
                var objcheck = from e in container.CheckBoxs where  e.GeneralInformationId == genId && e.QuestionId.Equals(questionNo) select e;


                //for (int i = 0; i<objcheck.Count(); i++)
                //{


                //}
                foreach (var subject in objcheck)
                {
                    container.CheckBoxs.Remove(subject);
                }
                container.SaveChanges();


            }

        }


        public void removeSafeLearningCheck(int? userId, int? sflId, string questionNo)
        {

            using (var container = new DMISDBEntities())
            {
                var objcheck = from e in container.CheckBoxs where  e.SafeLearningId == sflId && e.QuestionId.Equals(questionNo) select e;


                //for (int i = 0; i<objcheck.Count(); i++)
                //{


                //}
                foreach (var subject in objcheck)
                {
                    container.CheckBoxs.Remove(subject);
                }
                container.SaveChanges();


            }

        }




        private void CheckRemoved(CheckBox removedBook)
        {
            var container = new DMISDBEntities();
            CheckBox CheckBoxs = container.CheckBoxs.SingleOrDefault(ba => ba.ChkId == removedBook.ChkId);
            if (CheckBoxs != null)
            {
                container.CheckBoxs.Remove(CheckBoxs);
                container.SaveChanges();

            }
        }
    }
}
