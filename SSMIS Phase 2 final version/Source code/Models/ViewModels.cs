﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIMS.Models
{
    public class ViewModels
    {
        public List<UserProfile> userInfo { get; set; }
        public List<webpages_Roles> allRole { get; set; }

        public List<DomainDisasterManagement> DisasterManagementInfo { get; set; }

        public List<DomainGeneralInformation> GeneralInformationInfo { get; set; }

        public List<DomainSafeLearning> SafeLearningInfo { get; set; }

        public List<RiskReductionResilienceEducation> RiskReductionResilienceEducationInfo { get; set; }

        public List<DomainDisasterManagement> DisasterManagementInfoUnsv { get; set; }

        public List<DomainGeneralInformation> GeneralInformationInfoUnsv { get; set; }

        public List<DomainSafeLearning> SafeLearningInfoUnsv { get; set; }

        public List<RiskReductionResilienceEducation> RiskReductionResilienceEducationInfoUnsv { get; set; }

        public List<Domain> DomainInfo { get; set; }
        public List<DisasterHistory> DisasterManagementInfoHistory1 { get; set; }
        public List<DisasterHistory> DisasterManagementInfoHistory2 { get; set; }
        public List<DisasterHistory> DisasterManagementInfoHistory3 { get; set; }

        public List<CheckBox> CheckBoxInfo15_8 { get; set; }
        public List<CheckBox> CheckBoxInfo17_1 { get; set; }
        public List<CheckBox> CheckBoxInfo21_1 { get; set; }
        public List<CheckBox> CheckBoxInfo21_2 { get; set; }
        public List<CheckBox> CheckBoxInfo21_3 { get; set; }
        public List<CheckBox> CheckBoxInfo21_0 { get; set; }
       
        public List<CheckBox> CheckBoxInfo19_0 { get; set; }
        public List<CheckBox> CheckBoxInfo24_7 { get; set; }
        public List<CheckBox> CheckBoxInfo23_3 { get; set; }

        public List<Division> DivisionInfo { get; set; }
        public List<District> DistrictInfo { get; set; }
        public List<Upazilla> UpazillaInfo { get; set; }
        public List<Union> UnionInfo { get; set; }

        public List<PostDisasterInfo> PostDisasterInfo { get; set; }
       
    }
}