﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace DIMS.Models
{
    public class DomainGeneralInformationModels
    {
        public void Add(DomainGeneralInformation DTO)
        {
            using (var container = new DMISDBEntities())
            {
                //DomainGeneralInformation domaingen = new DomainGeneralInformation();
                //container.DomainGeneralInformations.Add(DTO);
                container.DomainGeneralInformations.Add(DTO);
                container.SaveChanges();
            }
        }

        public void Edit(DomainGeneralInformation DTO)
        {
            using (var container = new DMISDBEntities())
            {
                var Comp = new DomainGeneralInformation();
                Comp = container.DomainGeneralInformations.FirstOrDefault(o => o.GeneralInformationId==DTO.GeneralInformationId);
                Comp.GeneralInformationId = DTO.GeneralInformationId;
                Comp.Altitude = DTO.Altitude;
                Comp.GpsLatitude = DTO.GpsLatitude;
                Comp.GpsLongtitude = DTO.GpsLongtitude;
                Comp.GraphicalLocation = DTO.GraphicalLocation;

                Comp.AuthorityType = DTO.AuthorityType;
                Comp.BankACNo = DTO.BankACNo;
                Comp.BankAddress = DTO.BankAddress;
                Comp.BankBranch = DTO.BankBranch;
                Comp.BankContact = DTO.BankContact;
                Comp.BankName = DTO.BankName;
                Comp.ComputerLab = DTO.ComputerLab;
                Comp.ElectricityFacillity = DTO.ElectricityFacillity;
                Comp.EstabilishmentDate =DTO.EstabilishmentDate ;
                Comp.FirstAid = DTO.FirstAid;
                Comp.Embankment = DTO.Embankment;

                Comp.FunctionalFireExtinguise = DTO.FunctionalFireExtinguise;

                Comp.GobEIIN = DTO.GobEIIN;
                Comp.HasFireExtinguiser = DTO.HasFireExtinguiser;

                Comp.HowManyShift = DTO.HowManyShift;

                Comp.InstitudeDistric = DTO.InstitudeDistric;
                Comp.InstitudeDivision = DTO.InstitudeDivision;
                Comp.InstitudeHouse = DTO.InstitudeHouse;
                Comp.InstitudeRoad = DTO.InstitudeRoad;
                Comp.InstitudeSection = DTO.InstitudeSection;
                Comp.InstitudeUpazilla = DTO.InstitudeUpazilla;
                Comp.InstitudeVillage = DTO.InstitudeVillage;
                Comp.InstituteAddress = DTO.InstituteAddress;
                Comp.InstituteBng = DTO.InstituteBng;
                Comp.InstituteEng = DTO.InstituteEng;

                Comp.InstituteId = DTO.InstituteId;

                Comp.InstituteFax = DTO.InstituteFax;
                Comp.InstituteMobile = DTO.InstituteMobile;
                Comp.InstitutePhone = DTO.InstitutePhone;
                Comp.InstituteType = DTO.InstituteType;
                Comp.InstituteWebUrl = DTO.InstituteWebUrl;
                Comp.InternetFacility = DTO.InternetFacility;
                Comp.ManagementType = DTO.ManagementType;
                Comp.NonEducationalStuff = DTO.NonEducationalStuff;
                Comp.PlayingGround = DTO.PlayingGround;
                Comp.SchoolArea = DTO.SchoolArea;
                Comp.SmcExpiredate = DTO.SmcExpiredate;
                Comp.SmcPrisedentMobile = DTO.SmcPrisedentMobile;
                Comp.SmcPrisedentName = DTO.SmcPrisedentName;

                Comp.StudentDisableFeMale = DTO.StudentDisableFeMale;

                Comp.StudentDisableMale = DTO.StudentDisableMale;
                Comp.StudentEthnicFeMale = DTO.StudentEthnicFeMale;
                Comp.StudentEthnicMale = DTO.StudentEthnicMale;
                Comp.StudentFemale = DTO.StudentFemale;
                Comp.StudentMale = DTO.StudentMale;
                Comp.TeacherFemale = DTO.TeacherFemale;
                Comp.TeacherMale = DTO.TeacherMale;

                Comp.BankACNo = DTO.BankACNo;
                Comp.BankName = DTO.BankName;
                Comp.BankAddress = DTO.BankAddress;
                Comp.BankContact = DTO.BankContact;
                Comp.BankBranch = DTO.BankBranch;

                Comp.TiffinFacility = DTO.TiffinFacility;
                Comp.TransportionMode = DTO.TransportionMode;
                Comp.VacantTeacherPosition = DTO.VacantTeacherPosition;
               
              //  Comp.userId = DTO.userId;


                Comp.InstitudeUnion = DTO.InstitudeUnion;
                Comp.InstitudeUpazilla = DTO.InstitudeUpazilla;
                Comp.InstitudeDivision = DTO.InstitudeDivision;
                Comp.InstitudeDistric = DTO.InstitudeDistric;
                Comp.InstitudeSection = DTO.InstitudeSection;
                Comp.InstitudeVillage = DTO.InstitudeVillage;
                Comp.InstitudeRoad = DTO.InstitudeRoad;
                Comp.InstitudeHouse = DTO.InstitudeHouse;

                container.SaveChanges();
            }
        }


        public void EditStatus(DomainGeneralInformation DTO)
        {
            using (var container = new DMISDBEntities())
            {
                var Comp = new DomainGeneralInformation();
                Comp = container.DomainGeneralInformations.FirstOrDefault(o => o.GeneralInformationId.Equals(DTO.GeneralInformationId));
                Comp.GeneralInformationId = DTO.GeneralInformationId;
                Comp.DomainStatus = DTO.DomainStatus;

                container.SaveChanges();
            }
        }


        public void removeGenDomain(int? userId, int? mgmId)
        {

            using (var container = new DMISDBEntities())
            {
                var objcheck = (from e in container.DomainGeneralInformations where  e.GeneralInformationId == mgmId select e).FirstOrDefault();
                if (objcheck != null)
                {
                    container.DomainGeneralInformations.Remove(objcheck);
                    container.SaveChanges();
                }

            }

        }
        public List<DomainGeneralInformation> CheckInstitutedIdExist(string insid)
        {
            using (var Container = new DMISDBEntities())
            {
                var query = from s in Container.DomainGeneralInformations

                            where s.InstituteId.Equals(insid)
                            
                            select s;

                return query.ToList<DomainGeneralInformation>();


           
            }

        }

        public List<DomainGeneralInformation> GetDomainGeneral(int proid, string productname, int catid, int unitid, int compid)
        {
            using (var Container = new DMISDBEntities())
            {
                var query = from s in Container.DomainGeneralInformations
                            //join unit in Container.Units on s.UnitId equals unit.UnitId
                            //join cat in Container.Categories on s.CategoryId equals cat.CatId
                            select new { s };


              //  return query.ToList<DomainGeneralInformation>();

                var result = from o in query
                             orderby o.s.GeneralInformationId descending

                             select new DomainGeneralInformation
                             {
                                GobEIIN=o.s.GobEIIN
                             };
                return result.ToList<DomainGeneralInformation>();


                //if (proid != 0)
                //    query = query.Where(c => c.s.ProductId.Equals(proid));
                //if (compid != 0)
                //    query = query.Where(c => c.s.CompanyInfo.CompId.Equals(compid));
                //if (!string.IsNullOrEmpty(productname))
                //    query = query.Where(c => c.s.ProductName.Contains(productname));
                //if (catid != 0)
                //    query = query.Where(c => c.cat.CatId.Equals(catid));
                //if (unitid != 0)
                //    query = query.Where(c => c.unit.UnitId.Equals(unitid));

                //    var result = from o in query
                //                 orderby o.s.ProductId descending

                //                 select new DomainGeneralInformation
                //                 {
                //                     CompName = o.s.CompanyInfo.CompName,
                //                     CompId = o.s.CompanyInfo.CompId,
                //                     CatId = o.cat.CatId,
                //                     UnitId = o.unit.UnitId,
                //                     UnitName = o.unit.UnitName,
                //                     ProductPurchasePrice = o.s.ProductPurchasePrice,
                //                     CategoryId = o.cat.CatId,
                //                     CategoryName = o.cat.CategoryName,
                //                     ProductId = o.s.ProductId,
                //                     ProductName = o.s.ProductName,
                //                     CreateBy = o.s.CreateBy,
                //                     CenterReorderValue = o.s.CenterReorderValue,
                //                     CreateDate = o.s.CreateDate,
                //                     ProductSalePrice = o.s.ProductSalePrice
                //                 };
                //    return result.ToList<ProductDTO>();
                //}
            }

        }
    }
}