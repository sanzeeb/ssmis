﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIMS.Models
{
    class DisasterHistoryModels
    {

        public void Add(DisasterHistory DTO)
        {
            using (var container = new DMISDBEntities())
            {
                //DisasterHistory domaingen = new DisasterHistory();
                container.DisasterHistories.Add(DTO);
                container.SaveChanges();
            }
        }


        public void removeDisHistory(string hisno, int? mgmId)
        {

            using (var container = new DMISDBEntities())
            {
                var objcheck = (from e in container.DisasterHistories where e.HistoryNo.Equals(hisno) && e.DisasterManagementId == mgmId select e).FirstOrDefault();
                if (objcheck != null)
                {
                    container.DisasterHistories.Remove(objcheck);
                    container.SaveChanges();
                }


            }

        }

        public void Edit(DisasterHistory DTO)
        {
            using (var container = new DMISDBEntities())
            {
                var Comp = new DisasterHistory();
                Comp = container.DisasterHistories.FirstOrDefault(o => o.DisasterManagementId.Equals(DTO.DisasterManagementId));
                Comp.DisasterImpact = DTO.DisasterImpact;

                container.SaveChanges();
            }
        }


        public List<DisasterHistory> GetDomainGeneral(int proid, string productname, int catid, int unitid, int compid)
        {
            using (var Container = new DMISDBEntities())
            {
                var query = from s in Container.DisasterHistories

                            select new { s };


                var result = from o in query
                             orderby o.s.DisasterManagementId descending

                             select new DisasterHistory
                             {
                                 DisasterManagementId = o.s.DisasterManagementId
                             };
                return result.ToList<DisasterHistory>();


            }

        }
    }
}
