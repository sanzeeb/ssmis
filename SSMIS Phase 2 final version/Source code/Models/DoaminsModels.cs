﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIMS.Models
{
    class DomainsModels
    {
        public void Add(Domain DTO)
        {
            using (var container = new DMISDBEntities())
            {
                //DisasterHistory domaingen = new DisasterHistory();
                container.Domains.Add(DTO);
                container.SaveChanges();
            }
        }

        


     

        public void removeDomain(int? userId, int mgmId)
        {

            using (var container = new DMISDBEntities())
            {

                var objcheck = (from e in container.Domains where  e.DomainId == mgmId select e).FirstOrDefault();
                //int? genId, safeId, domainMgmId, riskId;

                //genId = objcheck.GeneralInformationId;
                //safeId = objcheck.SafeLearningId;
                //domainMgmId = objcheck.DisasterManagementId;
                //riskId = objcheck.RiskResilienceId;
                //var objcheckGen = (from e in container.DomainGeneralInformations where  e.GeneralInformationId == genId select e).FirstOrDefault();
                //var objcheckSafe = (from e in container.DomainSafeLearnings where  e.SafeLearningId == safeId select e).FirstOrDefault();
                //var objcheckMgm = (from e in container.DomainDisasterManagements where  e.DisasterManagementId == domainMgmId select e).FirstOrDefault();
                //var objcheckRiks = (from e in container.RiskReductionResilienceEducations where e.RiskResilienceId==riskId select e).FirstOrDefault();

                if (objcheck != null)
                {
                    container.Domains.Remove(objcheck);
                    //container.DomainGeneralInformations.Remove(objcheckGen);
                    //container.DomainSafeLearnings.Remove(objcheckSafe);
                    //container.DomainDisasterManagements.Remove(objcheckMgm);
                    //container.RiskReductionResilienceEducations.Remove(objcheckRiks);
                    
                    container.SaveChanges();
                }

            }

        }
    }
}
