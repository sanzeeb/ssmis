﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Web;

namespace DIMS.Models
{
    public class PostdisasterModels
    {

        private DMISDBEntities db = new DMISDBEntities();
        public List<PostDisasterInfo> GetDomainPost()
        {
            using (var Container = new DMISDBEntities())
            {
                var query = from s in Container.PostDisasterInfoes
                            
                            select s;

                return query.ToList<PostDisasterInfo>();


            }

        }
        public List<PostDisasterInfo> GetDomainPostById(int id)
        {
            using (var Container = new DMISDBEntities())
            {
                var query = from s in Container.PostDisasterInfoes where s.postId==id

                            select s;

                return query.ToList<PostDisasterInfo>();


            }

        }
        public void Edit(PostDisasterInfo DTO)
        {
            using (var container = new DMISDBEntities())
            {
                try
                {
                    var Comp = new PostDisasterInfo();
                    Comp = container.PostDisasterInfoes.FirstOrDefault(o => o.postId == DTO.postId);


                    Comp.Altitude = DTO.Altitude;
                    Comp.GpsLatitude = DTO.GpsLatitude; ;
                    Comp.GpsLongtitude = DTO.GpsLongtitude;
                    Comp.GraphicalLocation = DTO.GraphicalLocation;

                    Comp.AuthorityType = DTO.AuthorityType;
                    Comp.SerialNo = DTO.SerialNo;


                    Comp.InstituteId = DTO.InstituteId;

                    Comp.GobEIIN = DTO.GobEIIN;
                    Comp.HazardName = DTO.HazardName;
                    Comp.HazardType = DTO.HazardType;
                    Comp.DamageType = DTO.DamageType;
                    Comp.DamageAmount = DTO.DamageAmount;
                    Comp.EstabilishmentDate = DTO.EstabilishmentDate;
                    Comp.DamageDescription = DTO.DamageDescription;
                    Comp.MinBdtReq = DTO.MinBdtReq;
                    Comp.MinimumReqDtl = DTO.MinimumReqDtl;
                    Comp.LocalSuptDtl = DTO.LocalSuptDtl;


                    Comp.LocalSupport = DTO.LocalSupport;

                    Comp.DataCollectionMethod = DTO.DataCollectionMethod;

                    Comp.GpsLongtitude = DTO.GpsLongtitude;
                    Comp.GpsLatitude = DTO.GpsLatitude;
                    Comp.Altitude = DTO.Altitude;

                    Comp.InstitudeHouse = DTO.InstitudeHouse;
                    Comp.InstitudeRoad = DTO.InstitudeRoad;
                    Comp.InstitudeSection = DTO.InstitudeSection;
                    Comp.InstitudeVillage = DTO.InstitudeVillage;

                    Comp.ProviderName = DTO.ProviderName;
                    Comp.ProviderDesignation = DTO.ProviderDesignation;
                    Comp.ProviderSignature = DTO.ProviderSignature;

                    Comp.InstituteBng = DTO.InstituteBng;
                    Comp.InstituteEng = DTO.InstituteEng;
                    Comp.Embankment = DTO.Embankment;

                    Comp.VerifierName = DTO.VerifierName;
                    Comp.VerifierSignature = DTO.VerifierSignature;
                    Comp.VerifierDesignation = DTO.VerifierSignature;


                    Comp.InstituteType = DTO.InstituteType;
                    Comp.ManagementType = DTO.ManagementType;
                    Comp.DataCollectionDate = DTO.DataCollectionDate;
                    Comp.DataEntryDate = DTO.DataEntryDate;
                    Comp.SchoolArea = DTO.SchoolArea;


                    Comp.StudentDisableFeMale = DTO.StudentDisableFeMale;

                    Comp.StudentDisableMale = DTO.StudentDisableMale;
                    Comp.StudentEthnicFeMale = DTO.StudentEthnicFeMale;
                    Comp.StudentEthnicMale = DTO.StudentEthnicMale;
                    Comp.StudentFemale = DTO.StudentFemale;
                    Comp.StudentMale = DTO.StudentMale;
                    Comp.TeacherFemale = DTO.TeacherFemale;
                    Comp.TeacherMale = DTO.TeacherMale;



                    Comp.InstitudeSection = DTO.InstitudeSection;
                    Comp.InstitudeVillage = DTO.InstitudeVillage;
                    Comp.InstitudeRoad = DTO.InstitudeRoad;
                    Comp.InstitudeHouse = DTO.InstitudeHouse;

                    Comp.UnionId = DTO.UnionId;
                    Comp.UpazillaId = DTO.UpazillaId;
                    Comp.DivisionId = DTO.DivisionId;
                    Comp.DistrictId = DTO.DistrictId;
                    Comp.NameOfAgency = DTO.NameOfAgency;

                    container.SaveChanges();
                }
                catch (DbEntityValidationException dbValEx)
                {
                    var outputLines = new StringBuilder();
                    foreach (var eve in dbValEx.EntityValidationErrors)
                    {
                        outputLines.AppendFormat("{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:"
                          , DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State);

                        foreach (var ve in eve.ValidationErrors)
                        {
                            outputLines.AppendFormat("- Property: \"{0}\", Error: \"{1}\""
                             , ve.PropertyName, ve.ErrorMessage);
                        }
                    }

                    throw new DbEntityValidationException(string.Format("Validation errors\r\n{0}"
                     , outputLines.ToString()), dbValEx);
                }

            }
        }


        public void Add(PostDisasterInfo DTO)
        {
            using (var container = new DMISDBEntities())
            {
               
                container.PostDisasterInfoes.Add(DTO);
                container.SaveChanges();
            }
        }



        public List<PostDisasterInfo> GetPostDisasterRpt()
        {


            var query = from s in db.PostDisasterInfoes select s;

            return query.ToList<PostDisasterInfo>();
        }

        public void removePostDomain(int? userId, int mgmId)
        {

            using (var container = new DMISDBEntities())
            {

                var objcheck = (from e in container.PostDisasterInfoes where  e.postId == mgmId select e).FirstOrDefault();
           
                if (objcheck != null)
                {
                    container.PostDisasterInfoes.Remove(objcheck);
                  
                    container.SaveChanges();
                }

            }

        }

    }
}